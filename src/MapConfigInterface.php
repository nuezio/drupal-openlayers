<?php

namespace Drupal\openlayers;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use function array_keys;

/**
 * Provides an interface defining an openlayers entity type.
 */
interface MapConfigInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Returns the layer name.
   *
   * @return string
   *   The name of the layer.
   */
  public function getName();

  /**
   * Sets the name of the layer.
   *
   * @param string $name
   *   The name of the layer.
   *
   * @return $this
   *   The class instance this method is called on.
   */
  public function setName(string $name);

  /**
   * Returns a layer.
   *
   * @param string $layer
   *   The layer id.
   *
   * @return \Drupal\openlayers\LayerPluginInterface
   *   The layer object.
   */
  public function getLayer(string $layer);

  /**
   * Returns the layers for this map configuration.
   *
   * @return \Drupal\openlayers\LayerPluginCollection|\Drupal\openlayers\LayerPluginInterface[]
   *   The layers plugin collection.
   */
  public function getLayers();

  /**
   * Saves a layer for this map configuration.
   *
   * @param array $configuration
   *   An array of layer configuration.
   *
   * @return string
   *   The layer ID.
   */
  public function addLayer(array $configuration);

  /**
   * Deletes a layer from this map configuration.
   *
   * @param \Drupal\openlayers\LayerPluginInterface $layer
   *   The layer object.
   *
   * @return $this
   */
  public function deleteLayer(LayerPluginInterface $layer);

}
