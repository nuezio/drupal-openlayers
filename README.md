Openlayers module for Drupal 8|9

## Requirements

* Latest D8 or D9 release
* Geofield
* Views
* Token

## Installation

1. Add the following to the 'repositories' section of your composer.json file.
```
   {
        "type": "vcs",
        "url":  "git@gitlab.com:nuezio/drupal-openlayers.git"
    },
```
2. Run ```composer require nuezio/drupal-openlayers:dev-master```

## Development
The javascript in this module uses Typescript and Parcel for compiling.

1. In the ```js/app``` install Node depenencies using ```npm install```
1. For watching run ```npm run watch```. This will start a watcher and the browser should automatically reload.
1. For building run ```npm run build```. This will rebuild the compiled javascript files.
1. Append a new hash to the openlayers.libraries.yml entries for the app. Use for example
the commit hash. This will enforce new versions to be loaded, as they are excluded from Drupal's default
   aggregation.

## Features

The openlayers module aims implement the [Openlayers](https://openlayers.org/) library in a flexible and agnostic way
so it can be used by Drupal Core's Views and Field modules but also in custom code.

The openlayers module allows you to:

- Create 'Openlayers Map Configuration' entities (admin/config/openlayers).
- Add different layers (vector and other) to the map configuration.
- Create 'Openlayers Style Configuration' entities that can be attached to each layer.
- Apply different styling depending on the properties of each vector feature.
- Add a openlayers map by adding the new ```['#type' => 'openlayers_map']``` render element.

### Integration with views

The openlayers module also allows you to:

- Create a new 'Openlayers' Views display.
- Select which fields have to be added as properties of the geo feature.
- Style your openlayers features based on the properties added.
- Add hover elements based on the properties added.
- React to different openlayers events from Drupal by subscribing to the ```openlayers.map_event``` Event.
- Enable Ajax to get 100% ajax based integration, which dynamically updating the map upon use of exposed filtering.
- Attach those styles and configurations to a nieuw Views Openlayers Display or to Field widgets.
- Enable a spatial filter based on a polygon.

### Integration with Fields

The openlayers module also allows you to:
- Draw geometries directly in a map as a widget for the ```geofield``` field type.
- Insert or edit geojson (and see live feedback).

## Disclaimers

This module is by no means production ready. It's a POC covering specific needs of client work.

## Configuration

Module ships with the UI tools create map configurations and style configurations. Go to /admin/config/openlayers
for configuration and use the Views UI to create an Openlayers Map configuration.

## Creator
 - Teun van Veggel (@nuez) drupal.org/user/758020/edit
