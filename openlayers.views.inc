<?php

/**
 * @file
 * Contains all the views hooks.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data_alter().
 */
function openlayers_field_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage) {

  // Adds a spatial filter for views exposed filters.
  if ($field_storage->getType() == 'geofield') {
    foreach ($data as $field_table => $field_data) {
      $field_views_data = $field_data[$field_storage->getName() . '_value'];
      $field_views_data['filter']['id'] = 'openlayers_spatial';
      $field_views_data['title'] = new TranslatableMarkup('@label (Spatial filter)', ['@label' => $field_views_data['title']]);
      $data[$field_table][$field_storage->getName() . '_st'] = $field_views_data;
    }
  }
}

/**
 * Implements hook_views_data_alter().
 */
function openlayers_views_data_alter(array &$data) {
  $entity_types = \Drupal::entityTypeManager()->getDefinitions();

  // Provide a relationship for each entity type that has a base table.
  foreach ($entity_types as $type => $entity_type) {

    if (empty($data[$entity_type->getBaseTable()]) || !$entity_type->hasKey('id') || !$entity_type->entityClassImplements(ContentEntityInterface::class)) {
      continue;
    }

    $tables = [];
    if ($base_table = $entity_type->getBaseTable()) {
      $tables[] = $base_table;
    }
    if ($revision_table = $entity_type->getRevisionTable()) {
      $tables[] = $revision_table;
    }
    if ($tables) {
      foreach ($tables as $table) {
        $data[$table]['openlayers_ajax_callback'] = [
          'title' => t('Openlayers ajax interaction url', ['@entity_type' => $entity_type->getLabel()]),
          'help' => t('Creates a link allowing ajax interaction with the openlayers map.'),
          'field' => [
            'title' => t('Openlayers ajax interaction url'),
            'help' => t('Creates a link allowing ajax interaction with the openlayers map.'),
            'id' => 'openlayers_ajax_callback',
            'no group by' => FALSE,
            'click sortable' => FALSE,
          ],
        ];
      }
    }
  }
}
