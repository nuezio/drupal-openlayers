<?php

namespace Drupal\openlayers\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides an openlayers map render element.
 *
 * Data can be fed to each vector layer that is configured in the map
 * by adding the UUID as a key to the data attribute with the geojson
 * as a value.
 *
 * @code
 * $build['openlayers'] = [
 *   '#type' => 'openlayers_map_tools',
 *   '#map_id' => '',
 *   '#map_config' => $mapConfig',
 *   '#tools' => ['geojson', 'draw'],
 *   '#default_tool' => 'geojson'
 *   '#geometry_types' => ['Point', 'Polygon', 'LineString'],
 *   '#input_layer' => '',
 *   '#cardinality' => 1,
 *   '#data' => [
 *     '7c463f64-0f8e-4935-be60-a7d51ede9a9c' => [
 *        ...geojson...
 *     ],
 *   ],
 *   '#default_value' => [
 *
 *   ]
 *   '#provider' => '',
 * ];
 * @endcode
 *
 * @FormElement("openlayers_map_tools")
 */
class OpenlayersMapTools extends FormElement {

  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'process'],
      ],
      '#element_validate' => [
        [$class, 'validate'],
      ],
    ];
  }

  /**
   * Returns the tool options for the openlayers widget.
   *
   * @return array
   *   The options.
   */
  public static function getToolOptions() {
    return [
      'draw' => new TranslatableMarkup('Draw'),
      'geojson' => new TranslatableMarkup('GeoJSON'),
    ];
  }

  /**
   * Get a list of geometry types to add.
   *
   * @return array
   *   The options.
   */
  public static function getGeometryTypeOptions() {
    return [
      'Point' => new TranslatableMarkup('Point'),
      'Polygon' => new TranslatableMarkup('Polygon'),
      'LineString' => new TranslatableMarkup('LineString'),
    ];
  }

  /**
   * Prerender the element.
   */
  public static function process($element) {
    $ops = [
      'add' => new TranslatableMarkup('Add'),
      'remove' => new TranslatableMarkup('Remove'),
      'modify' => new TranslatableMarkup('Modify'),
    ];
    $element['#tree'] = TRUE;
    $geometry_types = $element['#geometry_types'];
    $input_layer = $element['#input_layer'];
    $tools = $element['#tools'];
    $element['#attached']['library'][] = 'openlayers/widget';
    $element['wrapper']['#type'] = 'container';
    $element['wrapper']['#attributes']['class'][] = 'openlayers-map-tools';
    $element['wrapper']['#attributes']['data-cardinality'] = $element['#cardinality'] ?? FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED;
    $element['wrapper']['map'] = [
      '#input' => TRUE,
      '#type' => 'openlayers_map',
      '#map_id' => $element['#map_id'],
      '#width' => $element['#width'] ?? OpenlayersMap::DEFAULT_WIDTH,
      '#weight' => $element['#weight'] ?? OpenlayersMap::DEFAULT_HEIGHT,
      '#provider' => $element['#provider'],
      '#map_config' => $element['#map_config'],
      '#data' => array_merge($element['#data'] ?? [], $element['#default_value']),
    ];
    $element['wrapper']['tools'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'openlayers-tools',
      ],
      'tool_select' => [
        '#type' => 'select',
        '#options' => array_filter(static::getToolOptions(), function ($key) use ($tools) {
          return in_array($key, $tools);
        }, ARRAY_FILTER_USE_KEY),
        '#empty_option' => new TranslatableMarkup('- Select tool -'),
        '#default_value' => $element['#default_tool'] ?? NULL,
      ],
      'tool' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'openlayers-tools-inner',
        ],
        'draw' => [
          '#states' => [
            'visible' => [
              '[data-drupal-selector="' . Html::cleanCssIdentifier(implode('-', array_merge(['edit'], $element['#parents'], [
                'wrapper',
                'tools',
                'tool-select',
              ]))) . '"]' => ['value' => 'draw'],
            ],
          ],
          '#access' => in_array('draw', $tools),
          '#type' => 'container',
          'interaction' => [
            '#type' => 'select',
            '#attributes' => [
              'class' => [
                'interaction-select',
              ],
            ],
            '#options' => array_filter(static::getGeometryTypeOptions(), function ($key) use ($geometry_types) {
              return in_array($key, $geometry_types);
            }, ARRAY_FILTER_USE_KEY),
          ],
          'op_select' => [
            '#type' => 'radios',
            '#attributes' => [
              'class' => [
                'op-select',
              ],
            ],
            '#options' => $ops,
          ],

        ],
        'geojson' => [
          '#type' => 'container',
          '#states' => [
            'visible' => [
              '[data-drupal-selector="' . Html::cleanCssIdentifier(implode('-', array_merge(['edit'], $element['#parents'], [
                'wrapper',
                'tools',
                'tool-select',
              ]))) . '"]' => ['value' => 'geojson'],
            ],
          ],
          'source' => [
            '#type' => 'textarea',
            '#rows' => 10,
            '#attributes' => [
              'class' => ['source'],
            ],
            '#default_value' => json_encode($element['#default_value'][$input_layer], JSON_PRETTY_PRINT),
          ],
          'source_error' => [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'source-error',
              ],
            ],
          ],
        ],
      ],

    ];
    return $element;
  }

  /**
   * Form element validation handler for #type 'color'.
   */
  public static function validate(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = $element['#value']['wrapper']['tools']['tool']['geojson']['source'];
    $json = Json::decode($value);
    if (isset($element['#cardinality']) && $element['#cardinality'] != FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED && count($json['features']) > ($element['#cardinality'] ?? FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)) {
      $form_state->setError($element, new PluralTranslatableMarkup($element['#cardinality'], 'Only one feature is allowed.', 'Only @count features are allowed.'));
    }
    $form_state->setValueForElement($element, $value);
  }

}
