<?php

namespace Drupal\openlayers\Plugin\Field;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openlayers\Entity\MapConfig;
use Drupal\openlayers\MapConfigInterface;
use Drupal\openlayers\VectorLayerInterface;

/**
 * Trait for some functionality shared by both formatter and widget.
 *
 * @package Drupal\openlayers\Plugin\Field
 */
trait OpenlayersFieldTrait {

  /**
   *
   */
  protected function sharedSettingsSummary() {
    return [
      $this->t('Map config: @config', [
        '@config' => $this->getSetting('map_config') ?? $this->t('Empty'),
      ]),
      $this->t('Data layer: @data_layer', [
        '@data_layer' => $this->getSetting('data_layer') ?? $this->t('Data layer'),
      ]),
    ];
  }

  /**
   * Form with settings that are shared by formatter and widget.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form array.
   */
  protected function sharedSettingsForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\openlayers\MapConfigInterface $selectedMapConfig */
    $selectedMapConfig = MapConfig::load($this->getSetting('map_config'));
    if ($triggeringElement = $form_state->getTriggeringElement()) {
      if (isset($triggeringElement['#op']) && $triggeringElement['#op'] == 'map_config_select') {
        $selectedMapConfig = MapConfig::load($triggeringElement['#value']);
      }
    }
    /** @var \Drupal\openlayers\MapConfigInterface[] $mapConfigs */
    $mapConfigs = $this->entityTypeManager
      ->getStorage('openlayers_map_config')
      ->loadMultiple();

    $form['map_config'] = [
      '#title' => $this->t('Map configuration'),
      '#type' => 'select',
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select'),
      '#options' => array_map(function (MapConfigInterface $mapConfig) {
        return $mapConfig->label();
      }, $mapConfigs),
      '#default_value' => $selectedMapConfig ? $selectedMapConfig->id() : NULL,
      '#op' => 'map_config_select',
      '#ajax' => [
        'wrapper' => 'data-layer-container',
        'callback' => [get_called_class(), 'updateDataLayers'],
      ],
    ];
    $form['data_layer'] = [
      '#prefix' => '<div id="data-layer-container">',
      '#suffix' => '</div>',
      '#type' => 'select',
      '#empty_option' => $selectedMapConfig ? $this->t('- Select -') : $this->t('- Select a map configuration first -'),
      '#disabled' => !$selectedMapConfig,
      '#title' => $this->t('Data layer'),
      '#description' => $this->t('Select the layer to feed to.'),
      '#default_value' => $this->getSetting('data_layer'),
      '#required' => TRUE,
      '#options' => $this->getDataLayerOptions($selectedMapConfig),
    ];
    $form['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $this->getSetting('width'),
    ];

    $form['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#default_value' => $this->getSetting('height'),
    ];
    return $form;
  }

  /**
   * Get the data layer options.
   *
   * @param \Drupal\openlayers\MapConfigInterface|null $map_config
   *   The selected map configuration entity.
   *
   * @return array
   *   A list of options.
   */
  protected function getDataLayerOptions(MapConfigInterface $map_config = NULL) {
    $options = [];
    if ($map_config) {
      foreach ($map_config->getLayers() as $layer) {
        if ($layer instanceof VectorLayerInterface) {
          $options[$layer->getUuid()] = $layer->label();
        }
      }

    }
    return $options;
  }

  /**
   * Ajax callback for updating the data layers element.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The form element replacing the original.
   */
  public static function updateDataLayers(array $form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#array_parents'];
    array_pop($parents);
    $parents[] = 'data_layer';
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Default settings that are used by formatter and widget.
   *
   * @return array
   *   The settings.
   */
  public static function sharedDefaultSettings() {
    return [
      'map_config' => '',
      'data_layer' => '',
      'width' => '',
      'height' => '',
    ];
  }

}
