<?php

namespace Drupal\openlayers;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the interface for configurable openlayers layers.
 */
interface ConfigurableLayerPluginInterface extends LayerPluginInterface, PluginFormInterface {
}
