<?php

namespace Drupal\openlayers\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\openlayers\ConfigurableLayerPluginInterface;
use Drupal\openlayers\LayerPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for map config edit form.
 *
 * @internal
 */
class MapConfigEditForm extends MapConfigFormBase {

  /**
   * The layer plugin manager.
   *
   * @var \Drupal\openlayers\LayerPluginManager
   */
  protected $layerPluginManager;

  /**
   * Constructs an MapConfigEditForm object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $openlayers_map_config_storage
   *   The storage.
   * @param \Drupal\openlayers\LayerPluginManager $layer_plugin_manager
   *   The layer plugin manager service.
   */
  public function __construct(EntityStorageInterface $openlayers_map_config_storage, LayerPluginManager $layer_plugin_manager) {
    parent::__construct($openlayers_map_config_storage);
    $this->layerPluginManager = $layer_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('openlayers_map_config'),
      $container->get('plugin.manager.openlayers.layer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $form['#title'] = $this->t('Edit map configuration %name', ['%name' => $this->entity->label()]);
    $form['#tree'] = TRUE;

    $form['#attached']['library'][] = 'openlayers/admin';

    $view_settings = $this->entity->get('view');
    $form['view'] = [
      '#type' => 'fieldset',
      '#title' => new TranslatableMarkup('Default View settings'),
      '#tree' => TRUE,
      'center' => [
        '#type' => 'fieldset',
        '#title' => new TranslatableMarkup('Center'),
        'longitude' => [
          '#type' => 'number',
          '#title' => new TranslatableMarkup('Longitude'),
          '#step' => '.00000000000001',
          '#default_value' => $view_settings['center']['longitude'] ?? 0,
        ],
        'latitude' => [
          '#type' => 'number',
          '#step' => '.00000000000001',
          '#title' => new TranslatableMarkup('Latitude'),
          '#default_value' => $view_settings['center']['latitude'] ?? 0,
        ],
      ],
      'zoom' => [
        '#type' => 'number',
        '#title' => new TranslatableMarkup('Zoom'),
        '#default_value' => $view_settings['zoom'] ?? 0,
      ],
    ];

    // Build the list of existing layers for this map config.
    $form['layers'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Layers'),
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'layer-order-weight',
        ],
      ],
      '#attributes' => [
        'id' => 'layer-order-weight',
      ],
      '#empty' => $this->t('There are currently no layers in this map configuration. Add one by selecting an option below.'),
      // Render layers below parent elements.
      '#weight' => 5,
    ];
    foreach ($this->entity->getLayers() as $layer) {
      $key = $layer->getUuid();
      $form['layers'][$key]['#attributes']['class'][] = 'draggable';
      $form['layers'][$key]['#weight'] = isset($user_input['layers']) ? $user_input['layers'][$key]['weight'] : NULL;
      $form['layers'][$key]['layer'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $layer->label(),
          ],
        ],
      ];

      $summary = $layer->getSummary();

      if (!empty($summary)) {
        $summary['#prefix'] = ' ';
        $form['layers'][$key]['layer']['data']['summary'] = $summary;
      }

      $form['layers'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $layer->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $layer->getWeight(),
        '#attributes' => [
          'class' => ['layer-order-weight'],
        ],
      ];

      $links = [];
      $is_configurable = $layer instanceof ConfigurableLayerPluginInterface;
      if ($is_configurable) {
        $links['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('openlayers.layer_edit_form', [
            'openlayers_map_config' => $this->entity->id(),
            'layer' => $key,
          ]),
        ];
      }
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('openlayers.layer_delete', [
          'openlayers_map_config' => $this->entity->id(),
          'layer' => $key,
        ]),
      ];
      $form['layers'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
    }

    // Build the new layer addition form and add it to the layer list.
    $new_layer_options = [];
    $layers = $this->layerPluginManager->getDefinitions();
    uasort($layers, function ($a, $b) {
      return Unicode::strcasecmp($a['label'], $b['label']);
    });
    foreach ($layers as $layer => $definition) {
      $new_layer_options[$layer] = $definition['label'];
    }
    $form['layers']['new'] = [
      '#tree' => FALSE,
      '#weight' => isset($user_input['weight']) ? $user_input['weight'] : NULL,
      '#attributes' => ['class' => ['draggable']],
    ];
    $form['layers']['new']['layer'] = [
      'data' => [
        'new' => [
          '#type' => 'select',
          '#title' => $this->t('Effect'),
          '#title_display' => 'invisible',
          '#options' => $new_layer_options,
          '#empty_option' => $this->t('- Select a new layer -'),
        ],
        [
          'add' => [
            '#type' => 'submit',
            '#value' => $this->t('Add'),
            '#validate' => ['::layerValidate'],
            '#submit' => ['::submitForm', '::layerSave'],
          ],
        ],
      ],
      '#prefix' => '<div class="openlayers-layer-new">',
      '#suffix' => '</div>',
    ];

    $form['layers']['new']['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for new layer'),
      '#title_display' => 'invisible',
      '#default_value' => count($this->entity->getLayers()) + 1,
      '#attributes' => ['class' => ['layer-order-weight']],
    ];
    $form['layers']['new']['operations'] = [
      'data' => [],
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Validate handler for layer.
   */
  public function layerValidate($form, FormStateInterface $form_state) {
    if (!$form_state->getValue('new')) {
      $form_state->setErrorByName('new', $this->t('Select an layer to add.'));
    }
  }

  /**
   * Submit handler for layer.
   */
  public function layerSave($form, FormStateInterface $form_state) {
    $this->save($form, $form_state);

    // Check if this field has any configuration options.
    $layer = $this->layerPluginManager->getDefinition($form_state->getValue('new'));

    // Load the configuration form for this option.
    if (is_subclass_of($layer['class'], ConfigurableLayerPluginInterface::class)) {
      // We can't use the $form_state->setRedirect() because it conflicts
      // with the 'destination' query parameter.
      $form_state->setRedirect('openlayers.layer_add_form',
        [
          'openlayers_map_config' => $this->entity->id(),
          'layer' => $form_state->getValue('new'),
        ],
        ['query' => ['weight' => $form_state->getValue('weight')]]);
    }
    // If there's no form, immediately add the layer.
    else {
      $layer = [
        'id' => $layer['id'],
        'data' => [],
        'weight' => $form_state->getValue('weight'),
      ];
      $layer_id = $this->entity->addLayer($layer);
      $this->entity->save();
      if (!empty($layer_id)) {
        $this->messenger()->addStatus($this->t('The layer was successfully applied.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Update layer weights.
    if (!$form_state->isValueEmpty('layers')) {
      $this->updateLayerWeights($form_state->getValue('layers'));
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Updates layer weights.
   *
   * @param array $layers
   *   Associative array with layers having layer uuid as keys and array
   *   with layer data as values.
   */
  protected function updateLayerWeights(array $layers) {
    foreach ($layers as $uuid => $layer_data) {
      if ($this->entity->getLayers()->has($uuid)) {
        $this->entity->getLayer($uuid)->setWeight($layer_data['weight']);
      }
    }
  }

}
