<?php

namespace Drupal\openlayers;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an openlayers entity type.
 */
interface StyleConfigInterface extends ConfigEntityInterface {

  /**
   * Returns the layer name.
   *
   * @return string
   *   The name of the style.
   */
  public function getName();

  /**
   * Sets the name of the layer.
   *
   * @param string $name
   *   The name of the layer.
   *
   * @return $this
   *   The class instance this method is called on.
   */
  public function setName(string $name);

  /**
   * Returns the layers for this style.
   *
   * @return \Drupal\openlayers\StyleRuleInterface[]
   *   The layers plugin collection.
   */
  public function getStyleRules();

  /**
   * Gets an instantiated style rule based on the rules uuid.
   *
   * @param string $uuid
   *   The uuid of the style rule.
   *
   * @return \Drupal\openlayers\StyleRuleInterface
   *   The instantiated style rule.
   */
  public function getStyleRule(string $uuid);

  /**
   * Saves a layer for this style.
   *
   * @return string
   *   The layer ID.
   */
  public function addStyleRule();

  /**
   * Removes the style rule for this style config.
   *
   * @param \Drupal\openlayers\StyleRuleInterface $style_rule
   *   Style rule instance.
   */
  public function removeStyleRule(StyleRuleInterface $style_rule);

  /**
   * Adds a condition based on a geojson feature.
   *
   * @param string $style_rule_uuid
   *   The style rule uuid.
   */
  public function addConditionFeature(string $style_rule_uuid);

  /**
   * Removes a feature based condition from the style rule.
   *
   * @param string $style_rule_uuid
   *   The style rule uuid.
   * @param int $delta
   *   The delta.
   */
  public function removeConditionFeature(string $style_rule_uuid, int $delta);

}
