<?php

namespace Drupal\openlayers\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\openlayers\StyleConfigInterface;
use Drupal\openlayers\StyleRule;
use Drupal\openlayers\StyleRuleInterface;

/**
 * Defines the style entity type.
 *
 * @ConfigEntityType(
 *   id = "openlayers_style_config",
 *   label = @Translation("Style configuration"),
 *   label_collection = @Translation("Style configurations"),
 *   label_singular = @Translation("Style configuration"),
 *   label_plural = @Translation("Style configurations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count style",
 *     plural = "@count styles",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\openlayers\StyleConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\openlayers\Form\StyleConfigAddForm",
 *       "edit" = "Drupal\openlayers\Form\StyleConfigEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "style_config",
 *   admin_permission = "administer openlayers configuration",
 *   links = {
 *     "collection" = "/admin/config/openlayers/styles",
 *     "add-form" = "/admin/structure/openlayers-styles/add",
 *     "edit-form" = "/admin/structure/openlayers-styles/{openlayers_style_config}",
 *     "delete-form" = "/admin/structure/openlayers-styles/{openlayers_style_config}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "style_rules",
 *   }
 * )
 */
class StyleConfig extends ConfigEntityBase implements StyleConfigInterface {


  /**
   * The style name.
   *
   * @var string
   */
  protected $name;

  /**
   * The style label.
   *
   * @var string
   */
  protected $label;

  /**
   * The style rules.
   *
   * @var \Drupal\openlayers\StyleRule[]
   */
  protected $styleRules = [];

  /**
   * Feature conditions.
   *
   * @var array
   */
  protected $featureConditions = [];

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addStyleRule() {
    $styleRule = StyleRule::create([]);
    $this->styleRules[$styleRule->getUuid()] = $styleRule;
  }

  /**
   * {@inheritdoc}
   */
  public function removeStyleRule(StyleRuleInterface $style_rule) {
    $styleRules = $this->get('style_rules');
    unset($styleRules[$style_rule->getUuid()]);
    unset($this->styleRules[$style_rule->getUuid()]);
    $this->set('style_rules', $styleRules);
  }

  /**
   * {@inheritdoc}
   */
  public function addConditionFeature(string $style_rule_uuid) {
    $style_rules = $this->get('style_rules');
    $style_rules[$style_rule_uuid]['conditions']['features'][] = [
      'property' => '',
      'value' => '',
      'operator' => '',
    ];
    $this->set('style_rules', $style_rules);
  }

  /**
   * {@inheritdoc}
   */
  public function removeConditionFeature(string $style_rule_uuid, int $delta) {
    $style_rules = $this->get('style_rules');
    unset($style_rules[$style_rule_uuid]['conditions']['features'][$delta]);
    $style_rules[$style_rule_uuid]['conditions']['features'] = array_values($style_rules[$style_rule_uuid]['conditions']['features']);
    $this->set('style_rules', $style_rules);
  }

  /**
   * {@inheritdoc}
   */
  public function getStyleRules() {
    $styleRulesConfig = $this->get('style_rules');
    if (!empty($styleRulesConfig)) {
      foreach ($styleRulesConfig as $key => $config) {
        $styleRule = StyleRule::create($config, $key);
        $this->styleRules[$styleRule->getUuid()] = $styleRule;
      }
    }
    return $this->styleRules;
  }

  /**
   * {@inheritdoc}
   */
  public function getStyleRule(string $uuid) {
    $styleRules = $this->getStyleRules();
    return $styleRules[$uuid];
  }

  /**
   * {@inheritdoc}
   */
  public function getListCacheTagsToInvalidate() {
    $mapConfigs = \Drupal::entityTypeManager()->getStorage('openlayers_map_config')->loadMultiple();
    $tags = parent::getListCacheTagsToInvalidate();
    foreach ($mapConfigs as $mapConfig) {
      $tags = Cache::mergeTags($tags, $mapConfig->getCacheTags());
    }
    return $tags;
  }

}
