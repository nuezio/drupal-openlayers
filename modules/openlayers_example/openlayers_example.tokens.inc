<?php

/**
 * @file
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_token_info_alter().
 */
function openlayers_example_token_info_alter(&$data) {
  $data['tokens']['site']['openlayers_example'] = [
    'name' => t('Example'),
    'description' => t('Example Geojson token'),
  ];
}

/**
 * Implements hook_tokens().
 */
function openlayers_example_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'site') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'openlayers_example':
          $replacements[$original] = Markup::create(file_get_contents(__DIR__ . '/europe.json'));
          break;
      }
    }
  }
  return $replacements;
}
