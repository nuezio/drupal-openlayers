import { Style as OlStyle, Icon, Circle, Text, Fill, Stroke, Image } from 'ol/style';
import { Feature } from 'ol';
import Style, { StyleFunction, StyleLike } from 'ol/style/Style';
import { evaluate } from 'mathjs';
import { Openlayers } from './Openlayers';

type FillType = {
    color: string;
};

type StrokeType = {
    color: string;
    line_cap: CanvasLineCap;
    line_join: CanvasLineJoin;
    line_dash: string;
    width: string;
};

type CircleType = {
    fill: FillType;
    radius: string;
    stroke: StrokeType;
};

type TextType = {
    font: string;
    text: string;
    fill: FillType;
    stroke: StrokeType;
};

type IconType = {
    source: string;
    anchor: {
        vertical: number;
        horizontal: number;
    };
};

type ImageType = {
    image_type: string;
    data: IconType | CircleType;
};

/**
 * Styler tool that styles.
 */
export default class Styler {
    openlayers: Openlayers;

    marker: string;

    markerHover: string;

    constructor(openlayers: Openlayers, marker = '', marker_hover = '') {
        this.openlayers = openlayers;
        this.marker = marker;
        this.markerHover = marker_hover;
    }

    widgetStyle: StyleFunction = (feature: Feature) => {
        return new OlStyle({
            image: new Icon({
                anchor: [0.5, 1],
                src: this.marker,
            }),
            fill: new Fill({
                color: 'rgba(255, 225, 0, .2)',
            }),
            stroke: new Stroke({
                color: 'rgba(255, 225, 0, 1)',
                width: 2,
            }),
        });
    };

    /**
     * Gets the openlayers fill style element.
     *
     * @param config
     *   The configuration to pass.
     */
    protected getFillStyle(config: FillType): Fill | undefined {
        if (config.color) {
            return new Fill({
                color: config.color,
            });
        }
    }

    /**
     * Gets the openlayers stroke style element.
     *
     * @param config
     *   The configuration to pass.
     * @param placeholders
     *   The placeholders to pass
     */
    protected getStrokeStyle(config: StrokeType, placeholders: Record<string, unknown>): Stroke | undefined {
        if (config.color) {
            return new Stroke({
                color: config.color,
                lineCap: config.line_cap,
                lineJoin: config.line_join,
                lineDash: config.line_dash.split(',').map(Number),
                width: parseFloat(this.evalFunction(config.width, placeholders)),
            });
        }
    }

    /**
     * Gets the openlayers circle style element.
     *
     * @param config
     *   The configuration to pass.
     * @param placeholders
     *   The placeholders to pass
     */
    protected getCircleStyle(config: CircleType, placeholders: Record<string, unknown>): Circle {
        return new Circle({
            radius: parseInt(this.evalFunction(config.radius, placeholders)),
            fill: this.getFillStyle(config.fill),
            stroke: this.getStrokeStyle(config.stroke, placeholders),
        });
    }

    /**
     * Gets the openlayers text style element.
     *
     * @param config
     *   The configuration to pass.
     * @param placeholders
     *   The placeholders to pass
     */
    protected getTextStyle(config: TextType, placeholders: Record<string, unknown>): Text {
        return new Text({
            font: config?.font,
            text: placeholders?.text,
            fill: this.getFillStyle(config.fill),
            stroke: this.getStrokeStyle(config.stroke, placeholders),
        });
    }

    /**
     * Gets the openlayers icon style element.
     *
     * @param config
     *   The configuration to pass.
     */
    protected getIconStyle(config: IconType): Icon {
        return new Icon({
            src: config.source,
            anchor: [config.anchor.horizontal, config.anchor.vertical],
        });
    }

    /**
     * Gets the openlayers image style element.
     *
     * @param config
     *   The configuration to pass.
     * @param placeholders
     *   The placeholders to pass
     */
    protected getImageStyle(config: ImageType, placeholders: Record<string, unknown>): Circle | Icon | undefined {
        switch (config.image_type) {
            case 'circle':
                const circle = config.data as CircleType;
                return this.getCircleStyle(circle, placeholders);
            case 'icon':
                const icon = config.data as IconType;
                return this.getIconStyle(icon);
        }
    }

    /**
     * Base function for styling.
     *
     * @param feature
     *   The feature to style.
     * @param map_id
     *   The map id.
     * @param layer_id
     *   The layer id.
     */
    protected styleBase(feature: Feature, layer_id: string): Style | undefined {
        const map = this.openlayers.getMap();
        const styleConfig = this.openlayers.mapConfig.getStyleConfigForLayer(layer_id);
        const zoom = map.getView().getZoom();
        const style = new OlStyle();
        for (const style_rule of Object.values(styleConfig.style_rules)) {
            const style_definition = style_rule.style_definition;
            if (!this.zoomApplies(style_rule, zoom)) {
                continue;
            }
            if (!this.featureConditionsApply(style_rule, feature)) {
                continue;
            }

            // If conditions apply and style is set to 'hidden', hide the feature
            // by returning an empty OlStyle();
            if (style_definition.hidden) {
                return new OlStyle();
            }

            if (!style.getFill() && style_definition?.fill) {
                style.setFill(this.getFillStyle(style_definition.fill));
            }
            if (!style.getStroke() && style_definition?.stroke) {
                style.setStroke(this.getStrokeStyle(style_definition.stroke));
            }
            if (!style.getImage() && style_definition?.image) {
                style.setImage(this.getImageStyle(style_definition.image, zoom));
            }
        }
        return style;
    }

    /**
     * Style a cluster.
     *
     * @param feature
     *   The cluster feature.
     */
    public styleCluster(feature: Feature): StyleLike | undefined {
        const firstFeature = feature.get('features')[0];
        const layer_id = firstFeature.get('_layer_id');
        if (feature.get('features').length == 1) {
            return this.styleBase(feature.get('features')[0], layer_id);
        } else {
            const map = this.openlayers.getMap();
            const styleConfig = this.openlayers.mapConfig.getStyleConfigForLayer(layer_id);
            const zoom = map.getView().getZoom();
            let style_definition;

            const clusterStyle = new OlStyle();
            const placeholders = {
                zoom: zoom,
                cluster_size: feature.get('features').length,
                text: feature.get('features').length.toString(),
            };

            // The feature providing the style
            styleRule: for (const style_rule of Object.values(styleConfig.style_rules)) {
                for (const featureInCluster of feature.get('features')) {
                    if (
                        this.zoomApplies(style_rule, zoom) &&
                        this.featureConditionsApply(style_rule, featureInCluster)
                    ) {
                        style_definition = style_rule.style_definition;
                        if (!clusterStyle.getImage() && style_rule.style_definition.cluster?.image.image_type) {
                            clusterStyle.setImage(this.getImageStyle(style_definition.cluster.image, placeholders));
                        }
                        if (
                            !clusterStyle.getText() &&
                            style_rule.style_definition.cluster?.text &&
                            style_rule.style_definition.cluster?.text?.font
                        ) {
                            clusterStyle.setText(this.getTextStyle(style_definition.cluster.text, placeholders));
                        }
                    }
                }
            }

            return clusterStyle;
        }
    }

    /**
     * Check if the style rule applies with the current zoom level.
     */
    protected zoomApplies(style_rule, zoom) {
        const min = style_rule.conditions.view.zoom.min;
        const max = style_rule.conditions.view.zoom.max;
        return (min == undefined || zoom >= min) && (max == undefined || zoom <= max);
    }

    /**
     * Check if the styling has to be applied according to the feature conditions set.
     *
     * @param style_rule
     * @param feature
     */
    protected featureConditionsApply(style_rule, feature: Feature) {
        if (style_rule.conditions.features.length === 0) {
            return true;
        }
        let applies = true;
        Object.values(style_rule.conditions.features).forEach((item) => {
            switch (item.operator) {
                case 'is_true':
                    if (applies && feature.get(item.property) === true) {
                        applies = true;
                    } else {
                        applies = false;
                    }
                    break;
                case 'is_false':
                    if (applies && feature.get(item.property) === false) {
                        applies = true;
                    } else {
                        applies = false;
                    }
                    break;
                case 'equals':
                    if (applies && feature.get(item.property) == item.value) {
                        applies = true;
                    } else {
                        applies = false;
                    }
                    break;
                case 'not_equals':
                    if (applies && feature.get(item.property) != item.value) {
                        applies = true;
                    } else {
                        applies = false;
                    }
                    break;
                case 'is_buffer':
                    if (applies && feature.getId() == 'buffer') {
                        applies = true;
                    } else {
                        applies = false;
                    }
                    break;
                case 'regex':
                    if (applies && feature.get(item.property) && feature.get(item.property).match(item.value)) {
                        applies = true;
                    } else {
                        applies = false;
                    }
                    break;
                default:
                    applies = false;
                    break;
            }
        });
        return applies;
    }

    /**
     * Style the features that are excluded from clustering.
     *
     * @param feature
     *   The feature.
     */
    public styleExcludedFromCluster(feature: Feature): Style | null {
        if (feature.getGeometry().getType() == 'Point') {
            return null;
        }
        return this.styleFeature(feature);
    }

    /**
     * Style an indivitual feature.
     *
     * @param feature
     *   The feature to style.
     */
    public styleFeature(feature: Feature): Style | null {
        const map_id = feature.get('_map_id');
        const layer_id = feature.get('_layer_id');
        if (map_id && layer_id) {
            return this.styleBase(feature, layer_id);
        }
        return null;
    }

    /**
     * Evaluates a mathematical equation useing MathJS.
     *
     * @param value
     *   The string to evaluate.
     * @param placeholders
     *   THe placeholders to apply.
     */
    evalFunction(value: string, placeholders: never): string {
        try {
            value = value.replaceAll('@', '');
            return evaluate(value, placeholders);
        } catch (err) {
            return value;
        }
    }
}
