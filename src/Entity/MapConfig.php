<?php

namespace Drupal\openlayers\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\openlayers\LayerPluginCollection;
use Drupal\openlayers\LayerPluginInterface;
use Drupal\openlayers\MapConfigInterface;

/**
 * Defines the map configuration entity type.
 *
 * @ConfigEntityType(
 *   id = "openlayers_map_config",
 *   label = @Translation("Map configuration"),
 *   label_collection = @Translation("Map configurations"),
 *   label_singular = @Translation("Map configuration"),
 *   label_plural = @Translation("Map configurations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count map configuration",
 *     plural = "@count map configuration",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\openlayers\MapConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\openlayers\Form\MapConfigAddForm",
 *       "edit" = "Drupal\openlayers\Form\MapConfigEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "map_config",
 *   admin_permission = "administer openlayers configuration",
 *   links = {
 *     "collection" = "/admin/config/openlayers/map-configurations",
 *     "add-form" = "/admin/structure/openlayers-map-configurations/add",
 *     "edit-form" = "/admin/structure/openlayers-map-configurations/{openlayers_map_config}",
 *     "delete-form" = "/admin/structure/openlayers-map-configurations/{openlayers_map_config}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "view",
 *     "layers"
 *   }
 * )
 */
class MapConfig extends ConfigEntityBase implements MapConfigInterface {

  /**
   * The map configuration name.
   *
   * @var string
   */
  protected $name;

  /**
   * The map configuration label.
   *
   * @var string
   */
  protected $label;

  /**
   * The layers for this map.
   *
   * @var array
   */
  protected $layers = [];

  /**
   * The view settings of the map.
   *
   * @var array
   */
  protected $view = [
    'center' => [
      'longitude' => 0,
      'latitude' => 0,
    ],
    'zoom' => 9,
  ];

  /**
   * Holds the collection of layers that is used in this map configuration.
   *
   * @var \Drupal\openlayers\LayerPluginCollection
   */
  protected $layersCollection;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['layers' => $this->getLayers()];
  }

  /**
   * {@inheritdoc}
   */
  public function addLayer(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getLayers()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLayer(LayerPluginInterface $layer) {
    $this->getLayers()->removeInstanceId($layer->getUuid());
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLayer($layer) {
    return $this->getLayers()->get($layer);
  }

  /**
   * {@inheritdoc}
   */
  public function getLayers() {
    if (!$this->layersCollection) {
      $this->layersCollection = new LayerPluginCollection($this->getLayerPluginManager(), $this->layers);
      $this->layersCollection->sort();
    }
    return $this->layersCollection;
  }

  /**
   * Returns the layers plugin manager.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The layers plugin manager.
   */
  protected function getLayerPluginManager() {
    return \Drupal::service('plugin.manager.openlayers.layer');
  }

}
