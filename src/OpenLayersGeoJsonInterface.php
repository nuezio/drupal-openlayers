<?php

namespace Drupal\openlayers;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Geojson service.
 */
interface OpenLayersGeoJsonInterface {

  /**
   * Get the geoJson for a specific field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field item list.
   *
   * @return array
   *   The unserialized geojson.
   */
  public function convertFieldItemsToGeoJson(FieldItemListInterface $items);

  /**
   * Convert field items to geojson.
   *
   * @param string $geojson
   *   The serialized geojson.
   *
   * @return array
   *   Field values in array.
   */
  public function convertGeoJsonToFieldValues(string $geojson);

}
