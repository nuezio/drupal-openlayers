<?php

namespace Drupal\openlayers\Plugin\openlayers\Layer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\openlayers\ConfigurableLayerPluginBase;

/**
 * Implements the Open Street Map as an openlayers layer.
 *
 * @Layer(
 *   id = "osm",
 *   label = @Translation("OSM"),
 *   description = @Translation("OSM."),
 * )
 */
class OSM extends ConfigurableLayerPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [
      'settings' => [
        '#type' => 'markup',
        '#markup' => 'No settings yet.',
      ],
    ];
  }

}
