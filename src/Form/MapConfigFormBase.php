<?php

namespace Drupal\openlayers\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form for map config add and edit forms.
 */
abstract class MapConfigFormBase extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\openlayers\MapConfigInterface
   */
  protected $entity;

  /**
   * The map config entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $mapConfigStorage;

  /**
   * Constructs a base class for map config add and edit forms.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $map_config_storage
   *   The map config entity storage.
   */
  public function __construct(EntityStorageInterface $map_config_storage) {
    $this->mapConfigStorage = $map_config_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('openlayers_map_config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Map configuration name'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this->mapConfigStorage, 'load'],
      ],
      '#default_value' => $this->entity->id(),
      '#required' => TRUE,
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
  }

}
