<?php

namespace Drupal\openlayers\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openlayers\MapConfigInterface;

/**
 * Form for deleting a layer.
 *
 * @internal
 */
class LayerDeleteForm extends ConfirmFormBase {

  /**
   * The map config containing the layer to be deleted.
   *
   * @var \Drupal\openlayers\MapConfigInterface
   */
  protected $mapConfig;

  /**
   * The layer to be deleted.
   *
   * @var \Drupal\openlayers\LayerPluginInterface
   */
  protected $layer;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the @layer layer from the %map_config map configuration?', [
      '%map_config' => $this->mapConfig->label(),
      '@layer' => $this->layer->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->mapConfig->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openlayers_layer_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MapConfigInterface $openlayers_map_config = NULL, $layer = NULL) {
    $this->mapConfig = $openlayers_map_config;
    $this->layer = $this->mapConfig->getLayer($layer);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->mapConfig->deleteLayer($this->layer);
    $this->messenger()->addStatus($this->t('The layer %name has been deleted.', ['%name' => $this->layer->label()]));
    $form_state->setRedirectUrl($this->mapConfig->toUrl('edit-form'));
  }

}
