<?php

namespace Drupal\openlayers;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Layer plugin manager.
 */
class LayerPluginManager extends DefaultPluginManager {

  /**
   * Constructs LayerPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/openlayers/Layer',
      $namespaces,
      $module_handler,
      'Drupal\openlayers\LayerPluginInterface',
      'Drupal\openlayers\Annotation\Layer'
    );
    $this->alterInfo('openlayers_layer_info');
    $this->setCacheBackend($cache_backend, 'openlayers_layer_plugins');
  }

}
