<?php

namespace Drupal\openlayers\Plugin\openlayers\Layer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\openlayers\ConfigurableLayerPluginBase;

/**
 * Geojson based map layer.
 *
 * @Layer(
 *   id = "esri",
 *   label = @Translation("Esri"),
 *   description = @Translation("ESRI.")
 * )
 */
class Esri extends ConfigurableLayerPluginBase implements ContainerFactoryPluginInterface {

  /**
   *
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [];
  }

}
