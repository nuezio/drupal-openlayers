<?php

namespace Drupal\openlayers\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Controller for style addition forms.
 *
 * @internal
 */
class StyleConfigAddForm extends StyleConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Style configuration %name was created.', ['%name' => $this->entity->label()]));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create new style configuration');
    return $actions;
  }

}
