<?php

namespace Drupal\openlayers\Plugin\views\style;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\openlayers\Controller\LazyViewDataController;
use Drupal\openlayers\MapConfigInterface;
use Drupal\openlayers\OpenLayersEvents;
use Drupal\openlayers\Plugin\views\field\OpenlayersAjaxCallback;
use Drupal\openlayers\Plugin\views\filter\SpatialFilter;
use Drupal\openlayers\VectorLayerInterface;
use Drupal\openlayers\FeaturePropertiesEvent;
use Drupal\views\Plugin\views\field\EntityField;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * OpenLayers style plugin.
 *
 * @ViewsStyle(
 *   id = "openlayers",
 *   title = @Translation("OpenLayers"),
 *   help = @Translation("Openlayers style plugin to render openlayers maps."),
 *   display_types = {"normal"}
 * )
 */
class Openlayers extends StylePluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * The wrapped GeoPHP service.
   *
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geophp;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Cache backend interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBin;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Features per field.
   *
   * @var array
   */
  protected $featuresPerField;

  /**
   * The constructor of the plugin.
   *
   * @param array $configuration
   *   The plugin config.
   * @param mixed $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\geofield\GeoPHP\GeoPHPInterface $geophp
   *   The GeoPHP wrapper service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Drupal messenger.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_bin
   *   The cache bin.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GeoPHPInterface $geophp, EntityFieldManager $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, Messenger $messenger, CacheBackendInterface $cache_bin, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->geophp = $geophp;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->cacheBin = $cache_bin;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('geofield.geophp'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('cache.data'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesFields() {
    return TRUE;
  }

  /**
   * Generate a hash.
   *
   * @param array $feature
   * @param \Drupal\views\ResultRow $row
   * @param string $field
   */
  protected function getFeatureHash(array $feature, ResultRow $row, string $field) {
    $fieldInfo = $this->view->field[$field];
    $id = $row->_entity->id();
    if ($fieldInfo->relationship && !($this->options['layers'][$field]['dedupe_use_base'] ?? FALSE)) {
      foreach ($row as $property => $value) {
        if (strpos($fieldInfo->relationship, $property) === 0) {
          $id = $row->{$property};
          break;
        }
      }
    }
    // Hash the data based on the entity ID of the row or the
    // related entity.
    $hash_data = [
      $id,
      $field,
    ];
    $hash_data[] = $feature['geometry'];
    return hash('md5', serialize($hash_data));
  }

  /**
   * Get the feature.
   *
   * @param \Drupal\views\ResultRow $row
   *   Row object.
   * @param string $field
   *   The source field name.
   * @param array $options
   *   Options per data source.
   *
   * @return array
   *   Geojson formatted array.
   */
  protected function getFeaturesPerRow(ResultRow $row, string $field, array $options) {

    // Rendering of the features is expensive, so we cache the result.
    $cache_keys = [
      $this->view->id(),
      $this->view->current_display,
      $field,
      $row->_entity->id(),
      \Drupal::languageManager()->getCurrentLanguage()->getId(),
    ];
    // Cache the result.
    $cacheTags = $row->_entity->getCacheTags();
    foreach ($row->_relationship_entities as $related_entity) {
      $cache_keys[] = $related_entity->id();
      $cacheTags = Cache::mergeTags($related_entity->getCacheTags(), $cacheTags);
    }
    $cid = implode('.', $cache_keys);
    if ($cached_features = $this->cacheBin->get($cid)) {
      return $cached_features->data;
    }
    try {
      $values = $this->getFieldValue($row->index, $field);
    }
    catch (\Exception $e) {
      $this->messenger->addWarning('Incorrect geofield used in view.');
      return NULL;
    }
    if (!$values) {
      return NULL;
    }
    if(is_scalar($values)){
      $values = [$values];
    }

    $features = [];
    $properties = $this->getFeatureProperties($row, $options);

    $event = new FeaturePropertiesEvent($properties, [
      'views_row' => $row,
      'views_id' => $this->view->id(),
      'views_display' => $this->view->current_display,
    ]);
    $this->eventDispatcher->dispatch(OpenLayersEvents::FEATURE_PROPERTIES_EVENT, $event);
    foreach ($values as $value) {
      $geometry = $this->geophp->load($value);
      $feature = [
        'type' => 'Feature',
        'geometry' => Json::decode($geometry->out('json')),
        'properties' => $event->getProperties(),
      ];
      // If dedupe is enabled, we add a unique ID to each feature so it
      // only gets rendered once per field in Openlayers.
      if ($options['dedupe'] ?? FALSE) {
        $feature['id'] = $this->getFeatureHash($feature, $row, $field);
      }
      $features[] = $feature;
    }

    $this->cacheBin->set($cid, $features, Cache::PERMANENT, Cache::mergeTags($event->getCacheTags(), $cacheTags));
    return $features;
  }

  /**
   * Gets the feature properties per row.
   *
   * @param \Drupal\views\ResultRow $row
   *   The results row.
   *
   * @return array
   *   A key value array of properties to add to the feature.
   */
  protected function getFeatureProperties(ResultRow $row, array $options) {
    $properties_to_apply = array_filter(array_values($this->options['feature_properties']));
    $properties = [];
    if (!empty($properties_to_apply)) {
      foreach ($properties_to_apply as $key) {
        $this->view->row_index = $row->index;
        $rendered_value = $this->view->field[$key]->advancedRender($row);
        $properties[$key] = $rendered_value instanceof MarkupInterface ? $rendered_value->__toString() : $rendered_value;
      }
    }

    // The ajax callback url is added as a protected key (starting with '_').
    if ($options['ajax_callback_url'] ?? FALSE) {
      $properties['_ajax_callback_url'] = $this->getFieldValue($row->index, $options['ajax_callback_url']);
    }
    return $properties;
  }

  /**
   * Gets the exposed filter features.
   */
  public function getFilterFeatures() {
    if ($filters = $this->getSpatialFilters()) {
      $filter_handler = key($filters);
      $data = $this->view->exposed_data[$filter_handler];
      return $data ? Json::decode($data) : [
        'type' => 'FeatureCollection',
        'features' => [],
      ];
    }
    return FALSE;
  }

  /**
   * Get GeoJSON features based on the views rows.
   *
   * @param string $field
   *   The field to collect.
   * @param array $options
   *   Options for the different data layers.
   *
   * @return array|mixed
   *   An array of features.
   */
  public function getFeaturesPerField(string $field, $options) {
    if ($this->featuresPerField[$field] ?? NULL) {
      return $this->featuresPerField[$field];
    }

    $features = [];

    // Render each row.
    foreach ($this->view->result as $i => $row) {
      $this->view->row_index = $i;
      if ($featuresPerRow = $this->getFeaturesPerRow($row, $field, $options)) {
        foreach ($featuresPerRow as $feature) {
          $features[] = $feature;
        }
      }
    }
    $this->featuresPerField[$field] = [
      'type' => 'FeatureCollection',
      'features' => $features,
    ];
    return $this->featuresPerField[$field];

  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $map_config = $this->entityTypeManager->getStorage('openlayers_map_config')
      ->load($this->options['map_config']);

    $data = [];
    $lazy_data = [];
    foreach ($this->options['layers'] as $field => $field_data) {

      if ($this->view->total_rows && $field_data['lazy'] ?? FALSE) {
        $lazy_data[$field_data['layer']] = [
          'route' => 'openlayers.views_lazy_data',
          'route_parameters' => [
            'view' => $this->view->id(),
            'display_id' => $this->view->current_display,
            'field' => $field,
          ],
          'items_per_page' => LazyViewDataController::DEFAULT_ITEMS_PER_PAGE,
          'total' => $this->view->total_rows,
        ];
      }
      else {
        $data[$field_data['layer']] = $this->getFeaturesPerField($field, $field_data);
      }

    }
    if ($filterFeatures = $this->getFilterFeatures()) {
      $data[$this->options['spatial_filter_layer']] = $filterFeatures;
    }

    $element['map'] = [
      '#type' => 'openlayers_map',
      '#map_id' => Openlayers::getUniqueMapCssId($this->view),
      '#map_config' => $map_config,
      '#data' => $data,
      '#lazy_data' => $lazy_data,
      '#provider' => 'views',
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function evenEmpty() {
    return TRUE;
  }

  /**
   * Get eligible fields as geojson source for the map.
   *
   * @return array
   *   An array of select options.
   */
  protected function getGeofieldFieldOptions() {
    $handlers = $this->displayHandler->getHandlers('field');
    $options = [];
    $labels = $this->displayHandler->getFieldLabels();
    foreach ($handlers as $field_name => $handler) {
      if ($handler instanceof EntityField) {
        $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($handler->definition['entity_type']);
        $fieldDefinition = $field_storage_definitions[$handler->definition['field_name']];
        if ($fieldDefinition && $fieldDefinition->getType() === "geofield") {
          $options[$field_name] = $labels[$field_name];
        }
      }
    }
    return $options;
  }

  /**
   * Returns an option list of spatial filters.
   *
   * @return array|bool
   *   The options list. False if none present.
   */
  protected function getSpatialFilters() {
    $handlers = $this->displayHandler->getHandlers('filter');
    $options = [];
    if ($handlers) {
      foreach ($handlers as $id => $handler) {
        if ($handler instanceof SpatialFilter) {
          $options[$id] = $handler;
        }
      }
    }
    if ($options) {
      return $options;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function elementPreRenderRow(array $data) {
    // Instead of using the theme layer, we print the result to like it is
    // or in a json array.
    foreach ($this->view->field as $id => $field) {
      $value = $field->getValue($data['#row']);
      $data[$id] = ['#markup' => is_array($value) ? Json::encode($value) : $value];
    }
    return $data;
  }

  /**
   * Get a list of elegible fields to add as properties to geojson features.
   *
   * @return array
   *   A list of fields.
   */
  protected function getFeaturePropertyOptions() {
    $handlers = $this->displayHandler->getHandlers('field');
    $options = [];
    $labels = $this->displayHandler->getFieldLabels();
    foreach ($handlers as $field_name => $handler) {
      if ($handler instanceof EntityField) {
        $options[$field_name] = $field_name . ' (' . $labels[$field_name] . ')';
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['uses_fields']['#access'] = FALSE;

    /** @var \Drupal\openlayers\MapConfigInterface[] $mapConfigs */
    $mapConfigs = $this->entityTypeManager
      ->getStorage('openlayers_map_config')
      ->loadMultiple();

    $form['map_config'] = [
      '#title' => $this->t('Map configuration'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => array_map(function (MapConfigInterface $mapConfig) {
        return $mapConfig->label();
      }, $mapConfigs),
      '#default_value' => $this->options['map_config'],
      '#ajax' => [
        'wrapper' => 'data-layer-container',
        'callback' => [get_called_class(), 'updateDataLayers'],
      ],
    ];

    if ($form_state->getUserInput()['style_options']['map_config'] ?? NULL) {
      $selectedMapConfig = $mapConfigs[$form_state->getUserInput()['style_options']['map_config']];
    }
    elseif ($this->options['map_config']) {
      $selectedMapConfig = $mapConfigs[$this->options['map_config']];
    }
    else {
      $selectedMapConfig = NULL;
    }

    $form['layers'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
      '#title' => $this->t('Data layers'),
      '#prefix' => '<div id="data-layer-container">',
      '#suffix' => '</div>',
    ];
    foreach ($this->getGeofieldFieldOptions() as $field_name => $field_label) {
      $form['layers'][$field_name] = [
        'layer' => [
          '#type' => 'select',
          '#empty_option' => $selectedMapConfig ? $this->t('- Select -') : $this->t('- Select a map configuration first -'),
          '#disabled' => !$selectedMapConfig,
          '#title' => $field_label,
          '#description' => $this->t('Select the layer to feed to.'),
          '#default_value' => $this->options['layers'][$field_name]['layer'],
          '#required' => TRUE,
          '#options' => $this->getDataLayerOptions($selectedMapConfig),
          '#ajax' => [
            'wrapper' => 'data-layer-container',
            'callback' => [get_called_class(), 'updateDataLayers'],
          ],
        ],
        'dedupe' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Dedupe'),
          '#description' => $this->t('Only renders the same geometry once.'),
          '#default_value' => $this->options['layers'][$field_name]['dedupe'],
        ],
        'dedupe_use_base' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Use base table for deduping'),
          '#default_value' => $this->options['layers'][$field_name]['dedupe_use_base'] ?? FALSE,
        ],
        'lazy' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Lazy loading'),
          '#description' => $this->t('Loading features asynchronously.'),
          '#default_value' => $this->options['layers'][$field_name]['lazy'],
        ],
        'ajax_callback_url' => [
          '#type' => 'select',
          '#title' => $this->t('Ajax callback property'),
          '#description' => $this->t('Creates an ajax callback property that can be used with map interactions. To enable ajax interaction, you need to add the Ajax interaction callback url as a field.'),
          '#default_value' => $this->options['layers'][$field_name]['ajax_callback_url'],
          '#options' => $this->getAjaxCallbackPropertyOptions(),
          '#empty_option' => $this->t('- Select an ajax callback handler -'),
        ],
      ];
    }

    $spatialFilters = $this->getSpatialFilters();

    if ($spatialFilters && count($spatialFilters) > 1) {
      $this->messenger->addWarning('Only one spatial filter is currently supported.');
    }
    // Spatial filter layer.
    if ($spatialFilters) {
      $form['spatial_filter_layer'] = [
        '#type' => 'select',
        '#empty_option' => $this->t('- Select -'),
        '#disabled' => !$selectedMapConfig,
        '#title' => $this->t('Spatial filter layer'),
        '#description' => $this->t('Select the layer to use for the spatial filter. This cannot be the same layer as the data layer.'),
        '#default_value' => $this->options['spatial_filter_layer'],
        '#required' => TRUE,
        '#options' => $this->getDataLayerOptions($selectedMapConfig),
      ];
    }

    $form['feature_properties'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Feature properties'),
      '#description' => $this->t('Add fields as properties for each feature.'),
      '#default_value' => $this->options['feature_properties'],
      '#options' => $this->getFeaturePropertyOptions(),
    ];


  }

  /**
   * Returns a list of elegible ajax_callback_uri options.
   */
  protected function getAjaxCallbackPropertyOptions() {
    $handlers = $this->displayHandler->getHandlers('field');
    $options = [];
    $labels = $this->displayHandler->getFieldLabels();
    foreach ($handlers as $field_name => $handler) {
      if ($handler instanceof OpenlayersAjaxCallback) {
        $options[$field_name] = $labels[$field_name];
      }
    }
    return $options;
  }

  /**
   * Ajax callback for updating the data layers element.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The form element replacing the original.
   */
  public static function updateDataLayers(array $form, FormStateInterface $form_state) {
    // Reset data layer form value.
    $form_state->setValue(['style_options', 'layers'], NULL);
    $form_state->setValue(['style_options', 'spatial_filter_layer'], NULL);
    return $form['options']['style_options']['layers'];
  }

  /**
   * Get the data layer options.
   *
   * @param \Drupal\openlayers\MapConfigInterface|null $map_config
   *   The selected map configuration entity.
   *
   * @return array
   *   A list of options.
   */
  protected function getDataLayerOptions(MapConfigInterface $map_config = NULL) {
    $options = [];
    if ($map_config) {
      foreach ($map_config->getLayers() as $layer) {
        if ($layer instanceof VectorLayerInterface) {
          $options[$layer->getUuid()] = $layer->label();
        }
      }

    }
    return $options;
  }

  /**
   * Returns an unique identifier to be used in javascript interactions.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   *
   * @return string
   *   The identifier.
   *
   * @todo Think of an unique map ID (based on dom_id) that would allow
   *   multiple maps on the same page.
   */
  public static function getUniqueMapCssId(ViewExecutable $view) {
    return implode('-', ['views', $view->id(), $view->current_display]);
  }

  /**
   * Gets the map configuration for the current view.
   *
   * @return \Drupal\openlayers\MapConfigInterface
   *   The map config entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMapConfig() {
    /** @var \Drupal\openlayers\MapConfigInterface $mapConfig */
    $mapConfig = $this->entityTypeManager->getStorage('openlayers_map_config')
      ->load($this->options['map_config']);
    return $mapConfig;
  }

}
