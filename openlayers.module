<?php

/**
 * @file
 * Implements hook_entity_update().
 */

use Drupal\Core\Entity\EntityInterface;


/**
 * Implements hook_entity_bundle_field_info_alter().
 */
function openlayers_entity_bundle_field_info_alter(&$fields, \Drupal\Core\Entity\EntityTypeInterface $entity_type, $bundle) {
  foreach($fields as $field_name => $field){
    if($field->getType() == 'geofield'){
      $field->addConstraint('OpenlayersGeoSync');
    }
  }
}

/**
 * Implements hook_entity_base_field_info_alter().
 */
function openlayers_entity_base_field_info_alter(&$fields, \Drupal\Core\Entity\EntityTypeInterface $entity_type) {
  foreach($fields as $field_name => $field){
    if($field->getType() == 'geofield'){
      $field->addConstraint('OpenlayersGeoSync');
    }
  }
}

/**
 * Implements hook_entity_delete().
 *
 * @see \Drupal\openlayers\Plugin\Validation\Constraint\GeoSyncConstraint
 */
function openlayers_entity_delete(EntityInterface $entity) {
  // Sync geofield data with dedicated table used for geospatial querying.
  Drupal::service('openlayers.geofield_sync')->deleteSyncedGeoData($entity, TRUE);
}

/**
 * Implements hook_theme().
 */
function openlayers_theme($existing, $type, $theme, $path) {
  return [
    'openlayers_map' => [
      'variables' => [
        'attributes' => [],
        'provider' => NULL,
      ],
    ],
    'openlayers_tooltip' => [
      'variables' => [
        'attributes' => [],
      ],
    ],
    'openlayers_map_legacy' => [
      'render element' => 'element',
    ],
    'openlayers_spatial_select' => [
      'render element' => 'element',
    ],
    'openlayers_lazy_loading_progress' => [
      'variables' => [
        'total' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function openlayers_theme_suggestions_openlayers_map(array $variables) {
  $suggestions = [];
  $suggestions[] = $variables['theme_hook_original'] . '__' . $variables['attributes']['id'];
  if ($variables['provider']) {
    $suggestions[] = $variables['theme_hook_original'] . '__' . $variables['provider'];
  }
  return $suggestions;
}

/**
 * Implements hook_page_attachments().
 *
 * @todo Implement the once library once I understand how it works.
 */
function openlayers_page_attachments(array &$attachments) {
  // Unconditionally attach an asset to the page.
  $attachments['#attached']['library'][] = 'openlayers/once';
}
