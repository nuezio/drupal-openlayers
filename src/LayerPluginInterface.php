<?php

namespace Drupal\openlayers;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines the interface for layers.
 */
interface LayerPluginInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Returns a render array summarizing the configuration of the layer.
   *
   * @return array
   *   A render array.
   */
  public function getSummary();

  /**
   * Returns the layer label.
   *
   * @return string
   *   The layer label.
   */
  public function label();

  /**
   * Sets the label for this layer.
   *
   * @param string $label
   *   THe layer name.
   *
   * @return $this
   */
  public function setLabel(string $label);

  /**
   * Returns the unique ID representing the layer.
   *
   * @return string
   *   The layer ID.
   */
  public function getUuid();

  /**
   * Returns the weight of the layer.
   *
   * @return int|string
   *   Either the integer weight of the layer, or an empty string.
   */
  public function getWeight();

  /**
   * Sets the weight for this layer.
   *
   * @param int $weight
   *   The weight for this layer.
   *
   * @return $this
   */
  public function setWeight(int $weight);

  /**
   * Checks if the layer is vector based.
   *
   * @return bool
   *   TRUE if vector based.
   */
  public function isVectorLayer(): bool;

}
