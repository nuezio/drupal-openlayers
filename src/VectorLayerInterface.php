<?php

namespace Drupal\openlayers;

/**
 * Defines the interface for configurable openlayers layers.
 */
interface VectorLayerInterface {

  /**
   * Get the style config to apply to the vector layer.
   *
   * @return \Drupal\openlayers\StyleConfigInterface
   *   The style configuration.
   */
  public function getStyleConfig(): StyleConfigInterface;

  /**
   * Gets static data to populate the layer.
   *
   * @return array
   *   The data.
   */
  public function getStaticData(): array;

}
