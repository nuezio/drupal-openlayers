import {Openlayers} from "./Openlayers";
import {MapConfig} from "./MapConfig";

export type DrupalJS = {
  t: (string: string) => {},
  formatPlural: (count: number, singular: string, plural: string) => {},
  openlayers: {
    maps: {
      [key: string]: Openlayers
    }
  },
  behaviors: {
    openlayers: {

    }
  }
  AjaxCommands: {
    prototype: {
      updateMap: {}
     }
    [key: string]: {}
  }
  theme: {
    openlayersLazyProgress: (lazy_loader_element: LazyLoaderElement, loaded_items: number) => void
  }
}

export interface LazyLoaderElement {
  dataset: {
    progress: number
    total: number
  }
}

export type OpenlayersSettings = {
  openlayers: {
    maps: {
      [key: string]: {
        data: {}
        lazy_data: {}
        mapConfig: MapConfig
      }
    }
  }
}
