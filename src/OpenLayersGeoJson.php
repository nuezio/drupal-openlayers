<?php

namespace Drupal\openlayers;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;

/**
 * Geojson service.
 */
class OpenLayersGeoJson implements OpenLayersGeoJsonInterface {


  /**
   * Default extent that the map without any data should be zoomed into.
   */
  const DEFAULT_EXTENT = [
    -1424151.6059555786,
    4791169.836772656,
    2215405.8396076346,
    7908328.561944159,
  ];

  /**
   * The GeoPHP wrapper service.
   *
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geoPhp;

  /**
   * The constructor.
   *
   * @param \Drupal\geofield\GeoPHP\GeoPHPInterface $geo_php
   *   The geophp wrapper service.
   */
  public function __construct(GeoPHPInterface $geo_php) {
    $this->geoPhp = $geo_php;
  }

  /**
   * {@inheritdoc}
   */
  public function convertFieldItemsToGeoJson(FieldItemListInterface $items) {

    $features = [];

    foreach ($items->getIterator() as $item) {
      $value = $item->getValue();
      if ($value) {
        $geometry = $this->geoPhp->load($value['value'], 'wkt');
        if ($geometry) {
          $features[] = [
            'type' => 'Feature',
            'geometry' => Json::decode($geometry->out('json')),
          ];
        }
      }
    }

    return [
      'type' => 'FeatureCollection',
      'features' => $features,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function convertGeoJsonToFieldValues(string $geojson) {
    $geojson = Json::decode($geojson);
    $values = [];
    if (!empty($geojson['features'])) {
      foreach ($geojson['features'] as $delta => $feature) {
        try {
          $geom = $this->geoPhp->load(Json::encode($feature['geometry']), 'json');
          $values[$delta]['value'] = $geom->out('wkt');
        }
        catch (\Exception $e) {
        }
      }
    }
    return $values;
  }

}
