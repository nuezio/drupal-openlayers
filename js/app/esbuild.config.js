const path = require('path')

require("esbuild").build({
  entryPoints: ['./src/openlayers-drupal.ts', './src/widget.ts'],
  outdir: 'dist',
  bundle: true,
  sourcemap: process.argv.includes('--sourcemap'),
  watch: process.argv.includes("--watch"),
  plugins: [],
}).catch(() => process.exit(1))
