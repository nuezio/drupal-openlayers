import { LayerPluginInterface } from '../LayerInterface';
import TileLayer from 'ol/layer/Tile';
import { LayerBase } from './LayerBase';
import { OSM as OpenlayersOSM } from 'ol/source';
import BaseLayer from 'ol/layer/Base';

export class OSM extends LayerBase implements LayerPluginInterface {
    /**
     * Build the vector based layer.
     */
    buildLayer(): BaseLayer | null {
        return new TileLayer({
            source: new OpenlayersOSM(),
        });
    }
}
