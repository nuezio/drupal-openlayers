<?php

namespace Drupal\openlayers_example\EventSubscriber;

use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\openlayers\MapEvent;
use Drupal\openlayers\OpenLayersEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * OpenLayers event subscriber to open entity in dialog.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      OpenLayersEvents::MAP_EVENT => ['onMapEvent'],
    ];
  }

  /**
   * Kernel response event handler.
   *
   * @param \Drupal\openlayers\MapEvent $event
   *   The map event being dispatched.
   */
  public function onMapEvent(MapEvent $event) {
    $ajaxResponse = $event->getResponse();
    $entity = $event->getEntity();
    $view = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId())->view($entity, 'offset');
    $ajaxResponse->addCommand(new OpenModalDialogCommand($entity->label(), $view));
  }

}
