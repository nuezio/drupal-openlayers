<?php

namespace Drupal\openlayers;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for configurable openlayer layers.
 *
 * @see plugin_api
 */
abstract class ConfigurableVectorLayerPluginBase extends ConfigurableLayerPluginBase implements VectorLayerInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterfacee
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('openlayers'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [];
  }

  /**
   * {@inheritdoc}
   */
  public function getStyleConfig(): StyleConfigInterface {
    $styleConfigObject = NULL;
    if ($style_config = $this->getConfiguration()['data']['style_config']) {
      /** @var \Drupal\openlayers\StyleConfigInterface $styleConfigObject */
      $styleConfigObject = $this->entityTypeManager
        ->getStorage('openlayers_style_config')
        ->load($style_config);
    }
    if (!$style_config) {
      throw new \Exception('A style configuration is required for this vector layer plugin');
    }
    return $styleConfigObject;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['style_config'] = $form_state->getValue('style_config');
  }

}
