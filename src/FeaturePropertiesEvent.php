<?php

namespace Drupal\openlayers;

use Symfony\Component\EventDispatcher\Event;

/**
 * The event that is dispatched upon a map event.
 */
class FeaturePropertiesEvent extends Event {

  /**
   * The event.
   *
   * @var string
   */
  protected $properties;

  /**
   * The context of the properties.
   *
   * @var array
   */
  protected $context;

  /**
   * Cache tags that might be added by the event subscriber.
   *
   * @var array
   */
  protected $cacheTags = [];

  /**
   * MapInteractionEvent constructor.
   *
   * @param array $properties
   *   An array of properties.
   * @param array $context
   *   The context of the properties being set.
   */
  public function __construct(array $properties, array $context) {
    $this->properties = $properties;
    $this->context = $context;
  }

  /**
   * Get the properties.
   */
  public function getProperties() {
    return $this->properties;
  }

  /**
   * Return the cache tags.
   *
   * @return array
   *   The cache tags.
   */
  public function getCacheTags() {
    return $this->cacheTags;
  }

  /**
   * Set cache tags relevant to anything that subscribes to this event.
   *
   * @param array $tags
   *   Cache tags.
   */
  public function setCacheTags(array $tags) {
    $this->cacheTags = $tags;
  }

  /**
   * Set the feature properties.
   *
   * @param array $properties
   *   An array of properties.
   */
  public function setProperties(array $properties) {
    $this->properties = $properties;
  }

  /**
   * Get the context.
   *
   * @return array
   *   The context.
   */
  public function getContext() {
    return $this->context;
  }

}
