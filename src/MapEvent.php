<?php

namespace Drupal\openlayers;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * The event that is dispatched upon a map event.
 */
class MapEvent extends Event {

  /**
   * The event.
   *
   * @var string
   */
  protected $event;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * The ajax reponse to return upon event.
   *
   * @var \Drupal\Core\Ajax\AjaxResponse
   */
  protected $response;

  /**
   * MapInteractionEvent constructor.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The ajax response that can be altered.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $event
   *   The event.
   */
  public function __construct(AjaxResponse $response, ContentEntityInterface $entity, string $event) {
    $this->response = $response;
    $this->entity = $entity;
    $this->event = $event;
  }

  /**
   * Gets the map event.
   *
   * @return string
   *   The event.
   */
  public function getEvent() {
    return $this->event;
  }

  /**
   * Gets the entity on which the event was executed.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * The ajax response to return.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function getResponse() {
    return $this->response;
  }

}
