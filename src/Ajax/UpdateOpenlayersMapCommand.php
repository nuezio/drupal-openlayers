<?php

namespace Drupal\openlayers\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\openlayers\Plugin\views\style\Openlayers;
use Drupal\views\ViewExecutable;

/**
 * AJAX commands for interacting with the openlayers map from the server.
 *
 * @ingroup ajax
 */
class UpdateOpenlayersMapCommand implements CommandInterface {

  /**
   * The views executable.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected $view;

  /**
   * GeoJSON formatted geojson data to update the map with.
   *
   * @var string
   */
  protected $data;

  /**
   * The data used for spatial filtering.
   *
   * @var string
   */
  protected $stFilterData;

  /**
   * The constructor for the map command.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view to update.
   * @param string $data
   *   GeoJSON encoded array of geo data to display in the view.
   * @param string $st_filter_data
   *   GeoJSON encoded array of gis data to use for filtering.
   */
  public function __construct(ViewExecutable $view, array $data, string $st_filter_data = '') {
    $this->view = $view;
    $this->data = $data;
    $this->stFilterData = $st_filter_data;
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {
    return [
      'command' => 'updateMap',
      'map_id' => Openlayers::getUniqueMapCssId($this->view),
      'data' => $this->data,
      'st_filter_data' => $this->stFilterData,
    ];
  }

}
