<?php

namespace Drupal\openlayers\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\openlayers\ConfigurableLayerPluginInterface;
use Drupal\openlayers\MapConfigInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base form for layers.
 */
abstract class LayerFormBase extends FormBase {

  /**
   * The openlayers map config.
   *
   * @var \Drupal\openlayers\MapConfigInterface
   */
  protected $mapConfig;

  /**
   * The layer.
   *
   * @var \Drupal\openlayers\LayerPluginInterface|\Drupal\openlayers\ConfigurableLayerPluginInterface
   */
  protected $layer;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openlayers_layer_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MapConfigInterface $openlayers_map_config = NULL, $layer = NULL) {
    $this->mapConfig = $openlayers_map_config;
    try {
      $this->layer = $this->prepareLayer($layer);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Invalid layer id: '$layer'.");
    }
    $request = $this->getRequest();

    if (!($this->layer instanceof ConfigurableLayerPluginInterface)) {
      throw new NotFoundHttpException();
    }

    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->layer->getUuid(),
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->layer->getPluginId(),
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#default_value' => $this->layer->label(),
      '#title' => $this->t('Label'),
      '#required' => TRUE,
    ];

    $form['data'] = [];
    $subform_state = SubformState::createForSubform($form['data'], $form, $form_state);
    $form['data'] = $this->layer->buildConfigurationForm($form['data'], $subform_state);
    $form['data']['#tree'] = TRUE;

    // Check the URL for a weight, then the layer, otherwise use default.
    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $request->query->has('weight') ? (int) $request->query->get('weight') : $this->layer->getWeight(),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->mapConfig->toUrl('edit-form'),
      '#attributes' => ['class' => ['button']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The layer configuration is stored in the 'data' key in the form,
    // pass that through for validation.
    if ($this->layer) {
      $this->layer->validateConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    // The layer configuration is stored in the 'data' key in the form,
    // pass that through for submission.
    $this->layer->submitConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));

    $this->layer->setWeight($form_state->getValue('weight'));
    $this->layer->setLabel($form_state->getValue('label'));
    if (!$this->layer->getUuid()) {
      $this->mapConfig->addLayer($this->layer->getConfiguration());
    }

    $this->mapConfig->save();

    $this->messenger()->addStatus($this->t('The layer was successfully applied.'));
    $form_state->setRedirectUrl($this->mapConfig->toUrl('edit-form'));
  }

  /**
   * Converts a layer ID into an object.
   *
   * @param string $layer
   *   The Layer id.
   *
   * @return \Drupal\openlayers\LayerPluginInterface
   *   The layer plugin object.
   */
  abstract protected function prepareLayer(string $layer);

}
