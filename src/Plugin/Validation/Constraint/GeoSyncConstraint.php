<?php

namespace Drupal\openlayers\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a GeoSync constraint.
 *
 * @Constraint(
 *   id = "OpenlayersGeoSync",
 *   label = @Translation("GeoSync", context = "Validation"),
 * )
 */
class GeoSyncConstraint extends Constraint {

  /**
   * The error message
   *
   * @var string
   */
  public $errorMessage = 'The geometry could not be processed correctly. Please revise the provided geometry.';

}
