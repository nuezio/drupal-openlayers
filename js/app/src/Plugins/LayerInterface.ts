import BaseLayer from 'ol/layer/Base';

export interface LayerPluginInterface {
    buildLayer(): BaseLayer | null;
}
