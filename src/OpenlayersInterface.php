<?php

namespace Drupal\openlayers;

/**
 * Contains the events dispatched by the open layers module.
 */
interface OpenlayersInterface {

  /**
   * Returns projections supported by Openlayers.
   *
   * @return array
   *   Keyed by ID, value is translatable markup.
   */
  public static function getProjections();

}
