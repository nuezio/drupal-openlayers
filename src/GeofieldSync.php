<?php

namespace Drupal\openlayers;

use Drupal;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Exception;

/**
 * Service to sync all geofield data into a table that supports spatial filters.
 *
 * @todo Create a backend storage for geofield that supports ST filters
 *   and use it here.
 */
class GeofieldSync {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The database connectoin service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs an EntityReplacer object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, Connection $connection) {
    $this->entityFieldManager = $entity_field_manager;
    $this->connection = $connection;
  }

  /**
   * Sync geospatial data.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Content entity interface.
   *
   * @see \Drupal\openlayers\Plugin\Validation\Constraint\GeoSyncConstraint
   */
  public function deleteSyncedGeoData(EntityInterface $entity) {
    if (!$entity instanceof ContentEntityInterface) {
      return;
    }
    $fields = $this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    foreach ($fields as $name => $field) {
      if ($field->getType() != 'geofield') {
        continue;
      }
      $this->connection->delete('openlayers_geofield_sync')
        ->condition('entity_id', $entity->id())
        ->condition('entity_type_id', $entity->getEntityTypeId())
        ->condition('field_name', $name)
        ->execute();
    }
  }

}
