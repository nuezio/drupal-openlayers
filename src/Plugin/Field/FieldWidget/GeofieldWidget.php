<?php

namespace Drupal\openlayers\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\openlayers\Element\OpenlayersMapTools;
use Drupal\openlayers\Entity\MapConfig;
use Drupal\openlayers\OpenLayersGeoJsonInterface;
use Drupal\openlayers\Plugin\Field\OpenlayersFieldTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use function array_filter;

/**
 * Defines the 'openlayers_geofield_widget' field widget.
 *
 * @FieldWidget(
 *   id = "openlayers_geofield_widget",
 *   label = @Translation("Openlayers widget"),
 *   field_types = {"geofield"},
 *   multiple_values= TRUE,
 * )
 */
class GeofieldWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  use OpenlayersFieldTrait;

  /**
   * The geojson service.
   *
   * @var \Drupal\openlayers\OpenLayersGeoJsonInterface
   */
  protected $geojson;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $container->get('openlayers.geojson'), $container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, OpenLayersGeoJsonInterface $geojson, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->geojson = $geojson;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() +
      [
        'tools' => [],
        'geometry_types' => ['Point'],
        'default_tool' => NULL,
      ] + OpenlayersFieldTrait::sharedDefaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return array_merge([
      $this->t('Tools: @tools', ['@tools' => implode(', ', array_filter($this->getSetting('tools')))]),
      $this->t('Geometry types: @types', [
        '@types' => implode(',', array_filter($this->getSetting('geometry_types'))),
      ]),
    ],
      $this->sharedSettingsSummary()
    );
  }

  /**
   * Returns the tool options for the openlayers widget.
   *
   * @return array
   *   The options.
   */
  protected function getToolOptions() {
    return [
      'draw' => $this->t('Draw'),
      'geojson' => $this->t('GeoJSON'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['tools'] = [
      '#title' => $this->t('Tools'),
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $this->getToolOptions(),
      '#default_value' => $this->getSetting('tools'),
    ];

    $form['geometry_types'] = [
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#title' => $this->t('Geometry types'),
      '#options' => OpenlayersMapTools::getGeometryTypeOptions(),
      '#default_value' => $this->getSetting('geometry_types'),
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][tools][draw]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['default_tool'] = [
      '#type' => 'select',
      '#title' => $this->t('Default tool'),
      '#options' => [
        'draw' => $this->t('Draw'),
        'geojson' => $this->t('Geojson'),
        'shapefile' => $this->t('Shapefile'),
      ],
      '#empty_option' => $this->t('- No default'),
      '#default_value' => $this->getSetting('default_tool'),
    ];
    $form += $this->sharedSettingsForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $error, array $form, FormStateInterface $form_state) {
    return $element['value']['errors'];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $features = $this->geojson->convertFieldItemsToGeoJson($items);

    $geometry_types = array_keys(array_filter($this->getSetting('geometry_types')));
    $map_config = MapConfig::load($this->getSetting('map_config'));
    $data_layer = $this->getSetting('data_layer');
    if (!$map_config || !$data_layer) {
      return [];
    }
    $map_id = $items->getFieldDefinition()->getName();
    if ($features['features']) {
      foreach ($features['features'] as $delta => &$feature) {
        $feature['properties'] = [
          '_map_id' => $map_id,
          '_layer_id' => $this->getSetting('data_layer'),
        ];
      }
    }
    if (!$form_state->get('wrapper_id')) {
      $form_state->set('wrapper_id', Html::getUniqueId('openlayers_geofield_widget'));
    }

    $element['value'] = $element +
      [
        '#type' => 'fieldset',
        '#attributes' => ['data-openlayers-widget' => $form_state->get('wrapper_id')],
        'wrapper' => [
          '#type' => 'openlayers_map_tools',
          '#map_config' => $map_config,
          '#map_id' => $map_id,
          '#tools' => array_filter($this->getSetting('tools')),
          '#default_tool' => $this->getSetting('default_tool'),
          '#data' => [
            $this->getSetting('data_layer') => $features,
          ],
          '#default_value' => $element['#default_value'] ?? [
              $this->getSetting('data_layer') => $features,
            ],
          '#input_layer' => $this->getSetting('data_layer'),
          '#provider' => 'field_widget',
          '#geometry_types' => $geometry_types,
        ],
        'errors' => [
          '#type' => 'fieldset',
        ],
      ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    parent::massageFormValues($values, $form, $form_state);
    return $this->geojson->convertGeoJsonToFieldValues($values['value']['wrapper']);
  }

}
