<?php

namespace Drupal\openlayers\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\openlayers\LayerPluginManager;
use Drupal\openlayers\MapConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add form for layers.
 *
 * @internal
 */
class LayerAddForm extends LayerFormBase {

  /**
   * The layer plugin manager.
   *
   * @var \Drupal\openlayers\LayerPluginManager
   */
  protected $layerPluginManager;

  /**
   * Constructs a new layer add form.
   *
   * @param \Drupal\openlayers\LayerPluginManager $layer_plugin_manager
   *   The layer plugin manager.
   */
  public function __construct(LayerPluginManager $layer_plugin_manager) {
    $this->layerPluginManager = $layer_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.openlayers.layer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MapConfigInterface $openlayers_map_config = NULL, $layer = NULL) {
    $form = parent::buildForm($form, $form_state, $openlayers_map_config, $layer);

    $form['#title'] = $this->t('Add %label effect', ['%label' => $this->layer->label()]);
    $form['actions']['submit']['#value'] = $this->t('Add layer');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareLayer($layer) {
    $layer = $this->layerPluginManager->createInstance($layer);
    $layer->setWeight(count($this->mapConfig->getLayers()));
    return $layer;
  }

}
