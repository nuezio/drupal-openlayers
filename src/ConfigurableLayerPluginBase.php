<?php

namespace Drupal\openlayers;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for configurable openlayer layers.
 *
 * @see plugin_api
 */
abstract class ConfigurableLayerPluginBase extends LayerPluginPluginBase implements ConfigurableLayerPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function isVectorLayer(): bool {
    return FALSE;
  }

}
