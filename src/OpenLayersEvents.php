<?php

namespace Drupal\openlayers;

/**
 * Contains the events dispatched by the open layers module.
 */
final class OpenLayersEvents {

  /**
   * Dispatched on some of the map events that might require an ajax response.
   *
   * @Event("Drupal\openlayers\MapEvent")
   */
  const MAP_EVENT = 'openlayers.map_event';

  /**
   * Event to alter properties used by views.
   *
   * @Event("Drupal\openlayers\ViewsFeaturePropertiesEvent")
   */
  const FEATURE_PROPERTIES_EVENT = 'openlayers.feature_properties_event';

}
