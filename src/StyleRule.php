<?php

namespace Drupal\openlayers;

/**
 * Class wrapper to deal with style rule config within a style config entity.
 */
class StyleRule implements StyleRuleInterface {

  /**
   * The uuid of the style rule.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The conditions when the style rule should apply.
   *
   * @var array
   */
  protected $conditions;

  /**
   * The style definition array.
   *
   * @var array
   */
  protected $styleDefinition;

  /**
   * The operator for comparing values.
   *
   * @var string
   */
  protected $operator;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight of the style rule.
   *
   * @var int
   */
  protected $weight;

  /**
   * Static create method for instanciating the style rule.
   */
  public static function create($config, $uuid = NULL) {
    return new static(
      $uuid ?? \Drupal::service('uuid')->generate(),
      $config
    );
  }

  /**
   * THe constructor.
   *
   * @param string $uuid
   *   The uuid of the style rule.
   * @param array $config
   *   The array of config.
   */
  public function __construct(string $uuid, array $config) {
    $config = array_replace_recursive($this->getDefaultConfiguration(), $config);
    $this->uuid = $uuid;
    $this->label = $config['label'];
    $this->conditions = $config['conditions'];
    $this->styleDefinition = $config['style_definition'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions(): array {
    return $this->conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function getStyleDefinition(): array {
    return $this->styleDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * Gets the default configuration.
   *
   * @return array
   *   The default config.
   *
   * @todo Can this be loaded from config schema?
   */
  public function getDefaultConfiguration() {
    return [
      'label' => '',
      'conditions' => [
        'features' => [],
        'view' => [
          'zoom' => [
            'min' => '',
            'max' => '',
          ],
        ],
      ],
      'style_definition' => [
        'image' => [
          'image_type' => '',
          'data' => [],
        ],
        'stroke' => $this->getDefaultConfigurationStroke(),
        'fill' => $this->getDefaultConfigurationFill(),
        'cluster' => $this->getDefaultConfigurationCluster(),
      ],
    ];
  }

  /**
   * Default configuration for Text.
   *
   * @return array
   *   The default config.
   */
  protected function getDefaultConfigurationText() {
    return [
      'font' => '',
      'text' => '',
      'fill' => $this->getDefaultConfigurationFill(),
      'stroke' => $this->getDefaultConfigurationStroke(),
    ];
  }

  /**
   * Default configuration for Circle.
   *
   * @return array
   *   The default config.
   */
  protected function getDefaultConfigurationCircle() {
    return [
      'radius' => '',
      'fill' => $this->getDefaultConfigurationFill(),
      'stroke' => $this->getDefaultConfigurationStroke(),
    ] + $this->getDefaultConfigurationImage();
  }

  /**
   * Default configuration for Image.
   *
   * @return array
   *   The default config.
   */
  protected function getDefaultConfigurationImage() {
    return [
      'data' => [],
      'opacity' => 1,
    ];
  }

  /**
   * Default configuration for Cluster.
   *
   * @return array
   *   The default config.
   */
  protected function getDefaultConfigurationCluster() {
    return [
      'image' => $this->getDefaultConfigurationImage(),
      'text' => $this->getDefaultConfigurationText(),
    ];
  }

  /**
   * Default configuration for Fill.
   *
   * @return array
   *   The default config.
   */
  protected function getDefaultConfigurationFill() {
    return [
      'color' => '',
    ];
  }

  /**
   * Default configuration for Stroke.
   *
   * @return array
   *   The default config.
   */
  protected function getDefaultConfigurationStroke() {
    return [
      'color' => '',
      'line_cap' => '',
      'line_join' => '',
      'line_dash' => '',
      'width' => 0,
    ];
  }

}
