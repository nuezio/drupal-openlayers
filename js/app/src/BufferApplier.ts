import OL3Parser from 'jsts/org/locationtech/jts/io/OL3Parser';
import { BufferOp } from 'jsts/org/locationtech/jts/operation/buffer';
import { LineString, MultiLineString, MultiPoint, MultiPolygon, Point, Polygon, LinearRing } from 'ol/geom';
import { Feature } from 'ol';
import VectorSource from 'ol/source/Vector';
import GeoJSON from "ol/format/GeoJSON";

/**
 * Toolset for creating buffers from polygons.
 */
export default class BufferApplier {
    /**
     * Applies Buffer feature to filter select area.
     *
     * This can receive both the buffer vector and deduce the original poligon
     * or process the original polygon and add the buffer feature to it.
     *
     * @param filterAreaFeature
     * @param buffer
     */
    createBuffer(filterAreaFeature: Feature, buffer = 1000): Feature | null {

        const parser = new OL3Parser();
        parser.inject(Point, LineString, LinearRing, Polygon, MultiPoint, MultiLineString, MultiPolygon);

        const bufferFeature = filterAreaFeature.clone();
        const jstsGeometry = parser.read(filterAreaFeature?.getGeometry());
        const bufferedGeometry = BufferOp.bufferOp(jstsGeometry, buffer);
        bufferFeature?.setGeometry(parser.write(bufferedGeometry));



        return bufferFeature;
    }

    /**
     * From a buffer geometry, deduce the original geometry.
     */
    deduceFilterArea(source: VectorSource): Feature {
        const parser = new OL3Parser();
        parser.inject(Point, LineString, LinearRing, Polygon, MultiPoint, MultiLineString, MultiPolygon);
        const bufferFeature = source.getFeatureById('buffer');
        const bufferSize = bufferFeature.get('buffer_size');
        const filterAreaFeature = bufferFeature.clone();
        const jstsGeometry = parser.read(bufferFeature?.getGeometry());
        const bufferedGeometry = BufferOp.bufferOp(jstsGeometry, -bufferSize);
        filterAreaFeature?.setGeometry(parser.write(bufferedGeometry));
        filterAreaFeature.setId('filter_area');
        return filterAreaFeature;
    }

    /**
     * Adjust the buffer based on an integer.
     */
    adjustBuffer(source: VectorSource, buffer: number) {
        const parser = new OL3Parser();
        parser.inject(Point, LineString, LinearRing, Polygon, MultiPoint, MultiLineString, MultiPolygon);
        const bufferFeature = source.getFeatureById('buffer');
        const filterAreaFeature = source.getFeatureById('filter_area');
        if (bufferFeature) {
            source.removeFeature(bufferFeature);
        }
        const newBuffer = this.createBuffer(filterAreaFeature, buffer);
        source.addFeature(newBuffer);
        return newBuffer;
    }
}
