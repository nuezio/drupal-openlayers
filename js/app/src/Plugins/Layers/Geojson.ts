import { LayerPluginInterface } from '../LayerInterface';
import VectorLayer from 'ol/layer/Vector';
import LayerGroup from 'ol/layer/Group';
import GeoJSON from 'ol/format/GeoJSON';
import { Feature } from 'ol';
import VectorSource from 'ol/source/Vector';
import { Cluster } from 'ol/source';
import Styler from '../../Styler';
import { StyleLike } from 'ol/style/Style';
import { ceil } from 'mathjs';
import { LayerBase } from './LayerBase';
import BaseLayer from 'ol/layer/Base';
import { Openlayers } from '../../Openlayers';

export interface GeojsonLayerConfig {
    static_data: string;
    projection: string;
    cluster_enable: boolean;
    cluster_distance: number;
    style_config: string;
    hover: {
        property: string;
        property_match: string;
        tooltip: string;
    };
    fit_extents: string;
}

export class Geojson extends LayerBase implements LayerPluginInterface {
    protected styler: Styler;

    protected layerConfig: GeojsonLayerConfig;

    protected data: Feature[];

    protected lazyData: {
        url: string;
        total: number;
        items_per_page: number;
        page_query_param: string;
    };

    protected source: VectorSource;

    constructor(openlayers: Openlayers, layer_id: string) {
        super(openlayers, layer_id);
        this.layerConfig = this.openlayers.mapConfig.getLayerConfig(this.layerId);
        this.styler = new Styler(this.openlayers);
        this.data = this.openlayers.getDataPerLayer(this.layerId);
        this.lazyData = this.openlayers.getLazyDataPerLayer(this.layerId);
        this.source = new VectorSource();
        this.openlayers.sources[layer_id] = this.source;
    }

    /**
     * Build the vector based layer.
     */
    buildLayer(): BaseLayer | null {
        if (this.data.length === 0 && this.lazyData === undefined) {
            return null;
        }

        // Instantiate variable.
        let features;

        if (this.data.length !== 0) {
            features = new GeoJSON().readFeatures(JSON.stringify(this.data), {
                featureProjection: this.layerConfig.projection,
            });

            // Add some basic properties to each feature for
            // future reference.
            features.forEach((feature: Feature) => {
                feature.set('_layer_id', this.layerId, true);
                feature.set('_map_id', this.openlayers.getMapId(), true);
            });
        }
        let layer: VectorLayer | LayerGroup;
        if (features) {
            this.source.addFeatures(features);
        }
        if (this.layerConfig.cluster_enable) {
            const clusterSource = new Cluster({
                distance: this.layerConfig.cluster_distance,
                source: this.source,
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                geometryFunction: (feature) => {
                    const geom = feature.getGeometry();
                    return geom?.getType() == 'Point' && !feature.get('hidden') ? geom : null;
                },
            });
            layer = new LayerGroup({
                layers: [
                    new VectorLayer({
                        source: clusterSource,
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        style: (feature: Feature): StyleLike | undefined => {
                            return this.styler.styleCluster(feature);
                        },
                    }),
                    new VectorLayer({
                        source: this.source,
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        style: (feature: Feature) => {
                            return this.styler.styleExcludedFromCluster(feature);
                        },
                    }),
                ],
            });
        } else {
            layer = new VectorLayer({
                source: this.source,
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                style: (feature: Feature) => {
                    return this.styler.styleFeature(feature);
                },
            });
        }
        if (this.lazyData) {
            this.lazyLoad();
        }
        return layer;
    }

    public updateLayer(data: any): void {
        const features = new GeoJSON().readFeatures(data, {
            featureProjection: this.layerConfig.projection,
        });
        this.source.clear();
        features.forEach((feature: Feature) => {
            feature.set('_map_id', this.openlayers.getMapId(), true);
            feature.set('_layer_id', this.layerId, true);
        });
        this.source.addFeatures(features);
        this.openlayers.getMap().dispatchEvent('lazy_loading_finished');
    }

    protected lazyLoad(): void {
        const items_per_page = this.lazyData.items_per_page;
        const pages = ceil(this.lazyData.total / items_per_page);
        const url = new URL(this.lazyData.url);
        for (let i = 0; i < pages; i++) {
            url.searchParams.set('page', String(i));
            this.openlayers.promisesCollection.push(
                fetch(url.toString())
                    .then((response) => {
                        if (!response.ok) {
                            console.log(response);
                        }
                        return response.json();
                    })
                    .then((data) => {
                        const features = new GeoJSON().readFeatures(JSON.stringify(data), {
                            featureProjection: this.layerConfig.projection,
                        });
                        features.forEach((feature: Feature) => {
                            feature.set('_layer_id', this.layerId, true);
                            feature.set('_map_id', this.openlayers.getMapId(), true);
                        });
                        this.source.addFeatures(features);
                        Drupal.theme(
                            'openlayersLazyProgress',
                            this.openlayers.lazyLoadingProgressElement,
                            items_per_page,
                        );
                    }),
            );
        }
    }
}
