import { MapConfig, MapLayerConfig } from './MapConfig';
import { Feature, Map, Overlay, View } from 'ol';
import { fromLonLat } from 'ol/proj';
import LayerGroup from 'ol/layer/Group';
import { Geojson, GeojsonLayerConfig } from './Plugins/Layers/Geojson';
import { OSM } from './Plugins/Layers/OSM';
import { createEmpty, extend, Extent, isEmpty } from 'ol/extent';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import { FeatureLike } from 'ol/Feature';
import BufferApplier from './BufferApplier';
import GeoJSON from 'ol/format/GeoJSON';
import VectorSource from 'ol/source/Vector';

/**
 * Class that instantiates the Openlayers maps.
 */
export class Openlayers {
    protected mapId: string;

    public mapConfig: MapConfig;

    protected data: never;

    protected lazyData: never;

    protected map: Map;

    protected tooltipElement: HTMLElement;

    public lazyLoadingProgressElement: HTMLElement;

    protected featureSelectContainer: HTMLElement;

    public sources: any;

    public layers: any;

    public promisesCollection: PromiseLike<any>[];

    protected spatialFilterEnabled = false;

    public layerPlugins: any = {};

    public spatialFilterSource: VectorSource | null = null;

    public tools = {
        bufferApplier: new BufferApplier(),
        geojson: new GeoJSON(),
        getCombinedExtent: this.getCombinedExtent,
    };

    /**
     * The Openlayers object constructor.
     *
     * @param map_id
     * @param map_config
     * @param data
     * @param lazy_data
     * @param tooltip_element
     * @param lazy_loading_progress_element
     * @param feature_select_container
     */
    constructor(
        map_id: string,
        map_config: never,
        data: never,
        lazy_data: never,
        tooltip_element: HTMLElement,
        lazy_loading_progress_element: HTMLElement,
        feature_select_container: HTMLElement,
    ) {
        this.mapId = map_id;
        this.mapConfig = new MapConfig(map_config);
        this.tooltipElement = tooltip_element;
        this.lazyLoadingProgressElement = lazy_loading_progress_element;
        this.featureSelectContainer = feature_select_container;
        this.data = data;
        this.lazyData = lazy_data;
        // Initiate a collection of promises for all layers.
        this.promisesCollection = [];
        this.layers = [];
        this.sources = [];
        this.map = new Map({
            controls: [],
            target: map_id,
            view: new View({
                center: fromLonLat([
                    this.mapConfig.getView().center.longitude,
                    this.mapConfig.getView().center.latitude,
                ]),
                zoom: this.mapConfig.getView()['zoom'],
            }),
            overlays: [
                new Overlay({
                    id: 'tooltip',
                    element: tooltip_element,
                    stopEvent: false,
                }),
            ],
        });
        this.initLayers();

        this.addClickInteractions();

        this.addHoverInteractions();

        // Dispatch an event after all promises have been fulfulled.
        Promise.all(this.promisesCollection).then((values) => {
            console.log('lazy loading finished dispatching event.');
            this.map.dispatchEvent('lazy_loading_finished');
            const extent = this.getCombinedExtent();
            if (!isEmpty(extent)) {
                this.getMap()
                    .getView()
                    .fit(extent, {
                        padding: [100, 100, 100, 100],
                        maxZoom: 15,
                        duration: 1000,
                    });
            }
        });

        // Zoom to extends.
        const extent = this.getCombinedExtent();
        if (!isEmpty(extent)) {
            this.map.getView().fit(extent, {
                padding: [100, 100, 100, 100],
                maxZoom: 15,
            });
        }
    }

    /**
     * Get the combined extent of the map and all its sources.
     */
    getCombinedExtent(): Extent {
        const extent = createEmpty();
        this.map.getLayers().forEach((layer: BaseLayer) => {
            const layerConfig: GeojsonLayerConfig = this.mapConfig.getLayerConfig(layer.get('id'));
            if (layerConfig.fit_extents) {
                if (layer instanceof LayerGroup) {
                    layer.getLayers().forEach((layer: BaseLayer) => {
                        if (layer instanceof VectorLayer) {
                            layer
                                .getSource()
                                .getFeatures()
                                .forEach((feature: Feature) => {
                                    if (!feature.get('hidden')) {
                                        extend(extent, feature.getGeometry().getExtent());
                                    }
                                });
                        }
                    });
                } else if (layer instanceof VectorLayer) {
                    layer
                        .getSource()
                        .getFeatures()
                        .forEach((feature: Feature) => {
                            if (!feature.get('hidden')) {
                                extend(extent, feature.getGeometry().getExtent());
                            }
                        });
                }
            }
        });
        return extent;
    }

    /**
     * Build all the layers.
     */
    protected initLayers(): void {
        const layerConfigs: MapLayerConfig[] = this.mapConfig.getLayers();
        const layer_ids = Object.keys(layerConfigs);

        // Sort layers by weight.
        layer_ids.sort(
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            // @todo Fix typing.
            (a, b) => {
                return layerConfigs[a].weight - layerConfigs[b].weight;
            },
        );

        // Add layers to the map.
        // @todo Find way to work with plugins and variable class names.
        layer_ids.forEach((layer_id: string, index) => {
            const layerConfig: MapLayerConfig = layerConfigs[layer_id];

            const plugin_id = layerConfig.id;
            let layer;
            switch (plugin_id) {
                case 'geojson':
                    layer = new Geojson(this, layer_id);
                    break;
                case 'osm':
                    layer = new OSM(this, layer_id);
                    break;
            }
            if (layer) {
                this.layerPlugins[layer_id] = layer;
                const build = layer.buildLayer();

                if (build) {
                    build.set('id', layer_id);
                    this.map.addLayer(build);
                    this.layers[layer_id] = build;
                }
            }
        });
    }

    /**
     * Get the lazy data per layer.
     *
     * @param layer_id
     */
    public getLazyDataPerLayer(layer_id: string) {
        return this.lazyData[layer_id] ?? undefined;
    }

    /**
     * Get all the layers
     */
    public getLayers() {
        return this.layers;
    }

    /**
     * Get all the data keyed by layer id.
     */
    public getData() {
        return this.data;
    }

    /**
     * Get all the sources keyed by layer id.
     */
    public getSources() {
        return this.sources;
    }
    /**
     * Get all the data for a specific layer id.
     * @param layer_id
     */
    public getDataPerLayer(layer_id: string) {
        return this.data[layer_id];
    }

    /**
     * Gets the map id.
     */
    public getMapId() {
        return this.mapId;
    }

    /**
     * Determines if the map has spatial filters.
     */
    public hasSpatialFilter() {
        return this.spatialFilterEnabled;
    }

    /**
     * Enables the spatial filters.
     */
    public enableSpatialFilter() {
        this.spatialFilterEnabled = true;
    }

    /**
     * Sets the spatial filter source belonging to this openlayers instance.
     */
    public setSpatialFilterSource(source: VectorSource) {
        this.spatialFilterSource = source;
    }

    /**
     * Gets the spatial filter source belonging to this openlayers instance.
     */
    public getSpatialFilterSource(): VectorSource | null {
        return this.spatialFilterSource;
    }

    /**
     * Get a source for a specific layer id.
     */
    public getSourceByLayerId(layer_id: string) {
        return this.sources[layer_id];
    }

    /**
     * Get the Openlayers Map object.
     */
    public getMap() {
        return this.map;
    }

    /**
     * Get all features in the map.
     */
    protected getAllFeatures = (): Feature[] => {
        const features: Feature[] = [];
        // Gather all features in all sources.
        for (const id in this.sources) {
            this.sources[id].getFeatures().forEach((feature: Feature) => {
                features.push(feature);
            });
        }
        return features;
    };

    /**
     * Explode features that might be inside a cluster feature.
     */
    protected explodeClusterFeatures(features: FeatureLike[]): FeatureLike[] {
        for (const delta in features) {
            if (features[delta].get('features') != undefined) {
                const clusterFeatures = features[delta].get('features');
                features.splice(delta, 1);
                for (const clusterDelta in clusterFeatures) {
                    features.push(clusterFeatures[clusterDelta]);
                }
            }
        }
        return features;
    }

    /**
     * Add click interactions to the map.
     */
    addClickInteractions(): void {
        this.map.on('click', (e) => {
            if (this.map.get('drawing')) {
                return;
            }
            // Reset the tooltip container.
            if (this.map.get('feature_select_container')) {
                this.featureSelectContainer.classList.remove('active');
                this.tooltipElement.innerHTML = '';
                this.map.set('feature_select_container', false);
                return;
            }

            // empty the layer select container.
            this.featureSelectContainer.innerHTML = '';

            // Only continue if features were clicked.
            const clickedFeatures = this.map.getFeaturesAtPixel(e.pixel);

            if (clickedFeatures.length === 0) {
                return;
            }

            this.explodeClusterFeatures(clickedFeatures);

            for (const delta in clickedFeatures) {
                const feature = clickedFeatures[delta];
                if (!feature.get('_ajax_callback_url')) {
                    clickedFeatures.splice(delta, 1);
                }
            }

            if (clickedFeatures.length === 1) {
                const currentFeature = clickedFeatures[0];
                this.executeAjaxCallback(currentFeature);
            } else if (clickedFeatures.length > 1) {
                const selectContainerElement = document.createElement('div');
                selectContainerElement.classList.add('inner');

                for (const delta in clickedFeatures) {
                    const tooltip = this.getTooltip(clickedFeatures[delta]);
                    if (tooltip) {
                        selectContainerElement.appendChild(tooltip);
                    }
                }
                this.featureSelectContainer.appendChild(selectContainerElement);

                this.tooltipElement.innerHTML = '';
                const tooltipLocator = document.createElement('div');
                tooltipLocator.classList.add('tooltip-locator');
                const coordinate = e.coordinate;
                this.map.getOverlayById('tooltip').setPosition(coordinate);
                this.tooltipElement.appendChild(tooltipLocator);
                this.featureSelectContainer.classList.add('active');
                this.map.set('feature_select_container', true);
            }
        });
    }

    /**
     * Execute the Ajax callback.
     */
    executeAjaxCallback(currentFeature: FeatureLike): void {
        this.featureSelectContainer.classList.remove('active');
        this.featureSelectContainer.innerHTML = '';
        this.tooltipElement.innerHTML = '';
        this.map.set('feature_select_container', false);

        if (currentFeature.get('features')?.length == 1) {
            currentFeature = currentFeature.get('features')[0];
        }
        if (currentFeature.get('features')?.length > 1) {
            return;
        }
        if (!currentFeature.get('_ajax_callback_url')) {
            return;
        }

        // Add throbber. The throbber will be removed after finishing the request.
        const body = document.querySelector('body');
        if (!body) {
            throw 'No body?';
        }

        body.insertAdjacentHTML('afterEnd', Drupal.theme.ajaxProgressIndicatorFullscreen());

        // @todo Find out how we can decouple the Drupal ajax call from this Openlayers class.
        Drupal.ajax({ url: currentFeature.get('_ajax_callback_url') })
            .execute()
            .done(function (commands, statusString, ajaxObject) {
                console.log('click callback executed.');
            });
    }

    /**
     * Create a tooltip.
     */
    getTooltip(currentFeature: FeatureLike): Element {
        const tooltip = document.createElement('div');
        tooltip.classList.add('tooltip');
        tooltip.dataset.feature = currentFeature.get('id');
        tooltip.addEventListener('mouseover', this.hoverTooltip);
        tooltip.addEventListener('click', this.clickTooltip);
        const layer_id = currentFeature.get('_layer_id');
        if (!layer_id) {
            return;
        }
        const layerConfig: GeojsonLayerConfig = this.mapConfig.getLayerConfig(layer_id);
        const tooltip_property = layerConfig.hover?.tooltip;

        if (!tooltip_property) {
            return;
        }
        tooltip.innerHTML = currentFeature.get(tooltip_property);

        // The tooltip is a dom element object and doesn't have
        // custom properties. This works but should be
        // solved otherwise.
        // @todo Somehow assign the current feature to the dom element.
        tooltip.currentFeature = currentFeature;

        // Set the tooltip property on the feature for future reference.
        currentFeature.set('_tooltip', tooltip.innerHTML);
        return tooltip;
    }

    /**
     * Get the layer config belonging to a specific feature.
     * @param feature
     * @protected
     */
    protected getLayerConfig(feature: FeatureLike) {
        const layer_id = feature.get('_layer_id');
        if (!layer_id) {
            return;
        }
        const layerConfig: GeojsonLayerConfig = this.mapConfig.getLayerConfig(layer_id);
        if (!layerConfig) {
            return;
        }
        return layerConfig;
    }

    /**
     * Get the hover property of a specific feature.
     *
     * @param feature
     * @protected
     */
    protected getHoverProperty(feature: FeatureLike) {
        return this.getLayerConfig(feature)?.hover.property;
    }

    /**
     * Get the hover match property of a specific feature.
     *
     * @param feature
     * @protected
     */
    protected getHoverMatchProperty(feature: FeatureLike) {
        return this.getLayerConfig(feature)?.hover.property_match;
    }

    /**
     * Add hover interactions to the map.
     */
    addHoverInteractions() {
        this.map.on('pointermove', (e) => {
            if (this.map.get('drawing')) {
                return;
            }
            // Reset the cursor.
            this.map.getViewport().style.cursor = '';

            // Don't react to dragging.
            if (e.dragging) return;

            // Don't show hover interactions when the
            // feature select container is active.
            if (this.map.get('feature_select_container') === true) {
                return;
            }

            // Reset the tooltip.
            this.tooltipElement.innerHTML = '';

            const coordinate = e.coordinate;
            this.map.getOverlayById('tooltip').setPosition(coordinate);

            const featuresToAlter = this.getAllFeatures();

            this.map.forEachFeatureAtPixel(e.pixel, (currentFeature: FeatureLike) => {
                // Only apply properties to geometry features.
                // If the currentFeature is a clusterFeature containing one feature,
                // only address the parent feature.
                if (currentFeature.get('features')?.length == 1) {
                    currentFeature = currentFeature.get('features')[0];
                }
                if (currentFeature.get('features')?.length > 1) {
                    return;
                }

                const hover_property = this.getHoverProperty(currentFeature);
                const hover_property_match = this.getHoverMatchProperty(currentFeature);

                if (!hover_property) {
                    return;
                }

                currentFeature.set(hover_property, true, true);

                // Apply the hover property to all matching features
                // based on the property_match setting.
                for (const delta in featuresToAlter) {
                    const feature = featuresToAlter[delta];
                    if (currentFeature.getId() == feature.getId()) {
                        if (feature.get(hover_property) !== true) {
                            featuresToAlter[delta].set(hover_property, true, true);
                        }
                        featuresToAlter.splice(delta, 1);
                    } else if (currentFeature.get(hover_property_match) === feature.get(hover_property_match)) {
                        if (feature.get(hover_property) !== true) {
                            featuresToAlter[delta].set(hover_property, true, true);
                        }
                        featuresToAlter.splice(delta, 1);
                    }
                }
            });

            // Unset the hover property. Only update those features that have been changed.
            featuresToAlter.forEach((feature) => {
                const hover_property = this.getHoverProperty(feature);
                if (feature.get(hover_property) === true) {
                    feature.set(hover_property, false, true);
                }
            });

            // Add tooltips:
            const hoverFeatures = this.map.getFeaturesAtPixel(e.pixel);
            this.explodeClusterFeatures(hoverFeatures);

            // Remove hovered features that are created when using draw tools by
            // checking if they have a hover property.

            let hovereableFeatures = [];

            for (const delta in hoverFeatures) {
                if (this.getHoverProperty(hoverFeatures[delta])) {
                    hovereableFeatures.push(hoverFeatures[delta]);
                }
            }

            // Only show one tooltip.
            if (hovereableFeatures.length > 0) {
                this.map.getViewport().style.cursor = 'pointer';
                const tooltip = this.getTooltip(hovereableFeatures[0]);
                if (tooltip) {
                    this.tooltipElement.appendChild(tooltip);
                }
            }

            // If there are more layers, add a '+X' element.
            if (hovereableFeatures.length > 1) {
                const tooltipCountElement = document.createElement('div');
                tooltipCountElement.classList.add('tooltip-count');
                tooltipCountElement.innerHTML = '+' + (hovereableFeatures.length - 1);
                this.tooltipElement.appendChild(tooltipCountElement);
            }
        });
    }

    /**
     * Execute callback on a tooltip.
     */
    clickTooltip = (e: PointerEvent): void => {
        const currentFeature = e.currentTarget?.currentFeature;
        if (!currentFeature) {
            return;
        }
        this.executeAjaxCallback(currentFeature);
        e.preventDefault();
        return;
    };

    /**
     * Hover on a tooltip.
     */
    hoverTooltip = (e: MouseEvent): void => {
        const currentFeature = e.currentTarget?.currentFeature;
        if (!currentFeature) {
            return;
        }
        const layer_id = currentFeature.get('_layer_id');
        const layerConfig: GeojsonLayerConfig = this.mapConfig.getLayerConfig(layer_id);
        const hover_property = layerConfig.hover.property;
        const hover_property_match = layerConfig.hover.property_match;

        // Unset the hover property.
        this.getAllFeatures().forEach((feature) => {
            feature.set(hover_property, null, true);
        });
        // Apply the hover property to all matching features
        // based on the property_match setting.
        this.getAllFeatures().forEach((feature: Feature) => {
            if (
                feature.get(hover_property_match) !== undefined &&
                currentFeature.get(hover_property_match) !== undefined &&
                feature.get(hover_property_match) === currentFeature.get(hover_property_match)
            ) {
                feature.set(hover_property, true, true);
            }
        });
        this.sources[layer_id].dispatchEvent('change');
    };
}
