<?php

namespace Drupal\openlayers\Plugin\views\filter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\openlayers\Plugin\views\style\Openlayers;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Spatial filter.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("openlayers_spatial")
 */
class SpatialFilter extends FilterPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The geophp wrapper.
   *
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geophp;

  /**
   * {@inheritdoc}
   */
  protected $alwaysMultiple = TRUE;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('geofield.geophp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GeoPHPInterface $geophp) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->geophp = $geophp;
  }

  /**
   * {@inheritdoc}
   */
  public function operatorOptions() {
    return [
      'ST_Contains' => $this->t('ST Contains'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return '@todo: summary';
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if (!$this->value) {
      return;
    }

    $geom = $this->geophp->load($this->value);
    if (!$geom || $geom->geometryType() != 'Polygon') {
      $this->messenger()->addWarning($this->t('Currently only a single Polygon is supported.'));
      return;
    }

    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $this->ensureMyTable();
    $query->addWhereExpression('gs', $this->tableAlias . '.entity_id IN (SELECT DISTINCT entity_id FROM openlayers_geofield_sync WHERE ST_INTERSECTS(ST_GeomFromText(:geometry), geometry))', [
      ':geometry' => $geom->out('wkt'),
    ]);

  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function valueForm(&$form, FormStateInterface $form_state) {
    $spatial_filter_layer = $this->view->style_plugin->options['spatial_filter_layer'];
    $form['value'] = [
      '#type' => 'openlayers_spatial_select',
      '#default_value' => $this->value,
      '#view_id' => Openlayers::getUniqueMapCssId($this->view),
      '#spatial_filter_layer' => $spatial_filter_layer,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function spatialFilterEnabled(ViewExecutable $view) {
    foreach ($view->filter as $id => $filter) {
      if ($filter instanceof SpatialFilter) {
        if (isset($view->getExposedInput()[$id])) {
          return TRUE;
        }
        break;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getStFilterData(ViewExecutable $view) {
    $exposed_st_filter_data = ['type' => 'FeatureCollection', 'features' => []];
    foreach ($view->filter as $id => $filter) {
      if ($filter instanceof SpatialFilter) {
        if (isset($view->getExposedInput()[$id]) && !empty($view->getExposedInput()[$id])) {
          try {
            $exposed_st_filter_data = Json::decode($view->getExposedInput()[$id]);
            break;
          }
          catch (\Exception $e) {
            $exposed_st_filter_data = [
              'type' => 'FeatureCollection',
              'features' => [],
            ];
            break;
          }
        }
      }
    }
    return $exposed_st_filter_data;
  }

}
