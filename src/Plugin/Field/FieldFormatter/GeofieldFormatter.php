<?php

namespace Drupal\openlayers\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\openlayers\Entity\MapConfig;
use Drupal\openlayers\OpenLayersGeoJsonInterface;
use Drupal\openlayers\Plugin\Field\OpenlayersFieldTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'openlayers_geofield_widget' field widget.
 *
 * @FieldFormatter (
 *   id = "openlayers_geofield_formatter",
 *   label = @Translation("Openlayers"),
 *   field_types = {"geofield"},
 * )
 */
class GeofieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  use OpenlayersFieldTrait;

  /**
   * The geojson service.
   *
   * @var \Drupal\openlayers\OpenLayersGeoJsonInterface
   */
  protected $geojson;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, OpenLayersGeoJsonInterface $geojson, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->geojson = $geojson;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('openlayers.geojson'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + OpenlayersFieldTrait::sharedDefaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return $this->sharedSettingsSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form += $this->sharedSettingsForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if ($items->isEmpty()) {
      return [];
    }
    $map_config = MapConfig::load($this->getSetting('map_config'));
    $data_layer = $this->getSetting('data_layer');
    $features = $this->geojson->convertFieldItemsToGeoJson($items);
    $map_id = 'ol-formatter-' . $items->getName() . '-' . $items->getEntity()->uuid();
    if ($features['features']) {
      foreach ($features['features'] as &$feature) {
        $feature['properties'] = [
          '_map_id' => $map_id,
          '_layer_id' => $this->getSetting('data_layer'),
        ];
      }
    }
    $elements[0] = [
      'map' => [
        '#type' => 'openlayers_map',
        '#map_id' => $map_id,
        '#map_config' => $map_config,
        '#data' => [
          $data_layer => $features,
        ],
        '#provider' => 'field_formatter',
        '#width' => $this->getSetting('width'),
        '#height' => $this->getSetting('height'),
      ],
    ];
    return $elements;
  }

}
