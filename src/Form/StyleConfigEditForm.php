<?php

namespace Drupal\openlayers\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\openlayers\StyleRuleInterface;

/**
 * The edit form controller for style config entities.
 *
 * @internal
 */
class StyleConfigEditForm extends StyleConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['#style'] = $this->entity;
    $form['#title'] = $this->t('Edit style %name', ['%name' => $this->entity->label()]);
    $form['#tree'] = TRUE;

    if ($trigger = $form_state->getTriggeringElement()) {

      if ($trigger['#op'] == 'add_style_rule') {
        $this->entity->addStyleRule();
      }
      elseif ($trigger['#op'] == 'remove_style_rule') {
        $styleRule = $this->entity->getStyleRule($trigger['#uuid']);
        $this->entity->removeStyleRule($styleRule);
      }
      elseif ($trigger['#op'] == 'add_conditions_feature') {
        $this->entity->addConditionFeature($trigger['#style_rule']);
      }
      elseif ($trigger['#op'] == 'remove_conditions_feature') {
        $this->entity->removeConditionFeature($trigger['#style_rule'], $trigger['#delta']);
      }
      else {
        $form_state->cleanValues();
        $this->entity = $this->buildEntity($form, $form_state);
      }
    }
    $form['#attached']['library'][] = 'openlayers/admin';

    $form['style_rule_help'] = [
      '#weight' => 4,
      '#type' => 'markup',
      '#markup' => $this->t('Style rules are applied in order. If the rule does not return a style for a specific feature, it will try and apply the next one.'),
    ];

    // Build the list of existing layers for this style.
    $form['style_rules'] = [
      '#prefix' => '<div id="style-rules-wrapper">',
      '#suffix' => '</div>',
      '#type' => 'table',
      '#header' => [
        '',
        '',
        $this->t('Style rules'),
        $this->t('Condition'),
        $this->t('Styles'),
        '',
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'style-rules-order-weight',
        ],
      ],
      '#empty' => $this->t('Please add a style rule.'),
      '#weight' => 5,
    ];
    foreach ($this->entity->getStyleRules() as $styleRule) {
      $key = $styleRule->getUuid();
      $form['style_rules'][$key] = [
        '#attributes' => ['class' => ['draggable']],
        '#weight' => $styleRule->getWeight(),
        'handler' => [
          '#type' => 'markup',
          '#markup' => '',
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => $styleRule->label()]),
          '#title_display' => 'invisible',
          '#default_value' => $styleRule->getWeight(),
          '#attributes' => [
            'class' => ['style-rules-order-weight'],
          ],
        ],
        'label' => [
          '#title' => 'Label',
          '#type' => 'textfield',
          '#required' => TRUE,
          '#default_value' => $styleRule->label(),
        ],
        'conditions' => $this->getConditionsElement($styleRule),
        'style_definition' => $this->getStylesElement($styleRule),
        'remove' => [
          '#type' => 'button',
          '#op' => 'remove_style_rule',
          '#uuid' => $key,
          '#value' => $this->t('Remove'),
          '#limit_validation_errors' => [],
          '#ajax' => [
            'callback' => [get_called_class(), 'refreshStyleRules'],
            'wrapper' => 'style-rules-wrapper',
          ],
        ],
      ];
    }

    $form['add_style_rule'] = [
      '#type' => 'button',
      '#value' => $this->t('Add style rule'),
      '#op' => 'add_style_rule',
      '#weight' => 6,
      '#ajax' => [
        'callback' => [get_called_class(), 'refreshStyleRules'],
        'wrapper' => 'style-rules-wrapper',
      ],
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Element for styles belonging to this style rule.
   *
   * @param \Drupal\openlayers\StyleRuleInterface $style_rule
   *   The style rule.
   *
   * @return array
   *   Form render array of the element.
   */
  protected function getStylesElement(StyleRuleInterface $style_rule) {
    $style_definition = $style_rule->getStyleDefinition();
    return [
      'hidden' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Hide feature'),
        '#default_value' => $style_definition['hidden'] ?? FALSE,
      ],
      'image' => $this->imageWrapperElement($style_definition['image'], 'image-wrapper' . $style_rule->getUuid()) +
        [
          '#states' => [
            'visible' => [
              ':input[name="style_rules[' . $style_rule->getUuid() . '][style_definition][hidden]"]' => ['checked' => FALSE],
            ],
          ],
        ],
      'stroke' => $this->strokeElement($style_definition['stroke']) +
        [
          '#states' => [
            'visible' => [
              ':input[name="style_rules[' . $style_rule->getUuid() . '][style_definition][hidden]"]' => ['checked' => FALSE],
            ],
          ],
        ],
      'fill' => $this->fillElement($style_definition['fill']) +
        [
          '#states' => [
            'visible' => [
              ':input[name="style_rules[' . $style_rule->getUuid() . '][style_definition][hidden]"]' => ['checked' => FALSE],
            ],
          ],
        ],
      'cluster' => $this->clusterElement($style_definition['cluster'], 'cluster-wrapper' . $style_rule->getUuid()) +
        [
          '#states' => [
            'visible' => [
              ':input[name="style_rules[' . $style_rule->getUuid() . '][style_definition][hidden]"]' => ['checked' => FALSE],
            ],
          ],
        ],
    ];
  }

  /**
   * Element for cluster styles.
   *
   * @param array $default_values
   *   The default values.
   * @param string $style_rule_uuid
   *   The style rule id.
   *
   * @return array
   *   The render array.
   */
  protected function clusterElement(array $default_values, string $style_rule_uuid) {
    $element = [
      '#type' => 'details',
      '#title' => $this->t('Cluster'),
      'image' => $this->imageWrapperElement($default_values['image'], $style_rule_uuid),
      'text' => $this->textElement($default_values['text']),
    ];
    $element['image']['data']['radius']['#description'] = $this->t('Enter a value or a mathematical formula. Placeholders @cluster_size and @zoom are available.');
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // We use a wrapper in the form tree that needs to be excluded from the
    // config tree.
    foreach ($values['style_rules'] as $uuid => $style_rule) {
      $features = is_array($values['style_rules'][$uuid]['conditions']['features']['features']) ? $values['style_rules'][$uuid]['conditions']['features']['features'] : [];
      $values['style_rules'][$uuid]['conditions']['features'] = $features;
    }
    foreach ($values as $key => $value) {
      $entity->set($key, $value);
    }
  }

  /**
   * Form elements for text styling.
   *
   * @param array $default_values
   *   The default values.
   *
   * @return array
   *   The form element array.
   */
  protected function textElement(array $default_values) {
    return [
      '#type' => 'details',
      '#title' => $this->t('Text'),
      'font' => [
        '#type' => 'textfield',
        '#title' => $this->t('Font'),
        '#default_value' => $default_values['font'],
        '#description' => $this->t("Font style as CSS 'font' value, see: @link. Default is '10px sans-serif'", [
          '@link' => Link::fromTextAndUrl('https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/font', Url::fromUri('https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/font', ['target' => '_blank']))
            ->toString(),
        ]),
      ],
      'fill' => $this->fillElement($default_values['fill']),
      'stroke' => $this->strokeElement($default_values['stroke']),
    ];
  }

  /**
   * The form element for styling an openlayers image.
   *
   * @param array $default_values
   *   Default values.
   * @param string $unique_element_id
   *   A unique ID for ajax purposes.
   *
   * @return array
   *   The form element.
   */
  protected function imageWrapperElement(array $default_values, string $unique_element_id) {
    $element = [
      '#type' => 'details',
      '#title' => $this->t('Image'),
      '#description' => $this->t('Image or shape to be used for points'),
      'image_type' => [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#empty_option' => $this->t('- Select -'),
        '#options' => [
          'icon' => $this->t('Icon'),
          'circle' => $this->t('Circle'),
          'regular_shape' => $this->t('Regular shape'),
        ],
        '#default_value' => $default_values['image_type'] ?? NULL,
        '#ajax' => [
          'callback' => [get_called_class(), 'refreshImageDataElement'],
          'wrapper' => $unique_element_id . '-image-data',
        ],
      ],
      'data' => $this->imageDataElement($default_values['image_type'] ?? '', $default_values['data'] ?? []) +
        [
          '#prefix' => "<div id=\"$unique_element_id-image-data\">",
          '#suffix' => '</div>',
        ],
    ];
    return $element;
  }

  /**
   * Ajax callback to remove a feature based condition.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array|mixed|null
   *   The element to replace.
   */
  public static function removeConditionFeature(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#array_parents'];
    array_pop($parents);
    array_pop($parents);
    $element = NestedArray::getValue($form, $parents);
    return $element;
  }

  /**
   * Ajax callback to add a feature based condition.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array|mixed|null
   *   The element to replace.
   */
  public static function addConditionFeature(array &$form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    $parents[] = 'features';
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Ajax callback to update an image element.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array|mixed|null
   *   The element to replace.
   */
  public static function refreshImageDataElement(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    $parents[] = 'data';
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Image data form element.
   *
   * @param string|null $type
   *   Icon or cirlce.
   * @param array $default_values
   *   The default values.
   *
   * @return array
   *   The form element.
   */
  protected function imageDataElement(string $type, array $default_values) {
    if ($type) {
      switch ($type) {
        case 'icon':
          return $this->iconElement($default_values);

        case 'circle';
          return $this->circleElement($default_values);

        case 'regular_shape':
          return [
            '#type' => 'markup',
            '#markup' => '@todo not yet implemented',
          ];
      }
    }
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Please select an image type.'),
    ];
  }

  /**
   * Circle element for default values.
   */
  protected function circleElement($default_values) {
    return [
      '#type' => 'details',
      '#title' => $this->t('Circle'),
      'radius' => [
        '#type' => 'textfield',
        '#title' => $this->t('Radius'),
        '#description' => $this->t('Enter a value or a mathematical formula. Placeholders @zoom is available.'),
        '#default_value' => $default_values['radius'] ?? '',
      ],
      'fill' => $this->fillElement($default_values['fill'] ?? []),
      'stroke' => $this->strokeElement($default_values['stroke'] ?? []),
    ] + $this->imageBaseElement($default_values);
  }

  /**
   * Base element vor image type style elements.
   *
   * @param array $default_values
   *   The default values.
   *
   * @return array
   *   The form element.
   */
  protected function imageBaseElement(array $default_values) {
    return [
      'opacity' => [
        '#type' => 'number',
        '#title' => $this->t('Opacity'),
        '#min' => 0,
        '#max' => 1,
        '#step' => 0.01,
        '#default_value' => $default_values['opacity'],
      ],
    ];
  }

  /**
   * The icon element.
   *
   * @param array $default_values
   *   The default values.
   *
   * @return array
   *   The form element.
   */
  protected function iconElement(array $default_values) {
    return [
      '#type' => 'details',
      '#title' => $this->t('Icon'),
      'source' => [
        '#type' => 'textfield',
        '#title' => $this->t('URI'),
        '#description' => $this->t('You can use module:// or theme:// stream wrappers.'),
        '#default_value' => $default_values['source'],
      ],
      'anchor' => [
        '#type' => 'container',
        '#title' => $this->t('Anchor'),
        'horizontal' => [
          '#title' => $this->t('Horizontal anchor'),
          '#description' => $this->t('0 is left. 1 is right'),
          '#type' => 'number',
          '#min' => 0,
          '#max' => 1,
          '#step' => 0.01,
          '#default_value' => $default_values['anchor']['horizontal'],
        ],
        'vertical' => [
          '#type' => 'number',
          '#title' => $this->t('Vertical anchor'),
          '#description' => $this->t('0 is top. 1 is bottom'),
          '#min' => 0,
          '#max' => 1,
          '#step' => 0.01,
          '#default_value' => $default_values['anchor']['vertical'],
        ],
      ],
    ] + $this->imageBaseElement($default_values);
  }

  /**
   * The fill element.
   *
   * @param array $default_values
   *   The default values.
   *
   * @return array
   *   The element for 'Fill'.
   */
  protected function fillElement(array $default_values) {
    return [
      '#type' => 'details',
      '#title' => $this->t('Fill'),
      'color' => $this->colorElement($default_values['color'] ?? ''),
    ];
  }

  /**
   * The stroke element.
   *
   * @param array $default_values
   *   The default values.
   *
   * @return array
   *   The form element.
   */
  protected function strokeElement(array $default_values) {
    return [
      '#type' => 'details',
      '#title' => $this->t('Stroke'),
      'color' => $this->colorElement($default_values['color'] ?? ''),
      'line_cap' => [
        '#type' => 'select',
        '#title' => $this->t('Line cap'),
        '#options' => [
          'butt' => $this->t('Butt'),
          'round' => $this->t('Round'),
          'square' => $this->t('Square'),
        ],
        '#default_value' => $default_values['line_cap'],
      ],
      'line_join' => [
        '#type' => 'select',
        '#title' => $this->t('Line join'),
        '#options' => [
          'bevel' => $this->t('Bevel'),
          'round' => $this->t('Round'),
          'miter' => $this->t('Miter'),
        ],
        '#default_value' => $default_values['line_join'],
      ],
      'line_dash' => [
        '#type' => 'textfield',
        '#title' => $this->t('Line dash'),
        '#description' => $this->t('Comma separated list of numbers.'),
        '#default_value' => $default_values['line_dash'],
        '#size' => 10,
      ],
      'width' => [
        '#type' => 'number',
        '#title' => $this->t('Width'),
        '#suffix' => $this->t('px'),
        '#step' => '.01',
        '#min' => 0,
        '#max' => 99,
        '#default_value' => $default_values['width'],
      ],
    ];
  }

  /**
   * The color element.
   *
   * @param string $default_value
   *   The default elements.
   *
   * @return array
   *   The form element.
   */
  protected function colorElement(string $default_value) {
    return [
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => $this->t('Color'),
      '#default_value' => $default_value,
    ];
  }

  /**
   * The feature conditions element.
   *
   * @param array $default_values
   *   The feature conditions element.
   * @param \Drupal\openlayers\StyleRuleInterface $styleRule
   *   The style rule.
   *
   * @return array
   *   The form element.
   */
  protected function getFeatureConditionsElement(array $default_values, StyleRuleInterface $styleRule) {
    $wrapper = 'features-conditions-' . $styleRule->getUuid();
    $rows = [];
    $element = [
      '#type' => 'details',
      '#title' => $this->t('Features'),
      'features' => [
        '#type' => 'table',
        '#title' => $this->t('Features'),
        '#prefix' => '<div id="' . $wrapper . '">',
        '#suffix' => '</div>',
        '#header' => [
          $this->t('Property'),
          $this->t('Value'),
          $this->t('Operator'),
          '',
        ],
      ],
    ];
    $features = $default_values ?? [];
    foreach ($features as $delta => $feature) {
      $rows[] = [
        'property' => [
          '#title' => $this->t('Property'),
          '#type' => 'textfield',
          '#size' => 15,
          '#default_value' => $feature['property'],
        ],
        'value' => [
          '#title' => $this->t('Value'),
          '#type' => 'textfield',
          '#size' => 15,
          '#default_value' => $feature['value'],
        ],
        'operator' => [
          '#title' => $this->t('Operator'),
          '#type' => 'select',
          '#options' => [
            'equals' => $this->t('Equals'),
            'not_equals' => $this->t('Not equals'),
            'is_true' => $this->t('Is true'),
            'is_false' => $this->t('Is false'),
            'is_buffer' => $this->t('Is buffer'),
            'regex' => $this->t('Regex match'),
          ],
          '#default_value' => $feature['operator'],
        ],
        'remove' => [
          '#type' => 'button',
          '#value' => $this->t('remove'),
          '#op' => 'remove_conditions_feature',
          '#limit_validation_errors' => [],
          '#name' => 'remove_conditions_feature_' . $delta . $styleRule->getUuid(),
          '#delta' => $delta,
          '#style_rule' => $styleRule->getUuid(),
          '#ajax' => [
            'callback' => [get_called_class(), 'removeConditionFeature'],
            'wrapper' => $wrapper,
          ],
        ],
      ];
    }
    $element['features'] += $rows;
    $element['_add_row'] = [
      '#value' => $this->t('Add feature'),
      '#type' => 'button',
      '#limit_validation_errors' => [],
      '#op' => 'add_conditions_feature',
      '#name' => 'add_conditions_feature_' . $styleRule->getUuid(),
      '#style_rule' => $styleRule->getUuid(),
      '#ajax' => [
        'callback' => [get_called_class(), 'addConditionFeature'],
        'wrapper' => $wrapper,
      ],
    ];
    return $element;
  }

  /**
   * The style rules conditions element.
   *
   * @param \Drupal\openlayers\StyleRuleInterface $style_rule
   *   The style rule.
   *
   * @return array
   *   The form element.
   */
  protected function getConditionsElement(StyleRuleInterface $style_rule) {
    $default_values = $style_rule->getConditions();
    return [
      '#tree' => TRUE,
      'features' => $this->getFeatureConditionsElement($default_values['features'] ?? [], $style_rule),
      'view' => [
        '#type' => 'details',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => $this->t('View condition'),
        '#description' => $this->t('Map view based conditions.'),
        'zoom' => [
          '#type' => 'details',
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#title' => $this->t('Zoom'),
          'min' => [
            '#title' => $this->t('Min'),
            '#type' => 'number',
            '#min' => 1,
            '#max' => 30,
            '#default_value' => $default_values['view']['zoom']['min'],
          ],
          'max' => [
            '#title' => $this->t('Max'),
            '#type' => 'number',
            '#min' => 1,
            '#max' => 30,
            '#default_value' => $default_values['view']['zoom']['max'],
          ],
        ],
      ],
    ];
  }

  /**
   * Ajax callback for refreshing style rules.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The style rules form element.
   */
  public static function refreshStyleRules(array &$form, FormStateInterface $form_state) {
    return $form['style_rules'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()
      ->addStatus($this->t('Changes to the style have been saved.'));
  }

}
