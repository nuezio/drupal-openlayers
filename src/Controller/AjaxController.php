<?php

namespace Drupal\openlayers\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\openlayers\MapEvent;
use Drupal\openlayers\OpenLayersEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Returns responses for openlayers routes.
 */
class AjaxController extends ControllerBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The controller constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher')
    );
  }

  /**
   * Access callback for interacting with the map using Drupal ajax.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity we interact with.
   * @param string $event
   *   The event type. Defaults to 'click'.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(ContentEntityInterface $entity, string $event, AccountInterface $account) {
    return $entity->access('view', $account, TRUE);
  }

  /**
   * Builds the response.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param string $event
   *   The openlayers map event to interact to.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Return an ajax response.
   */
  public function build(ContentEntityInterface $entity, string $event) {
    $response = new AjaxResponse();
    $event = new MapEvent($response, $entity, $event);
    $this->eventDispatcher->dispatch(OpenLayersEvents::MAP_EVENT, $event);
    if (empty($event->getResponse()->getCommands())) {
      $response->addCommand(new AlertCommand($this->t('Add an event subscriber for the "openlayers.map_event" that adds Ajax Commands to the ajax response.')));
    }
    $response->addCommand(new RemoveCommand('.ajax-progress'));
    return $response;
  }

}
