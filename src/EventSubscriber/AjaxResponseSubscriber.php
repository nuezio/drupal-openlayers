<?php

namespace Drupal\openlayers\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\openlayers\Ajax\UpdateOpenlayersMapCommand;
use Drupal\openlayers\OpenLayersGeoJsonInterface;
use Drupal\openlayers\Plugin\views\filter\SpatialFilter;
use Drupal\openlayers\Plugin\views\style\Openlayers;
use Drupal\views\Ajax\ViewAjaxResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber that interacts with ajax responses related to openlayers.
 */
class AjaxResponseSubscriber implements EventSubscriberInterface {

  /**
   * The openlayers geojson service.
   *
   * @var \Drupal\openlayers\OpenLayersGeoJsonInterface
   */
  protected $geojson;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The class constructor.
   *
   * @param \Drupal\openlayers\OpenLayersGeoJsonInterface $openlayers_geojson
   *   The open layers geojson service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(OpenLayersGeoJsonInterface $openlayers_geojson, Renderer $renderer) {
    $this->geojson = $openlayers_geojson;
    $this->renderer = $renderer;
  }

  /**
   * Kernel response event handler.
   *
   * Intercept the ajax response after updating the exposed filters and
   * alter the features for the openlayers map directly without a page
   * reload.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event.
   *
   * @return \Drupal\views\Ajax\ViewAjaxResponse|\Symfony\Component\HttpFoundation\Response|null
   *   The ajax response if any.
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();
    if (!$response instanceof ViewAjaxResponse) {
      return NULL;
    }
    $view = $response->getView();
    if (!$view->getStyle() instanceof Openlayers) {
      return NULL;
    }

    $commands = &$response->getCommands();

    // Remove the original 'replace view' command.
    foreach ($commands as $key => $command) {
      if ($command['command'] == 'insert' && strpos($command['selector'], '.js-view-dom-id') === 0) {
        unset($commands[$key]);
      }
      if ($command['command'] == 'viewsScrollTop') {
        unset($commands[$key]);
      }
    }

    /** @var \Drupal\openlayers\Plugin\views\style\Openlayers $style */
    $style = $view->getStyle();
    $data = [];
    foreach ($style->options['layers'] as $source_field => $options) {
      $data[$options['layer']] = $this->renderer->executeInRenderContext(new RenderContext(), function () use ($style, $source_field, $options) {
        return $style->getFeaturesPerField($source_field, $options);
      });
    }

    $filter = Json::encode(SpatialFilter::getStFilterData($view));
    $response->addCommand(new UpdateOpenlayersMapCommand($view, $data, $filter));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse', 9999],
    ];
  }

}
