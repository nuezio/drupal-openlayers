(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __reExport = (target, module, copyDefault, desc) => {
    if (module && typeof module === "object" || typeof module === "function") {
      for (let key of __getOwnPropNames(module))
        if (!__hasOwnProp.call(target, key) && (copyDefault || key !== "default"))
          __defProp(target, key, { get: () => module[key], enumerable: !(desc = __getOwnPropDesc(module, key)) || desc.enumerable });
    }
    return target;
  };
  var __toESM = (module, isNodeMode) => {
    return __reExport(__markAsModule(__defProp(module != null ? __create(__getProtoOf(module)) : {}, "default", !isNodeMode && module && module.__esModule ? { get: () => module.default, enumerable: true } : { value: module, enumerable: true })), module);
  };

  // node_modules/rbush/rbush.min.js
  var require_rbush_min = __commonJS({
    "node_modules/rbush/rbush.min.js"(exports, module) {
      !function(t, i) {
        typeof exports == "object" && typeof module != "undefined" ? module.exports = i() : typeof define == "function" && define.amd ? define(i) : (t = t || self).RBush = i();
      }(exports, function() {
        "use strict";
        function t(t2, r2, e2, a2, h2) {
          !function t3(n2, r3, e3, a3, h3) {
            for (; a3 > e3; ) {
              if (a3 - e3 > 600) {
                var o2 = a3 - e3 + 1, s2 = r3 - e3 + 1, l2 = Math.log(o2), f2 = 0.5 * Math.exp(2 * l2 / 3), u2 = 0.5 * Math.sqrt(l2 * f2 * (o2 - f2) / o2) * (s2 - o2 / 2 < 0 ? -1 : 1), m2 = Math.max(e3, Math.floor(r3 - s2 * f2 / o2 + u2)), c2 = Math.min(a3, Math.floor(r3 + (o2 - s2) * f2 / o2 + u2));
                t3(n2, r3, m2, c2, h3);
              }
              var p5 = n2[r3], d2 = e3, x = a3;
              for (i(n2, e3, r3), h3(n2[a3], p5) > 0 && i(n2, e3, a3); d2 < x; ) {
                for (i(n2, d2, x), d2++, x--; h3(n2[d2], p5) < 0; )
                  d2++;
                for (; h3(n2[x], p5) > 0; )
                  x--;
              }
              h3(n2[e3], p5) === 0 ? i(n2, e3, x) : i(n2, ++x, a3), x <= r3 && (e3 = x + 1), r3 <= x && (a3 = x - 1);
            }
          }(t2, r2, e2 || 0, a2 || t2.length - 1, h2 || n);
        }
        function i(t2, i2, n2) {
          var r2 = t2[i2];
          t2[i2] = t2[n2], t2[n2] = r2;
        }
        function n(t2, i2) {
          return t2 < i2 ? -1 : t2 > i2 ? 1 : 0;
        }
        var r = function(t2) {
          t2 === void 0 && (t2 = 9), this._maxEntries = Math.max(4, t2), this._minEntries = Math.max(2, Math.ceil(0.4 * this._maxEntries)), this.clear();
        };
        function e(t2, i2, n2) {
          if (!n2)
            return i2.indexOf(t2);
          for (var r2 = 0; r2 < i2.length; r2++)
            if (n2(t2, i2[r2]))
              return r2;
          return -1;
        }
        function a(t2, i2) {
          h(t2, 0, t2.children.length, i2, t2);
        }
        function h(t2, i2, n2, r2, e2) {
          e2 || (e2 = p(null)), e2.minX = 1 / 0, e2.minY = 1 / 0, e2.maxX = -1 / 0, e2.maxY = -1 / 0;
          for (var a2 = i2; a2 < n2; a2++) {
            var h2 = t2.children[a2];
            o(e2, t2.leaf ? r2(h2) : h2);
          }
          return e2;
        }
        function o(t2, i2) {
          return t2.minX = Math.min(t2.minX, i2.minX), t2.minY = Math.min(t2.minY, i2.minY), t2.maxX = Math.max(t2.maxX, i2.maxX), t2.maxY = Math.max(t2.maxY, i2.maxY), t2;
        }
        function s(t2, i2) {
          return t2.minX - i2.minX;
        }
        function l(t2, i2) {
          return t2.minY - i2.minY;
        }
        function f(t2) {
          return (t2.maxX - t2.minX) * (t2.maxY - t2.minY);
        }
        function u(t2) {
          return t2.maxX - t2.minX + (t2.maxY - t2.minY);
        }
        function m(t2, i2) {
          return t2.minX <= i2.minX && t2.minY <= i2.minY && i2.maxX <= t2.maxX && i2.maxY <= t2.maxY;
        }
        function c(t2, i2) {
          return i2.minX <= t2.maxX && i2.minY <= t2.maxY && i2.maxX >= t2.minX && i2.maxY >= t2.minY;
        }
        function p(t2) {
          return { children: t2, height: 1, leaf: true, minX: 1 / 0, minY: 1 / 0, maxX: -1 / 0, maxY: -1 / 0 };
        }
        function d(i2, n2, r2, e2, a2) {
          for (var h2 = [n2, r2]; h2.length; )
            if (!((r2 = h2.pop()) - (n2 = h2.pop()) <= e2)) {
              var o2 = n2 + Math.ceil((r2 - n2) / e2 / 2) * e2;
              t(i2, o2, n2, r2, a2), h2.push(n2, o2, o2, r2);
            }
        }
        return r.prototype.all = function() {
          return this._all(this.data, []);
        }, r.prototype.search = function(t2) {
          var i2 = this.data, n2 = [];
          if (!c(t2, i2))
            return n2;
          for (var r2 = this.toBBox, e2 = []; i2; ) {
            for (var a2 = 0; a2 < i2.children.length; a2++) {
              var h2 = i2.children[a2], o2 = i2.leaf ? r2(h2) : h2;
              c(t2, o2) && (i2.leaf ? n2.push(h2) : m(t2, o2) ? this._all(h2, n2) : e2.push(h2));
            }
            i2 = e2.pop();
          }
          return n2;
        }, r.prototype.collides = function(t2) {
          var i2 = this.data;
          if (!c(t2, i2))
            return false;
          for (var n2 = []; i2; ) {
            for (var r2 = 0; r2 < i2.children.length; r2++) {
              var e2 = i2.children[r2], a2 = i2.leaf ? this.toBBox(e2) : e2;
              if (c(t2, a2)) {
                if (i2.leaf || m(t2, a2))
                  return true;
                n2.push(e2);
              }
            }
            i2 = n2.pop();
          }
          return false;
        }, r.prototype.load = function(t2) {
          if (!t2 || !t2.length)
            return this;
          if (t2.length < this._minEntries) {
            for (var i2 = 0; i2 < t2.length; i2++)
              this.insert(t2[i2]);
            return this;
          }
          var n2 = this._build(t2.slice(), 0, t2.length - 1, 0);
          if (this.data.children.length)
            if (this.data.height === n2.height)
              this._splitRoot(this.data, n2);
            else {
              if (this.data.height < n2.height) {
                var r2 = this.data;
                this.data = n2, n2 = r2;
              }
              this._insert(n2, this.data.height - n2.height - 1, true);
            }
          else
            this.data = n2;
          return this;
        }, r.prototype.insert = function(t2) {
          return t2 && this._insert(t2, this.data.height - 1), this;
        }, r.prototype.clear = function() {
          return this.data = p([]), this;
        }, r.prototype.remove = function(t2, i2) {
          if (!t2)
            return this;
          for (var n2, r2, a2, h2 = this.data, o2 = this.toBBox(t2), s2 = [], l2 = []; h2 || s2.length; ) {
            if (h2 || (h2 = s2.pop(), r2 = s2[s2.length - 1], n2 = l2.pop(), a2 = true), h2.leaf) {
              var f2 = e(t2, h2.children, i2);
              if (f2 !== -1)
                return h2.children.splice(f2, 1), s2.push(h2), this._condense(s2), this;
            }
            a2 || h2.leaf || !m(h2, o2) ? r2 ? (n2++, h2 = r2.children[n2], a2 = false) : h2 = null : (s2.push(h2), l2.push(n2), n2 = 0, r2 = h2, h2 = h2.children[0]);
          }
          return this;
        }, r.prototype.toBBox = function(t2) {
          return t2;
        }, r.prototype.compareMinX = function(t2, i2) {
          return t2.minX - i2.minX;
        }, r.prototype.compareMinY = function(t2, i2) {
          return t2.minY - i2.minY;
        }, r.prototype.toJSON = function() {
          return this.data;
        }, r.prototype.fromJSON = function(t2) {
          return this.data = t2, this;
        }, r.prototype._all = function(t2, i2) {
          for (var n2 = []; t2; )
            t2.leaf ? i2.push.apply(i2, t2.children) : n2.push.apply(n2, t2.children), t2 = n2.pop();
          return i2;
        }, r.prototype._build = function(t2, i2, n2, r2) {
          var e2, h2 = n2 - i2 + 1, o2 = this._maxEntries;
          if (h2 <= o2)
            return a(e2 = p(t2.slice(i2, n2 + 1)), this.toBBox), e2;
          r2 || (r2 = Math.ceil(Math.log(h2) / Math.log(o2)), o2 = Math.ceil(h2 / Math.pow(o2, r2 - 1))), (e2 = p([])).leaf = false, e2.height = r2;
          var s2 = Math.ceil(h2 / o2), l2 = s2 * Math.ceil(Math.sqrt(o2));
          d(t2, i2, n2, l2, this.compareMinX);
          for (var f2 = i2; f2 <= n2; f2 += l2) {
            var u2 = Math.min(f2 + l2 - 1, n2);
            d(t2, f2, u2, s2, this.compareMinY);
            for (var m2 = f2; m2 <= u2; m2 += s2) {
              var c2 = Math.min(m2 + s2 - 1, u2);
              e2.children.push(this._build(t2, m2, c2, r2 - 1));
            }
          }
          return a(e2, this.toBBox), e2;
        }, r.prototype._chooseSubtree = function(t2, i2, n2, r2) {
          for (; r2.push(i2), !i2.leaf && r2.length - 1 !== n2; ) {
            for (var e2 = 1 / 0, a2 = 1 / 0, h2 = void 0, o2 = 0; o2 < i2.children.length; o2++) {
              var s2 = i2.children[o2], l2 = f(s2), u2 = (m2 = t2, c2 = s2, (Math.max(c2.maxX, m2.maxX) - Math.min(c2.minX, m2.minX)) * (Math.max(c2.maxY, m2.maxY) - Math.min(c2.minY, m2.minY)) - l2);
              u2 < a2 ? (a2 = u2, e2 = l2 < e2 ? l2 : e2, h2 = s2) : u2 === a2 && l2 < e2 && (e2 = l2, h2 = s2);
            }
            i2 = h2 || i2.children[0];
          }
          var m2, c2;
          return i2;
        }, r.prototype._insert = function(t2, i2, n2) {
          var r2 = n2 ? t2 : this.toBBox(t2), e2 = [], a2 = this._chooseSubtree(r2, this.data, i2, e2);
          for (a2.children.push(t2), o(a2, r2); i2 >= 0 && e2[i2].children.length > this._maxEntries; )
            this._split(e2, i2), i2--;
          this._adjustParentBBoxes(r2, e2, i2);
        }, r.prototype._split = function(t2, i2) {
          var n2 = t2[i2], r2 = n2.children.length, e2 = this._minEntries;
          this._chooseSplitAxis(n2, e2, r2);
          var h2 = this._chooseSplitIndex(n2, e2, r2), o2 = p(n2.children.splice(h2, n2.children.length - h2));
          o2.height = n2.height, o2.leaf = n2.leaf, a(n2, this.toBBox), a(o2, this.toBBox), i2 ? t2[i2 - 1].children.push(o2) : this._splitRoot(n2, o2);
        }, r.prototype._splitRoot = function(t2, i2) {
          this.data = p([t2, i2]), this.data.height = t2.height + 1, this.data.leaf = false, a(this.data, this.toBBox);
        }, r.prototype._chooseSplitIndex = function(t2, i2, n2) {
          for (var r2, e2, a2, o2, s2, l2, u2, m2 = 1 / 0, c2 = 1 / 0, p5 = i2; p5 <= n2 - i2; p5++) {
            var d2 = h(t2, 0, p5, this.toBBox), x = h(t2, p5, n2, this.toBBox), v = (e2 = d2, a2 = x, o2 = void 0, s2 = void 0, l2 = void 0, u2 = void 0, o2 = Math.max(e2.minX, a2.minX), s2 = Math.max(e2.minY, a2.minY), l2 = Math.min(e2.maxX, a2.maxX), u2 = Math.min(e2.maxY, a2.maxY), Math.max(0, l2 - o2) * Math.max(0, u2 - s2)), M = f(d2) + f(x);
            v < m2 ? (m2 = v, r2 = p5, c2 = M < c2 ? M : c2) : v === m2 && M < c2 && (c2 = M, r2 = p5);
          }
          return r2 || n2 - i2;
        }, r.prototype._chooseSplitAxis = function(t2, i2, n2) {
          var r2 = t2.leaf ? this.compareMinX : s, e2 = t2.leaf ? this.compareMinY : l;
          this._allDistMargin(t2, i2, n2, r2) < this._allDistMargin(t2, i2, n2, e2) && t2.children.sort(r2);
        }, r.prototype._allDistMargin = function(t2, i2, n2, r2) {
          t2.children.sort(r2);
          for (var e2 = this.toBBox, a2 = h(t2, 0, i2, e2), s2 = h(t2, n2 - i2, n2, e2), l2 = u(a2) + u(s2), f2 = i2; f2 < n2 - i2; f2++) {
            var m2 = t2.children[f2];
            o(a2, t2.leaf ? e2(m2) : m2), l2 += u(a2);
          }
          for (var c2 = n2 - i2 - 1; c2 >= i2; c2--) {
            var p5 = t2.children[c2];
            o(s2, t2.leaf ? e2(p5) : p5), l2 += u(s2);
          }
          return l2;
        }, r.prototype._adjustParentBBoxes = function(t2, i2, n2) {
          for (var r2 = n2; r2 >= 0; r2--)
            o(i2[r2], t2);
        }, r.prototype._condense = function(t2) {
          for (var i2 = t2.length - 1, n2 = void 0; i2 >= 0; i2--)
            t2[i2].children.length === 0 ? i2 > 0 ? (n2 = t2[i2 - 1].children).splice(n2.indexOf(t2[i2]), 1) : this.clear() : a(t2[i2], this.toBBox);
        }, r;
      });
    }
  });

  // node_modules/ol/events/Event.js
  var BaseEvent = function() {
    function BaseEvent2(type) {
      this.propagationStopped;
      this.defaultPrevented;
      this.type = type;
      this.target = null;
    }
    BaseEvent2.prototype.preventDefault = function() {
      this.defaultPrevented = true;
    };
    BaseEvent2.prototype.stopPropagation = function() {
      this.propagationStopped = true;
    };
    return BaseEvent2;
  }();
  var Event_default = BaseEvent;

  // node_modules/ol/ObjectEventType.js
  var ObjectEventType_default = {
    PROPERTYCHANGE: "propertychange"
  };

  // node_modules/ol/Disposable.js
  var Disposable = function() {
    function Disposable2() {
      this.disposed = false;
    }
    Disposable2.prototype.dispose = function() {
      if (!this.disposed) {
        this.disposed = true;
        this.disposeInternal();
      }
    };
    Disposable2.prototype.disposeInternal = function() {
    };
    return Disposable2;
  }();
  var Disposable_default = Disposable;

  // node_modules/ol/array.js
  function binarySearch(haystack, needle, opt_comparator) {
    var mid, cmp;
    var comparator = opt_comparator || numberSafeCompareFunction;
    var low = 0;
    var high = haystack.length;
    var found = false;
    while (low < high) {
      mid = low + (high - low >> 1);
      cmp = +comparator(haystack[mid], needle);
      if (cmp < 0) {
        low = mid + 1;
      } else {
        high = mid;
        found = !cmp;
      }
    }
    return found ? low : ~low;
  }
  function numberSafeCompareFunction(a, b) {
    return a > b ? 1 : a < b ? -1 : 0;
  }
  function includes(arr, obj) {
    return arr.indexOf(obj) >= 0;
  }
  function reverseSubArray(arr, begin, end) {
    while (begin < end) {
      var tmp = arr[begin];
      arr[begin] = arr[end];
      arr[end] = tmp;
      ++begin;
      --end;
    }
  }
  function extend(arr, data) {
    var extension = Array.isArray(data) ? data : [data];
    var length = extension.length;
    for (var i = 0; i < length; i++) {
      arr[arr.length] = extension[i];
    }
  }
  function equals(arr1, arr2) {
    var len1 = arr1.length;
    if (len1 !== arr2.length) {
      return false;
    }
    for (var i = 0; i < len1; i++) {
      if (arr1[i] !== arr2[i]) {
        return false;
      }
    }
    return true;
  }

  // node_modules/ol/functions.js
  function TRUE() {
    return true;
  }
  function FALSE() {
    return false;
  }
  function VOID() {
  }
  function memoizeOne(fn) {
    var called = false;
    var lastResult;
    var lastArgs;
    var lastThis;
    return function() {
      var nextArgs = Array.prototype.slice.call(arguments);
      if (!called || this !== lastThis || !equals(nextArgs, lastArgs)) {
        called = true;
        lastThis = this;
        lastArgs = nextArgs;
        lastResult = fn.apply(this, arguments);
      }
      return lastResult;
    };
  }

  // node_modules/ol/obj.js
  var assign = typeof Object.assign === "function" ? Object.assign : function(target, var_sources) {
    if (target === void 0 || target === null) {
      throw new TypeError("Cannot convert undefined or null to object");
    }
    var output = Object(target);
    for (var i = 1, ii = arguments.length; i < ii; ++i) {
      var source = arguments[i];
      if (source !== void 0 && source !== null) {
        for (var key in source) {
          if (source.hasOwnProperty(key)) {
            output[key] = source[key];
          }
        }
      }
    }
    return output;
  };
  function clear(object) {
    for (var property in object) {
      delete object[property];
    }
  }
  var getValues = typeof Object.values === "function" ? Object.values : function(object) {
    var values = [];
    for (var property in object) {
      values.push(object[property]);
    }
    return values;
  };
  function isEmpty(object) {
    var property;
    for (property in object) {
      return false;
    }
    return !property;
  }

  // node_modules/ol/events/Target.js
  var __extends = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Target = function(_super) {
    __extends(Target2, _super);
    function Target2(opt_target) {
      var _this = _super.call(this) || this;
      _this.eventTarget_ = opt_target;
      _this.pendingRemovals_ = null;
      _this.dispatching_ = null;
      _this.listeners_ = null;
      return _this;
    }
    Target2.prototype.addEventListener = function(type, listener) {
      if (!type || !listener) {
        return;
      }
      var listeners = this.listeners_ || (this.listeners_ = {});
      var listenersForType = listeners[type] || (listeners[type] = []);
      if (listenersForType.indexOf(listener) === -1) {
        listenersForType.push(listener);
      }
    };
    Target2.prototype.dispatchEvent = function(event) {
      var isString = typeof event === "string";
      var type = isString ? event : event.type;
      var listeners = this.listeners_ && this.listeners_[type];
      if (!listeners) {
        return;
      }
      var evt = isString ? new Event_default(event) : event;
      if (!evt.target) {
        evt.target = this.eventTarget_ || this;
      }
      var dispatching = this.dispatching_ || (this.dispatching_ = {});
      var pendingRemovals = this.pendingRemovals_ || (this.pendingRemovals_ = {});
      if (!(type in dispatching)) {
        dispatching[type] = 0;
        pendingRemovals[type] = 0;
      }
      ++dispatching[type];
      var propagate;
      for (var i = 0, ii = listeners.length; i < ii; ++i) {
        if ("handleEvent" in listeners[i]) {
          propagate = listeners[i].handleEvent(evt);
        } else {
          propagate = listeners[i].call(this, evt);
        }
        if (propagate === false || evt.propagationStopped) {
          propagate = false;
          break;
        }
      }
      if (--dispatching[type] === 0) {
        var pr = pendingRemovals[type];
        delete pendingRemovals[type];
        while (pr--) {
          this.removeEventListener(type, VOID);
        }
        delete dispatching[type];
      }
      return propagate;
    };
    Target2.prototype.disposeInternal = function() {
      this.listeners_ && clear(this.listeners_);
    };
    Target2.prototype.getListeners = function(type) {
      return this.listeners_ && this.listeners_[type] || void 0;
    };
    Target2.prototype.hasListener = function(opt_type) {
      if (!this.listeners_) {
        return false;
      }
      return opt_type ? opt_type in this.listeners_ : Object.keys(this.listeners_).length > 0;
    };
    Target2.prototype.removeEventListener = function(type, listener) {
      var listeners = this.listeners_ && this.listeners_[type];
      if (listeners) {
        var index = listeners.indexOf(listener);
        if (index !== -1) {
          if (this.pendingRemovals_ && type in this.pendingRemovals_) {
            listeners[index] = VOID;
            ++this.pendingRemovals_[type];
          } else {
            listeners.splice(index, 1);
            if (listeners.length === 0) {
              delete this.listeners_[type];
            }
          }
        }
      }
    };
    return Target2;
  }(Disposable_default);
  var Target_default = Target;

  // node_modules/ol/events/EventType.js
  var EventType_default = {
    CHANGE: "change",
    ERROR: "error",
    BLUR: "blur",
    CLEAR: "clear",
    CONTEXTMENU: "contextmenu",
    CLICK: "click",
    DBLCLICK: "dblclick",
    DRAGENTER: "dragenter",
    DRAGOVER: "dragover",
    DROP: "drop",
    FOCUS: "focus",
    KEYDOWN: "keydown",
    KEYPRESS: "keypress",
    LOAD: "load",
    RESIZE: "resize",
    TOUCHMOVE: "touchmove",
    WHEEL: "wheel"
  };

  // node_modules/ol/events.js
  function listen(target, type, listener, opt_this, opt_once) {
    if (opt_this && opt_this !== target) {
      listener = listener.bind(opt_this);
    }
    if (opt_once) {
      var originalListener_1 = listener;
      listener = function() {
        target.removeEventListener(type, listener);
        originalListener_1.apply(this, arguments);
      };
    }
    var eventsKey = {
      target,
      type,
      listener
    };
    target.addEventListener(type, listener);
    return eventsKey;
  }
  function listenOnce(target, type, listener, opt_this) {
    return listen(target, type, listener, opt_this, true);
  }
  function unlistenByKey(key) {
    if (key && key.target) {
      key.target.removeEventListener(key.type, key.listener);
      clear(key);
    }
  }

  // node_modules/ol/Observable.js
  var __extends2 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Observable = function(_super) {
    __extends2(Observable2, _super);
    function Observable2() {
      var _this = _super.call(this) || this;
      _this.on = _this.onInternal;
      _this.once = _this.onceInternal;
      _this.un = _this.unInternal;
      _this.revision_ = 0;
      return _this;
    }
    Observable2.prototype.changed = function() {
      ++this.revision_;
      this.dispatchEvent(EventType_default.CHANGE);
    };
    Observable2.prototype.getRevision = function() {
      return this.revision_;
    };
    Observable2.prototype.onInternal = function(type, listener) {
      if (Array.isArray(type)) {
        var len = type.length;
        var keys = new Array(len);
        for (var i = 0; i < len; ++i) {
          keys[i] = listen(this, type[i], listener);
        }
        return keys;
      } else {
        return listen(this, type, listener);
      }
    };
    Observable2.prototype.onceInternal = function(type, listener) {
      var key;
      if (Array.isArray(type)) {
        var len = type.length;
        key = new Array(len);
        for (var i = 0; i < len; ++i) {
          key[i] = listenOnce(this, type[i], listener);
        }
      } else {
        key = listenOnce(this, type, listener);
      }
      listener.ol_key = key;
      return key;
    };
    Observable2.prototype.unInternal = function(type, listener) {
      var key = listener.ol_key;
      if (key) {
        unByKey(key);
      } else if (Array.isArray(type)) {
        for (var i = 0, ii = type.length; i < ii; ++i) {
          this.removeEventListener(type[i], listener);
        }
      } else {
        this.removeEventListener(type, listener);
      }
    };
    return Observable2;
  }(Target_default);
  Observable.prototype.on;
  Observable.prototype.once;
  Observable.prototype.un;
  function unByKey(key) {
    if (Array.isArray(key)) {
      for (var i = 0, ii = key.length; i < ii; ++i) {
        unlistenByKey(key[i]);
      }
    } else {
      unlistenByKey(key);
    }
  }
  var Observable_default = Observable;

  // node_modules/ol/util.js
  function abstract() {
    return function() {
      throw new Error("Unimplemented abstract method.");
    }();
  }
  var uidCounter_ = 0;
  function getUid(obj) {
    return obj.ol_uid || (obj.ol_uid = String(++uidCounter_));
  }
  var VERSION = "6.13.0";

  // node_modules/ol/Object.js
  var __extends3 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var ObjectEvent = function(_super) {
    __extends3(ObjectEvent2, _super);
    function ObjectEvent2(type, key, oldValue) {
      var _this = _super.call(this, type) || this;
      _this.key = key;
      _this.oldValue = oldValue;
      return _this;
    }
    return ObjectEvent2;
  }(Event_default);
  var BaseObject = function(_super) {
    __extends3(BaseObject2, _super);
    function BaseObject2(opt_values) {
      var _this = _super.call(this) || this;
      _this.on;
      _this.once;
      _this.un;
      getUid(_this);
      _this.values_ = null;
      if (opt_values !== void 0) {
        _this.setProperties(opt_values);
      }
      return _this;
    }
    BaseObject2.prototype.get = function(key) {
      var value;
      if (this.values_ && this.values_.hasOwnProperty(key)) {
        value = this.values_[key];
      }
      return value;
    };
    BaseObject2.prototype.getKeys = function() {
      return this.values_ && Object.keys(this.values_) || [];
    };
    BaseObject2.prototype.getProperties = function() {
      return this.values_ && assign({}, this.values_) || {};
    };
    BaseObject2.prototype.hasProperties = function() {
      return !!this.values_;
    };
    BaseObject2.prototype.notify = function(key, oldValue) {
      var eventType;
      eventType = "change:".concat(key);
      if (this.hasListener(eventType)) {
        this.dispatchEvent(new ObjectEvent(eventType, key, oldValue));
      }
      eventType = ObjectEventType_default.PROPERTYCHANGE;
      if (this.hasListener(eventType)) {
        this.dispatchEvent(new ObjectEvent(eventType, key, oldValue));
      }
    };
    BaseObject2.prototype.addChangeListener = function(key, listener) {
      this.addEventListener("change:".concat(key), listener);
    };
    BaseObject2.prototype.removeChangeListener = function(key, listener) {
      this.removeEventListener("change:".concat(key), listener);
    };
    BaseObject2.prototype.set = function(key, value, opt_silent) {
      var values = this.values_ || (this.values_ = {});
      if (opt_silent) {
        values[key] = value;
      } else {
        var oldValue = values[key];
        values[key] = value;
        if (oldValue !== value) {
          this.notify(key, oldValue);
        }
      }
    };
    BaseObject2.prototype.setProperties = function(values, opt_silent) {
      for (var key in values) {
        this.set(key, values[key], opt_silent);
      }
    };
    BaseObject2.prototype.applyProperties = function(source) {
      if (!source.values_) {
        return;
      }
      assign(this.values_ || (this.values_ = {}), source.values_);
    };
    BaseObject2.prototype.unset = function(key, opt_silent) {
      if (this.values_ && key in this.values_) {
        var oldValue = this.values_[key];
        delete this.values_[key];
        if (isEmpty(this.values_)) {
          this.values_ = null;
        }
        if (!opt_silent) {
          this.notify(key, oldValue);
        }
      }
    };
    return BaseObject2;
  }(Observable_default);
  var Object_default = BaseObject;

  // node_modules/ol/AssertionError.js
  var __extends4 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var AssertionError = function(_super) {
    __extends4(AssertionError2, _super);
    function AssertionError2(code) {
      var _this = this;
      var path = VERSION === "latest" ? VERSION : "v" + VERSION.split("-")[0];
      var message = "Assertion failed. See https://openlayers.org/en/" + path + "/doc/errors/#" + code + " for details.";
      _this = _super.call(this, message) || this;
      _this.code = code;
      _this.name = "AssertionError";
      _this.message = message;
      return _this;
    }
    return AssertionError2;
  }(Error);
  var AssertionError_default = AssertionError;

  // node_modules/ol/asserts.js
  function assert(assertion, errorCode) {
    if (!assertion) {
      throw new AssertionError_default(errorCode);
    }
  }

  // node_modules/ol/Feature.js
  var __extends5 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Feature = function(_super) {
    __extends5(Feature2, _super);
    function Feature2(opt_geometryOrProperties) {
      var _this = _super.call(this) || this;
      _this.on;
      _this.once;
      _this.un;
      _this.id_ = void 0;
      _this.geometryName_ = "geometry";
      _this.style_ = null;
      _this.styleFunction_ = void 0;
      _this.geometryChangeKey_ = null;
      _this.addChangeListener(_this.geometryName_, _this.handleGeometryChanged_);
      if (opt_geometryOrProperties) {
        if (typeof opt_geometryOrProperties.getSimplifiedGeometry === "function") {
          var geometry = opt_geometryOrProperties;
          _this.setGeometry(geometry);
        } else {
          var properties = opt_geometryOrProperties;
          _this.setProperties(properties);
        }
      }
      return _this;
    }
    Feature2.prototype.clone = function() {
      var clone2 = new Feature2(this.hasProperties() ? this.getProperties() : null);
      clone2.setGeometryName(this.getGeometryName());
      var geometry = this.getGeometry();
      if (geometry) {
        clone2.setGeometry(geometry.clone());
      }
      var style = this.getStyle();
      if (style) {
        clone2.setStyle(style);
      }
      return clone2;
    };
    Feature2.prototype.getGeometry = function() {
      return this.get(this.geometryName_);
    };
    Feature2.prototype.getId = function() {
      return this.id_;
    };
    Feature2.prototype.getGeometryName = function() {
      return this.geometryName_;
    };
    Feature2.prototype.getStyle = function() {
      return this.style_;
    };
    Feature2.prototype.getStyleFunction = function() {
      return this.styleFunction_;
    };
    Feature2.prototype.handleGeometryChange_ = function() {
      this.changed();
    };
    Feature2.prototype.handleGeometryChanged_ = function() {
      if (this.geometryChangeKey_) {
        unlistenByKey(this.geometryChangeKey_);
        this.geometryChangeKey_ = null;
      }
      var geometry = this.getGeometry();
      if (geometry) {
        this.geometryChangeKey_ = listen(geometry, EventType_default.CHANGE, this.handleGeometryChange_, this);
      }
      this.changed();
    };
    Feature2.prototype.setGeometry = function(geometry) {
      this.set(this.geometryName_, geometry);
    };
    Feature2.prototype.setStyle = function(opt_style) {
      this.style_ = opt_style;
      this.styleFunction_ = !opt_style ? void 0 : createStyleFunction(opt_style);
      this.changed();
    };
    Feature2.prototype.setId = function(id) {
      this.id_ = id;
      this.changed();
    };
    Feature2.prototype.setGeometryName = function(name) {
      this.removeChangeListener(this.geometryName_, this.handleGeometryChanged_);
      this.geometryName_ = name;
      this.addChangeListener(this.geometryName_, this.handleGeometryChanged_);
      this.handleGeometryChanged_();
    };
    return Feature2;
  }(Object_default);
  function createStyleFunction(obj) {
    if (typeof obj === "function") {
      return obj;
    } else {
      var styles_1;
      if (Array.isArray(obj)) {
        styles_1 = obj;
      } else {
        assert(typeof obj.getZIndex === "function", 41);
        var style = obj;
        styles_1 = [style];
      }
      return function() {
        return styles_1;
      };
    }
  }
  var Feature_default = Feature;

  // node_modules/ol/proj/Units.js
  var Units = {
    RADIANS: "radians",
    DEGREES: "degrees",
    FEET: "ft",
    METERS: "m",
    PIXELS: "pixels",
    TILE_PIXELS: "tile-pixels",
    USFEET: "us-ft"
  };
  var unitByCode = {
    "9001": Units.METERS,
    "9002": Units.FEET,
    "9003": Units.USFEET,
    "9101": Units.RADIANS,
    "9102": Units.DEGREES
  };
  var METERS_PER_UNIT = {};
  METERS_PER_UNIT[Units.RADIANS] = 6370997 / (2 * Math.PI);
  METERS_PER_UNIT[Units.DEGREES] = 2 * Math.PI * 6370997 / 360;
  METERS_PER_UNIT[Units.FEET] = 0.3048;
  METERS_PER_UNIT[Units.METERS] = 1;
  METERS_PER_UNIT[Units.USFEET] = 1200 / 3937;
  var Units_default = Units;

  // node_modules/ol/has.js
  var ua = typeof navigator !== "undefined" && typeof navigator.userAgent !== "undefined" ? navigator.userAgent.toLowerCase() : "";
  var FIREFOX = ua.indexOf("firefox") !== -1;
  var SAFARI = ua.indexOf("safari") !== -1 && ua.indexOf("chrom") == -1;
  var WEBKIT = ua.indexOf("webkit") !== -1 && ua.indexOf("edge") == -1;
  var MAC = ua.indexOf("macintosh") !== -1;
  var WORKER_OFFSCREEN_CANVAS = typeof WorkerGlobalScope !== "undefined" && typeof OffscreenCanvas !== "undefined" && self instanceof WorkerGlobalScope;
  var IMAGE_DECODE = typeof Image !== "undefined" && Image.prototype.decode;
  var PASSIVE_EVENT_LISTENERS = function() {
    var passive = false;
    try {
      var options = Object.defineProperty({}, "passive", {
        get: function() {
          passive = true;
        }
      });
      window.addEventListener("_", null, options);
      window.removeEventListener("_", null, options);
    } catch (error) {
    }
    return passive;
  }();

  // node_modules/ol/transform.js
  var tmp_ = new Array(6);
  function create() {
    return [1, 0, 0, 1, 0, 0];
  }
  function set(transform2, a, b, c, d, e, f) {
    transform2[0] = a;
    transform2[1] = b;
    transform2[2] = c;
    transform2[3] = d;
    transform2[4] = e;
    transform2[5] = f;
    return transform2;
  }
  function setFromArray(transform1, transform2) {
    transform1[0] = transform2[0];
    transform1[1] = transform2[1];
    transform1[2] = transform2[2];
    transform1[3] = transform2[3];
    transform1[4] = transform2[4];
    transform1[5] = transform2[5];
    return transform1;
  }
  function apply(transform2, coordinate) {
    var x = coordinate[0];
    var y = coordinate[1];
    coordinate[0] = transform2[0] * x + transform2[2] * y + transform2[4];
    coordinate[1] = transform2[1] * x + transform2[3] * y + transform2[5];
    return coordinate;
  }
  function makeScale(target, x, y) {
    return set(target, x, 0, 0, y, 0, 0);
  }
  function compose(transform2, dx1, dy1, sx, sy, angle, dx2, dy2) {
    var sin = Math.sin(angle);
    var cos = Math.cos(angle);
    transform2[0] = sx * cos;
    transform2[1] = sy * sin;
    transform2[2] = -sx * sin;
    transform2[3] = sy * cos;
    transform2[4] = dx2 * sx * cos - dy2 * sx * sin + dx1;
    transform2[5] = dx2 * sy * sin + dy2 * sy * cos + dy1;
    return transform2;
  }
  function makeInverse(target, source) {
    var det = determinant(source);
    assert(det !== 0, 32);
    var a = source[0];
    var b = source[1];
    var c = source[2];
    var d = source[3];
    var e = source[4];
    var f = source[5];
    target[0] = d / det;
    target[1] = -b / det;
    target[2] = -c / det;
    target[3] = a / det;
    target[4] = (c * f - d * e) / det;
    target[5] = -(a * f - b * e) / det;
    return target;
  }
  function determinant(mat) {
    return mat[0] * mat[3] - mat[1] * mat[2];
  }
  var transformStringDiv;
  function toString(mat) {
    var transformString = "matrix(" + mat.join(", ") + ")";
    if (WORKER_OFFSCREEN_CANVAS) {
      return transformString;
    }
    var node = transformStringDiv || (transformStringDiv = document.createElement("div"));
    node.style.transform = transformString;
    return node.style.transform;
  }

  // node_modules/ol/extent/Relationship.js
  var Relationship_default = {
    UNKNOWN: 0,
    INTERSECTING: 1,
    ABOVE: 2,
    RIGHT: 4,
    BELOW: 8,
    LEFT: 16
  };

  // node_modules/ol/extent.js
  function boundingExtent(coordinates2) {
    var extent = createEmpty();
    for (var i = 0, ii = coordinates2.length; i < ii; ++i) {
      extendCoordinate(extent, coordinates2[i]);
    }
    return extent;
  }
  function _boundingExtentXYs(xs, ys, opt_extent) {
    var minX = Math.min.apply(null, xs);
    var minY = Math.min.apply(null, ys);
    var maxX = Math.max.apply(null, xs);
    var maxY = Math.max.apply(null, ys);
    return createOrUpdate(minX, minY, maxX, maxY, opt_extent);
  }
  function buffer(extent, value, opt_extent) {
    if (opt_extent) {
      opt_extent[0] = extent[0] - value;
      opt_extent[1] = extent[1] - value;
      opt_extent[2] = extent[2] + value;
      opt_extent[3] = extent[3] + value;
      return opt_extent;
    } else {
      return [
        extent[0] - value,
        extent[1] - value,
        extent[2] + value,
        extent[3] + value
      ];
    }
  }
  function clone(extent, opt_extent) {
    if (opt_extent) {
      opt_extent[0] = extent[0];
      opt_extent[1] = extent[1];
      opt_extent[2] = extent[2];
      opt_extent[3] = extent[3];
      return opt_extent;
    } else {
      return extent.slice();
    }
  }
  function closestSquaredDistanceXY(extent, x, y) {
    var dx, dy;
    if (x < extent[0]) {
      dx = extent[0] - x;
    } else if (extent[2] < x) {
      dx = x - extent[2];
    } else {
      dx = 0;
    }
    if (y < extent[1]) {
      dy = extent[1] - y;
    } else if (extent[3] < y) {
      dy = y - extent[3];
    } else {
      dy = 0;
    }
    return dx * dx + dy * dy;
  }
  function containsCoordinate(extent, coordinate) {
    return containsXY(extent, coordinate[0], coordinate[1]);
  }
  function containsExtent(extent1, extent2) {
    return extent1[0] <= extent2[0] && extent2[2] <= extent1[2] && extent1[1] <= extent2[1] && extent2[3] <= extent1[3];
  }
  function containsXY(extent, x, y) {
    return extent[0] <= x && x <= extent[2] && extent[1] <= y && y <= extent[3];
  }
  function coordinateRelationship(extent, coordinate) {
    var minX = extent[0];
    var minY = extent[1];
    var maxX = extent[2];
    var maxY = extent[3];
    var x = coordinate[0];
    var y = coordinate[1];
    var relationship = Relationship_default.UNKNOWN;
    if (x < minX) {
      relationship = relationship | Relationship_default.LEFT;
    } else if (x > maxX) {
      relationship = relationship | Relationship_default.RIGHT;
    }
    if (y < minY) {
      relationship = relationship | Relationship_default.BELOW;
    } else if (y > maxY) {
      relationship = relationship | Relationship_default.ABOVE;
    }
    if (relationship === Relationship_default.UNKNOWN) {
      relationship = Relationship_default.INTERSECTING;
    }
    return relationship;
  }
  function createEmpty() {
    return [Infinity, Infinity, -Infinity, -Infinity];
  }
  function createOrUpdate(minX, minY, maxX, maxY, opt_extent) {
    if (opt_extent) {
      opt_extent[0] = minX;
      opt_extent[1] = minY;
      opt_extent[2] = maxX;
      opt_extent[3] = maxY;
      return opt_extent;
    } else {
      return [minX, minY, maxX, maxY];
    }
  }
  function createOrUpdateEmpty(opt_extent) {
    return createOrUpdate(Infinity, Infinity, -Infinity, -Infinity, opt_extent);
  }
  function createOrUpdateFromCoordinate(coordinate, opt_extent) {
    var x = coordinate[0];
    var y = coordinate[1];
    return createOrUpdate(x, y, x, y, opt_extent);
  }
  function createOrUpdateFromFlatCoordinates(flatCoordinates, offset, end, stride, opt_extent) {
    var extent = createOrUpdateEmpty(opt_extent);
    return extendFlatCoordinates(extent, flatCoordinates, offset, end, stride);
  }
  function equals2(extent1, extent2) {
    return extent1[0] == extent2[0] && extent1[2] == extent2[2] && extent1[1] == extent2[1] && extent1[3] == extent2[3];
  }
  function extend2(extent1, extent2) {
    if (extent2[0] < extent1[0]) {
      extent1[0] = extent2[0];
    }
    if (extent2[2] > extent1[2]) {
      extent1[2] = extent2[2];
    }
    if (extent2[1] < extent1[1]) {
      extent1[1] = extent2[1];
    }
    if (extent2[3] > extent1[3]) {
      extent1[3] = extent2[3];
    }
    return extent1;
  }
  function extendCoordinate(extent, coordinate) {
    if (coordinate[0] < extent[0]) {
      extent[0] = coordinate[0];
    }
    if (coordinate[0] > extent[2]) {
      extent[2] = coordinate[0];
    }
    if (coordinate[1] < extent[1]) {
      extent[1] = coordinate[1];
    }
    if (coordinate[1] > extent[3]) {
      extent[3] = coordinate[1];
    }
  }
  function extendFlatCoordinates(extent, flatCoordinates, offset, end, stride) {
    for (; offset < end; offset += stride) {
      extendXY(extent, flatCoordinates[offset], flatCoordinates[offset + 1]);
    }
    return extent;
  }
  function extendXY(extent, x, y) {
    extent[0] = Math.min(extent[0], x);
    extent[1] = Math.min(extent[1], y);
    extent[2] = Math.max(extent[2], x);
    extent[3] = Math.max(extent[3], y);
  }
  function forEachCorner(extent, callback) {
    var val;
    val = callback(getBottomLeft(extent));
    if (val) {
      return val;
    }
    val = callback(getBottomRight(extent));
    if (val) {
      return val;
    }
    val = callback(getTopRight(extent));
    if (val) {
      return val;
    }
    val = callback(getTopLeft(extent));
    if (val) {
      return val;
    }
    return false;
  }
  function getBottomLeft(extent) {
    return [extent[0], extent[1]];
  }
  function getBottomRight(extent) {
    return [extent[2], extent[1]];
  }
  function getCenter(extent) {
    return [(extent[0] + extent[2]) / 2, (extent[1] + extent[3]) / 2];
  }
  function getHeight(extent) {
    return extent[3] - extent[1];
  }
  function getTopLeft(extent) {
    return [extent[0], extent[3]];
  }
  function getTopRight(extent) {
    return [extent[2], extent[3]];
  }
  function getWidth(extent) {
    return extent[2] - extent[0];
  }
  function intersects(extent1, extent2) {
    return extent1[0] <= extent2[2] && extent1[2] >= extent2[0] && extent1[1] <= extent2[3] && extent1[3] >= extent2[1];
  }
  function isEmpty2(extent) {
    return extent[2] < extent[0] || extent[3] < extent[1];
  }
  function returnOrUpdate(extent, opt_extent) {
    if (opt_extent) {
      opt_extent[0] = extent[0];
      opt_extent[1] = extent[1];
      opt_extent[2] = extent[2];
      opt_extent[3] = extent[3];
      return opt_extent;
    } else {
      return extent;
    }
  }
  function intersectsSegment(extent, start, end) {
    var intersects2 = false;
    var startRel = coordinateRelationship(extent, start);
    var endRel = coordinateRelationship(extent, end);
    if (startRel === Relationship_default.INTERSECTING || endRel === Relationship_default.INTERSECTING) {
      intersects2 = true;
    } else {
      var minX = extent[0];
      var minY = extent[1];
      var maxX = extent[2];
      var maxY = extent[3];
      var startX = start[0];
      var startY = start[1];
      var endX = end[0];
      var endY = end[1];
      var slope = (endY - startY) / (endX - startX);
      var x = void 0, y = void 0;
      if (!!(endRel & Relationship_default.ABOVE) && !(startRel & Relationship_default.ABOVE)) {
        x = endX - (endY - maxY) / slope;
        intersects2 = x >= minX && x <= maxX;
      }
      if (!intersects2 && !!(endRel & Relationship_default.RIGHT) && !(startRel & Relationship_default.RIGHT)) {
        y = endY - (endX - maxX) * slope;
        intersects2 = y >= minY && y <= maxY;
      }
      if (!intersects2 && !!(endRel & Relationship_default.BELOW) && !(startRel & Relationship_default.BELOW)) {
        x = endX - (endY - minY) / slope;
        intersects2 = x >= minX && x <= maxX;
      }
      if (!intersects2 && !!(endRel & Relationship_default.LEFT) && !(startRel & Relationship_default.LEFT)) {
        y = endY - (endX - minX) * slope;
        intersects2 = y >= minY && y <= maxY;
      }
    }
    return intersects2;
  }
  function applyTransform(extent, transformFn, opt_extent, opt_stops) {
    var coordinates2 = [];
    if (opt_stops > 1) {
      var width = extent[2] - extent[0];
      var height = extent[3] - extent[1];
      for (var i = 0; i < opt_stops; ++i) {
        coordinates2.push(extent[0] + width * i / opt_stops, extent[1], extent[2], extent[1] + height * i / opt_stops, extent[2] - width * i / opt_stops, extent[3], extent[0], extent[3] - height * i / opt_stops);
      }
    } else {
      coordinates2 = [
        extent[0],
        extent[1],
        extent[2],
        extent[1],
        extent[2],
        extent[3],
        extent[0],
        extent[3]
      ];
    }
    transformFn(coordinates2, coordinates2, 2);
    var xs = [];
    var ys = [];
    for (var i = 0, l = coordinates2.length; i < l; i += 2) {
      xs.push(coordinates2[i]);
      ys.push(coordinates2[i + 1]);
    }
    return _boundingExtentXYs(xs, ys, opt_extent);
  }
  function wrapX(extent, projection) {
    var projectionExtent = projection.getExtent();
    var center = getCenter(extent);
    if (projection.canWrapX() && (center[0] < projectionExtent[0] || center[0] >= projectionExtent[2])) {
      var worldWidth = getWidth(projectionExtent);
      var worldsAway = Math.floor((center[0] - projectionExtent[0]) / worldWidth);
      var offset = worldsAway * worldWidth;
      extent[0] -= offset;
      extent[2] -= offset;
    }
    return extent;
  }

  // node_modules/ol/proj/Projection.js
  var Projection = function() {
    function Projection2(options) {
      this.code_ = options.code;
      this.units_ = options.units;
      this.extent_ = options.extent !== void 0 ? options.extent : null;
      this.worldExtent_ = options.worldExtent !== void 0 ? options.worldExtent : null;
      this.axisOrientation_ = options.axisOrientation !== void 0 ? options.axisOrientation : "enu";
      this.global_ = options.global !== void 0 ? options.global : false;
      this.canWrapX_ = !!(this.global_ && this.extent_);
      this.getPointResolutionFunc_ = options.getPointResolution;
      this.defaultTileGrid_ = null;
      this.metersPerUnit_ = options.metersPerUnit;
    }
    Projection2.prototype.canWrapX = function() {
      return this.canWrapX_;
    };
    Projection2.prototype.getCode = function() {
      return this.code_;
    };
    Projection2.prototype.getExtent = function() {
      return this.extent_;
    };
    Projection2.prototype.getUnits = function() {
      return this.units_;
    };
    Projection2.prototype.getMetersPerUnit = function() {
      return this.metersPerUnit_ || METERS_PER_UNIT[this.units_];
    };
    Projection2.prototype.getWorldExtent = function() {
      return this.worldExtent_;
    };
    Projection2.prototype.getAxisOrientation = function() {
      return this.axisOrientation_;
    };
    Projection2.prototype.isGlobal = function() {
      return this.global_;
    };
    Projection2.prototype.setGlobal = function(global) {
      this.global_ = global;
      this.canWrapX_ = !!(global && this.extent_);
    };
    Projection2.prototype.getDefaultTileGrid = function() {
      return this.defaultTileGrid_;
    };
    Projection2.prototype.setDefaultTileGrid = function(tileGrid) {
      this.defaultTileGrid_ = tileGrid;
    };
    Projection2.prototype.setExtent = function(extent) {
      this.extent_ = extent;
      this.canWrapX_ = !!(this.global_ && extent);
    };
    Projection2.prototype.setWorldExtent = function(worldExtent) {
      this.worldExtent_ = worldExtent;
    };
    Projection2.prototype.setGetPointResolution = function(func) {
      this.getPointResolutionFunc_ = func;
    };
    Projection2.prototype.getPointResolutionFunc = function() {
      return this.getPointResolutionFunc_;
    };
    return Projection2;
  }();
  var Projection_default = Projection;

  // node_modules/ol/math.js
  function clamp(value, min, max) {
    return Math.min(Math.max(value, min), max);
  }
  var cosh = function() {
    var cosh2;
    if ("cosh" in Math) {
      cosh2 = Math.cosh;
    } else {
      cosh2 = function(x) {
        var y = Math.exp(x);
        return (y + 1 / y) / 2;
      };
    }
    return cosh2;
  }();
  var log2 = function() {
    var log22;
    if ("log2" in Math) {
      log22 = Math.log2;
    } else {
      log22 = function(x) {
        return Math.log(x) * Math.LOG2E;
      };
    }
    return log22;
  }();
  function squaredSegmentDistance(x, y, x1, y1, x2, y2) {
    var dx = x2 - x1;
    var dy = y2 - y1;
    if (dx !== 0 || dy !== 0) {
      var t = ((x - x1) * dx + (y - y1) * dy) / (dx * dx + dy * dy);
      if (t > 1) {
        x1 = x2;
        y1 = y2;
      } else if (t > 0) {
        x1 += dx * t;
        y1 += dy * t;
      }
    }
    return squaredDistance(x, y, x1, y1);
  }
  function squaredDistance(x1, y1, x2, y2) {
    var dx = x2 - x1;
    var dy = y2 - y1;
    return dx * dx + dy * dy;
  }
  function modulo(a, b) {
    var r = a % b;
    return r * b < 0 ? r + b : r;
  }
  function lerp(a, b, x) {
    return a + x * (b - a);
  }

  // node_modules/ol/proj/epsg3857.js
  var __extends6 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var RADIUS = 6378137;
  var HALF_SIZE = Math.PI * RADIUS;
  var EXTENT = [-HALF_SIZE, -HALF_SIZE, HALF_SIZE, HALF_SIZE];
  var WORLD_EXTENT = [-180, -85, 180, 85];
  var MAX_SAFE_Y = RADIUS * Math.log(Math.tan(Math.PI / 2));
  var EPSG3857Projection = function(_super) {
    __extends6(EPSG3857Projection2, _super);
    function EPSG3857Projection2(code) {
      return _super.call(this, {
        code,
        units: Units_default.METERS,
        extent: EXTENT,
        global: true,
        worldExtent: WORLD_EXTENT,
        getPointResolution: function(resolution, point) {
          return resolution / cosh(point[1] / RADIUS);
        }
      }) || this;
    }
    return EPSG3857Projection2;
  }(Projection_default);
  var PROJECTIONS = [
    new EPSG3857Projection("EPSG:3857"),
    new EPSG3857Projection("EPSG:102100"),
    new EPSG3857Projection("EPSG:102113"),
    new EPSG3857Projection("EPSG:900913"),
    new EPSG3857Projection("http://www.opengis.net/def/crs/EPSG/0/3857"),
    new EPSG3857Projection("http://www.opengis.net/gml/srs/epsg.xml#3857")
  ];
  function fromEPSG4326(input, opt_output, opt_dimension) {
    var length = input.length;
    var dimension = opt_dimension > 1 ? opt_dimension : 2;
    var output = opt_output;
    if (output === void 0) {
      if (dimension > 2) {
        output = input.slice();
      } else {
        output = new Array(length);
      }
    }
    for (var i = 0; i < length; i += dimension) {
      output[i] = HALF_SIZE * input[i] / 180;
      var y = RADIUS * Math.log(Math.tan(Math.PI * (+input[i + 1] + 90) / 360));
      if (y > MAX_SAFE_Y) {
        y = MAX_SAFE_Y;
      } else if (y < -MAX_SAFE_Y) {
        y = -MAX_SAFE_Y;
      }
      output[i + 1] = y;
    }
    return output;
  }
  function toEPSG4326(input, opt_output, opt_dimension) {
    var length = input.length;
    var dimension = opt_dimension > 1 ? opt_dimension : 2;
    var output = opt_output;
    if (output === void 0) {
      if (dimension > 2) {
        output = input.slice();
      } else {
        output = new Array(length);
      }
    }
    for (var i = 0; i < length; i += dimension) {
      output[i] = 180 * input[i] / HALF_SIZE;
      output[i + 1] = 360 * Math.atan(Math.exp(input[i + 1] / RADIUS)) / Math.PI - 90;
    }
    return output;
  }

  // node_modules/ol/proj/epsg4326.js
  var __extends7 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var RADIUS2 = 6378137;
  var EXTENT2 = [-180, -90, 180, 90];
  var METERS_PER_UNIT2 = Math.PI * RADIUS2 / 180;
  var EPSG4326Projection = function(_super) {
    __extends7(EPSG4326Projection2, _super);
    function EPSG4326Projection2(code, opt_axisOrientation) {
      return _super.call(this, {
        code,
        units: Units_default.DEGREES,
        extent: EXTENT2,
        axisOrientation: opt_axisOrientation,
        global: true,
        metersPerUnit: METERS_PER_UNIT2,
        worldExtent: EXTENT2
      }) || this;
    }
    return EPSG4326Projection2;
  }(Projection_default);
  var PROJECTIONS2 = [
    new EPSG4326Projection("CRS:84"),
    new EPSG4326Projection("EPSG:4326", "neu"),
    new EPSG4326Projection("urn:ogc:def:crs:OGC:1.3:CRS84"),
    new EPSG4326Projection("urn:ogc:def:crs:OGC:2:84"),
    new EPSG4326Projection("http://www.opengis.net/def/crs/OGC/1.3/CRS84"),
    new EPSG4326Projection("http://www.opengis.net/gml/srs/epsg.xml#4326", "neu"),
    new EPSG4326Projection("http://www.opengis.net/def/crs/EPSG/0/4326", "neu")
  ];

  // node_modules/ol/proj/projections.js
  var cache = {};
  function get(code) {
    return cache[code] || cache[code.replace(/urn:(x-)?ogc:def:crs:EPSG:(.*:)?(\w+)$/, "EPSG:$3")] || null;
  }
  function add(code, projection) {
    cache[code] = projection;
  }

  // node_modules/ol/proj/transforms.js
  var transforms = {};
  function add2(source, destination, transformFn) {
    var sourceCode = source.getCode();
    var destinationCode = destination.getCode();
    if (!(sourceCode in transforms)) {
      transforms[sourceCode] = {};
    }
    transforms[sourceCode][destinationCode] = transformFn;
  }
  function get2(sourceCode, destinationCode) {
    var transform2;
    if (sourceCode in transforms && destinationCode in transforms[sourceCode]) {
      transform2 = transforms[sourceCode][destinationCode];
    }
    return transform2;
  }

  // node_modules/ol/coordinate.js
  function closestOnSegment(coordinate, segment) {
    var x0 = coordinate[0];
    var y0 = coordinate[1];
    var start = segment[0];
    var end = segment[1];
    var x1 = start[0];
    var y1 = start[1];
    var x2 = end[0];
    var y2 = end[1];
    var dx = x2 - x1;
    var dy = y2 - y1;
    var along = dx === 0 && dy === 0 ? 0 : (dx * (x0 - x1) + dy * (y0 - y1)) / (dx * dx + dy * dy || 0);
    var x, y;
    if (along <= 0) {
      x = x1;
      y = y1;
    } else if (along >= 1) {
      x = x2;
      y = y2;
    } else {
      x = x1 + along * dx;
      y = y1 + along * dy;
    }
    return [x, y];
  }
  function equals3(coordinate1, coordinate2) {
    var equals4 = true;
    for (var i = coordinate1.length - 1; i >= 0; --i) {
      if (coordinate1[i] != coordinate2[i]) {
        equals4 = false;
        break;
      }
    }
    return equals4;
  }
  function squaredDistance2(coord1, coord2) {
    var dx = coord1[0] - coord2[0];
    var dy = coord1[1] - coord2[1];
    return dx * dx + dy * dy;
  }
  function distance(coord1, coord2) {
    return Math.sqrt(squaredDistance2(coord1, coord2));
  }
  function squaredDistanceToSegment(coordinate, segment) {
    return squaredDistance2(coordinate, closestOnSegment(coordinate, segment));
  }
  function wrapX2(coordinate, projection) {
    if (projection.canWrapX()) {
      var worldWidth = getWidth(projection.getExtent());
      var worldsAway = getWorldsAway(coordinate, projection, worldWidth);
      if (worldsAway) {
        coordinate[0] -= worldsAway * worldWidth;
      }
    }
    return coordinate;
  }
  function getWorldsAway(coordinate, projection, opt_sourceExtentWidth) {
    var projectionExtent = projection.getExtent();
    var worldsAway = 0;
    if (projection.canWrapX() && (coordinate[0] < projectionExtent[0] || coordinate[0] > projectionExtent[2])) {
      var sourceExtentWidth = opt_sourceExtentWidth || getWidth(projectionExtent);
      worldsAway = Math.floor((coordinate[0] - projectionExtent[0]) / sourceExtentWidth);
    }
    return worldsAway;
  }

  // node_modules/ol/geom/GeometryType.js
  var GeometryType_default = {
    POINT: "Point",
    LINE_STRING: "LineString",
    LINEAR_RING: "LinearRing",
    POLYGON: "Polygon",
    MULTI_POINT: "MultiPoint",
    MULTI_LINE_STRING: "MultiLineString",
    MULTI_POLYGON: "MultiPolygon",
    GEOMETRY_COLLECTION: "GeometryCollection",
    CIRCLE: "Circle"
  };

  // node_modules/ol/proj.js
  var showCoordinateWarning = true;
  function cloneTransform(input, opt_output, opt_dimension) {
    var output;
    if (opt_output !== void 0) {
      for (var i = 0, ii = input.length; i < ii; ++i) {
        opt_output[i] = input[i];
      }
      output = opt_output;
    } else {
      output = input.slice();
    }
    return output;
  }
  function identityTransform(input, opt_output, opt_dimension) {
    if (opt_output !== void 0 && input !== opt_output) {
      for (var i = 0, ii = input.length; i < ii; ++i) {
        opt_output[i] = input[i];
      }
      input = opt_output;
    }
    return input;
  }
  function addProjection(projection) {
    add(projection.getCode(), projection);
    add2(projection, projection, cloneTransform);
  }
  function addProjections(projections) {
    projections.forEach(addProjection);
  }
  function get3(projectionLike) {
    return typeof projectionLike === "string" ? get(projectionLike) : projectionLike || null;
  }
  function addEquivalentProjections(projections) {
    addProjections(projections);
    projections.forEach(function(source) {
      projections.forEach(function(destination) {
        if (source !== destination) {
          add2(source, destination, cloneTransform);
        }
      });
    });
  }
  function addEquivalentTransforms(projections1, projections2, forwardTransform, inverseTransform) {
    projections1.forEach(function(projection1) {
      projections2.forEach(function(projection2) {
        add2(projection1, projection2, forwardTransform);
        add2(projection2, projection1, inverseTransform);
      });
    });
  }
  function equivalent(projection1, projection2) {
    if (projection1 === projection2) {
      return true;
    }
    var equalUnits = projection1.getUnits() === projection2.getUnits();
    if (projection1.getCode() === projection2.getCode()) {
      return equalUnits;
    } else {
      var transformFunc = getTransformFromProjections(projection1, projection2);
      return transformFunc === cloneTransform && equalUnits;
    }
  }
  function getTransformFromProjections(sourceProjection, destinationProjection) {
    var sourceCode = sourceProjection.getCode();
    var destinationCode = destinationProjection.getCode();
    var transformFunc = get2(sourceCode, destinationCode);
    if (!transformFunc) {
      transformFunc = identityTransform;
    }
    return transformFunc;
  }
  function getTransform(source, destination) {
    var sourceProjection = get3(source);
    var destinationProjection = get3(destination);
    return getTransformFromProjections(sourceProjection, destinationProjection);
  }
  function transform(coordinate, source, destination) {
    var transformFunc = getTransform(source, destination);
    return transformFunc(coordinate, void 0, coordinate.length);
  }
  function transformExtent(extent, source, destination, opt_stops) {
    var transformFunc = getTransform(source, destination);
    return applyTransform(extent, transformFunc, void 0, opt_stops);
  }
  var userProjection = null;
  function getUserProjection() {
    return userProjection;
  }
  function toUserCoordinate(coordinate, sourceProjection) {
    if (!userProjection) {
      return coordinate;
    }
    return transform(coordinate, sourceProjection, userProjection);
  }
  function fromUserCoordinate(coordinate, destProjection) {
    if (!userProjection) {
      if (showCoordinateWarning && !equals3(coordinate, [0, 0]) && coordinate[0] >= -180 && coordinate[0] <= 180 && coordinate[1] >= -90 && coordinate[1] <= 90) {
        showCoordinateWarning = false;
        console.warn("Call useGeographic() ol/proj once to work with [longitude, latitude] coordinates.");
      }
      return coordinate;
    }
    return transform(coordinate, userProjection, destProjection);
  }
  function toUserExtent(extent, sourceProjection) {
    if (!userProjection) {
      return extent;
    }
    return transformExtent(extent, sourceProjection, userProjection);
  }
  function fromUserExtent(extent, destProjection) {
    if (!userProjection) {
      return extent;
    }
    return transformExtent(extent, userProjection, destProjection);
  }
  function toUserResolution(resolution, sourceProjection) {
    if (!userProjection) {
      return resolution;
    }
    var sourceUnits = get3(sourceProjection).getUnits();
    var userUnits = userProjection.getUnits();
    return sourceUnits && userUnits ? resolution * METERS_PER_UNIT[sourceUnits] / METERS_PER_UNIT[userUnits] : resolution;
  }
  function addCommon() {
    addEquivalentProjections(PROJECTIONS);
    addEquivalentProjections(PROJECTIONS2);
    addEquivalentTransforms(PROJECTIONS2, PROJECTIONS, fromEPSG4326, toEPSG4326);
  }
  addCommon();

  // node_modules/ol/geom/flat/transform.js
  function transform2D(flatCoordinates, offset, end, stride, transform2, opt_dest) {
    var dest = opt_dest ? opt_dest : [];
    var i = 0;
    for (var j = offset; j < end; j += stride) {
      var x = flatCoordinates[j];
      var y = flatCoordinates[j + 1];
      dest[i++] = transform2[0] * x + transform2[2] * y + transform2[4];
      dest[i++] = transform2[1] * x + transform2[3] * y + transform2[5];
    }
    if (opt_dest && dest.length != i) {
      dest.length = i;
    }
    return dest;
  }
  function rotate(flatCoordinates, offset, end, stride, angle, anchor, opt_dest) {
    var dest = opt_dest ? opt_dest : [];
    var cos = Math.cos(angle);
    var sin = Math.sin(angle);
    var anchorX = anchor[0];
    var anchorY = anchor[1];
    var i = 0;
    for (var j = offset; j < end; j += stride) {
      var deltaX = flatCoordinates[j] - anchorX;
      var deltaY = flatCoordinates[j + 1] - anchorY;
      dest[i++] = anchorX + deltaX * cos - deltaY * sin;
      dest[i++] = anchorY + deltaX * sin + deltaY * cos;
      for (var k = j + 2; k < j + stride; ++k) {
        dest[i++] = flatCoordinates[k];
      }
    }
    if (opt_dest && dest.length != i) {
      dest.length = i;
    }
    return dest;
  }
  function scale(flatCoordinates, offset, end, stride, sx, sy, anchor, opt_dest) {
    var dest = opt_dest ? opt_dest : [];
    var anchorX = anchor[0];
    var anchorY = anchor[1];
    var i = 0;
    for (var j = offset; j < end; j += stride) {
      var deltaX = flatCoordinates[j] - anchorX;
      var deltaY = flatCoordinates[j + 1] - anchorY;
      dest[i++] = anchorX + sx * deltaX;
      dest[i++] = anchorY + sy * deltaY;
      for (var k = j + 2; k < j + stride; ++k) {
        dest[i++] = flatCoordinates[k];
      }
    }
    if (opt_dest && dest.length != i) {
      dest.length = i;
    }
    return dest;
  }
  function translate(flatCoordinates, offset, end, stride, deltaX, deltaY, opt_dest) {
    var dest = opt_dest ? opt_dest : [];
    var i = 0;
    for (var j = offset; j < end; j += stride) {
      dest[i++] = flatCoordinates[j] + deltaX;
      dest[i++] = flatCoordinates[j + 1] + deltaY;
      for (var k = j + 2; k < j + stride; ++k) {
        dest[i++] = flatCoordinates[k];
      }
    }
    if (opt_dest && dest.length != i) {
      dest.length = i;
    }
    return dest;
  }

  // node_modules/ol/geom/Geometry.js
  var __extends8 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var tmpTransform = create();
  var Geometry = function(_super) {
    __extends8(Geometry2, _super);
    function Geometry2() {
      var _this = _super.call(this) || this;
      _this.extent_ = createEmpty();
      _this.extentRevision_ = -1;
      _this.simplifiedGeometryMaxMinSquaredTolerance = 0;
      _this.simplifiedGeometryRevision = 0;
      _this.simplifyTransformedInternal = memoizeOne(function(revision, squaredTolerance, opt_transform) {
        if (!opt_transform) {
          return this.getSimplifiedGeometry(squaredTolerance);
        }
        var clone2 = this.clone();
        clone2.applyTransform(opt_transform);
        return clone2.getSimplifiedGeometry(squaredTolerance);
      });
      return _this;
    }
    Geometry2.prototype.simplifyTransformed = function(squaredTolerance, opt_transform) {
      return this.simplifyTransformedInternal(this.getRevision(), squaredTolerance, opt_transform);
    };
    Geometry2.prototype.clone = function() {
      return abstract();
    };
    Geometry2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      return abstract();
    };
    Geometry2.prototype.containsXY = function(x, y) {
      var coord = this.getClosestPoint([x, y]);
      return coord[0] === x && coord[1] === y;
    };
    Geometry2.prototype.getClosestPoint = function(point, opt_closestPoint) {
      var closestPoint = opt_closestPoint ? opt_closestPoint : [NaN, NaN];
      this.closestPointXY(point[0], point[1], closestPoint, Infinity);
      return closestPoint;
    };
    Geometry2.prototype.intersectsCoordinate = function(coordinate) {
      return this.containsXY(coordinate[0], coordinate[1]);
    };
    Geometry2.prototype.computeExtent = function(extent) {
      return abstract();
    };
    Geometry2.prototype.getExtent = function(opt_extent) {
      if (this.extentRevision_ != this.getRevision()) {
        var extent = this.computeExtent(this.extent_);
        if (isNaN(extent[0]) || isNaN(extent[1])) {
          createOrUpdateEmpty(extent);
        }
        this.extentRevision_ = this.getRevision();
      }
      return returnOrUpdate(this.extent_, opt_extent);
    };
    Geometry2.prototype.rotate = function(angle, anchor) {
      abstract();
    };
    Geometry2.prototype.scale = function(sx, opt_sy, opt_anchor) {
      abstract();
    };
    Geometry2.prototype.simplify = function(tolerance) {
      return this.getSimplifiedGeometry(tolerance * tolerance);
    };
    Geometry2.prototype.getSimplifiedGeometry = function(squaredTolerance) {
      return abstract();
    };
    Geometry2.prototype.getType = function() {
      return abstract();
    };
    Geometry2.prototype.applyTransform = function(transformFn) {
      abstract();
    };
    Geometry2.prototype.intersectsExtent = function(extent) {
      return abstract();
    };
    Geometry2.prototype.translate = function(deltaX, deltaY) {
      abstract();
    };
    Geometry2.prototype.transform = function(source, destination) {
      var sourceProj = get3(source);
      var transformFn = sourceProj.getUnits() == Units_default.TILE_PIXELS ? function(inCoordinates, outCoordinates, stride) {
        var pixelExtent = sourceProj.getExtent();
        var projectedExtent = sourceProj.getWorldExtent();
        var scale2 = getHeight(projectedExtent) / getHeight(pixelExtent);
        compose(tmpTransform, projectedExtent[0], projectedExtent[3], scale2, -scale2, 0, 0, 0);
        transform2D(inCoordinates, 0, inCoordinates.length, stride, tmpTransform, outCoordinates);
        return getTransform(sourceProj, destination)(inCoordinates, outCoordinates, stride);
      } : getTransform(sourceProj, destination);
      this.applyTransform(transformFn);
      return this;
    };
    return Geometry2;
  }(Object_default);
  var Geometry_default = Geometry;

  // node_modules/ol/geom/GeometryCollection.js
  var __extends9 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var GeometryCollection = function(_super) {
    __extends9(GeometryCollection2, _super);
    function GeometryCollection2(opt_geometries) {
      var _this = _super.call(this) || this;
      _this.geometries_ = opt_geometries ? opt_geometries : null;
      _this.changeEventsKeys_ = [];
      _this.listenGeometriesChange_();
      return _this;
    }
    GeometryCollection2.prototype.unlistenGeometriesChange_ = function() {
      this.changeEventsKeys_.forEach(unlistenByKey);
      this.changeEventsKeys_.length = 0;
    };
    GeometryCollection2.prototype.listenGeometriesChange_ = function() {
      if (!this.geometries_) {
        return;
      }
      for (var i = 0, ii = this.geometries_.length; i < ii; ++i) {
        this.changeEventsKeys_.push(listen(this.geometries_[i], EventType_default.CHANGE, this.changed, this));
      }
    };
    GeometryCollection2.prototype.clone = function() {
      var geometryCollection = new GeometryCollection2(null);
      geometryCollection.setGeometries(this.geometries_);
      geometryCollection.applyProperties(this);
      return geometryCollection;
    };
    GeometryCollection2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      if (minSquaredDistance < closestSquaredDistanceXY(this.getExtent(), x, y)) {
        return minSquaredDistance;
      }
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        minSquaredDistance = geometries[i].closestPointXY(x, y, closestPoint, minSquaredDistance);
      }
      return minSquaredDistance;
    };
    GeometryCollection2.prototype.containsXY = function(x, y) {
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        if (geometries[i].containsXY(x, y)) {
          return true;
        }
      }
      return false;
    };
    GeometryCollection2.prototype.computeExtent = function(extent) {
      createOrUpdateEmpty(extent);
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        extend2(extent, geometries[i].getExtent());
      }
      return extent;
    };
    GeometryCollection2.prototype.getGeometries = function() {
      return cloneGeometries(this.geometries_);
    };
    GeometryCollection2.prototype.getGeometriesArray = function() {
      return this.geometries_;
    };
    GeometryCollection2.prototype.getGeometriesArrayRecursive = function() {
      var geometriesArray = [];
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        if (geometries[i].getType() === this.getType()) {
          geometriesArray = geometriesArray.concat(geometries[i].getGeometriesArrayRecursive());
        } else {
          geometriesArray.push(geometries[i]);
        }
      }
      return geometriesArray;
    };
    GeometryCollection2.prototype.getSimplifiedGeometry = function(squaredTolerance) {
      if (this.simplifiedGeometryRevision !== this.getRevision()) {
        this.simplifiedGeometryMaxMinSquaredTolerance = 0;
        this.simplifiedGeometryRevision = this.getRevision();
      }
      if (squaredTolerance < 0 || this.simplifiedGeometryMaxMinSquaredTolerance !== 0 && squaredTolerance < this.simplifiedGeometryMaxMinSquaredTolerance) {
        return this;
      }
      var simplifiedGeometries = [];
      var geometries = this.geometries_;
      var simplified = false;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        var geometry = geometries[i];
        var simplifiedGeometry = geometry.getSimplifiedGeometry(squaredTolerance);
        simplifiedGeometries.push(simplifiedGeometry);
        if (simplifiedGeometry !== geometry) {
          simplified = true;
        }
      }
      if (simplified) {
        var simplifiedGeometryCollection = new GeometryCollection2(null);
        simplifiedGeometryCollection.setGeometriesArray(simplifiedGeometries);
        return simplifiedGeometryCollection;
      } else {
        this.simplifiedGeometryMaxMinSquaredTolerance = squaredTolerance;
        return this;
      }
    };
    GeometryCollection2.prototype.getType = function() {
      return GeometryType_default.GEOMETRY_COLLECTION;
    };
    GeometryCollection2.prototype.intersectsExtent = function(extent) {
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        if (geometries[i].intersectsExtent(extent)) {
          return true;
        }
      }
      return false;
    };
    GeometryCollection2.prototype.isEmpty = function() {
      return this.geometries_.length === 0;
    };
    GeometryCollection2.prototype.rotate = function(angle, anchor) {
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        geometries[i].rotate(angle, anchor);
      }
      this.changed();
    };
    GeometryCollection2.prototype.scale = function(sx, opt_sy, opt_anchor) {
      var anchor = opt_anchor;
      if (!anchor) {
        anchor = getCenter(this.getExtent());
      }
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        geometries[i].scale(sx, opt_sy, anchor);
      }
      this.changed();
    };
    GeometryCollection2.prototype.setGeometries = function(geometries) {
      this.setGeometriesArray(cloneGeometries(geometries));
    };
    GeometryCollection2.prototype.setGeometriesArray = function(geometries) {
      this.unlistenGeometriesChange_();
      this.geometries_ = geometries;
      this.listenGeometriesChange_();
      this.changed();
    };
    GeometryCollection2.prototype.applyTransform = function(transformFn) {
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        geometries[i].applyTransform(transformFn);
      }
      this.changed();
    };
    GeometryCollection2.prototype.translate = function(deltaX, deltaY) {
      var geometries = this.geometries_;
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        geometries[i].translate(deltaX, deltaY);
      }
      this.changed();
    };
    GeometryCollection2.prototype.disposeInternal = function() {
      this.unlistenGeometriesChange_();
      _super.prototype.disposeInternal.call(this);
    };
    return GeometryCollection2;
  }(Geometry_default);
  function cloneGeometries(geometries) {
    var clonedGeometries = [];
    for (var i = 0, ii = geometries.length; i < ii; ++i) {
      clonedGeometries.push(geometries[i].clone());
    }
    return clonedGeometries;
  }
  var GeometryCollection_default = GeometryCollection;

  // node_modules/ol/format/Feature.js
  var FeatureFormat = function() {
    function FeatureFormat2() {
      this.dataProjection = void 0;
      this.defaultFeatureProjection = void 0;
      this.supportedMediaTypes = null;
    }
    FeatureFormat2.prototype.getReadOptions = function(source, opt_options) {
      var options;
      if (opt_options) {
        var dataProjection = opt_options.dataProjection ? get3(opt_options.dataProjection) : this.readProjection(source);
        if (opt_options.extent && dataProjection && dataProjection.getUnits() === Units_default.TILE_PIXELS) {
          dataProjection = get3(dataProjection);
          dataProjection.setWorldExtent(opt_options.extent);
        }
        options = {
          dataProjection,
          featureProjection: opt_options.featureProjection
        };
      }
      return this.adaptOptions(options);
    };
    FeatureFormat2.prototype.adaptOptions = function(options) {
      return assign({
        dataProjection: this.dataProjection,
        featureProjection: this.defaultFeatureProjection
      }, options);
    };
    FeatureFormat2.prototype.getType = function() {
      return abstract();
    };
    FeatureFormat2.prototype.readFeature = function(source, opt_options) {
      return abstract();
    };
    FeatureFormat2.prototype.readFeatures = function(source, opt_options) {
      return abstract();
    };
    FeatureFormat2.prototype.readGeometry = function(source, opt_options) {
      return abstract();
    };
    FeatureFormat2.prototype.readProjection = function(source) {
      return abstract();
    };
    FeatureFormat2.prototype.writeFeature = function(feature, opt_options) {
      return abstract();
    };
    FeatureFormat2.prototype.writeFeatures = function(features, opt_options) {
      return abstract();
    };
    FeatureFormat2.prototype.writeGeometry = function(geometry, opt_options) {
      return abstract();
    };
    return FeatureFormat2;
  }();
  var Feature_default2 = FeatureFormat;
  function transformGeometryWithOptions(geometry, write, opt_options) {
    var featureProjection = opt_options ? get3(opt_options.featureProjection) : null;
    var dataProjection = opt_options ? get3(opt_options.dataProjection) : null;
    var transformed;
    if (featureProjection && dataProjection && !equivalent(featureProjection, dataProjection)) {
      transformed = (write ? geometry.clone() : geometry).transform(write ? featureProjection : dataProjection, write ? dataProjection : featureProjection);
    } else {
      transformed = geometry;
    }
    if (write && opt_options && opt_options.decimals !== void 0) {
      var power_1 = Math.pow(10, opt_options.decimals);
      var transform2 = function(coordinates2) {
        for (var i = 0, ii = coordinates2.length; i < ii; ++i) {
          coordinates2[i] = Math.round(coordinates2[i] * power_1) / power_1;
        }
        return coordinates2;
      };
      if (transformed === geometry) {
        transformed = geometry.clone();
      }
      transformed.applyTransform(transform2);
    }
    return transformed;
  }

  // node_modules/ol/format/FormatType.js
  var FormatType_default = {
    ARRAY_BUFFER: "arraybuffer",
    JSON: "json",
    TEXT: "text",
    XML: "xml"
  };

  // node_modules/ol/format/JSONFeature.js
  var __extends10 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var JSONFeature = function(_super) {
    __extends10(JSONFeature2, _super);
    function JSONFeature2() {
      return _super.call(this) || this;
    }
    JSONFeature2.prototype.getType = function() {
      return FormatType_default.JSON;
    };
    JSONFeature2.prototype.readFeature = function(source, opt_options) {
      return this.readFeatureFromObject(getObject(source), this.getReadOptions(source, opt_options));
    };
    JSONFeature2.prototype.readFeatures = function(source, opt_options) {
      return this.readFeaturesFromObject(getObject(source), this.getReadOptions(source, opt_options));
    };
    JSONFeature2.prototype.readFeatureFromObject = function(object, opt_options) {
      return abstract();
    };
    JSONFeature2.prototype.readFeaturesFromObject = function(object, opt_options) {
      return abstract();
    };
    JSONFeature2.prototype.readGeometry = function(source, opt_options) {
      return this.readGeometryFromObject(getObject(source), this.getReadOptions(source, opt_options));
    };
    JSONFeature2.prototype.readGeometryFromObject = function(object, opt_options) {
      return abstract();
    };
    JSONFeature2.prototype.readProjection = function(source) {
      return this.readProjectionFromObject(getObject(source));
    };
    JSONFeature2.prototype.readProjectionFromObject = function(object) {
      return abstract();
    };
    JSONFeature2.prototype.writeFeature = function(feature, opt_options) {
      return JSON.stringify(this.writeFeatureObject(feature, opt_options));
    };
    JSONFeature2.prototype.writeFeatureObject = function(feature, opt_options) {
      return abstract();
    };
    JSONFeature2.prototype.writeFeatures = function(features, opt_options) {
      return JSON.stringify(this.writeFeaturesObject(features, opt_options));
    };
    JSONFeature2.prototype.writeFeaturesObject = function(features, opt_options) {
      return abstract();
    };
    JSONFeature2.prototype.writeGeometry = function(geometry, opt_options) {
      return JSON.stringify(this.writeGeometryObject(geometry, opt_options));
    };
    JSONFeature2.prototype.writeGeometryObject = function(geometry, opt_options) {
      return abstract();
    };
    return JSONFeature2;
  }(Feature_default2);
  function getObject(source) {
    if (typeof source === "string") {
      var object = JSON.parse(source);
      return object ? object : null;
    } else if (source !== null) {
      return source;
    } else {
      return null;
    }
  }
  var JSONFeature_default = JSONFeature;

  // node_modules/ol/geom/GeometryLayout.js
  var GeometryLayout_default = {
    XY: "XY",
    XYZ: "XYZ",
    XYM: "XYM",
    XYZM: "XYZM"
  };

  // node_modules/ol/geom/SimpleGeometry.js
  var __extends11 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var SimpleGeometry = function(_super) {
    __extends11(SimpleGeometry2, _super);
    function SimpleGeometry2() {
      var _this = _super.call(this) || this;
      _this.layout = GeometryLayout_default.XY;
      _this.stride = 2;
      _this.flatCoordinates = null;
      return _this;
    }
    SimpleGeometry2.prototype.computeExtent = function(extent) {
      return createOrUpdateFromFlatCoordinates(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, extent);
    };
    SimpleGeometry2.prototype.getCoordinates = function() {
      return abstract();
    };
    SimpleGeometry2.prototype.getFirstCoordinate = function() {
      return this.flatCoordinates.slice(0, this.stride);
    };
    SimpleGeometry2.prototype.getFlatCoordinates = function() {
      return this.flatCoordinates;
    };
    SimpleGeometry2.prototype.getLastCoordinate = function() {
      return this.flatCoordinates.slice(this.flatCoordinates.length - this.stride);
    };
    SimpleGeometry2.prototype.getLayout = function() {
      return this.layout;
    };
    SimpleGeometry2.prototype.getSimplifiedGeometry = function(squaredTolerance) {
      if (this.simplifiedGeometryRevision !== this.getRevision()) {
        this.simplifiedGeometryMaxMinSquaredTolerance = 0;
        this.simplifiedGeometryRevision = this.getRevision();
      }
      if (squaredTolerance < 0 || this.simplifiedGeometryMaxMinSquaredTolerance !== 0 && squaredTolerance <= this.simplifiedGeometryMaxMinSquaredTolerance) {
        return this;
      }
      var simplifiedGeometry = this.getSimplifiedGeometryInternal(squaredTolerance);
      var simplifiedFlatCoordinates = simplifiedGeometry.getFlatCoordinates();
      if (simplifiedFlatCoordinates.length < this.flatCoordinates.length) {
        return simplifiedGeometry;
      } else {
        this.simplifiedGeometryMaxMinSquaredTolerance = squaredTolerance;
        return this;
      }
    };
    SimpleGeometry2.prototype.getSimplifiedGeometryInternal = function(squaredTolerance) {
      return this;
    };
    SimpleGeometry2.prototype.getStride = function() {
      return this.stride;
    };
    SimpleGeometry2.prototype.setFlatCoordinates = function(layout, flatCoordinates) {
      this.stride = getStrideForLayout(layout);
      this.layout = layout;
      this.flatCoordinates = flatCoordinates;
    };
    SimpleGeometry2.prototype.setCoordinates = function(coordinates2, opt_layout) {
      abstract();
    };
    SimpleGeometry2.prototype.setLayout = function(layout, coordinates2, nesting) {
      var stride;
      if (layout) {
        stride = getStrideForLayout(layout);
      } else {
        for (var i = 0; i < nesting; ++i) {
          if (coordinates2.length === 0) {
            this.layout = GeometryLayout_default.XY;
            this.stride = 2;
            return;
          } else {
            coordinates2 = coordinates2[0];
          }
        }
        stride = coordinates2.length;
        layout = getLayoutForStride(stride);
      }
      this.layout = layout;
      this.stride = stride;
    };
    SimpleGeometry2.prototype.applyTransform = function(transformFn) {
      if (this.flatCoordinates) {
        transformFn(this.flatCoordinates, this.flatCoordinates, this.stride);
        this.changed();
      }
    };
    SimpleGeometry2.prototype.rotate = function(angle, anchor) {
      var flatCoordinates = this.getFlatCoordinates();
      if (flatCoordinates) {
        var stride = this.getStride();
        rotate(flatCoordinates, 0, flatCoordinates.length, stride, angle, anchor, flatCoordinates);
        this.changed();
      }
    };
    SimpleGeometry2.prototype.scale = function(sx, opt_sy, opt_anchor) {
      var sy = opt_sy;
      if (sy === void 0) {
        sy = sx;
      }
      var anchor = opt_anchor;
      if (!anchor) {
        anchor = getCenter(this.getExtent());
      }
      var flatCoordinates = this.getFlatCoordinates();
      if (flatCoordinates) {
        var stride = this.getStride();
        scale(flatCoordinates, 0, flatCoordinates.length, stride, sx, sy, anchor, flatCoordinates);
        this.changed();
      }
    };
    SimpleGeometry2.prototype.translate = function(deltaX, deltaY) {
      var flatCoordinates = this.getFlatCoordinates();
      if (flatCoordinates) {
        var stride = this.getStride();
        translate(flatCoordinates, 0, flatCoordinates.length, stride, deltaX, deltaY, flatCoordinates);
        this.changed();
      }
    };
    return SimpleGeometry2;
  }(Geometry_default);
  function getLayoutForStride(stride) {
    var layout;
    if (stride == 2) {
      layout = GeometryLayout_default.XY;
    } else if (stride == 3) {
      layout = GeometryLayout_default.XYZ;
    } else if (stride == 4) {
      layout = GeometryLayout_default.XYZM;
    }
    return layout;
  }
  function getStrideForLayout(layout) {
    var stride;
    if (layout == GeometryLayout_default.XY) {
      stride = 2;
    } else if (layout == GeometryLayout_default.XYZ || layout == GeometryLayout_default.XYM) {
      stride = 3;
    } else if (layout == GeometryLayout_default.XYZM) {
      stride = 4;
    }
    return stride;
  }
  function transformGeom2D(simpleGeometry, transform2, opt_dest) {
    var flatCoordinates = simpleGeometry.getFlatCoordinates();
    if (!flatCoordinates) {
      return null;
    } else {
      var stride = simpleGeometry.getStride();
      return transform2D(flatCoordinates, 0, flatCoordinates.length, stride, transform2, opt_dest);
    }
  }
  var SimpleGeometry_default = SimpleGeometry;

  // node_modules/ol/geom/flat/closest.js
  function assignClosest(flatCoordinates, offset1, offset2, stride, x, y, closestPoint) {
    var x1 = flatCoordinates[offset1];
    var y1 = flatCoordinates[offset1 + 1];
    var dx = flatCoordinates[offset2] - x1;
    var dy = flatCoordinates[offset2 + 1] - y1;
    var offset;
    if (dx === 0 && dy === 0) {
      offset = offset1;
    } else {
      var t = ((x - x1) * dx + (y - y1) * dy) / (dx * dx + dy * dy);
      if (t > 1) {
        offset = offset2;
      } else if (t > 0) {
        for (var i = 0; i < stride; ++i) {
          closestPoint[i] = lerp(flatCoordinates[offset1 + i], flatCoordinates[offset2 + i], t);
        }
        closestPoint.length = stride;
        return;
      } else {
        offset = offset1;
      }
    }
    for (var i = 0; i < stride; ++i) {
      closestPoint[i] = flatCoordinates[offset + i];
    }
    closestPoint.length = stride;
  }
  function maxSquaredDelta(flatCoordinates, offset, end, stride, max) {
    var x1 = flatCoordinates[offset];
    var y1 = flatCoordinates[offset + 1];
    for (offset += stride; offset < end; offset += stride) {
      var x2 = flatCoordinates[offset];
      var y2 = flatCoordinates[offset + 1];
      var squaredDelta = squaredDistance(x1, y1, x2, y2);
      if (squaredDelta > max) {
        max = squaredDelta;
      }
      x1 = x2;
      y1 = y2;
    }
    return max;
  }
  function arrayMaxSquaredDelta(flatCoordinates, offset, ends, stride, max) {
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      var end = ends[i];
      max = maxSquaredDelta(flatCoordinates, offset, end, stride, max);
      offset = end;
    }
    return max;
  }
  function multiArrayMaxSquaredDelta(flatCoordinates, offset, endss, stride, max) {
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      max = arrayMaxSquaredDelta(flatCoordinates, offset, ends, stride, max);
      offset = ends[ends.length - 1];
    }
    return max;
  }
  function assignClosestPoint(flatCoordinates, offset, end, stride, maxDelta, isRing, x, y, closestPoint, minSquaredDistance, opt_tmpPoint) {
    if (offset == end) {
      return minSquaredDistance;
    }
    var i, squaredDistance3;
    if (maxDelta === 0) {
      squaredDistance3 = squaredDistance(x, y, flatCoordinates[offset], flatCoordinates[offset + 1]);
      if (squaredDistance3 < minSquaredDistance) {
        for (i = 0; i < stride; ++i) {
          closestPoint[i] = flatCoordinates[offset + i];
        }
        closestPoint.length = stride;
        return squaredDistance3;
      } else {
        return minSquaredDistance;
      }
    }
    var tmpPoint = opt_tmpPoint ? opt_tmpPoint : [NaN, NaN];
    var index = offset + stride;
    while (index < end) {
      assignClosest(flatCoordinates, index - stride, index, stride, x, y, tmpPoint);
      squaredDistance3 = squaredDistance(x, y, tmpPoint[0], tmpPoint[1]);
      if (squaredDistance3 < minSquaredDistance) {
        minSquaredDistance = squaredDistance3;
        for (i = 0; i < stride; ++i) {
          closestPoint[i] = tmpPoint[i];
        }
        closestPoint.length = stride;
        index += stride;
      } else {
        index += stride * Math.max((Math.sqrt(squaredDistance3) - Math.sqrt(minSquaredDistance)) / maxDelta | 0, 1);
      }
    }
    if (isRing) {
      assignClosest(flatCoordinates, end - stride, offset, stride, x, y, tmpPoint);
      squaredDistance3 = squaredDistance(x, y, tmpPoint[0], tmpPoint[1]);
      if (squaredDistance3 < minSquaredDistance) {
        minSquaredDistance = squaredDistance3;
        for (i = 0; i < stride; ++i) {
          closestPoint[i] = tmpPoint[i];
        }
        closestPoint.length = stride;
      }
    }
    return minSquaredDistance;
  }
  function assignClosestArrayPoint(flatCoordinates, offset, ends, stride, maxDelta, isRing, x, y, closestPoint, minSquaredDistance, opt_tmpPoint) {
    var tmpPoint = opt_tmpPoint ? opt_tmpPoint : [NaN, NaN];
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      var end = ends[i];
      minSquaredDistance = assignClosestPoint(flatCoordinates, offset, end, stride, maxDelta, isRing, x, y, closestPoint, minSquaredDistance, tmpPoint);
      offset = end;
    }
    return minSquaredDistance;
  }
  function assignClosestMultiArrayPoint(flatCoordinates, offset, endss, stride, maxDelta, isRing, x, y, closestPoint, minSquaredDistance, opt_tmpPoint) {
    var tmpPoint = opt_tmpPoint ? opt_tmpPoint : [NaN, NaN];
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      minSquaredDistance = assignClosestArrayPoint(flatCoordinates, offset, ends, stride, maxDelta, isRing, x, y, closestPoint, minSquaredDistance, tmpPoint);
      offset = ends[ends.length - 1];
    }
    return minSquaredDistance;
  }

  // node_modules/ol/geom/flat/deflate.js
  function deflateCoordinate(flatCoordinates, offset, coordinate, stride) {
    for (var i = 0, ii = coordinate.length; i < ii; ++i) {
      flatCoordinates[offset++] = coordinate[i];
    }
    return offset;
  }
  function deflateCoordinates(flatCoordinates, offset, coordinates2, stride) {
    for (var i = 0, ii = coordinates2.length; i < ii; ++i) {
      var coordinate = coordinates2[i];
      for (var j = 0; j < stride; ++j) {
        flatCoordinates[offset++] = coordinate[j];
      }
    }
    return offset;
  }
  function deflateCoordinatesArray(flatCoordinates, offset, coordinatess, stride, opt_ends) {
    var ends = opt_ends ? opt_ends : [];
    var i = 0;
    for (var j = 0, jj = coordinatess.length; j < jj; ++j) {
      var end = deflateCoordinates(flatCoordinates, offset, coordinatess[j], stride);
      ends[i++] = end;
      offset = end;
    }
    ends.length = i;
    return ends;
  }
  function deflateMultiCoordinatesArray(flatCoordinates, offset, coordinatesss, stride, opt_endss) {
    var endss = opt_endss ? opt_endss : [];
    var i = 0;
    for (var j = 0, jj = coordinatesss.length; j < jj; ++j) {
      var ends = deflateCoordinatesArray(flatCoordinates, offset, coordinatesss[j], stride, endss[i]);
      endss[i++] = ends;
      offset = ends[ends.length - 1];
    }
    endss.length = i;
    return endss;
  }

  // node_modules/ol/geom/flat/simplify.js
  function douglasPeucker(flatCoordinates, offset, end, stride, squaredTolerance, simplifiedFlatCoordinates, simplifiedOffset) {
    var n = (end - offset) / stride;
    if (n < 3) {
      for (; offset < end; offset += stride) {
        simplifiedFlatCoordinates[simplifiedOffset++] = flatCoordinates[offset];
        simplifiedFlatCoordinates[simplifiedOffset++] = flatCoordinates[offset + 1];
      }
      return simplifiedOffset;
    }
    var markers = new Array(n);
    markers[0] = 1;
    markers[n - 1] = 1;
    var stack = [offset, end - stride];
    var index = 0;
    while (stack.length > 0) {
      var last = stack.pop();
      var first = stack.pop();
      var maxSquaredDistance = 0;
      var x1 = flatCoordinates[first];
      var y1 = flatCoordinates[first + 1];
      var x2 = flatCoordinates[last];
      var y2 = flatCoordinates[last + 1];
      for (var i = first + stride; i < last; i += stride) {
        var x = flatCoordinates[i];
        var y = flatCoordinates[i + 1];
        var squaredDistance_1 = squaredSegmentDistance(x, y, x1, y1, x2, y2);
        if (squaredDistance_1 > maxSquaredDistance) {
          index = i;
          maxSquaredDistance = squaredDistance_1;
        }
      }
      if (maxSquaredDistance > squaredTolerance) {
        markers[(index - offset) / stride] = 1;
        if (first + stride < index) {
          stack.push(first, index);
        }
        if (index + stride < last) {
          stack.push(index, last);
        }
      }
    }
    for (var i = 0; i < n; ++i) {
      if (markers[i]) {
        simplifiedFlatCoordinates[simplifiedOffset++] = flatCoordinates[offset + i * stride];
        simplifiedFlatCoordinates[simplifiedOffset++] = flatCoordinates[offset + i * stride + 1];
      }
    }
    return simplifiedOffset;
  }
  function douglasPeuckerArray(flatCoordinates, offset, ends, stride, squaredTolerance, simplifiedFlatCoordinates, simplifiedOffset, simplifiedEnds) {
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      var end = ends[i];
      simplifiedOffset = douglasPeucker(flatCoordinates, offset, end, stride, squaredTolerance, simplifiedFlatCoordinates, simplifiedOffset);
      simplifiedEnds.push(simplifiedOffset);
      offset = end;
    }
    return simplifiedOffset;
  }
  function snap(value, tolerance) {
    return tolerance * Math.round(value / tolerance);
  }
  function quantize(flatCoordinates, offset, end, stride, tolerance, simplifiedFlatCoordinates, simplifiedOffset) {
    if (offset == end) {
      return simplifiedOffset;
    }
    var x1 = snap(flatCoordinates[offset], tolerance);
    var y1 = snap(flatCoordinates[offset + 1], tolerance);
    offset += stride;
    simplifiedFlatCoordinates[simplifiedOffset++] = x1;
    simplifiedFlatCoordinates[simplifiedOffset++] = y1;
    var x2, y2;
    do {
      x2 = snap(flatCoordinates[offset], tolerance);
      y2 = snap(flatCoordinates[offset + 1], tolerance);
      offset += stride;
      if (offset == end) {
        simplifiedFlatCoordinates[simplifiedOffset++] = x2;
        simplifiedFlatCoordinates[simplifiedOffset++] = y2;
        return simplifiedOffset;
      }
    } while (x2 == x1 && y2 == y1);
    while (offset < end) {
      var x3 = snap(flatCoordinates[offset], tolerance);
      var y3 = snap(flatCoordinates[offset + 1], tolerance);
      offset += stride;
      if (x3 == x2 && y3 == y2) {
        continue;
      }
      var dx1 = x2 - x1;
      var dy1 = y2 - y1;
      var dx2 = x3 - x1;
      var dy2 = y3 - y1;
      if (dx1 * dy2 == dy1 * dx2 && (dx1 < 0 && dx2 < dx1 || dx1 == dx2 || dx1 > 0 && dx2 > dx1) && (dy1 < 0 && dy2 < dy1 || dy1 == dy2 || dy1 > 0 && dy2 > dy1)) {
        x2 = x3;
        y2 = y3;
        continue;
      }
      simplifiedFlatCoordinates[simplifiedOffset++] = x2;
      simplifiedFlatCoordinates[simplifiedOffset++] = y2;
      x1 = x2;
      y1 = y2;
      x2 = x3;
      y2 = y3;
    }
    simplifiedFlatCoordinates[simplifiedOffset++] = x2;
    simplifiedFlatCoordinates[simplifiedOffset++] = y2;
    return simplifiedOffset;
  }
  function quantizeArray(flatCoordinates, offset, ends, stride, tolerance, simplifiedFlatCoordinates, simplifiedOffset, simplifiedEnds) {
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      var end = ends[i];
      simplifiedOffset = quantize(flatCoordinates, offset, end, stride, tolerance, simplifiedFlatCoordinates, simplifiedOffset);
      simplifiedEnds.push(simplifiedOffset);
      offset = end;
    }
    return simplifiedOffset;
  }
  function quantizeMultiArray(flatCoordinates, offset, endss, stride, tolerance, simplifiedFlatCoordinates, simplifiedOffset, simplifiedEndss) {
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      var simplifiedEnds = [];
      simplifiedOffset = quantizeArray(flatCoordinates, offset, ends, stride, tolerance, simplifiedFlatCoordinates, simplifiedOffset, simplifiedEnds);
      simplifiedEndss.push(simplifiedEnds);
      offset = ends[ends.length - 1];
    }
    return simplifiedOffset;
  }

  // node_modules/ol/geom/flat/segments.js
  function forEach(flatCoordinates, offset, end, stride, callback) {
    var ret;
    offset += stride;
    for (; offset < end; offset += stride) {
      ret = callback(flatCoordinates.slice(offset - stride, offset), flatCoordinates.slice(offset, offset + stride));
      if (ret) {
        return ret;
      }
    }
    return false;
  }

  // node_modules/ol/geom/flat/inflate.js
  function inflateCoordinates(flatCoordinates, offset, end, stride, opt_coordinates) {
    var coordinates2 = opt_coordinates !== void 0 ? opt_coordinates : [];
    var i = 0;
    for (var j = offset; j < end; j += stride) {
      coordinates2[i++] = flatCoordinates.slice(j, j + stride);
    }
    coordinates2.length = i;
    return coordinates2;
  }
  function inflateCoordinatesArray(flatCoordinates, offset, ends, stride, opt_coordinatess) {
    var coordinatess = opt_coordinatess !== void 0 ? opt_coordinatess : [];
    var i = 0;
    for (var j = 0, jj = ends.length; j < jj; ++j) {
      var end = ends[j];
      coordinatess[i++] = inflateCoordinates(flatCoordinates, offset, end, stride, coordinatess[i]);
      offset = end;
    }
    coordinatess.length = i;
    return coordinatess;
  }
  function inflateMultiCoordinatesArray(flatCoordinates, offset, endss, stride, opt_coordinatesss) {
    var coordinatesss = opt_coordinatesss !== void 0 ? opt_coordinatesss : [];
    var i = 0;
    for (var j = 0, jj = endss.length; j < jj; ++j) {
      var ends = endss[j];
      coordinatesss[i++] = inflateCoordinatesArray(flatCoordinates, offset, ends, stride, coordinatesss[i]);
      offset = ends[ends.length - 1];
    }
    coordinatesss.length = i;
    return coordinatesss;
  }

  // node_modules/ol/geom/flat/interpolate.js
  function interpolatePoint(flatCoordinates, offset, end, stride, fraction, opt_dest, opt_dimension) {
    var o, t;
    var n = (end - offset) / stride;
    if (n === 1) {
      o = offset;
    } else if (n === 2) {
      o = offset;
      t = fraction;
    } else if (n !== 0) {
      var x1 = flatCoordinates[offset];
      var y1 = flatCoordinates[offset + 1];
      var length_1 = 0;
      var cumulativeLengths = [0];
      for (var i = offset + stride; i < end; i += stride) {
        var x2 = flatCoordinates[i];
        var y2 = flatCoordinates[i + 1];
        length_1 += Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        cumulativeLengths.push(length_1);
        x1 = x2;
        y1 = y2;
      }
      var target = fraction * length_1;
      var index = binarySearch(cumulativeLengths, target);
      if (index < 0) {
        t = (target - cumulativeLengths[-index - 2]) / (cumulativeLengths[-index - 1] - cumulativeLengths[-index - 2]);
        o = offset + (-index - 2) * stride;
      } else {
        o = offset + index * stride;
      }
    }
    var dimension = opt_dimension > 1 ? opt_dimension : 2;
    var dest = opt_dest ? opt_dest : new Array(dimension);
    for (var i = 0; i < dimension; ++i) {
      dest[i] = o === void 0 ? NaN : t === void 0 ? flatCoordinates[o + i] : lerp(flatCoordinates[o + i], flatCoordinates[o + stride + i], t);
    }
    return dest;
  }
  function lineStringCoordinateAtM(flatCoordinates, offset, end, stride, m, extrapolate) {
    if (end == offset) {
      return null;
    }
    var coordinate;
    if (m < flatCoordinates[offset + stride - 1]) {
      if (extrapolate) {
        coordinate = flatCoordinates.slice(offset, offset + stride);
        coordinate[stride - 1] = m;
        return coordinate;
      } else {
        return null;
      }
    } else if (flatCoordinates[end - 1] < m) {
      if (extrapolate) {
        coordinate = flatCoordinates.slice(end - stride, end);
        coordinate[stride - 1] = m;
        return coordinate;
      } else {
        return null;
      }
    }
    if (m == flatCoordinates[offset + stride - 1]) {
      return flatCoordinates.slice(offset, offset + stride);
    }
    var lo = offset / stride;
    var hi = end / stride;
    while (lo < hi) {
      var mid = lo + hi >> 1;
      if (m < flatCoordinates[(mid + 1) * stride - 1]) {
        hi = mid;
      } else {
        lo = mid + 1;
      }
    }
    var m0 = flatCoordinates[lo * stride - 1];
    if (m == m0) {
      return flatCoordinates.slice((lo - 1) * stride, (lo - 1) * stride + stride);
    }
    var m1 = flatCoordinates[(lo + 1) * stride - 1];
    var t = (m - m0) / (m1 - m0);
    coordinate = [];
    for (var i = 0; i < stride - 1; ++i) {
      coordinate.push(lerp(flatCoordinates[(lo - 1) * stride + i], flatCoordinates[lo * stride + i], t));
    }
    coordinate.push(m);
    return coordinate;
  }
  function lineStringsCoordinateAtM(flatCoordinates, offset, ends, stride, m, extrapolate, interpolate) {
    if (interpolate) {
      return lineStringCoordinateAtM(flatCoordinates, offset, ends[ends.length - 1], stride, m, extrapolate);
    }
    var coordinate;
    if (m < flatCoordinates[stride - 1]) {
      if (extrapolate) {
        coordinate = flatCoordinates.slice(0, stride);
        coordinate[stride - 1] = m;
        return coordinate;
      } else {
        return null;
      }
    }
    if (flatCoordinates[flatCoordinates.length - 1] < m) {
      if (extrapolate) {
        coordinate = flatCoordinates.slice(flatCoordinates.length - stride);
        coordinate[stride - 1] = m;
        return coordinate;
      } else {
        return null;
      }
    }
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      var end = ends[i];
      if (offset == end) {
        continue;
      }
      if (m < flatCoordinates[offset + stride - 1]) {
        return null;
      } else if (m <= flatCoordinates[end - 1]) {
        return lineStringCoordinateAtM(flatCoordinates, offset, end, stride, m, false);
      }
      offset = end;
    }
    return null;
  }

  // node_modules/ol/geom/flat/contains.js
  function linearRingContainsExtent(flatCoordinates, offset, end, stride, extent) {
    var outside = forEachCorner(extent, function(coordinate) {
      return !linearRingContainsXY(flatCoordinates, offset, end, stride, coordinate[0], coordinate[1]);
    });
    return !outside;
  }
  function linearRingContainsXY(flatCoordinates, offset, end, stride, x, y) {
    var wn = 0;
    var x1 = flatCoordinates[end - stride];
    var y1 = flatCoordinates[end - stride + 1];
    for (; offset < end; offset += stride) {
      var x2 = flatCoordinates[offset];
      var y2 = flatCoordinates[offset + 1];
      if (y1 <= y) {
        if (y2 > y && (x2 - x1) * (y - y1) - (x - x1) * (y2 - y1) > 0) {
          wn++;
        }
      } else if (y2 <= y && (x2 - x1) * (y - y1) - (x - x1) * (y2 - y1) < 0) {
        wn--;
      }
      x1 = x2;
      y1 = y2;
    }
    return wn !== 0;
  }
  function linearRingsContainsXY(flatCoordinates, offset, ends, stride, x, y) {
    if (ends.length === 0) {
      return false;
    }
    if (!linearRingContainsXY(flatCoordinates, offset, ends[0], stride, x, y)) {
      return false;
    }
    for (var i = 1, ii = ends.length; i < ii; ++i) {
      if (linearRingContainsXY(flatCoordinates, ends[i - 1], ends[i], stride, x, y)) {
        return false;
      }
    }
    return true;
  }
  function linearRingssContainsXY(flatCoordinates, offset, endss, stride, x, y) {
    if (endss.length === 0) {
      return false;
    }
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      if (linearRingsContainsXY(flatCoordinates, offset, ends, stride, x, y)) {
        return true;
      }
      offset = ends[ends.length - 1];
    }
    return false;
  }

  // node_modules/ol/geom/flat/intersectsextent.js
  function intersectsLineString(flatCoordinates, offset, end, stride, extent) {
    var coordinatesExtent = extendFlatCoordinates(createEmpty(), flatCoordinates, offset, end, stride);
    if (!intersects(extent, coordinatesExtent)) {
      return false;
    }
    if (containsExtent(extent, coordinatesExtent)) {
      return true;
    }
    if (coordinatesExtent[0] >= extent[0] && coordinatesExtent[2] <= extent[2]) {
      return true;
    }
    if (coordinatesExtent[1] >= extent[1] && coordinatesExtent[3] <= extent[3]) {
      return true;
    }
    return forEach(flatCoordinates, offset, end, stride, function(point1, point2) {
      return intersectsSegment(extent, point1, point2);
    });
  }
  function intersectsLineStringArray(flatCoordinates, offset, ends, stride, extent) {
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      if (intersectsLineString(flatCoordinates, offset, ends[i], stride, extent)) {
        return true;
      }
      offset = ends[i];
    }
    return false;
  }
  function intersectsLinearRing(flatCoordinates, offset, end, stride, extent) {
    if (intersectsLineString(flatCoordinates, offset, end, stride, extent)) {
      return true;
    }
    if (linearRingContainsXY(flatCoordinates, offset, end, stride, extent[0], extent[1])) {
      return true;
    }
    if (linearRingContainsXY(flatCoordinates, offset, end, stride, extent[0], extent[3])) {
      return true;
    }
    if (linearRingContainsXY(flatCoordinates, offset, end, stride, extent[2], extent[1])) {
      return true;
    }
    if (linearRingContainsXY(flatCoordinates, offset, end, stride, extent[2], extent[3])) {
      return true;
    }
    return false;
  }
  function intersectsLinearRingArray(flatCoordinates, offset, ends, stride, extent) {
    if (!intersectsLinearRing(flatCoordinates, offset, ends[0], stride, extent)) {
      return false;
    }
    if (ends.length === 1) {
      return true;
    }
    for (var i = 1, ii = ends.length; i < ii; ++i) {
      if (linearRingContainsExtent(flatCoordinates, ends[i - 1], ends[i], stride, extent)) {
        if (!intersectsLineString(flatCoordinates, ends[i - 1], ends[i], stride, extent)) {
          return false;
        }
      }
    }
    return true;
  }
  function intersectsLinearRingMultiArray(flatCoordinates, offset, endss, stride, extent) {
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      if (intersectsLinearRingArray(flatCoordinates, offset, ends, stride, extent)) {
        return true;
      }
      offset = ends[ends.length - 1];
    }
    return false;
  }

  // node_modules/ol/geom/flat/length.js
  function lineStringLength(flatCoordinates, offset, end, stride) {
    var x1 = flatCoordinates[offset];
    var y1 = flatCoordinates[offset + 1];
    var length = 0;
    for (var i = offset + stride; i < end; i += stride) {
      var x2 = flatCoordinates[i];
      var y2 = flatCoordinates[i + 1];
      length += Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
      x1 = x2;
      y1 = y2;
    }
    return length;
  }

  // node_modules/ol/geom/LineString.js
  var __extends12 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var LineString = function(_super) {
    __extends12(LineString2, _super);
    function LineString2(coordinates2, opt_layout) {
      var _this = _super.call(this) || this;
      _this.flatMidpoint_ = null;
      _this.flatMidpointRevision_ = -1;
      _this.maxDelta_ = -1;
      _this.maxDeltaRevision_ = -1;
      if (opt_layout !== void 0 && !Array.isArray(coordinates2[0])) {
        _this.setFlatCoordinates(opt_layout, coordinates2);
      } else {
        _this.setCoordinates(coordinates2, opt_layout);
      }
      return _this;
    }
    LineString2.prototype.appendCoordinate = function(coordinate) {
      if (!this.flatCoordinates) {
        this.flatCoordinates = coordinate.slice();
      } else {
        extend(this.flatCoordinates, coordinate);
      }
      this.changed();
    };
    LineString2.prototype.clone = function() {
      var lineString = new LineString2(this.flatCoordinates.slice(), this.layout);
      lineString.applyProperties(this);
      return lineString;
    };
    LineString2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      if (minSquaredDistance < closestSquaredDistanceXY(this.getExtent(), x, y)) {
        return minSquaredDistance;
      }
      if (this.maxDeltaRevision_ != this.getRevision()) {
        this.maxDelta_ = Math.sqrt(maxSquaredDelta(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, 0));
        this.maxDeltaRevision_ = this.getRevision();
      }
      return assignClosestPoint(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, this.maxDelta_, false, x, y, closestPoint, minSquaredDistance);
    };
    LineString2.prototype.forEachSegment = function(callback) {
      return forEach(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, callback);
    };
    LineString2.prototype.getCoordinateAtM = function(m, opt_extrapolate) {
      if (this.layout != GeometryLayout_default.XYM && this.layout != GeometryLayout_default.XYZM) {
        return null;
      }
      var extrapolate = opt_extrapolate !== void 0 ? opt_extrapolate : false;
      return lineStringCoordinateAtM(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, m, extrapolate);
    };
    LineString2.prototype.getCoordinates = function() {
      return inflateCoordinates(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride);
    };
    LineString2.prototype.getCoordinateAt = function(fraction, opt_dest) {
      return interpolatePoint(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, fraction, opt_dest, this.stride);
    };
    LineString2.prototype.getLength = function() {
      return lineStringLength(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride);
    };
    LineString2.prototype.getFlatMidpoint = function() {
      if (this.flatMidpointRevision_ != this.getRevision()) {
        this.flatMidpoint_ = this.getCoordinateAt(0.5, this.flatMidpoint_);
        this.flatMidpointRevision_ = this.getRevision();
      }
      return this.flatMidpoint_;
    };
    LineString2.prototype.getSimplifiedGeometryInternal = function(squaredTolerance) {
      var simplifiedFlatCoordinates = [];
      simplifiedFlatCoordinates.length = douglasPeucker(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, squaredTolerance, simplifiedFlatCoordinates, 0);
      return new LineString2(simplifiedFlatCoordinates, GeometryLayout_default.XY);
    };
    LineString2.prototype.getType = function() {
      return GeometryType_default.LINE_STRING;
    };
    LineString2.prototype.intersectsExtent = function(extent) {
      return intersectsLineString(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, extent);
    };
    LineString2.prototype.setCoordinates = function(coordinates2, opt_layout) {
      this.setLayout(opt_layout, coordinates2, 1);
      if (!this.flatCoordinates) {
        this.flatCoordinates = [];
      }
      this.flatCoordinates.length = deflateCoordinates(this.flatCoordinates, 0, coordinates2, this.stride);
      this.changed();
    };
    return LineString2;
  }(SimpleGeometry_default);
  var LineString_default = LineString;

  // node_modules/ol/geom/MultiLineString.js
  var __extends13 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var MultiLineString = function(_super) {
    __extends13(MultiLineString2, _super);
    function MultiLineString2(coordinates2, opt_layout, opt_ends) {
      var _this = _super.call(this) || this;
      _this.ends_ = [];
      _this.maxDelta_ = -1;
      _this.maxDeltaRevision_ = -1;
      if (Array.isArray(coordinates2[0])) {
        _this.setCoordinates(coordinates2, opt_layout);
      } else if (opt_layout !== void 0 && opt_ends) {
        _this.setFlatCoordinates(opt_layout, coordinates2);
        _this.ends_ = opt_ends;
      } else {
        var layout = _this.getLayout();
        var lineStrings = coordinates2;
        var flatCoordinates = [];
        var ends = [];
        for (var i = 0, ii = lineStrings.length; i < ii; ++i) {
          var lineString = lineStrings[i];
          if (i === 0) {
            layout = lineString.getLayout();
          }
          extend(flatCoordinates, lineString.getFlatCoordinates());
          ends.push(flatCoordinates.length);
        }
        _this.setFlatCoordinates(layout, flatCoordinates);
        _this.ends_ = ends;
      }
      return _this;
    }
    MultiLineString2.prototype.appendLineString = function(lineString) {
      if (!this.flatCoordinates) {
        this.flatCoordinates = lineString.getFlatCoordinates().slice();
      } else {
        extend(this.flatCoordinates, lineString.getFlatCoordinates().slice());
      }
      this.ends_.push(this.flatCoordinates.length);
      this.changed();
    };
    MultiLineString2.prototype.clone = function() {
      var multiLineString = new MultiLineString2(this.flatCoordinates.slice(), this.layout, this.ends_.slice());
      multiLineString.applyProperties(this);
      return multiLineString;
    };
    MultiLineString2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      if (minSquaredDistance < closestSquaredDistanceXY(this.getExtent(), x, y)) {
        return minSquaredDistance;
      }
      if (this.maxDeltaRevision_ != this.getRevision()) {
        this.maxDelta_ = Math.sqrt(arrayMaxSquaredDelta(this.flatCoordinates, 0, this.ends_, this.stride, 0));
        this.maxDeltaRevision_ = this.getRevision();
      }
      return assignClosestArrayPoint(this.flatCoordinates, 0, this.ends_, this.stride, this.maxDelta_, false, x, y, closestPoint, minSquaredDistance);
    };
    MultiLineString2.prototype.getCoordinateAtM = function(m, opt_extrapolate, opt_interpolate) {
      if (this.layout != GeometryLayout_default.XYM && this.layout != GeometryLayout_default.XYZM || this.flatCoordinates.length === 0) {
        return null;
      }
      var extrapolate = opt_extrapolate !== void 0 ? opt_extrapolate : false;
      var interpolate = opt_interpolate !== void 0 ? opt_interpolate : false;
      return lineStringsCoordinateAtM(this.flatCoordinates, 0, this.ends_, this.stride, m, extrapolate, interpolate);
    };
    MultiLineString2.prototype.getCoordinates = function() {
      return inflateCoordinatesArray(this.flatCoordinates, 0, this.ends_, this.stride);
    };
    MultiLineString2.prototype.getEnds = function() {
      return this.ends_;
    };
    MultiLineString2.prototype.getLineString = function(index) {
      if (index < 0 || this.ends_.length <= index) {
        return null;
      }
      return new LineString_default(this.flatCoordinates.slice(index === 0 ? 0 : this.ends_[index - 1], this.ends_[index]), this.layout);
    };
    MultiLineString2.prototype.getLineStrings = function() {
      var flatCoordinates = this.flatCoordinates;
      var ends = this.ends_;
      var layout = this.layout;
      var lineStrings = [];
      var offset = 0;
      for (var i = 0, ii = ends.length; i < ii; ++i) {
        var end = ends[i];
        var lineString = new LineString_default(flatCoordinates.slice(offset, end), layout);
        lineStrings.push(lineString);
        offset = end;
      }
      return lineStrings;
    };
    MultiLineString2.prototype.getFlatMidpoints = function() {
      var midpoints = [];
      var flatCoordinates = this.flatCoordinates;
      var offset = 0;
      var ends = this.ends_;
      var stride = this.stride;
      for (var i = 0, ii = ends.length; i < ii; ++i) {
        var end = ends[i];
        var midpoint = interpolatePoint(flatCoordinates, offset, end, stride, 0.5);
        extend(midpoints, midpoint);
        offset = end;
      }
      return midpoints;
    };
    MultiLineString2.prototype.getSimplifiedGeometryInternal = function(squaredTolerance) {
      var simplifiedFlatCoordinates = [];
      var simplifiedEnds = [];
      simplifiedFlatCoordinates.length = douglasPeuckerArray(this.flatCoordinates, 0, this.ends_, this.stride, squaredTolerance, simplifiedFlatCoordinates, 0, simplifiedEnds);
      return new MultiLineString2(simplifiedFlatCoordinates, GeometryLayout_default.XY, simplifiedEnds);
    };
    MultiLineString2.prototype.getType = function() {
      return GeometryType_default.MULTI_LINE_STRING;
    };
    MultiLineString2.prototype.intersectsExtent = function(extent) {
      return intersectsLineStringArray(this.flatCoordinates, 0, this.ends_, this.stride, extent);
    };
    MultiLineString2.prototype.setCoordinates = function(coordinates2, opt_layout) {
      this.setLayout(opt_layout, coordinates2, 2);
      if (!this.flatCoordinates) {
        this.flatCoordinates = [];
      }
      var ends = deflateCoordinatesArray(this.flatCoordinates, 0, coordinates2, this.stride, this.ends_);
      this.flatCoordinates.length = ends.length === 0 ? 0 : ends[ends.length - 1];
      this.changed();
    };
    return MultiLineString2;
  }(SimpleGeometry_default);
  var MultiLineString_default = MultiLineString;

  // node_modules/ol/geom/Point.js
  var __extends14 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Point = function(_super) {
    __extends14(Point2, _super);
    function Point2(coordinates2, opt_layout) {
      var _this = _super.call(this) || this;
      _this.setCoordinates(coordinates2, opt_layout);
      return _this;
    }
    Point2.prototype.clone = function() {
      var point = new Point2(this.flatCoordinates.slice(), this.layout);
      point.applyProperties(this);
      return point;
    };
    Point2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      var flatCoordinates = this.flatCoordinates;
      var squaredDistance3 = squaredDistance(x, y, flatCoordinates[0], flatCoordinates[1]);
      if (squaredDistance3 < minSquaredDistance) {
        var stride = this.stride;
        for (var i = 0; i < stride; ++i) {
          closestPoint[i] = flatCoordinates[i];
        }
        closestPoint.length = stride;
        return squaredDistance3;
      } else {
        return minSquaredDistance;
      }
    };
    Point2.prototype.getCoordinates = function() {
      return !this.flatCoordinates ? [] : this.flatCoordinates.slice();
    };
    Point2.prototype.computeExtent = function(extent) {
      return createOrUpdateFromCoordinate(this.flatCoordinates, extent);
    };
    Point2.prototype.getType = function() {
      return GeometryType_default.POINT;
    };
    Point2.prototype.intersectsExtent = function(extent) {
      return containsXY(extent, this.flatCoordinates[0], this.flatCoordinates[1]);
    };
    Point2.prototype.setCoordinates = function(coordinates2, opt_layout) {
      this.setLayout(opt_layout, coordinates2, 0);
      if (!this.flatCoordinates) {
        this.flatCoordinates = [];
      }
      this.flatCoordinates.length = deflateCoordinate(this.flatCoordinates, 0, coordinates2, this.stride);
      this.changed();
    };
    return Point2;
  }(SimpleGeometry_default);
  var Point_default = Point;

  // node_modules/ol/geom/MultiPoint.js
  var __extends15 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var MultiPoint = function(_super) {
    __extends15(MultiPoint2, _super);
    function MultiPoint2(coordinates2, opt_layout) {
      var _this = _super.call(this) || this;
      if (opt_layout && !Array.isArray(coordinates2[0])) {
        _this.setFlatCoordinates(opt_layout, coordinates2);
      } else {
        _this.setCoordinates(coordinates2, opt_layout);
      }
      return _this;
    }
    MultiPoint2.prototype.appendPoint = function(point) {
      if (!this.flatCoordinates) {
        this.flatCoordinates = point.getFlatCoordinates().slice();
      } else {
        extend(this.flatCoordinates, point.getFlatCoordinates());
      }
      this.changed();
    };
    MultiPoint2.prototype.clone = function() {
      var multiPoint = new MultiPoint2(this.flatCoordinates.slice(), this.layout);
      multiPoint.applyProperties(this);
      return multiPoint;
    };
    MultiPoint2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      if (minSquaredDistance < closestSquaredDistanceXY(this.getExtent(), x, y)) {
        return minSquaredDistance;
      }
      var flatCoordinates = this.flatCoordinates;
      var stride = this.stride;
      for (var i = 0, ii = flatCoordinates.length; i < ii; i += stride) {
        var squaredDistance3 = squaredDistance(x, y, flatCoordinates[i], flatCoordinates[i + 1]);
        if (squaredDistance3 < minSquaredDistance) {
          minSquaredDistance = squaredDistance3;
          for (var j = 0; j < stride; ++j) {
            closestPoint[j] = flatCoordinates[i + j];
          }
          closestPoint.length = stride;
        }
      }
      return minSquaredDistance;
    };
    MultiPoint2.prototype.getCoordinates = function() {
      return inflateCoordinates(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride);
    };
    MultiPoint2.prototype.getPoint = function(index) {
      var n = !this.flatCoordinates ? 0 : this.flatCoordinates.length / this.stride;
      if (index < 0 || n <= index) {
        return null;
      }
      return new Point_default(this.flatCoordinates.slice(index * this.stride, (index + 1) * this.stride), this.layout);
    };
    MultiPoint2.prototype.getPoints = function() {
      var flatCoordinates = this.flatCoordinates;
      var layout = this.layout;
      var stride = this.stride;
      var points = [];
      for (var i = 0, ii = flatCoordinates.length; i < ii; i += stride) {
        var point = new Point_default(flatCoordinates.slice(i, i + stride), layout);
        points.push(point);
      }
      return points;
    };
    MultiPoint2.prototype.getType = function() {
      return GeometryType_default.MULTI_POINT;
    };
    MultiPoint2.prototype.intersectsExtent = function(extent) {
      var flatCoordinates = this.flatCoordinates;
      var stride = this.stride;
      for (var i = 0, ii = flatCoordinates.length; i < ii; i += stride) {
        var x = flatCoordinates[i];
        var y = flatCoordinates[i + 1];
        if (containsXY(extent, x, y)) {
          return true;
        }
      }
      return false;
    };
    MultiPoint2.prototype.setCoordinates = function(coordinates2, opt_layout) {
      this.setLayout(opt_layout, coordinates2, 1);
      if (!this.flatCoordinates) {
        this.flatCoordinates = [];
      }
      this.flatCoordinates.length = deflateCoordinates(this.flatCoordinates, 0, coordinates2, this.stride);
      this.changed();
    };
    return MultiPoint2;
  }(SimpleGeometry_default);
  var MultiPoint_default = MultiPoint;

  // node_modules/ol/geom/flat/area.js
  function linearRing(flatCoordinates, offset, end, stride) {
    var twiceArea = 0;
    var x1 = flatCoordinates[end - stride];
    var y1 = flatCoordinates[end - stride + 1];
    for (; offset < end; offset += stride) {
      var x2 = flatCoordinates[offset];
      var y2 = flatCoordinates[offset + 1];
      twiceArea += y1 * x2 - x1 * y2;
      x1 = x2;
      y1 = y2;
    }
    return twiceArea / 2;
  }
  function linearRings(flatCoordinates, offset, ends, stride) {
    var area = 0;
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      var end = ends[i];
      area += linearRing(flatCoordinates, offset, end, stride);
      offset = end;
    }
    return area;
  }
  function linearRingss(flatCoordinates, offset, endss, stride) {
    var area = 0;
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      area += linearRings(flatCoordinates, offset, ends, stride);
      offset = ends[ends.length - 1];
    }
    return area;
  }

  // node_modules/ol/geom/LinearRing.js
  var __extends16 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var LinearRing = function(_super) {
    __extends16(LinearRing2, _super);
    function LinearRing2(coordinates2, opt_layout) {
      var _this = _super.call(this) || this;
      _this.maxDelta_ = -1;
      _this.maxDeltaRevision_ = -1;
      if (opt_layout !== void 0 && !Array.isArray(coordinates2[0])) {
        _this.setFlatCoordinates(opt_layout, coordinates2);
      } else {
        _this.setCoordinates(coordinates2, opt_layout);
      }
      return _this;
    }
    LinearRing2.prototype.clone = function() {
      return new LinearRing2(this.flatCoordinates.slice(), this.layout);
    };
    LinearRing2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      if (minSquaredDistance < closestSquaredDistanceXY(this.getExtent(), x, y)) {
        return minSquaredDistance;
      }
      if (this.maxDeltaRevision_ != this.getRevision()) {
        this.maxDelta_ = Math.sqrt(maxSquaredDelta(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, 0));
        this.maxDeltaRevision_ = this.getRevision();
      }
      return assignClosestPoint(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, this.maxDelta_, true, x, y, closestPoint, minSquaredDistance);
    };
    LinearRing2.prototype.getArea = function() {
      return linearRing(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride);
    };
    LinearRing2.prototype.getCoordinates = function() {
      return inflateCoordinates(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride);
    };
    LinearRing2.prototype.getSimplifiedGeometryInternal = function(squaredTolerance) {
      var simplifiedFlatCoordinates = [];
      simplifiedFlatCoordinates.length = douglasPeucker(this.flatCoordinates, 0, this.flatCoordinates.length, this.stride, squaredTolerance, simplifiedFlatCoordinates, 0);
      return new LinearRing2(simplifiedFlatCoordinates, GeometryLayout_default.XY);
    };
    LinearRing2.prototype.getType = function() {
      return GeometryType_default.LINEAR_RING;
    };
    LinearRing2.prototype.intersectsExtent = function(extent) {
      return false;
    };
    LinearRing2.prototype.setCoordinates = function(coordinates2, opt_layout) {
      this.setLayout(opt_layout, coordinates2, 1);
      if (!this.flatCoordinates) {
        this.flatCoordinates = [];
      }
      this.flatCoordinates.length = deflateCoordinates(this.flatCoordinates, 0, coordinates2, this.stride);
      this.changed();
    };
    return LinearRing2;
  }(SimpleGeometry_default);
  var LinearRing_default = LinearRing;

  // node_modules/ol/geom/flat/interiorpoint.js
  function getInteriorPointOfArray(flatCoordinates, offset, ends, stride, flatCenters, flatCentersOffset, opt_dest) {
    var i, ii, x, x1, x2, y1, y2;
    var y = flatCenters[flatCentersOffset + 1];
    var intersections = [];
    for (var r = 0, rr = ends.length; r < rr; ++r) {
      var end = ends[r];
      x1 = flatCoordinates[end - stride];
      y1 = flatCoordinates[end - stride + 1];
      for (i = offset; i < end; i += stride) {
        x2 = flatCoordinates[i];
        y2 = flatCoordinates[i + 1];
        if (y <= y1 && y2 <= y || y1 <= y && y <= y2) {
          x = (y - y1) / (y2 - y1) * (x2 - x1) + x1;
          intersections.push(x);
        }
        x1 = x2;
        y1 = y2;
      }
    }
    var pointX = NaN;
    var maxSegmentLength = -Infinity;
    intersections.sort(numberSafeCompareFunction);
    x1 = intersections[0];
    for (i = 1, ii = intersections.length; i < ii; ++i) {
      x2 = intersections[i];
      var segmentLength = Math.abs(x2 - x1);
      if (segmentLength > maxSegmentLength) {
        x = (x1 + x2) / 2;
        if (linearRingsContainsXY(flatCoordinates, offset, ends, stride, x, y)) {
          pointX = x;
          maxSegmentLength = segmentLength;
        }
      }
      x1 = x2;
    }
    if (isNaN(pointX)) {
      pointX = flatCenters[flatCentersOffset];
    }
    if (opt_dest) {
      opt_dest.push(pointX, y, maxSegmentLength);
      return opt_dest;
    } else {
      return [pointX, y, maxSegmentLength];
    }
  }
  function getInteriorPointsOfMultiArray(flatCoordinates, offset, endss, stride, flatCenters) {
    var interiorPoints = [];
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      interiorPoints = getInteriorPointOfArray(flatCoordinates, offset, ends, stride, flatCenters, 2 * i, interiorPoints);
      offset = ends[ends.length - 1];
    }
    return interiorPoints;
  }

  // node_modules/ol/geom/flat/reverse.js
  function coordinates(flatCoordinates, offset, end, stride) {
    while (offset < end - stride) {
      for (var i = 0; i < stride; ++i) {
        var tmp = flatCoordinates[offset + i];
        flatCoordinates[offset + i] = flatCoordinates[end - stride + i];
        flatCoordinates[end - stride + i] = tmp;
      }
      offset += stride;
      end -= stride;
    }
  }

  // node_modules/ol/geom/flat/orient.js
  function linearRingIsClockwise(flatCoordinates, offset, end, stride) {
    var edge = 0;
    var x1 = flatCoordinates[end - stride];
    var y1 = flatCoordinates[end - stride + 1];
    for (; offset < end; offset += stride) {
      var x2 = flatCoordinates[offset];
      var y2 = flatCoordinates[offset + 1];
      edge += (x2 - x1) * (y2 + y1);
      x1 = x2;
      y1 = y2;
    }
    return edge === 0 ? void 0 : edge > 0;
  }
  function linearRingsAreOriented(flatCoordinates, offset, ends, stride, opt_right) {
    var right = opt_right !== void 0 ? opt_right : false;
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      var end = ends[i];
      var isClockwise = linearRingIsClockwise(flatCoordinates, offset, end, stride);
      if (i === 0) {
        if (right && isClockwise || !right && !isClockwise) {
          return false;
        }
      } else {
        if (right && !isClockwise || !right && isClockwise) {
          return false;
        }
      }
      offset = end;
    }
    return true;
  }
  function linearRingssAreOriented(flatCoordinates, offset, endss, stride, opt_right) {
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      if (!linearRingsAreOriented(flatCoordinates, offset, ends, stride, opt_right)) {
        return false;
      }
      if (ends.length) {
        offset = ends[ends.length - 1];
      }
    }
    return true;
  }
  function orientLinearRings(flatCoordinates, offset, ends, stride, opt_right) {
    var right = opt_right !== void 0 ? opt_right : false;
    for (var i = 0, ii = ends.length; i < ii; ++i) {
      var end = ends[i];
      var isClockwise = linearRingIsClockwise(flatCoordinates, offset, end, stride);
      var reverse = i === 0 ? right && isClockwise || !right && !isClockwise : right && !isClockwise || !right && isClockwise;
      if (reverse) {
        coordinates(flatCoordinates, offset, end, stride);
      }
      offset = end;
    }
    return offset;
  }
  function orientLinearRingsArray(flatCoordinates, offset, endss, stride, opt_right) {
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      offset = orientLinearRings(flatCoordinates, offset, endss[i], stride, opt_right);
    }
    return offset;
  }

  // node_modules/ol/geom/Polygon.js
  var __extends17 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Polygon = function(_super) {
    __extends17(Polygon2, _super);
    function Polygon2(coordinates2, opt_layout, opt_ends) {
      var _this = _super.call(this) || this;
      _this.ends_ = [];
      _this.flatInteriorPointRevision_ = -1;
      _this.flatInteriorPoint_ = null;
      _this.maxDelta_ = -1;
      _this.maxDeltaRevision_ = -1;
      _this.orientedRevision_ = -1;
      _this.orientedFlatCoordinates_ = null;
      if (opt_layout !== void 0 && opt_ends) {
        _this.setFlatCoordinates(opt_layout, coordinates2);
        _this.ends_ = opt_ends;
      } else {
        _this.setCoordinates(coordinates2, opt_layout);
      }
      return _this;
    }
    Polygon2.prototype.appendLinearRing = function(linearRing2) {
      if (!this.flatCoordinates) {
        this.flatCoordinates = linearRing2.getFlatCoordinates().slice();
      } else {
        extend(this.flatCoordinates, linearRing2.getFlatCoordinates());
      }
      this.ends_.push(this.flatCoordinates.length);
      this.changed();
    };
    Polygon2.prototype.clone = function() {
      var polygon = new Polygon2(this.flatCoordinates.slice(), this.layout, this.ends_.slice());
      polygon.applyProperties(this);
      return polygon;
    };
    Polygon2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      if (minSquaredDistance < closestSquaredDistanceXY(this.getExtent(), x, y)) {
        return minSquaredDistance;
      }
      if (this.maxDeltaRevision_ != this.getRevision()) {
        this.maxDelta_ = Math.sqrt(arrayMaxSquaredDelta(this.flatCoordinates, 0, this.ends_, this.stride, 0));
        this.maxDeltaRevision_ = this.getRevision();
      }
      return assignClosestArrayPoint(this.flatCoordinates, 0, this.ends_, this.stride, this.maxDelta_, true, x, y, closestPoint, minSquaredDistance);
    };
    Polygon2.prototype.containsXY = function(x, y) {
      return linearRingsContainsXY(this.getOrientedFlatCoordinates(), 0, this.ends_, this.stride, x, y);
    };
    Polygon2.prototype.getArea = function() {
      return linearRings(this.getOrientedFlatCoordinates(), 0, this.ends_, this.stride);
    };
    Polygon2.prototype.getCoordinates = function(opt_right) {
      var flatCoordinates;
      if (opt_right !== void 0) {
        flatCoordinates = this.getOrientedFlatCoordinates().slice();
        orientLinearRings(flatCoordinates, 0, this.ends_, this.stride, opt_right);
      } else {
        flatCoordinates = this.flatCoordinates;
      }
      return inflateCoordinatesArray(flatCoordinates, 0, this.ends_, this.stride);
    };
    Polygon2.prototype.getEnds = function() {
      return this.ends_;
    };
    Polygon2.prototype.getFlatInteriorPoint = function() {
      if (this.flatInteriorPointRevision_ != this.getRevision()) {
        var flatCenter = getCenter(this.getExtent());
        this.flatInteriorPoint_ = getInteriorPointOfArray(this.getOrientedFlatCoordinates(), 0, this.ends_, this.stride, flatCenter, 0);
        this.flatInteriorPointRevision_ = this.getRevision();
      }
      return this.flatInteriorPoint_;
    };
    Polygon2.prototype.getInteriorPoint = function() {
      return new Point_default(this.getFlatInteriorPoint(), GeometryLayout_default.XYM);
    };
    Polygon2.prototype.getLinearRingCount = function() {
      return this.ends_.length;
    };
    Polygon2.prototype.getLinearRing = function(index) {
      if (index < 0 || this.ends_.length <= index) {
        return null;
      }
      return new LinearRing_default(this.flatCoordinates.slice(index === 0 ? 0 : this.ends_[index - 1], this.ends_[index]), this.layout);
    };
    Polygon2.prototype.getLinearRings = function() {
      var layout = this.layout;
      var flatCoordinates = this.flatCoordinates;
      var ends = this.ends_;
      var linearRings2 = [];
      var offset = 0;
      for (var i = 0, ii = ends.length; i < ii; ++i) {
        var end = ends[i];
        var linearRing2 = new LinearRing_default(flatCoordinates.slice(offset, end), layout);
        linearRings2.push(linearRing2);
        offset = end;
      }
      return linearRings2;
    };
    Polygon2.prototype.getOrientedFlatCoordinates = function() {
      if (this.orientedRevision_ != this.getRevision()) {
        var flatCoordinates = this.flatCoordinates;
        if (linearRingsAreOriented(flatCoordinates, 0, this.ends_, this.stride)) {
          this.orientedFlatCoordinates_ = flatCoordinates;
        } else {
          this.orientedFlatCoordinates_ = flatCoordinates.slice();
          this.orientedFlatCoordinates_.length = orientLinearRings(this.orientedFlatCoordinates_, 0, this.ends_, this.stride);
        }
        this.orientedRevision_ = this.getRevision();
      }
      return this.orientedFlatCoordinates_;
    };
    Polygon2.prototype.getSimplifiedGeometryInternal = function(squaredTolerance) {
      var simplifiedFlatCoordinates = [];
      var simplifiedEnds = [];
      simplifiedFlatCoordinates.length = quantizeArray(this.flatCoordinates, 0, this.ends_, this.stride, Math.sqrt(squaredTolerance), simplifiedFlatCoordinates, 0, simplifiedEnds);
      return new Polygon2(simplifiedFlatCoordinates, GeometryLayout_default.XY, simplifiedEnds);
    };
    Polygon2.prototype.getType = function() {
      return GeometryType_default.POLYGON;
    };
    Polygon2.prototype.intersectsExtent = function(extent) {
      return intersectsLinearRingArray(this.getOrientedFlatCoordinates(), 0, this.ends_, this.stride, extent);
    };
    Polygon2.prototype.setCoordinates = function(coordinates2, opt_layout) {
      this.setLayout(opt_layout, coordinates2, 2);
      if (!this.flatCoordinates) {
        this.flatCoordinates = [];
      }
      var ends = deflateCoordinatesArray(this.flatCoordinates, 0, coordinates2, this.stride, this.ends_);
      this.flatCoordinates.length = ends.length === 0 ? 0 : ends[ends.length - 1];
      this.changed();
    };
    return Polygon2;
  }(SimpleGeometry_default);
  var Polygon_default = Polygon;
  function fromCircle(circle, opt_sides, opt_angle) {
    var sides = opt_sides ? opt_sides : 32;
    var stride = circle.getStride();
    var layout = circle.getLayout();
    var center = circle.getCenter();
    var arrayLength = stride * (sides + 1);
    var flatCoordinates = new Array(arrayLength);
    for (var i = 0; i < arrayLength; i += stride) {
      flatCoordinates[i] = 0;
      flatCoordinates[i + 1] = 0;
      for (var j = 2; j < stride; j++) {
        flatCoordinates[i + j] = center[j];
      }
    }
    var ends = [flatCoordinates.length];
    var polygon = new Polygon(flatCoordinates, layout, ends);
    makeRegular(polygon, center, circle.getRadius(), opt_angle);
    return polygon;
  }
  function makeRegular(polygon, center, radius, opt_angle) {
    var flatCoordinates = polygon.getFlatCoordinates();
    var stride = polygon.getStride();
    var sides = flatCoordinates.length / stride - 1;
    var startAngle = opt_angle ? opt_angle : 0;
    for (var i = 0; i <= sides; ++i) {
      var offset = i * stride;
      var angle = startAngle + modulo(i, sides) * 2 * Math.PI / sides;
      flatCoordinates[offset] = center[0] + radius * Math.cos(angle);
      flatCoordinates[offset + 1] = center[1] + radius * Math.sin(angle);
    }
    polygon.changed();
  }

  // node_modules/ol/geom/flat/center.js
  function linearRingss2(flatCoordinates, offset, endss, stride) {
    var flatCenters = [];
    var extent = createEmpty();
    for (var i = 0, ii = endss.length; i < ii; ++i) {
      var ends = endss[i];
      extent = createOrUpdateFromFlatCoordinates(flatCoordinates, offset, ends[0], stride);
      flatCenters.push((extent[0] + extent[2]) / 2, (extent[1] + extent[3]) / 2);
      offset = ends[ends.length - 1];
    }
    return flatCenters;
  }

  // node_modules/ol/geom/MultiPolygon.js
  var __extends18 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var MultiPolygon = function(_super) {
    __extends18(MultiPolygon2, _super);
    function MultiPolygon2(coordinates2, opt_layout, opt_endss) {
      var _this = _super.call(this) || this;
      _this.endss_ = [];
      _this.flatInteriorPointsRevision_ = -1;
      _this.flatInteriorPoints_ = null;
      _this.maxDelta_ = -1;
      _this.maxDeltaRevision_ = -1;
      _this.orientedRevision_ = -1;
      _this.orientedFlatCoordinates_ = null;
      if (!opt_endss && !Array.isArray(coordinates2[0])) {
        var layout = _this.getLayout();
        var polygons = coordinates2;
        var flatCoordinates = [];
        var endss = [];
        for (var i = 0, ii = polygons.length; i < ii; ++i) {
          var polygon = polygons[i];
          if (i === 0) {
            layout = polygon.getLayout();
          }
          var offset = flatCoordinates.length;
          var ends = polygon.getEnds();
          for (var j = 0, jj = ends.length; j < jj; ++j) {
            ends[j] += offset;
          }
          extend(flatCoordinates, polygon.getFlatCoordinates());
          endss.push(ends);
        }
        opt_layout = layout;
        coordinates2 = flatCoordinates;
        opt_endss = endss;
      }
      if (opt_layout !== void 0 && opt_endss) {
        _this.setFlatCoordinates(opt_layout, coordinates2);
        _this.endss_ = opt_endss;
      } else {
        _this.setCoordinates(coordinates2, opt_layout);
      }
      return _this;
    }
    MultiPolygon2.prototype.appendPolygon = function(polygon) {
      var ends;
      if (!this.flatCoordinates) {
        this.flatCoordinates = polygon.getFlatCoordinates().slice();
        ends = polygon.getEnds().slice();
        this.endss_.push();
      } else {
        var offset = this.flatCoordinates.length;
        extend(this.flatCoordinates, polygon.getFlatCoordinates());
        ends = polygon.getEnds().slice();
        for (var i = 0, ii = ends.length; i < ii; ++i) {
          ends[i] += offset;
        }
      }
      this.endss_.push(ends);
      this.changed();
    };
    MultiPolygon2.prototype.clone = function() {
      var len = this.endss_.length;
      var newEndss = new Array(len);
      for (var i = 0; i < len; ++i) {
        newEndss[i] = this.endss_[i].slice();
      }
      var multiPolygon = new MultiPolygon2(this.flatCoordinates.slice(), this.layout, newEndss);
      multiPolygon.applyProperties(this);
      return multiPolygon;
    };
    MultiPolygon2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      if (minSquaredDistance < closestSquaredDistanceXY(this.getExtent(), x, y)) {
        return minSquaredDistance;
      }
      if (this.maxDeltaRevision_ != this.getRevision()) {
        this.maxDelta_ = Math.sqrt(multiArrayMaxSquaredDelta(this.flatCoordinates, 0, this.endss_, this.stride, 0));
        this.maxDeltaRevision_ = this.getRevision();
      }
      return assignClosestMultiArrayPoint(this.getOrientedFlatCoordinates(), 0, this.endss_, this.stride, this.maxDelta_, true, x, y, closestPoint, minSquaredDistance);
    };
    MultiPolygon2.prototype.containsXY = function(x, y) {
      return linearRingssContainsXY(this.getOrientedFlatCoordinates(), 0, this.endss_, this.stride, x, y);
    };
    MultiPolygon2.prototype.getArea = function() {
      return linearRingss(this.getOrientedFlatCoordinates(), 0, this.endss_, this.stride);
    };
    MultiPolygon2.prototype.getCoordinates = function(opt_right) {
      var flatCoordinates;
      if (opt_right !== void 0) {
        flatCoordinates = this.getOrientedFlatCoordinates().slice();
        orientLinearRingsArray(flatCoordinates, 0, this.endss_, this.stride, opt_right);
      } else {
        flatCoordinates = this.flatCoordinates;
      }
      return inflateMultiCoordinatesArray(flatCoordinates, 0, this.endss_, this.stride);
    };
    MultiPolygon2.prototype.getEndss = function() {
      return this.endss_;
    };
    MultiPolygon2.prototype.getFlatInteriorPoints = function() {
      if (this.flatInteriorPointsRevision_ != this.getRevision()) {
        var flatCenters = linearRingss2(this.flatCoordinates, 0, this.endss_, this.stride);
        this.flatInteriorPoints_ = getInteriorPointsOfMultiArray(this.getOrientedFlatCoordinates(), 0, this.endss_, this.stride, flatCenters);
        this.flatInteriorPointsRevision_ = this.getRevision();
      }
      return this.flatInteriorPoints_;
    };
    MultiPolygon2.prototype.getInteriorPoints = function() {
      return new MultiPoint_default(this.getFlatInteriorPoints().slice(), GeometryLayout_default.XYM);
    };
    MultiPolygon2.prototype.getOrientedFlatCoordinates = function() {
      if (this.orientedRevision_ != this.getRevision()) {
        var flatCoordinates = this.flatCoordinates;
        if (linearRingssAreOriented(flatCoordinates, 0, this.endss_, this.stride)) {
          this.orientedFlatCoordinates_ = flatCoordinates;
        } else {
          this.orientedFlatCoordinates_ = flatCoordinates.slice();
          this.orientedFlatCoordinates_.length = orientLinearRingsArray(this.orientedFlatCoordinates_, 0, this.endss_, this.stride);
        }
        this.orientedRevision_ = this.getRevision();
      }
      return this.orientedFlatCoordinates_;
    };
    MultiPolygon2.prototype.getSimplifiedGeometryInternal = function(squaredTolerance) {
      var simplifiedFlatCoordinates = [];
      var simplifiedEndss = [];
      simplifiedFlatCoordinates.length = quantizeMultiArray(this.flatCoordinates, 0, this.endss_, this.stride, Math.sqrt(squaredTolerance), simplifiedFlatCoordinates, 0, simplifiedEndss);
      return new MultiPolygon2(simplifiedFlatCoordinates, GeometryLayout_default.XY, simplifiedEndss);
    };
    MultiPolygon2.prototype.getPolygon = function(index) {
      if (index < 0 || this.endss_.length <= index) {
        return null;
      }
      var offset;
      if (index === 0) {
        offset = 0;
      } else {
        var prevEnds = this.endss_[index - 1];
        offset = prevEnds[prevEnds.length - 1];
      }
      var ends = this.endss_[index].slice();
      var end = ends[ends.length - 1];
      if (offset !== 0) {
        for (var i = 0, ii = ends.length; i < ii; ++i) {
          ends[i] -= offset;
        }
      }
      return new Polygon_default(this.flatCoordinates.slice(offset, end), this.layout, ends);
    };
    MultiPolygon2.prototype.getPolygons = function() {
      var layout = this.layout;
      var flatCoordinates = this.flatCoordinates;
      var endss = this.endss_;
      var polygons = [];
      var offset = 0;
      for (var i = 0, ii = endss.length; i < ii; ++i) {
        var ends = endss[i].slice();
        var end = ends[ends.length - 1];
        if (offset !== 0) {
          for (var j = 0, jj = ends.length; j < jj; ++j) {
            ends[j] -= offset;
          }
        }
        var polygon = new Polygon_default(flatCoordinates.slice(offset, end), layout, ends);
        polygons.push(polygon);
        offset = end;
      }
      return polygons;
    };
    MultiPolygon2.prototype.getType = function() {
      return GeometryType_default.MULTI_POLYGON;
    };
    MultiPolygon2.prototype.intersectsExtent = function(extent) {
      return intersectsLinearRingMultiArray(this.getOrientedFlatCoordinates(), 0, this.endss_, this.stride, extent);
    };
    MultiPolygon2.prototype.setCoordinates = function(coordinates2, opt_layout) {
      this.setLayout(opt_layout, coordinates2, 3);
      if (!this.flatCoordinates) {
        this.flatCoordinates = [];
      }
      var endss = deflateMultiCoordinatesArray(this.flatCoordinates, 0, coordinates2, this.stride, this.endss_);
      if (endss.length === 0) {
        this.flatCoordinates.length = 0;
      } else {
        var lastEnds = endss[endss.length - 1];
        this.flatCoordinates.length = lastEnds.length === 0 ? 0 : lastEnds[lastEnds.length - 1];
      }
      this.changed();
    };
    return MultiPolygon2;
  }(SimpleGeometry_default);
  var MultiPolygon_default = MultiPolygon;

  // node_modules/ol/format/GeoJSON.js
  var __extends19 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var GeoJSON = function(_super) {
    __extends19(GeoJSON2, _super);
    function GeoJSON2(opt_options) {
      var _this = this;
      var options = opt_options ? opt_options : {};
      _this = _super.call(this) || this;
      _this.dataProjection = get3(options.dataProjection ? options.dataProjection : "EPSG:4326");
      if (options.featureProjection) {
        _this.defaultFeatureProjection = get3(options.featureProjection);
      }
      _this.geometryName_ = options.geometryName;
      _this.extractGeometryName_ = options.extractGeometryName;
      _this.supportedMediaTypes = [
        "application/geo+json",
        "application/vnd.geo+json"
      ];
      return _this;
    }
    GeoJSON2.prototype.readFeatureFromObject = function(object, opt_options) {
      var geoJSONFeature = null;
      if (object["type"] === "Feature") {
        geoJSONFeature = object;
      } else {
        geoJSONFeature = {
          "type": "Feature",
          "geometry": object,
          "properties": null
        };
      }
      var geometry = readGeometry(geoJSONFeature["geometry"], opt_options);
      var feature = new Feature_default();
      if (this.geometryName_) {
        feature.setGeometryName(this.geometryName_);
      } else if (this.extractGeometryName_ && "geometry_name" in geoJSONFeature !== void 0) {
        feature.setGeometryName(geoJSONFeature["geometry_name"]);
      }
      feature.setGeometry(geometry);
      if ("id" in geoJSONFeature) {
        feature.setId(geoJSONFeature["id"]);
      }
      if (geoJSONFeature["properties"]) {
        feature.setProperties(geoJSONFeature["properties"], true);
      }
      return feature;
    };
    GeoJSON2.prototype.readFeaturesFromObject = function(object, opt_options) {
      var geoJSONObject = object;
      var features = null;
      if (geoJSONObject["type"] === "FeatureCollection") {
        var geoJSONFeatureCollection = object;
        features = [];
        var geoJSONFeatures = geoJSONFeatureCollection["features"];
        for (var i = 0, ii = geoJSONFeatures.length; i < ii; ++i) {
          features.push(this.readFeatureFromObject(geoJSONFeatures[i], opt_options));
        }
      } else {
        features = [this.readFeatureFromObject(object, opt_options)];
      }
      return features;
    };
    GeoJSON2.prototype.readGeometryFromObject = function(object, opt_options) {
      return readGeometry(object, opt_options);
    };
    GeoJSON2.prototype.readProjectionFromObject = function(object) {
      var crs = object["crs"];
      var projection;
      if (crs) {
        if (crs["type"] == "name") {
          projection = get3(crs["properties"]["name"]);
        } else if (crs["type"] === "EPSG") {
          projection = get3("EPSG:" + crs["properties"]["code"]);
        } else {
          assert(false, 36);
        }
      } else {
        projection = this.dataProjection;
      }
      return projection;
    };
    GeoJSON2.prototype.writeFeatureObject = function(feature, opt_options) {
      opt_options = this.adaptOptions(opt_options);
      var object = {
        "type": "Feature",
        geometry: null,
        properties: null
      };
      var id = feature.getId();
      if (id !== void 0) {
        object.id = id;
      }
      if (!feature.hasProperties()) {
        return object;
      }
      var properties = feature.getProperties();
      var geometry = feature.getGeometry();
      if (geometry) {
        object.geometry = writeGeometry(geometry, opt_options);
        delete properties[feature.getGeometryName()];
      }
      if (!isEmpty(properties)) {
        object.properties = properties;
      }
      return object;
    };
    GeoJSON2.prototype.writeFeaturesObject = function(features, opt_options) {
      opt_options = this.adaptOptions(opt_options);
      var objects = [];
      for (var i = 0, ii = features.length; i < ii; ++i) {
        objects.push(this.writeFeatureObject(features[i], opt_options));
      }
      return {
        type: "FeatureCollection",
        features: objects
      };
    };
    GeoJSON2.prototype.writeGeometryObject = function(geometry, opt_options) {
      return writeGeometry(geometry, this.adaptOptions(opt_options));
    };
    return GeoJSON2;
  }(JSONFeature_default);
  function readGeometry(object, opt_options) {
    if (!object) {
      return null;
    }
    var geometry;
    switch (object["type"]) {
      case GeometryType_default.POINT: {
        geometry = readPointGeometry(object);
        break;
      }
      case GeometryType_default.LINE_STRING: {
        geometry = readLineStringGeometry(object);
        break;
      }
      case GeometryType_default.POLYGON: {
        geometry = readPolygonGeometry(object);
        break;
      }
      case GeometryType_default.MULTI_POINT: {
        geometry = readMultiPointGeometry(object);
        break;
      }
      case GeometryType_default.MULTI_LINE_STRING: {
        geometry = readMultiLineStringGeometry(object);
        break;
      }
      case GeometryType_default.MULTI_POLYGON: {
        geometry = readMultiPolygonGeometry(object);
        break;
      }
      case GeometryType_default.GEOMETRY_COLLECTION: {
        geometry = readGeometryCollectionGeometry(object);
        break;
      }
      default: {
        throw new Error("Unsupported GeoJSON type: " + object.type);
      }
    }
    return transformGeometryWithOptions(geometry, false, opt_options);
  }
  function readGeometryCollectionGeometry(object, opt_options) {
    var geometries = object["geometries"].map(function(geometry) {
      return readGeometry(geometry, opt_options);
    });
    return new GeometryCollection_default(geometries);
  }
  function readPointGeometry(object) {
    return new Point_default(object["coordinates"]);
  }
  function readLineStringGeometry(object) {
    return new LineString_default(object["coordinates"]);
  }
  function readMultiLineStringGeometry(object) {
    return new MultiLineString_default(object["coordinates"]);
  }
  function readMultiPointGeometry(object) {
    return new MultiPoint_default(object["coordinates"]);
  }
  function readMultiPolygonGeometry(object) {
    return new MultiPolygon_default(object["coordinates"]);
  }
  function readPolygonGeometry(object) {
    return new Polygon_default(object["coordinates"]);
  }
  function writeGeometry(geometry, opt_options) {
    geometry = transformGeometryWithOptions(geometry, true, opt_options);
    var type = geometry.getType();
    var geoJSON;
    switch (type) {
      case GeometryType_default.POINT: {
        geoJSON = writePointGeometry(geometry, opt_options);
        break;
      }
      case GeometryType_default.LINE_STRING: {
        geoJSON = writeLineStringGeometry(geometry, opt_options);
        break;
      }
      case GeometryType_default.POLYGON: {
        geoJSON = writePolygonGeometry(geometry, opt_options);
        break;
      }
      case GeometryType_default.MULTI_POINT: {
        geoJSON = writeMultiPointGeometry(geometry, opt_options);
        break;
      }
      case GeometryType_default.MULTI_LINE_STRING: {
        geoJSON = writeMultiLineStringGeometry(geometry, opt_options);
        break;
      }
      case GeometryType_default.MULTI_POLYGON: {
        geoJSON = writeMultiPolygonGeometry(geometry, opt_options);
        break;
      }
      case GeometryType_default.GEOMETRY_COLLECTION: {
        geoJSON = writeGeometryCollectionGeometry(geometry, opt_options);
        break;
      }
      case GeometryType_default.CIRCLE: {
        geoJSON = {
          type: "GeometryCollection",
          geometries: []
        };
        break;
      }
      default: {
        throw new Error("Unsupported geometry type: " + type);
      }
    }
    return geoJSON;
  }
  function writeGeometryCollectionGeometry(geometry, opt_options) {
    var geometries = geometry.getGeometriesArray().map(function(geometry2) {
      var options = assign({}, opt_options);
      delete options.featureProjection;
      return writeGeometry(geometry2, options);
    });
    return {
      type: "GeometryCollection",
      geometries
    };
  }
  function writeLineStringGeometry(geometry, opt_options) {
    return {
      type: "LineString",
      coordinates: geometry.getCoordinates()
    };
  }
  function writeMultiLineStringGeometry(geometry, opt_options) {
    return {
      type: "MultiLineString",
      coordinates: geometry.getCoordinates()
    };
  }
  function writeMultiPointGeometry(geometry, opt_options) {
    return {
      type: "MultiPoint",
      coordinates: geometry.getCoordinates()
    };
  }
  function writeMultiPolygonGeometry(geometry, opt_options) {
    var right;
    if (opt_options) {
      right = opt_options.rightHanded;
    }
    return {
      type: "MultiPolygon",
      coordinates: geometry.getCoordinates(right)
    };
  }
  function writePointGeometry(geometry, opt_options) {
    return {
      type: "Point",
      coordinates: geometry.getCoordinates()
    };
  }
  function writePolygonGeometry(geometry, opt_options) {
    var right;
    if (opt_options) {
      right = opt_options.rightHanded;
    }
    return {
      type: "Polygon",
      coordinates: geometry.getCoordinates(right)
    };
  }
  var GeoJSON_default = GeoJSON;

  // node_modules/ol/CollectionEventType.js
  var CollectionEventType_default = {
    ADD: "add",
    REMOVE: "remove"
  };

  // node_modules/ol/Collection.js
  var __extends20 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Property = {
    LENGTH: "length"
  };
  var CollectionEvent = function(_super) {
    __extends20(CollectionEvent2, _super);
    function CollectionEvent2(type, opt_element, opt_index) {
      var _this = _super.call(this, type) || this;
      _this.element = opt_element;
      _this.index = opt_index;
      return _this;
    }
    return CollectionEvent2;
  }(Event_default);
  var Collection = function(_super) {
    __extends20(Collection2, _super);
    function Collection2(opt_array, opt_options) {
      var _this = _super.call(this) || this;
      _this.on;
      _this.once;
      _this.un;
      var options = opt_options || {};
      _this.unique_ = !!options.unique;
      _this.array_ = opt_array ? opt_array : [];
      if (_this.unique_) {
        for (var i = 0, ii = _this.array_.length; i < ii; ++i) {
          _this.assertUnique_(_this.array_[i], i);
        }
      }
      _this.updateLength_();
      return _this;
    }
    Collection2.prototype.clear = function() {
      while (this.getLength() > 0) {
        this.pop();
      }
    };
    Collection2.prototype.extend = function(arr) {
      for (var i = 0, ii = arr.length; i < ii; ++i) {
        this.push(arr[i]);
      }
      return this;
    };
    Collection2.prototype.forEach = function(f) {
      var array = this.array_;
      for (var i = 0, ii = array.length; i < ii; ++i) {
        f(array[i], i, array);
      }
    };
    Collection2.prototype.getArray = function() {
      return this.array_;
    };
    Collection2.prototype.item = function(index) {
      return this.array_[index];
    };
    Collection2.prototype.getLength = function() {
      return this.get(Property.LENGTH);
    };
    Collection2.prototype.insertAt = function(index, elem) {
      if (this.unique_) {
        this.assertUnique_(elem);
      }
      this.array_.splice(index, 0, elem);
      this.updateLength_();
      this.dispatchEvent(new CollectionEvent(CollectionEventType_default.ADD, elem, index));
    };
    Collection2.prototype.pop = function() {
      return this.removeAt(this.getLength() - 1);
    };
    Collection2.prototype.push = function(elem) {
      if (this.unique_) {
        this.assertUnique_(elem);
      }
      var n = this.getLength();
      this.insertAt(n, elem);
      return this.getLength();
    };
    Collection2.prototype.remove = function(elem) {
      var arr = this.array_;
      for (var i = 0, ii = arr.length; i < ii; ++i) {
        if (arr[i] === elem) {
          return this.removeAt(i);
        }
      }
      return void 0;
    };
    Collection2.prototype.removeAt = function(index) {
      var prev = this.array_[index];
      this.array_.splice(index, 1);
      this.updateLength_();
      this.dispatchEvent(new CollectionEvent(CollectionEventType_default.REMOVE, prev, index));
      return prev;
    };
    Collection2.prototype.setAt = function(index, elem) {
      var n = this.getLength();
      if (index < n) {
        if (this.unique_) {
          this.assertUnique_(elem, index);
        }
        var prev = this.array_[index];
        this.array_[index] = elem;
        this.dispatchEvent(new CollectionEvent(CollectionEventType_default.REMOVE, prev, index));
        this.dispatchEvent(new CollectionEvent(CollectionEventType_default.ADD, elem, index));
      } else {
        for (var j = n; j < index; ++j) {
          this.insertAt(j, void 0);
        }
        this.insertAt(index, elem);
      }
    };
    Collection2.prototype.updateLength_ = function() {
      this.set(Property.LENGTH, this.array_.length);
    };
    Collection2.prototype.assertUnique_ = function(elem, opt_except) {
      for (var i = 0, ii = this.array_.length; i < ii; ++i) {
        if (this.array_[i] === elem && i !== opt_except) {
          throw new AssertionError_default(58);
        }
      }
    };
    return Collection2;
  }(Object_default);
  var Collection_default = Collection;

  // node_modules/ol/interaction/Property.js
  var Property_default = {
    ACTIVE: "active"
  };

  // node_modules/ol/interaction/Interaction.js
  var __extends21 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Interaction = function(_super) {
    __extends21(Interaction2, _super);
    function Interaction2(opt_options) {
      var _this = _super.call(this) || this;
      _this.on;
      _this.once;
      _this.un;
      if (opt_options && opt_options.handleEvent) {
        _this.handleEvent = opt_options.handleEvent;
      }
      _this.map_ = null;
      _this.setActive(true);
      return _this;
    }
    Interaction2.prototype.getActive = function() {
      return this.get(Property_default.ACTIVE);
    };
    Interaction2.prototype.getMap = function() {
      return this.map_;
    };
    Interaction2.prototype.handleEvent = function(mapBrowserEvent) {
      return true;
    };
    Interaction2.prototype.setActive = function(active) {
      this.set(Property_default.ACTIVE, active);
    };
    Interaction2.prototype.setMap = function(map) {
      this.map_ = map;
    };
    return Interaction2;
  }(Object_default);
  var Interaction_default = Interaction;

  // node_modules/ol/MapBrowserEventType.js
  var MapBrowserEventType_default = {
    SINGLECLICK: "singleclick",
    CLICK: EventType_default.CLICK,
    DBLCLICK: EventType_default.DBLCLICK,
    POINTERDRAG: "pointerdrag",
    POINTERMOVE: "pointermove",
    POINTERDOWN: "pointerdown",
    POINTERUP: "pointerup",
    POINTEROVER: "pointerover",
    POINTEROUT: "pointerout",
    POINTERENTER: "pointerenter",
    POINTERLEAVE: "pointerleave",
    POINTERCANCEL: "pointercancel"
  };

  // node_modules/ol/interaction/Pointer.js
  var __extends22 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var PointerInteraction = function(_super) {
    __extends22(PointerInteraction2, _super);
    function PointerInteraction2(opt_options) {
      var _this = this;
      var options = opt_options ? opt_options : {};
      _this = _super.call(this, options) || this;
      if (options.handleDownEvent) {
        _this.handleDownEvent = options.handleDownEvent;
      }
      if (options.handleDragEvent) {
        _this.handleDragEvent = options.handleDragEvent;
      }
      if (options.handleMoveEvent) {
        _this.handleMoveEvent = options.handleMoveEvent;
      }
      if (options.handleUpEvent) {
        _this.handleUpEvent = options.handleUpEvent;
      }
      if (options.stopDown) {
        _this.stopDown = options.stopDown;
      }
      _this.handlingDownUpSequence = false;
      _this.trackedPointers_ = {};
      _this.targetPointers = [];
      return _this;
    }
    PointerInteraction2.prototype.getPointerCount = function() {
      return this.targetPointers.length;
    };
    PointerInteraction2.prototype.handleDownEvent = function(mapBrowserEvent) {
      return false;
    };
    PointerInteraction2.prototype.handleDragEvent = function(mapBrowserEvent) {
    };
    PointerInteraction2.prototype.handleEvent = function(mapBrowserEvent) {
      if (!mapBrowserEvent.originalEvent) {
        return true;
      }
      var stopEvent = false;
      this.updateTrackedPointers_(mapBrowserEvent);
      if (this.handlingDownUpSequence) {
        if (mapBrowserEvent.type == MapBrowserEventType_default.POINTERDRAG) {
          this.handleDragEvent(mapBrowserEvent);
          mapBrowserEvent.originalEvent.preventDefault();
        } else if (mapBrowserEvent.type == MapBrowserEventType_default.POINTERUP) {
          var handledUp = this.handleUpEvent(mapBrowserEvent);
          this.handlingDownUpSequence = handledUp && this.targetPointers.length > 0;
        }
      } else {
        if (mapBrowserEvent.type == MapBrowserEventType_default.POINTERDOWN) {
          var handled = this.handleDownEvent(mapBrowserEvent);
          this.handlingDownUpSequence = handled;
          stopEvent = this.stopDown(handled);
        } else if (mapBrowserEvent.type == MapBrowserEventType_default.POINTERMOVE) {
          this.handleMoveEvent(mapBrowserEvent);
        }
      }
      return !stopEvent;
    };
    PointerInteraction2.prototype.handleMoveEvent = function(mapBrowserEvent) {
    };
    PointerInteraction2.prototype.handleUpEvent = function(mapBrowserEvent) {
      return false;
    };
    PointerInteraction2.prototype.stopDown = function(handled) {
      return handled;
    };
    PointerInteraction2.prototype.updateTrackedPointers_ = function(mapBrowserEvent) {
      if (isPointerDraggingEvent(mapBrowserEvent)) {
        var event_1 = mapBrowserEvent.originalEvent;
        var id = event_1.pointerId.toString();
        if (mapBrowserEvent.type == MapBrowserEventType_default.POINTERUP) {
          delete this.trackedPointers_[id];
        } else if (mapBrowserEvent.type == MapBrowserEventType_default.POINTERDOWN) {
          this.trackedPointers_[id] = event_1;
        } else if (id in this.trackedPointers_) {
          this.trackedPointers_[id] = event_1;
        }
        this.targetPointers = getValues(this.trackedPointers_);
      }
    };
    return PointerInteraction2;
  }(Interaction_default);
  function isPointerDraggingEvent(mapBrowserEvent) {
    var type = mapBrowserEvent.type;
    return type === MapBrowserEventType_default.POINTERDOWN || type === MapBrowserEventType_default.POINTERDRAG || type === MapBrowserEventType_default.POINTERUP;
  }
  var Pointer_default = PointerInteraction;

  // node_modules/ol/events/condition.js
  var altKeyOnly = function(mapBrowserEvent) {
    var originalEvent = mapBrowserEvent.originalEvent;
    return originalEvent.altKey && !(originalEvent.metaKey || originalEvent.ctrlKey) && !originalEvent.shiftKey;
  };
  var always = TRUE;
  var singleClick = function(mapBrowserEvent) {
    return mapBrowserEvent.type == MapBrowserEventType_default.SINGLECLICK;
  };
  var noModifierKeys = function(mapBrowserEvent) {
    var originalEvent = mapBrowserEvent.originalEvent;
    return !originalEvent.altKey && !(originalEvent.metaKey || originalEvent.ctrlKey) && !originalEvent.shiftKey;
  };
  var shiftKeyOnly = function(mapBrowserEvent) {
    var originalEvent = mapBrowserEvent.originalEvent;
    return !originalEvent.altKey && !(originalEvent.metaKey || originalEvent.ctrlKey) && originalEvent.shiftKey;
  };
  var primaryAction = function(mapBrowserEvent) {
    var pointerEvent = mapBrowserEvent.originalEvent;
    assert(pointerEvent !== void 0, 56);
    return pointerEvent.isPrimary && pointerEvent.button === 0;
  };

  // node_modules/ol/geom/Circle.js
  var __extends23 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Circle = function(_super) {
    __extends23(Circle2, _super);
    function Circle2(center, opt_radius, opt_layout) {
      var _this = _super.call(this) || this;
      if (opt_layout !== void 0 && opt_radius === void 0) {
        _this.setFlatCoordinates(opt_layout, center);
      } else {
        var radius = opt_radius ? opt_radius : 0;
        _this.setCenterAndRadius(center, radius, opt_layout);
      }
      return _this;
    }
    Circle2.prototype.clone = function() {
      var circle = new Circle2(this.flatCoordinates.slice(), void 0, this.layout);
      circle.applyProperties(this);
      return circle;
    };
    Circle2.prototype.closestPointXY = function(x, y, closestPoint, minSquaredDistance) {
      var flatCoordinates = this.flatCoordinates;
      var dx = x - flatCoordinates[0];
      var dy = y - flatCoordinates[1];
      var squaredDistance3 = dx * dx + dy * dy;
      if (squaredDistance3 < minSquaredDistance) {
        if (squaredDistance3 === 0) {
          for (var i = 0; i < this.stride; ++i) {
            closestPoint[i] = flatCoordinates[i];
          }
        } else {
          var delta = this.getRadius() / Math.sqrt(squaredDistance3);
          closestPoint[0] = flatCoordinates[0] + delta * dx;
          closestPoint[1] = flatCoordinates[1] + delta * dy;
          for (var i = 2; i < this.stride; ++i) {
            closestPoint[i] = flatCoordinates[i];
          }
        }
        closestPoint.length = this.stride;
        return squaredDistance3;
      } else {
        return minSquaredDistance;
      }
    };
    Circle2.prototype.containsXY = function(x, y) {
      var flatCoordinates = this.flatCoordinates;
      var dx = x - flatCoordinates[0];
      var dy = y - flatCoordinates[1];
      return dx * dx + dy * dy <= this.getRadiusSquared_();
    };
    Circle2.prototype.getCenter = function() {
      return this.flatCoordinates.slice(0, this.stride);
    };
    Circle2.prototype.computeExtent = function(extent) {
      var flatCoordinates = this.flatCoordinates;
      var radius = flatCoordinates[this.stride] - flatCoordinates[0];
      return createOrUpdate(flatCoordinates[0] - radius, flatCoordinates[1] - radius, flatCoordinates[0] + radius, flatCoordinates[1] + radius, extent);
    };
    Circle2.prototype.getRadius = function() {
      return Math.sqrt(this.getRadiusSquared_());
    };
    Circle2.prototype.getRadiusSquared_ = function() {
      var dx = this.flatCoordinates[this.stride] - this.flatCoordinates[0];
      var dy = this.flatCoordinates[this.stride + 1] - this.flatCoordinates[1];
      return dx * dx + dy * dy;
    };
    Circle2.prototype.getType = function() {
      return GeometryType_default.CIRCLE;
    };
    Circle2.prototype.intersectsExtent = function(extent) {
      var circleExtent = this.getExtent();
      if (intersects(extent, circleExtent)) {
        var center = this.getCenter();
        if (extent[0] <= center[0] && extent[2] >= center[0]) {
          return true;
        }
        if (extent[1] <= center[1] && extent[3] >= center[1]) {
          return true;
        }
        return forEachCorner(extent, this.intersectsCoordinate.bind(this));
      }
      return false;
    };
    Circle2.prototype.setCenter = function(center) {
      var stride = this.stride;
      var radius = this.flatCoordinates[stride] - this.flatCoordinates[0];
      var flatCoordinates = center.slice();
      flatCoordinates[stride] = flatCoordinates[0] + radius;
      for (var i = 1; i < stride; ++i) {
        flatCoordinates[stride + i] = center[i];
      }
      this.setFlatCoordinates(this.layout, flatCoordinates);
      this.changed();
    };
    Circle2.prototype.setCenterAndRadius = function(center, radius, opt_layout) {
      this.setLayout(opt_layout, center, 0);
      if (!this.flatCoordinates) {
        this.flatCoordinates = [];
      }
      var flatCoordinates = this.flatCoordinates;
      var offset = deflateCoordinate(flatCoordinates, 0, center, this.stride);
      flatCoordinates[offset++] = flatCoordinates[0] + radius;
      for (var i = 1, ii = this.stride; i < ii; ++i) {
        flatCoordinates[offset++] = flatCoordinates[i];
      }
      flatCoordinates.length = offset;
      this.changed();
    };
    Circle2.prototype.getCoordinates = function() {
      return null;
    };
    Circle2.prototype.setCoordinates = function(coordinates2, opt_layout) {
    };
    Circle2.prototype.setRadius = function(radius) {
      this.flatCoordinates[this.stride] = this.flatCoordinates[0] + radius;
      this.changed();
    };
    Circle2.prototype.rotate = function(angle, anchor) {
      var center = this.getCenter();
      var stride = this.getStride();
      this.setCenter(rotate(center, 0, center.length, stride, angle, anchor, center));
      this.changed();
    };
    Circle2.prototype.translate = function(deltaX, deltaY) {
      var center = this.getCenter();
      var stride = this.getStride();
      this.setCenter(translate(center, 0, center.length, stride, deltaX, deltaY, center));
      this.changed();
    };
    return Circle2;
  }(SimpleGeometry_default);
  Circle.prototype.transform;
  var Circle_default = Circle;

  // node_modules/ol/MapEvent.js
  var __extends24 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var MapEvent = function(_super) {
    __extends24(MapEvent2, _super);
    function MapEvent2(type, map, opt_frameState) {
      var _this = _super.call(this, type) || this;
      _this.map = map;
      _this.frameState = opt_frameState !== void 0 ? opt_frameState : null;
      return _this;
    }
    return MapEvent2;
  }(Event_default);
  var MapEvent_default = MapEvent;

  // node_modules/ol/MapBrowserEvent.js
  var __extends25 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var MapBrowserEvent = function(_super) {
    __extends25(MapBrowserEvent2, _super);
    function MapBrowserEvent2(type, map, originalEvent, opt_dragging, opt_frameState) {
      var _this = _super.call(this, type, map, opt_frameState) || this;
      _this.originalEvent = originalEvent;
      _this.pixel_ = null;
      _this.coordinate_ = null;
      _this.dragging = opt_dragging !== void 0 ? opt_dragging : false;
      return _this;
    }
    Object.defineProperty(MapBrowserEvent2.prototype, "pixel", {
      get: function() {
        if (!this.pixel_) {
          this.pixel_ = this.map.getEventPixel(this.originalEvent);
        }
        return this.pixel_;
      },
      set: function(pixel) {
        this.pixel_ = pixel;
      },
      enumerable: false,
      configurable: true
    });
    Object.defineProperty(MapBrowserEvent2.prototype, "coordinate", {
      get: function() {
        if (!this.coordinate_) {
          this.coordinate_ = this.map.getCoordinateFromPixel(this.pixel);
        }
        return this.coordinate_;
      },
      set: function(coordinate) {
        this.coordinate_ = coordinate;
      },
      enumerable: false,
      configurable: true
    });
    MapBrowserEvent2.prototype.preventDefault = function() {
      _super.prototype.preventDefault.call(this);
      if ("preventDefault" in this.originalEvent) {
        this.originalEvent.preventDefault();
      }
    };
    MapBrowserEvent2.prototype.stopPropagation = function() {
      _super.prototype.stopPropagation.call(this);
      if ("stopPropagation" in this.originalEvent) {
        this.originalEvent.stopPropagation();
      }
    };
    return MapBrowserEvent2;
  }(MapEvent_default);
  var MapBrowserEvent_default = MapBrowserEvent;

  // node_modules/ol/layer/Property.js
  var Property_default2 = {
    OPACITY: "opacity",
    VISIBLE: "visible",
    EXTENT: "extent",
    Z_INDEX: "zIndex",
    MAX_RESOLUTION: "maxResolution",
    MIN_RESOLUTION: "minResolution",
    MAX_ZOOM: "maxZoom",
    MIN_ZOOM: "minZoom",
    SOURCE: "source",
    MAP: "map"
  };

  // node_modules/ol/layer/Base.js
  var __extends26 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var BaseLayer = function(_super) {
    __extends26(BaseLayer2, _super);
    function BaseLayer2(options) {
      var _this = _super.call(this) || this;
      _this.on;
      _this.once;
      _this.un;
      _this.background_ = options.background;
      var properties = assign({}, options);
      if (typeof options.properties === "object") {
        delete properties.properties;
        assign(properties, options.properties);
      }
      properties[Property_default2.OPACITY] = options.opacity !== void 0 ? options.opacity : 1;
      assert(typeof properties[Property_default2.OPACITY] === "number", 64);
      properties[Property_default2.VISIBLE] = options.visible !== void 0 ? options.visible : true;
      properties[Property_default2.Z_INDEX] = options.zIndex;
      properties[Property_default2.MAX_RESOLUTION] = options.maxResolution !== void 0 ? options.maxResolution : Infinity;
      properties[Property_default2.MIN_RESOLUTION] = options.minResolution !== void 0 ? options.minResolution : 0;
      properties[Property_default2.MIN_ZOOM] = options.minZoom !== void 0 ? options.minZoom : -Infinity;
      properties[Property_default2.MAX_ZOOM] = options.maxZoom !== void 0 ? options.maxZoom : Infinity;
      _this.className_ = properties.className !== void 0 ? properties.className : "ol-layer";
      delete properties.className;
      _this.setProperties(properties);
      _this.state_ = null;
      return _this;
    }
    BaseLayer2.prototype.getBackground = function() {
      return this.background_;
    };
    BaseLayer2.prototype.getClassName = function() {
      return this.className_;
    };
    BaseLayer2.prototype.getLayerState = function(opt_managed) {
      var state = this.state_ || {
        layer: this,
        managed: opt_managed === void 0 ? true : opt_managed
      };
      var zIndex = this.getZIndex();
      state.opacity = clamp(Math.round(this.getOpacity() * 100) / 100, 0, 1);
      state.visible = this.getVisible();
      state.extent = this.getExtent();
      state.zIndex = zIndex === void 0 && !state.managed ? Infinity : zIndex;
      state.maxResolution = this.getMaxResolution();
      state.minResolution = Math.max(this.getMinResolution(), 0);
      state.minZoom = this.getMinZoom();
      state.maxZoom = this.getMaxZoom();
      this.state_ = state;
      return state;
    };
    BaseLayer2.prototype.getLayersArray = function(opt_array) {
      return abstract();
    };
    BaseLayer2.prototype.getLayerStatesArray = function(opt_states) {
      return abstract();
    };
    BaseLayer2.prototype.getExtent = function() {
      return this.get(Property_default2.EXTENT);
    };
    BaseLayer2.prototype.getMaxResolution = function() {
      return this.get(Property_default2.MAX_RESOLUTION);
    };
    BaseLayer2.prototype.getMinResolution = function() {
      return this.get(Property_default2.MIN_RESOLUTION);
    };
    BaseLayer2.prototype.getMinZoom = function() {
      return this.get(Property_default2.MIN_ZOOM);
    };
    BaseLayer2.prototype.getMaxZoom = function() {
      return this.get(Property_default2.MAX_ZOOM);
    };
    BaseLayer2.prototype.getOpacity = function() {
      return this.get(Property_default2.OPACITY);
    };
    BaseLayer2.prototype.getSourceState = function() {
      return abstract();
    };
    BaseLayer2.prototype.getVisible = function() {
      return this.get(Property_default2.VISIBLE);
    };
    BaseLayer2.prototype.getZIndex = function() {
      return this.get(Property_default2.Z_INDEX);
    };
    BaseLayer2.prototype.setBackground = function(opt_background) {
      this.background_ = opt_background;
      this.changed();
    };
    BaseLayer2.prototype.setExtent = function(extent) {
      this.set(Property_default2.EXTENT, extent);
    };
    BaseLayer2.prototype.setMaxResolution = function(maxResolution) {
      this.set(Property_default2.MAX_RESOLUTION, maxResolution);
    };
    BaseLayer2.prototype.setMinResolution = function(minResolution) {
      this.set(Property_default2.MIN_RESOLUTION, minResolution);
    };
    BaseLayer2.prototype.setMaxZoom = function(maxZoom) {
      this.set(Property_default2.MAX_ZOOM, maxZoom);
    };
    BaseLayer2.prototype.setMinZoom = function(minZoom) {
      this.set(Property_default2.MIN_ZOOM, minZoom);
    };
    BaseLayer2.prototype.setOpacity = function(opacity) {
      assert(typeof opacity === "number", 64);
      this.set(Property_default2.OPACITY, opacity);
    };
    BaseLayer2.prototype.setVisible = function(visible) {
      this.set(Property_default2.VISIBLE, visible);
    };
    BaseLayer2.prototype.setZIndex = function(zindex) {
      this.set(Property_default2.Z_INDEX, zindex);
    };
    BaseLayer2.prototype.disposeInternal = function() {
      if (this.state_) {
        this.state_.layer = null;
        this.state_ = null;
      }
      _super.prototype.disposeInternal.call(this);
    };
    return BaseLayer2;
  }(Object_default);
  var Base_default = BaseLayer;

  // node_modules/ol/render/EventType.js
  var EventType_default2 = {
    PRERENDER: "prerender",
    POSTRENDER: "postrender",
    PRECOMPOSE: "precompose",
    POSTCOMPOSE: "postcompose",
    RENDERCOMPLETE: "rendercomplete"
  };

  // node_modules/ol/source/State.js
  var State_default = {
    UNDEFINED: "undefined",
    LOADING: "loading",
    READY: "ready",
    ERROR: "error"
  };

  // node_modules/ol/layer/Layer.js
  var __extends27 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Layer = function(_super) {
    __extends27(Layer2, _super);
    function Layer2(options) {
      var _this = this;
      var baseOptions = assign({}, options);
      delete baseOptions.source;
      _this = _super.call(this, baseOptions) || this;
      _this.on;
      _this.once;
      _this.un;
      _this.mapPrecomposeKey_ = null;
      _this.mapRenderKey_ = null;
      _this.sourceChangeKey_ = null;
      _this.renderer_ = null;
      _this.rendered = false;
      if (options.render) {
        _this.render = options.render;
      }
      if (options.map) {
        _this.setMap(options.map);
      }
      _this.addChangeListener(Property_default2.SOURCE, _this.handleSourcePropertyChange_);
      var source = options.source ? options.source : null;
      _this.setSource(source);
      return _this;
    }
    Layer2.prototype.getLayersArray = function(opt_array) {
      var array = opt_array ? opt_array : [];
      array.push(this);
      return array;
    };
    Layer2.prototype.getLayerStatesArray = function(opt_states) {
      var states = opt_states ? opt_states : [];
      states.push(this.getLayerState());
      return states;
    };
    Layer2.prototype.getSource = function() {
      return this.get(Property_default2.SOURCE) || null;
    };
    Layer2.prototype.getRenderSource = function() {
      return this.getSource();
    };
    Layer2.prototype.getSourceState = function() {
      var source = this.getSource();
      return !source ? State_default.UNDEFINED : source.getState();
    };
    Layer2.prototype.handleSourceChange_ = function() {
      this.changed();
    };
    Layer2.prototype.handleSourcePropertyChange_ = function() {
      if (this.sourceChangeKey_) {
        unlistenByKey(this.sourceChangeKey_);
        this.sourceChangeKey_ = null;
      }
      var source = this.getSource();
      if (source) {
        this.sourceChangeKey_ = listen(source, EventType_default.CHANGE, this.handleSourceChange_, this);
      }
      this.changed();
    };
    Layer2.prototype.getFeatures = function(pixel) {
      if (!this.renderer_) {
        return new Promise(function(resolve) {
          return resolve([]);
        });
      }
      return this.renderer_.getFeatures(pixel);
    };
    Layer2.prototype.getData = function(pixel) {
      if (!this.renderer_ || !this.rendered) {
        return null;
      }
      return this.renderer_.getData(pixel);
    };
    Layer2.prototype.render = function(frameState, target) {
      var layerRenderer = this.getRenderer();
      if (layerRenderer.prepareFrame(frameState)) {
        this.rendered = true;
        return layerRenderer.renderFrame(frameState, target);
      }
    };
    Layer2.prototype.unrender = function() {
      this.rendered = false;
    };
    Layer2.prototype.setMapInternal = function(map) {
      if (!map) {
        this.unrender();
      }
      this.set(Property_default2.MAP, map);
    };
    Layer2.prototype.getMapInternal = function() {
      return this.get(Property_default2.MAP);
    };
    Layer2.prototype.setMap = function(map) {
      if (this.mapPrecomposeKey_) {
        unlistenByKey(this.mapPrecomposeKey_);
        this.mapPrecomposeKey_ = null;
      }
      if (!map) {
        this.changed();
      }
      if (this.mapRenderKey_) {
        unlistenByKey(this.mapRenderKey_);
        this.mapRenderKey_ = null;
      }
      if (map) {
        this.mapPrecomposeKey_ = listen(map, EventType_default2.PRECOMPOSE, function(evt) {
          var renderEvent = evt;
          var layerStatesArray = renderEvent.frameState.layerStatesArray;
          var layerState = this.getLayerState(false);
          assert(!layerStatesArray.some(function(arrayLayerState) {
            return arrayLayerState.layer === layerState.layer;
          }), 67);
          layerStatesArray.push(layerState);
        }, this);
        this.mapRenderKey_ = listen(this, EventType_default.CHANGE, map.render, map);
        this.changed();
      }
    };
    Layer2.prototype.setSource = function(source) {
      this.set(Property_default2.SOURCE, source);
    };
    Layer2.prototype.getRenderer = function() {
      if (!this.renderer_) {
        this.renderer_ = this.createRenderer();
      }
      return this.renderer_;
    };
    Layer2.prototype.hasRenderer = function() {
      return !!this.renderer_;
    };
    Layer2.prototype.createRenderer = function() {
      return null;
    };
    Layer2.prototype.disposeInternal = function() {
      if (this.renderer_) {
        this.renderer_.dispose();
        delete this.renderer_;
      }
      this.setSource(null);
      _super.prototype.disposeInternal.call(this);
    };
    return Layer2;
  }(Base_default);
  var Layer_default = Layer;

  // node_modules/ol/layer/BaseVector.js
  var import_rbush = __toESM(require_rbush_min(), 1);

  // node_modules/ol/ImageState.js
  var ImageState_default = {
    IDLE: 0,
    LOADING: 1,
    LOADED: 2,
    ERROR: 3,
    EMPTY: 4
  };

  // node_modules/ol/size.js
  function toSize(size, opt_size) {
    if (Array.isArray(size)) {
      return size;
    } else {
      if (opt_size === void 0) {
        opt_size = [size, size];
      } else {
        opt_size[0] = size;
        opt_size[1] = size;
      }
      return opt_size;
    }
  }

  // node_modules/ol/style/Image.js
  var ImageStyle = function() {
    function ImageStyle2(options) {
      this.opacity_ = options.opacity;
      this.rotateWithView_ = options.rotateWithView;
      this.rotation_ = options.rotation;
      this.scale_ = options.scale;
      this.scaleArray_ = toSize(options.scale);
      this.displacement_ = options.displacement;
    }
    ImageStyle2.prototype.clone = function() {
      var scale2 = this.getScale();
      return new ImageStyle2({
        opacity: this.getOpacity(),
        scale: Array.isArray(scale2) ? scale2.slice() : scale2,
        rotation: this.getRotation(),
        rotateWithView: this.getRotateWithView(),
        displacement: this.getDisplacement().slice()
      });
    };
    ImageStyle2.prototype.getOpacity = function() {
      return this.opacity_;
    };
    ImageStyle2.prototype.getRotateWithView = function() {
      return this.rotateWithView_;
    };
    ImageStyle2.prototype.getRotation = function() {
      return this.rotation_;
    };
    ImageStyle2.prototype.getScale = function() {
      return this.scale_;
    };
    ImageStyle2.prototype.getScaleArray = function() {
      return this.scaleArray_;
    };
    ImageStyle2.prototype.getDisplacement = function() {
      return this.displacement_;
    };
    ImageStyle2.prototype.getAnchor = function() {
      return abstract();
    };
    ImageStyle2.prototype.getImage = function(pixelRatio) {
      return abstract();
    };
    ImageStyle2.prototype.getHitDetectionImage = function() {
      return abstract();
    };
    ImageStyle2.prototype.getPixelRatio = function(pixelRatio) {
      return 1;
    };
    ImageStyle2.prototype.getImageState = function() {
      return abstract();
    };
    ImageStyle2.prototype.getImageSize = function() {
      return abstract();
    };
    ImageStyle2.prototype.getOrigin = function() {
      return abstract();
    };
    ImageStyle2.prototype.getSize = function() {
      return abstract();
    };
    ImageStyle2.prototype.setDisplacement = function(displacement) {
      this.displacement_ = displacement;
    };
    ImageStyle2.prototype.setOpacity = function(opacity) {
      this.opacity_ = opacity;
    };
    ImageStyle2.prototype.setRotateWithView = function(rotateWithView) {
      this.rotateWithView_ = rotateWithView;
    };
    ImageStyle2.prototype.setRotation = function(rotation) {
      this.rotation_ = rotation;
    };
    ImageStyle2.prototype.setScale = function(scale2) {
      this.scale_ = scale2;
      this.scaleArray_ = toSize(scale2);
    };
    ImageStyle2.prototype.listenImageChange = function(listener) {
      abstract();
    };
    ImageStyle2.prototype.load = function() {
      abstract();
    };
    ImageStyle2.prototype.unlistenImageChange = function(listener) {
      abstract();
    };
    return ImageStyle2;
  }();
  var Image_default = ImageStyle;

  // node_modules/ol/color.js
  var HEX_COLOR_RE_ = /^#([a-f0-9]{3}|[a-f0-9]{4}(?:[a-f0-9]{2}){0,2})$/i;
  var NAMED_COLOR_RE_ = /^([a-z]*)$|^hsla?\(.*\)$/i;
  function asString(color) {
    if (typeof color === "string") {
      return color;
    } else {
      return toString2(color);
    }
  }
  function fromNamed(color) {
    var el = document.createElement("div");
    el.style.color = color;
    if (el.style.color !== "") {
      document.body.appendChild(el);
      var rgb = getComputedStyle(el).color;
      document.body.removeChild(el);
      return rgb;
    } else {
      return "";
    }
  }
  var fromString = function() {
    var MAX_CACHE_SIZE = 1024;
    var cache2 = {};
    var cacheSize = 0;
    return function(s) {
      var color;
      if (cache2.hasOwnProperty(s)) {
        color = cache2[s];
      } else {
        if (cacheSize >= MAX_CACHE_SIZE) {
          var i = 0;
          for (var key in cache2) {
            if ((i++ & 3) === 0) {
              delete cache2[key];
              --cacheSize;
            }
          }
        }
        color = fromStringInternal_(s);
        cache2[s] = color;
        ++cacheSize;
      }
      return color;
    };
  }();
  function asArray(color) {
    if (Array.isArray(color)) {
      return color;
    } else {
      return fromString(color);
    }
  }
  function fromStringInternal_(s) {
    var r, g, b, a, color;
    if (NAMED_COLOR_RE_.exec(s)) {
      s = fromNamed(s);
    }
    if (HEX_COLOR_RE_.exec(s)) {
      var n = s.length - 1;
      var d = void 0;
      if (n <= 4) {
        d = 1;
      } else {
        d = 2;
      }
      var hasAlpha = n === 4 || n === 8;
      r = parseInt(s.substr(1 + 0 * d, d), 16);
      g = parseInt(s.substr(1 + 1 * d, d), 16);
      b = parseInt(s.substr(1 + 2 * d, d), 16);
      if (hasAlpha) {
        a = parseInt(s.substr(1 + 3 * d, d), 16);
      } else {
        a = 255;
      }
      if (d == 1) {
        r = (r << 4) + r;
        g = (g << 4) + g;
        b = (b << 4) + b;
        if (hasAlpha) {
          a = (a << 4) + a;
        }
      }
      color = [r, g, b, a / 255];
    } else if (s.indexOf("rgba(") == 0) {
      color = s.slice(5, -1).split(",").map(Number);
      normalize(color);
    } else if (s.indexOf("rgb(") == 0) {
      color = s.slice(4, -1).split(",").map(Number);
      color.push(1);
      normalize(color);
    } else {
      assert(false, 14);
    }
    return color;
  }
  function normalize(color) {
    color[0] = clamp(color[0] + 0.5 | 0, 0, 255);
    color[1] = clamp(color[1] + 0.5 | 0, 0, 255);
    color[2] = clamp(color[2] + 0.5 | 0, 0, 255);
    color[3] = clamp(color[3], 0, 1);
    return color;
  }
  function toString2(color) {
    var r = color[0];
    if (r != (r | 0)) {
      r = r + 0.5 | 0;
    }
    var g = color[1];
    if (g != (g | 0)) {
      g = g + 0.5 | 0;
    }
    var b = color[2];
    if (b != (b | 0)) {
      b = b + 0.5 | 0;
    }
    var a = color[3] === void 0 ? 1 : Math.round(color[3] * 100) / 100;
    return "rgba(" + r + "," + g + "," + b + "," + a + ")";
  }

  // node_modules/ol/colorlike.js
  function asColorLike(color) {
    if (Array.isArray(color)) {
      return toString2(color);
    } else {
      return color;
    }
  }

  // node_modules/ol/dom.js
  function createCanvasContext2D(opt_width, opt_height, opt_canvasPool, opt_Context2DSettings) {
    var canvas;
    if (opt_canvasPool && opt_canvasPool.length) {
      canvas = opt_canvasPool.shift();
    } else if (WORKER_OFFSCREEN_CANVAS) {
      canvas = new OffscreenCanvas(opt_width || 300, opt_height || 300);
    } else {
      canvas = document.createElement("canvas");
    }
    if (opt_width) {
      canvas.width = opt_width;
    }
    if (opt_height) {
      canvas.height = opt_height;
    }
    return canvas.getContext("2d", opt_Context2DSettings);
  }

  // node_modules/ol/css.js
  var fontRegEx = new RegExp([
    "^\\s*(?=(?:(?:[-a-z]+\\s*){0,2}(italic|oblique))?)",
    "(?=(?:(?:[-a-z]+\\s*){0,2}(small-caps))?)",
    "(?=(?:(?:[-a-z]+\\s*){0,2}(bold(?:er)?|lighter|[1-9]00 ))?)",
    "(?:(?:normal|\\1|\\2|\\3)\\s*){0,3}((?:xx?-)?",
    "(?:small|large)|medium|smaller|larger|[\\.\\d]+(?:\\%|in|[cem]m|ex|p[ctx]))",
    "(?:\\s*\\/\\s*(normal|[\\.\\d]+(?:\\%|in|[cem]m|ex|p[ctx])?))",
    `?\\s*([-,\\"\\'\\sa-z]+?)\\s*$`
  ].join(""), "i");
  var fontRegExMatchIndex = [
    "style",
    "variant",
    "weight",
    "size",
    "lineHeight",
    "family"
  ];
  var getFontParameters = function(fontSpec) {
    var match = fontSpec.match(fontRegEx);
    if (!match) {
      return null;
    }
    var style = {
      lineHeight: "normal",
      size: "1.2em",
      style: "normal",
      weight: "normal",
      variant: "normal"
    };
    for (var i = 0, ii = fontRegExMatchIndex.length; i < ii; ++i) {
      var value = match[i + 1];
      if (value !== void 0) {
        style[fontRegExMatchIndex[i]] = value;
      }
    }
    style.families = style.family.split(/,\s?/);
    return style;
  };
  function cssOpacity(opacity) {
    return opacity === 1 ? "" : String(Math.round(opacity * 100) / 100);
  }

  // node_modules/ol/render/canvas.js
  var defaultFont = "10px sans-serif";
  var defaultFillStyle = "#000";
  var defaultLineCap = "round";
  var defaultLineDash = [];
  var defaultLineDashOffset = 0;
  var defaultLineJoin = "round";
  var defaultMiterLimit = 10;
  var defaultStrokeStyle = "#000";
  var defaultTextAlign = "center";
  var defaultTextBaseline = "middle";
  var defaultPadding = [0, 0, 0, 0];
  var defaultLineWidth = 1;
  var checkedFonts = new Object_default();
  var labelCache = new Target_default();
  labelCache.setSize = function() {
    console.warn("labelCache is deprecated.");
  };
  var measureContext = null;
  var measureFont;
  var textHeights = {};
  var registerFont = function() {
    var retries = 100;
    var size = "32px ";
    var referenceFonts = ["monospace", "serif"];
    var len = referenceFonts.length;
    var text = "wmytzilWMYTZIL@#/&?$%10\uF013";
    var interval, referenceWidth;
    function isAvailable(fontStyle, fontWeight, fontFamily) {
      var available = true;
      for (var i = 0; i < len; ++i) {
        var referenceFont = referenceFonts[i];
        referenceWidth = measureTextWidth(fontStyle + " " + fontWeight + " " + size + referenceFont, text);
        if (fontFamily != referenceFont) {
          var width = measureTextWidth(fontStyle + " " + fontWeight + " " + size + fontFamily + "," + referenceFont, text);
          available = available && width != referenceWidth;
        }
      }
      if (available) {
        return true;
      }
      return false;
    }
    function check() {
      var done = true;
      var fonts = checkedFonts.getKeys();
      for (var i = 0, ii = fonts.length; i < ii; ++i) {
        var font = fonts[i];
        if (checkedFonts.get(font) < retries) {
          if (isAvailable.apply(this, font.split("\n"))) {
            clear(textHeights);
            measureContext = null;
            measureFont = void 0;
            checkedFonts.set(font, retries);
          } else {
            checkedFonts.set(font, checkedFonts.get(font) + 1, true);
            done = false;
          }
        }
      }
      if (done) {
        clearInterval(interval);
        interval = void 0;
      }
    }
    return function(fontSpec) {
      var font = getFontParameters(fontSpec);
      if (!font) {
        return;
      }
      var families = font.families;
      for (var i = 0, ii = families.length; i < ii; ++i) {
        var family = families[i];
        var key = font.style + "\n" + font.weight + "\n" + family;
        if (checkedFonts.get(key) === void 0) {
          checkedFonts.set(key, retries, true);
          if (!isAvailable(font.style, font.weight, family)) {
            checkedFonts.set(key, 0, true);
            if (interval === void 0) {
              interval = setInterval(check, 32);
            }
          }
        }
      }
    };
  }();
  var measureTextHeight = function() {
    var measureElement;
    return function(fontSpec) {
      var height = textHeights[fontSpec];
      if (height == void 0) {
        if (WORKER_OFFSCREEN_CANVAS) {
          var font = getFontParameters(fontSpec);
          var metrics = measureText(fontSpec, "\u017Dg");
          var lineHeight = isNaN(Number(font.lineHeight)) ? 1.2 : Number(font.lineHeight);
          height = lineHeight * (metrics.actualBoundingBoxAscent + metrics.actualBoundingBoxDescent);
        } else {
          if (!measureElement) {
            measureElement = document.createElement("div");
            measureElement.innerHTML = "M";
            measureElement.style.minHeight = "0";
            measureElement.style.maxHeight = "none";
            measureElement.style.height = "auto";
            measureElement.style.padding = "0";
            measureElement.style.border = "none";
            measureElement.style.position = "absolute";
            measureElement.style.display = "block";
            measureElement.style.left = "-99999px";
          }
          measureElement.style.font = fontSpec;
          document.body.appendChild(measureElement);
          height = measureElement.offsetHeight;
          document.body.removeChild(measureElement);
        }
        textHeights[fontSpec] = height;
      }
      return height;
    };
  }();
  function measureText(font, text) {
    if (!measureContext) {
      measureContext = createCanvasContext2D(1, 1);
    }
    if (font != measureFont) {
      measureContext.font = font;
      measureFont = measureContext.font;
    }
    return measureContext.measureText(text);
  }
  function measureTextWidth(font, text) {
    return measureText(font, text).width;
  }
  function measureAndCacheTextWidth(font, text, cache2) {
    if (text in cache2) {
      return cache2[text];
    }
    var width = measureTextWidth(font, text);
    cache2[text] = width;
    return width;
  }
  function getTextDimensions(baseStyle, chunks) {
    var widths = [];
    var heights = [];
    var lineWidths = [];
    var width = 0;
    var lineWidth = 0;
    var height = 0;
    var lineHeight = 0;
    for (var i = 0, ii = chunks.length; i <= ii; i += 2) {
      var text = chunks[i];
      if (text === "\n" || i === ii) {
        width = Math.max(width, lineWidth);
        lineWidths.push(lineWidth);
        lineWidth = 0;
        height += lineHeight;
        continue;
      }
      var font = chunks[i + 1] || baseStyle.font;
      var currentWidth = measureTextWidth(font, text);
      widths.push(currentWidth);
      lineWidth += currentWidth;
      var currentHeight = measureTextHeight(font);
      heights.push(currentHeight);
      lineHeight = Math.max(lineHeight, currentHeight);
    }
    return { width, height, widths, heights, lineWidths };
  }
  function drawImageOrLabel(context, transform2, opacity, labelOrImage, originX, originY, w, h, x, y, scale2) {
    context.save();
    if (opacity !== 1) {
      context.globalAlpha *= opacity;
    }
    if (transform2) {
      context.setTransform.apply(context, transform2);
    }
    if (labelOrImage.contextInstructions) {
      context.translate(x, y);
      context.scale(scale2[0], scale2[1]);
      executeLabelInstructions(labelOrImage, context);
    } else if (scale2[0] < 0 || scale2[1] < 0) {
      context.translate(x, y);
      context.scale(scale2[0], scale2[1]);
      context.drawImage(labelOrImage, originX, originY, w, h, 0, 0, w, h);
    } else {
      context.drawImage(labelOrImage, originX, originY, w, h, x, y, w * scale2[0], h * scale2[1]);
    }
    context.restore();
  }
  function executeLabelInstructions(label, context) {
    var contextInstructions = label.contextInstructions;
    for (var i = 0, ii = contextInstructions.length; i < ii; i += 2) {
      if (Array.isArray(contextInstructions[i + 1])) {
        context[contextInstructions[i]].apply(context, contextInstructions[i + 1]);
      } else {
        context[contextInstructions[i]] = contextInstructions[i + 1];
      }
    }
  }

  // node_modules/ol/style/RegularShape.js
  var __extends28 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var RegularShape = function(_super) {
    __extends28(RegularShape2, _super);
    function RegularShape2(options) {
      var _this = this;
      var rotateWithView = options.rotateWithView !== void 0 ? options.rotateWithView : false;
      _this = _super.call(this, {
        opacity: 1,
        rotateWithView,
        rotation: options.rotation !== void 0 ? options.rotation : 0,
        scale: options.scale !== void 0 ? options.scale : 1,
        displacement: options.displacement !== void 0 ? options.displacement : [0, 0]
      }) || this;
      _this.canvas_ = void 0;
      _this.hitDetectionCanvas_ = null;
      _this.fill_ = options.fill !== void 0 ? options.fill : null;
      _this.origin_ = [0, 0];
      _this.points_ = options.points;
      _this.radius_ = options.radius !== void 0 ? options.radius : options.radius1;
      _this.radius2_ = options.radius2;
      _this.angle_ = options.angle !== void 0 ? options.angle : 0;
      _this.stroke_ = options.stroke !== void 0 ? options.stroke : null;
      _this.size_ = null;
      _this.renderOptions_ = null;
      _this.render();
      return _this;
    }
    RegularShape2.prototype.clone = function() {
      var scale2 = this.getScale();
      var style = new RegularShape2({
        fill: this.getFill() ? this.getFill().clone() : void 0,
        points: this.getPoints(),
        radius: this.getRadius(),
        radius2: this.getRadius2(),
        angle: this.getAngle(),
        stroke: this.getStroke() ? this.getStroke().clone() : void 0,
        rotation: this.getRotation(),
        rotateWithView: this.getRotateWithView(),
        scale: Array.isArray(scale2) ? scale2.slice() : scale2,
        displacement: this.getDisplacement().slice()
      });
      style.setOpacity(this.getOpacity());
      return style;
    };
    RegularShape2.prototype.getAnchor = function() {
      var size = this.size_;
      if (!size) {
        return null;
      }
      var displacement = this.getDisplacement();
      return [size[0] / 2 - displacement[0], size[1] / 2 + displacement[1]];
    };
    RegularShape2.prototype.getAngle = function() {
      return this.angle_;
    };
    RegularShape2.prototype.getFill = function() {
      return this.fill_;
    };
    RegularShape2.prototype.getHitDetectionImage = function() {
      if (!this.hitDetectionCanvas_) {
        this.createHitDetectionCanvas_(this.renderOptions_);
      }
      return this.hitDetectionCanvas_;
    };
    RegularShape2.prototype.getImage = function(pixelRatio) {
      var image = this.canvas_[pixelRatio];
      if (!image) {
        var renderOptions = this.renderOptions_;
        var context = createCanvasContext2D(renderOptions.size * pixelRatio, renderOptions.size * pixelRatio);
        this.draw_(renderOptions, context, pixelRatio);
        image = context.canvas;
        this.canvas_[pixelRatio] = image;
      }
      return image;
    };
    RegularShape2.prototype.getPixelRatio = function(pixelRatio) {
      return pixelRatio;
    };
    RegularShape2.prototype.getImageSize = function() {
      return this.size_;
    };
    RegularShape2.prototype.getImageState = function() {
      return ImageState_default.LOADED;
    };
    RegularShape2.prototype.getOrigin = function() {
      return this.origin_;
    };
    RegularShape2.prototype.getPoints = function() {
      return this.points_;
    };
    RegularShape2.prototype.getRadius = function() {
      return this.radius_;
    };
    RegularShape2.prototype.getRadius2 = function() {
      return this.radius2_;
    };
    RegularShape2.prototype.getSize = function() {
      return this.size_;
    };
    RegularShape2.prototype.getStroke = function() {
      return this.stroke_;
    };
    RegularShape2.prototype.listenImageChange = function(listener) {
    };
    RegularShape2.prototype.load = function() {
    };
    RegularShape2.prototype.unlistenImageChange = function(listener) {
    };
    RegularShape2.prototype.calculateLineJoinSize_ = function(lineJoin, strokeWidth, miterLimit) {
      if (strokeWidth === 0 || this.points_ === Infinity || lineJoin !== "bevel" && lineJoin !== "miter") {
        return strokeWidth;
      }
      var r1 = this.radius_;
      var r2 = this.radius2_ === void 0 ? r1 : this.radius2_;
      if (r1 < r2) {
        var tmp = r1;
        r1 = r2;
        r2 = tmp;
      }
      var points = this.radius2_ === void 0 ? this.points_ : this.points_ * 2;
      var alpha = 2 * Math.PI / points;
      var a = r2 * Math.sin(alpha);
      var b = Math.sqrt(r2 * r2 - a * a);
      var d = r1 - b;
      var e = Math.sqrt(a * a + d * d);
      var miterRatio = e / a;
      if (lineJoin === "miter" && miterRatio <= miterLimit) {
        return miterRatio * strokeWidth;
      }
      var k = strokeWidth / 2 / miterRatio;
      var l = strokeWidth / 2 * (d / e);
      var maxr = Math.sqrt((r1 + k) * (r1 + k) + l * l);
      var bevelAdd = maxr - r1;
      if (this.radius2_ === void 0 || lineJoin === "bevel") {
        return bevelAdd * 2;
      }
      var aa = r1 * Math.sin(alpha);
      var bb = Math.sqrt(r1 * r1 - aa * aa);
      var dd = r2 - bb;
      var ee = Math.sqrt(aa * aa + dd * dd);
      var innerMiterRatio = ee / aa;
      if (innerMiterRatio <= miterLimit) {
        var innerLength = innerMiterRatio * strokeWidth / 2 - r2 - r1;
        return 2 * Math.max(bevelAdd, innerLength);
      }
      return bevelAdd * 2;
    };
    RegularShape2.prototype.createRenderOptions = function() {
      var lineJoin = defaultLineJoin;
      var miterLimit = 0;
      var lineDash = null;
      var lineDashOffset = 0;
      var strokeStyle;
      var strokeWidth = 0;
      if (this.stroke_) {
        strokeStyle = this.stroke_.getColor();
        if (strokeStyle === null) {
          strokeStyle = defaultStrokeStyle;
        }
        strokeStyle = asColorLike(strokeStyle);
        strokeWidth = this.stroke_.getWidth();
        if (strokeWidth === void 0) {
          strokeWidth = defaultLineWidth;
        }
        lineDash = this.stroke_.getLineDash();
        lineDashOffset = this.stroke_.getLineDashOffset();
        lineJoin = this.stroke_.getLineJoin();
        if (lineJoin === void 0) {
          lineJoin = defaultLineJoin;
        }
        miterLimit = this.stroke_.getMiterLimit();
        if (miterLimit === void 0) {
          miterLimit = defaultMiterLimit;
        }
      }
      var add3 = this.calculateLineJoinSize_(lineJoin, strokeWidth, miterLimit);
      var maxRadius = Math.max(this.radius_, this.radius2_ || 0);
      var size = Math.ceil(2 * maxRadius + add3);
      return {
        strokeStyle,
        strokeWidth,
        size,
        lineDash,
        lineDashOffset,
        lineJoin,
        miterLimit
      };
    };
    RegularShape2.prototype.render = function() {
      this.renderOptions_ = this.createRenderOptions();
      var size = this.renderOptions_.size;
      this.canvas_ = {};
      this.size_ = [size, size];
    };
    RegularShape2.prototype.draw_ = function(renderOptions, context, pixelRatio) {
      context.scale(pixelRatio, pixelRatio);
      context.translate(renderOptions.size / 2, renderOptions.size / 2);
      this.createPath_(context);
      if (this.fill_) {
        var color = this.fill_.getColor();
        if (color === null) {
          color = defaultFillStyle;
        }
        context.fillStyle = asColorLike(color);
        context.fill();
      }
      if (this.stroke_) {
        context.strokeStyle = renderOptions.strokeStyle;
        context.lineWidth = renderOptions.strokeWidth;
        if (context.setLineDash && renderOptions.lineDash) {
          context.setLineDash(renderOptions.lineDash);
          context.lineDashOffset = renderOptions.lineDashOffset;
        }
        context.lineJoin = renderOptions.lineJoin;
        context.miterLimit = renderOptions.miterLimit;
        context.stroke();
      }
    };
    RegularShape2.prototype.createHitDetectionCanvas_ = function(renderOptions) {
      if (this.fill_) {
        var color = this.fill_.getColor();
        var opacity = 0;
        if (typeof color === "string") {
          color = asArray(color);
        }
        if (color === null) {
          opacity = 1;
        } else if (Array.isArray(color)) {
          opacity = color.length === 4 ? color[3] : 1;
        }
        if (opacity === 0) {
          var context = createCanvasContext2D(renderOptions.size, renderOptions.size);
          this.hitDetectionCanvas_ = context.canvas;
          this.drawHitDetectionCanvas_(renderOptions, context);
        }
      }
      if (!this.hitDetectionCanvas_) {
        this.hitDetectionCanvas_ = this.getImage(1);
      }
    };
    RegularShape2.prototype.createPath_ = function(context) {
      var points = this.points_;
      var radius = this.radius_;
      if (points === Infinity) {
        context.arc(0, 0, radius, 0, 2 * Math.PI);
      } else {
        var radius2 = this.radius2_ === void 0 ? radius : this.radius2_;
        if (this.radius2_ !== void 0) {
          points *= 2;
        }
        var startAngle = this.angle_ - Math.PI / 2;
        var step = 2 * Math.PI / points;
        for (var i = 0; i < points; i++) {
          var angle0 = startAngle + i * step;
          var radiusC = i % 2 === 0 ? radius : radius2;
          context.lineTo(radiusC * Math.cos(angle0), radiusC * Math.sin(angle0));
        }
        context.closePath();
      }
    };
    RegularShape2.prototype.drawHitDetectionCanvas_ = function(renderOptions, context) {
      context.translate(renderOptions.size / 2, renderOptions.size / 2);
      this.createPath_(context);
      context.fillStyle = defaultFillStyle;
      context.fill();
      if (this.stroke_) {
        context.strokeStyle = renderOptions.strokeStyle;
        context.lineWidth = renderOptions.strokeWidth;
        if (renderOptions.lineDash) {
          context.setLineDash(renderOptions.lineDash);
          context.lineDashOffset = renderOptions.lineDashOffset;
        }
        context.lineJoin = renderOptions.lineJoin;
        context.miterLimit = renderOptions.miterLimit;
        context.stroke();
      }
    };
    return RegularShape2;
  }(Image_default);
  var RegularShape_default = RegularShape;

  // node_modules/ol/style/Circle.js
  var __extends29 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var CircleStyle = function(_super) {
    __extends29(CircleStyle2, _super);
    function CircleStyle2(opt_options) {
      var options = opt_options ? opt_options : {};
      return _super.call(this, {
        points: Infinity,
        fill: options.fill,
        radius: options.radius,
        stroke: options.stroke,
        scale: options.scale !== void 0 ? options.scale : 1,
        rotation: options.rotation !== void 0 ? options.rotation : 0,
        rotateWithView: options.rotateWithView !== void 0 ? options.rotateWithView : false,
        displacement: options.displacement !== void 0 ? options.displacement : [0, 0]
      }) || this;
    }
    CircleStyle2.prototype.clone = function() {
      var scale2 = this.getScale();
      var style = new CircleStyle2({
        fill: this.getFill() ? this.getFill().clone() : void 0,
        stroke: this.getStroke() ? this.getStroke().clone() : void 0,
        radius: this.getRadius(),
        scale: Array.isArray(scale2) ? scale2.slice() : scale2,
        rotation: this.getRotation(),
        rotateWithView: this.getRotateWithView(),
        displacement: this.getDisplacement().slice()
      });
      style.setOpacity(this.getOpacity());
      return style;
    };
    CircleStyle2.prototype.setRadius = function(radius) {
      this.radius_ = radius;
      this.render();
    };
    return CircleStyle2;
  }(RegularShape_default);
  var Circle_default2 = CircleStyle;

  // node_modules/ol/style/Fill.js
  var Fill = function() {
    function Fill2(opt_options) {
      var options = opt_options || {};
      this.color_ = options.color !== void 0 ? options.color : null;
    }
    Fill2.prototype.clone = function() {
      var color = this.getColor();
      return new Fill2({
        color: Array.isArray(color) ? color.slice() : color || void 0
      });
    };
    Fill2.prototype.getColor = function() {
      return this.color_;
    };
    Fill2.prototype.setColor = function(color) {
      this.color_ = color;
    };
    return Fill2;
  }();
  var Fill_default = Fill;

  // node_modules/ol/style/Stroke.js
  var Stroke = function() {
    function Stroke2(opt_options) {
      var options = opt_options || {};
      this.color_ = options.color !== void 0 ? options.color : null;
      this.lineCap_ = options.lineCap;
      this.lineDash_ = options.lineDash !== void 0 ? options.lineDash : null;
      this.lineDashOffset_ = options.lineDashOffset;
      this.lineJoin_ = options.lineJoin;
      this.miterLimit_ = options.miterLimit;
      this.width_ = options.width;
    }
    Stroke2.prototype.clone = function() {
      var color = this.getColor();
      return new Stroke2({
        color: Array.isArray(color) ? color.slice() : color || void 0,
        lineCap: this.getLineCap(),
        lineDash: this.getLineDash() ? this.getLineDash().slice() : void 0,
        lineDashOffset: this.getLineDashOffset(),
        lineJoin: this.getLineJoin(),
        miterLimit: this.getMiterLimit(),
        width: this.getWidth()
      });
    };
    Stroke2.prototype.getColor = function() {
      return this.color_;
    };
    Stroke2.prototype.getLineCap = function() {
      return this.lineCap_;
    };
    Stroke2.prototype.getLineDash = function() {
      return this.lineDash_;
    };
    Stroke2.prototype.getLineDashOffset = function() {
      return this.lineDashOffset_;
    };
    Stroke2.prototype.getLineJoin = function() {
      return this.lineJoin_;
    };
    Stroke2.prototype.getMiterLimit = function() {
      return this.miterLimit_;
    };
    Stroke2.prototype.getWidth = function() {
      return this.width_;
    };
    Stroke2.prototype.setColor = function(color) {
      this.color_ = color;
    };
    Stroke2.prototype.setLineCap = function(lineCap) {
      this.lineCap_ = lineCap;
    };
    Stroke2.prototype.setLineDash = function(lineDash) {
      this.lineDash_ = lineDash;
    };
    Stroke2.prototype.setLineDashOffset = function(lineDashOffset) {
      this.lineDashOffset_ = lineDashOffset;
    };
    Stroke2.prototype.setLineJoin = function(lineJoin) {
      this.lineJoin_ = lineJoin;
    };
    Stroke2.prototype.setMiterLimit = function(miterLimit) {
      this.miterLimit_ = miterLimit;
    };
    Stroke2.prototype.setWidth = function(width) {
      this.width_ = width;
    };
    return Stroke2;
  }();
  var Stroke_default = Stroke;

  // node_modules/ol/style/Style.js
  var Style = function() {
    function Style2(opt_options) {
      var options = opt_options || {};
      this.geometry_ = null;
      this.geometryFunction_ = defaultGeometryFunction;
      if (options.geometry !== void 0) {
        this.setGeometry(options.geometry);
      }
      this.fill_ = options.fill !== void 0 ? options.fill : null;
      this.image_ = options.image !== void 0 ? options.image : null;
      this.renderer_ = options.renderer !== void 0 ? options.renderer : null;
      this.hitDetectionRenderer_ = options.hitDetectionRenderer !== void 0 ? options.hitDetectionRenderer : null;
      this.stroke_ = options.stroke !== void 0 ? options.stroke : null;
      this.text_ = options.text !== void 0 ? options.text : null;
      this.zIndex_ = options.zIndex;
    }
    Style2.prototype.clone = function() {
      var geometry = this.getGeometry();
      if (geometry && typeof geometry === "object") {
        geometry = geometry.clone();
      }
      return new Style2({
        geometry,
        fill: this.getFill() ? this.getFill().clone() : void 0,
        image: this.getImage() ? this.getImage().clone() : void 0,
        renderer: this.getRenderer(),
        stroke: this.getStroke() ? this.getStroke().clone() : void 0,
        text: this.getText() ? this.getText().clone() : void 0,
        zIndex: this.getZIndex()
      });
    };
    Style2.prototype.getRenderer = function() {
      return this.renderer_;
    };
    Style2.prototype.setRenderer = function(renderer) {
      this.renderer_ = renderer;
    };
    Style2.prototype.setHitDetectionRenderer = function(renderer) {
      this.hitDetectionRenderer_ = renderer;
    };
    Style2.prototype.getHitDetectionRenderer = function() {
      return this.hitDetectionRenderer_;
    };
    Style2.prototype.getGeometry = function() {
      return this.geometry_;
    };
    Style2.prototype.getGeometryFunction = function() {
      return this.geometryFunction_;
    };
    Style2.prototype.getFill = function() {
      return this.fill_;
    };
    Style2.prototype.setFill = function(fill) {
      this.fill_ = fill;
    };
    Style2.prototype.getImage = function() {
      return this.image_;
    };
    Style2.prototype.setImage = function(image) {
      this.image_ = image;
    };
    Style2.prototype.getStroke = function() {
      return this.stroke_;
    };
    Style2.prototype.setStroke = function(stroke) {
      this.stroke_ = stroke;
    };
    Style2.prototype.getText = function() {
      return this.text_;
    };
    Style2.prototype.setText = function(text) {
      this.text_ = text;
    };
    Style2.prototype.getZIndex = function() {
      return this.zIndex_;
    };
    Style2.prototype.setGeometry = function(geometry) {
      if (typeof geometry === "function") {
        this.geometryFunction_ = geometry;
      } else if (typeof geometry === "string") {
        this.geometryFunction_ = function(feature) {
          return feature.get(geometry);
        };
      } else if (!geometry) {
        this.geometryFunction_ = defaultGeometryFunction;
      } else if (geometry !== void 0) {
        this.geometryFunction_ = function() {
          return geometry;
        };
      }
      this.geometry_ = geometry;
    };
    Style2.prototype.setZIndex = function(zIndex) {
      this.zIndex_ = zIndex;
    };
    return Style2;
  }();
  function toFunction(obj) {
    var styleFunction;
    if (typeof obj === "function") {
      styleFunction = obj;
    } else {
      var styles_1;
      if (Array.isArray(obj)) {
        styles_1 = obj;
      } else {
        assert(typeof obj.getZIndex === "function", 41);
        var style = obj;
        styles_1 = [style];
      }
      styleFunction = function() {
        return styles_1;
      };
    }
    return styleFunction;
  }
  var defaultStyles = null;
  function createDefaultStyle(feature, resolution) {
    if (!defaultStyles) {
      var fill = new Fill_default({
        color: "rgba(255,255,255,0.4)"
      });
      var stroke = new Stroke_default({
        color: "#3399CC",
        width: 1.25
      });
      defaultStyles = [
        new Style({
          image: new Circle_default2({
            fill,
            stroke,
            radius: 5
          }),
          fill,
          stroke
        })
      ];
    }
    return defaultStyles;
  }
  function createEditingStyle() {
    var styles = {};
    var white = [255, 255, 255, 1];
    var blue = [0, 153, 255, 1];
    var width = 3;
    styles[GeometryType_default.POLYGON] = [
      new Style({
        fill: new Fill_default({
          color: [255, 255, 255, 0.5]
        })
      })
    ];
    styles[GeometryType_default.MULTI_POLYGON] = styles[GeometryType_default.POLYGON];
    styles[GeometryType_default.LINE_STRING] = [
      new Style({
        stroke: new Stroke_default({
          color: white,
          width: width + 2
        })
      }),
      new Style({
        stroke: new Stroke_default({
          color: blue,
          width
        })
      })
    ];
    styles[GeometryType_default.MULTI_LINE_STRING] = styles[GeometryType_default.LINE_STRING];
    styles[GeometryType_default.CIRCLE] = styles[GeometryType_default.POLYGON].concat(styles[GeometryType_default.LINE_STRING]);
    styles[GeometryType_default.POINT] = [
      new Style({
        image: new Circle_default2({
          radius: width * 2,
          fill: new Fill_default({
            color: blue
          }),
          stroke: new Stroke_default({
            color: white,
            width: width / 2
          })
        }),
        zIndex: Infinity
      })
    ];
    styles[GeometryType_default.MULTI_POINT] = styles[GeometryType_default.POINT];
    styles[GeometryType_default.GEOMETRY_COLLECTION] = styles[GeometryType_default.POLYGON].concat(styles[GeometryType_default.LINE_STRING], styles[GeometryType_default.POINT]);
    return styles;
  }
  function defaultGeometryFunction(feature) {
    return feature.getGeometry();
  }

  // node_modules/ol/layer/BaseVector.js
  var __extends30 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Property2 = {
    RENDER_ORDER: "renderOrder"
  };
  var BaseVectorLayer = function(_super) {
    __extends30(BaseVectorLayer2, _super);
    function BaseVectorLayer2(opt_options) {
      var _this = this;
      var options = opt_options ? opt_options : {};
      var baseOptions = assign({}, options);
      delete baseOptions.style;
      delete baseOptions.renderBuffer;
      delete baseOptions.updateWhileAnimating;
      delete baseOptions.updateWhileInteracting;
      _this = _super.call(this, baseOptions) || this;
      _this.declutter_ = options.declutter !== void 0 ? options.declutter : false;
      _this.renderBuffer_ = options.renderBuffer !== void 0 ? options.renderBuffer : 100;
      _this.style_ = null;
      _this.styleFunction_ = void 0;
      _this.setStyle(options.style);
      _this.updateWhileAnimating_ = options.updateWhileAnimating !== void 0 ? options.updateWhileAnimating : false;
      _this.updateWhileInteracting_ = options.updateWhileInteracting !== void 0 ? options.updateWhileInteracting : false;
      return _this;
    }
    BaseVectorLayer2.prototype.getDeclutter = function() {
      return this.declutter_;
    };
    BaseVectorLayer2.prototype.getFeatures = function(pixel) {
      return _super.prototype.getFeatures.call(this, pixel);
    };
    BaseVectorLayer2.prototype.getRenderBuffer = function() {
      return this.renderBuffer_;
    };
    BaseVectorLayer2.prototype.getRenderOrder = function() {
      return this.get(Property2.RENDER_ORDER);
    };
    BaseVectorLayer2.prototype.getStyle = function() {
      return this.style_;
    };
    BaseVectorLayer2.prototype.getStyleFunction = function() {
      return this.styleFunction_;
    };
    BaseVectorLayer2.prototype.getUpdateWhileAnimating = function() {
      return this.updateWhileAnimating_;
    };
    BaseVectorLayer2.prototype.getUpdateWhileInteracting = function() {
      return this.updateWhileInteracting_;
    };
    BaseVectorLayer2.prototype.renderDeclutter = function(frameState) {
      if (!frameState.declutterTree) {
        frameState.declutterTree = new import_rbush.default(9);
      }
      this.getRenderer().renderDeclutter(frameState);
    };
    BaseVectorLayer2.prototype.setRenderOrder = function(renderOrder) {
      this.set(Property2.RENDER_ORDER, renderOrder);
    };
    BaseVectorLayer2.prototype.setStyle = function(opt_style) {
      this.style_ = opt_style !== void 0 ? opt_style : createDefaultStyle;
      this.styleFunction_ = opt_style === null ? void 0 : toFunction(this.style_);
      this.changed();
    };
    return BaseVectorLayer2;
  }(Layer_default);
  var BaseVector_default = BaseVectorLayer;

  // node_modules/ol/render/canvas/Instruction.js
  var Instruction = {
    BEGIN_GEOMETRY: 0,
    BEGIN_PATH: 1,
    CIRCLE: 2,
    CLOSE_PATH: 3,
    CUSTOM: 4,
    DRAW_CHARS: 5,
    DRAW_IMAGE: 6,
    END_GEOMETRY: 7,
    FILL: 8,
    MOVE_TO_LINE_TO: 9,
    SET_FILL_STYLE: 10,
    SET_STROKE_STYLE: 11,
    STROKE: 12
  };
  var fillInstruction = [Instruction.FILL];
  var strokeInstruction = [Instruction.STROKE];
  var beginPathInstruction = [Instruction.BEGIN_PATH];
  var closePathInstruction = [Instruction.CLOSE_PATH];
  var Instruction_default = Instruction;

  // node_modules/ol/render/VectorContext.js
  var VectorContext = function() {
    function VectorContext2() {
    }
    VectorContext2.prototype.drawCustom = function(geometry, feature, renderer, hitDetectionRenderer) {
    };
    VectorContext2.prototype.drawGeometry = function(geometry) {
    };
    VectorContext2.prototype.setStyle = function(style) {
    };
    VectorContext2.prototype.drawCircle = function(circleGeometry, feature) {
    };
    VectorContext2.prototype.drawFeature = function(feature, style) {
    };
    VectorContext2.prototype.drawGeometryCollection = function(geometryCollectionGeometry, feature) {
    };
    VectorContext2.prototype.drawLineString = function(lineStringGeometry, feature) {
    };
    VectorContext2.prototype.drawMultiLineString = function(multiLineStringGeometry, feature) {
    };
    VectorContext2.prototype.drawMultiPoint = function(multiPointGeometry, feature) {
    };
    VectorContext2.prototype.drawMultiPolygon = function(multiPolygonGeometry, feature) {
    };
    VectorContext2.prototype.drawPoint = function(pointGeometry, feature) {
    };
    VectorContext2.prototype.drawPolygon = function(polygonGeometry, feature) {
    };
    VectorContext2.prototype.drawText = function(geometry, feature) {
    };
    VectorContext2.prototype.setFillStrokeStyle = function(fillStyle, strokeStyle) {
    };
    VectorContext2.prototype.setImageStyle = function(imageStyle, opt_declutterImageWithText) {
    };
    VectorContext2.prototype.setTextStyle = function(textStyle, opt_declutterImageWithText) {
    };
    return VectorContext2;
  }();
  var VectorContext_default = VectorContext;

  // node_modules/ol/render/canvas/Builder.js
  var __extends31 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var CanvasBuilder = function(_super) {
    __extends31(CanvasBuilder2, _super);
    function CanvasBuilder2(tolerance, maxExtent, resolution, pixelRatio) {
      var _this = _super.call(this) || this;
      _this.tolerance = tolerance;
      _this.maxExtent = maxExtent;
      _this.pixelRatio = pixelRatio;
      _this.maxLineWidth = 0;
      _this.resolution = resolution;
      _this.beginGeometryInstruction1_ = null;
      _this.beginGeometryInstruction2_ = null;
      _this.bufferedMaxExtent_ = null;
      _this.instructions = [];
      _this.coordinates = [];
      _this.tmpCoordinate_ = [];
      _this.hitDetectionInstructions = [];
      _this.state = {};
      return _this;
    }
    CanvasBuilder2.prototype.applyPixelRatio = function(dashArray) {
      var pixelRatio = this.pixelRatio;
      return pixelRatio == 1 ? dashArray : dashArray.map(function(dash) {
        return dash * pixelRatio;
      });
    };
    CanvasBuilder2.prototype.appendFlatPointCoordinates = function(flatCoordinates, stride) {
      var extent = this.getBufferedMaxExtent();
      var tmpCoord = this.tmpCoordinate_;
      var coordinates2 = this.coordinates;
      var myEnd = coordinates2.length;
      for (var i = 0, ii = flatCoordinates.length; i < ii; i += stride) {
        tmpCoord[0] = flatCoordinates[i];
        tmpCoord[1] = flatCoordinates[i + 1];
        if (containsCoordinate(extent, tmpCoord)) {
          coordinates2[myEnd++] = tmpCoord[0];
          coordinates2[myEnd++] = tmpCoord[1];
        }
      }
      return myEnd;
    };
    CanvasBuilder2.prototype.appendFlatLineCoordinates = function(flatCoordinates, offset, end, stride, closed, skipFirst) {
      var coordinates2 = this.coordinates;
      var myEnd = coordinates2.length;
      var extent = this.getBufferedMaxExtent();
      if (skipFirst) {
        offset += stride;
      }
      var lastXCoord = flatCoordinates[offset];
      var lastYCoord = flatCoordinates[offset + 1];
      var nextCoord = this.tmpCoordinate_;
      var skipped = true;
      var i, lastRel, nextRel;
      for (i = offset + stride; i < end; i += stride) {
        nextCoord[0] = flatCoordinates[i];
        nextCoord[1] = flatCoordinates[i + 1];
        nextRel = coordinateRelationship(extent, nextCoord);
        if (nextRel !== lastRel) {
          if (skipped) {
            coordinates2[myEnd++] = lastXCoord;
            coordinates2[myEnd++] = lastYCoord;
            skipped = false;
          }
          coordinates2[myEnd++] = nextCoord[0];
          coordinates2[myEnd++] = nextCoord[1];
        } else if (nextRel === Relationship_default.INTERSECTING) {
          coordinates2[myEnd++] = nextCoord[0];
          coordinates2[myEnd++] = nextCoord[1];
          skipped = false;
        } else {
          skipped = true;
        }
        lastXCoord = nextCoord[0];
        lastYCoord = nextCoord[1];
        lastRel = nextRel;
      }
      if (closed && skipped || i === offset + stride) {
        coordinates2[myEnd++] = lastXCoord;
        coordinates2[myEnd++] = lastYCoord;
      }
      return myEnd;
    };
    CanvasBuilder2.prototype.drawCustomCoordinates_ = function(flatCoordinates, offset, ends, stride, builderEnds) {
      for (var i = 0, ii = ends.length; i < ii; ++i) {
        var end = ends[i];
        var builderEnd = this.appendFlatLineCoordinates(flatCoordinates, offset, end, stride, false, false);
        builderEnds.push(builderEnd);
        offset = end;
      }
      return offset;
    };
    CanvasBuilder2.prototype.drawCustom = function(geometry, feature, renderer, hitDetectionRenderer) {
      this.beginGeometry(geometry, feature);
      var type = geometry.getType();
      var stride = geometry.getStride();
      var builderBegin = this.coordinates.length;
      var flatCoordinates, builderEnd, builderEnds, builderEndss;
      var offset;
      switch (type) {
        case GeometryType_default.MULTI_POLYGON:
          flatCoordinates = geometry.getOrientedFlatCoordinates();
          builderEndss = [];
          var endss = geometry.getEndss();
          offset = 0;
          for (var i = 0, ii = endss.length; i < ii; ++i) {
            var myEnds = [];
            offset = this.drawCustomCoordinates_(flatCoordinates, offset, endss[i], stride, myEnds);
            builderEndss.push(myEnds);
          }
          this.instructions.push([
            Instruction_default.CUSTOM,
            builderBegin,
            builderEndss,
            geometry,
            renderer,
            inflateMultiCoordinatesArray
          ]);
          this.hitDetectionInstructions.push([
            Instruction_default.CUSTOM,
            builderBegin,
            builderEndss,
            geometry,
            hitDetectionRenderer || renderer,
            inflateMultiCoordinatesArray
          ]);
          break;
        case GeometryType_default.POLYGON:
        case GeometryType_default.MULTI_LINE_STRING:
          builderEnds = [];
          flatCoordinates = type == GeometryType_default.POLYGON ? geometry.getOrientedFlatCoordinates() : geometry.getFlatCoordinates();
          offset = this.drawCustomCoordinates_(flatCoordinates, 0, geometry.getEnds(), stride, builderEnds);
          this.instructions.push([
            Instruction_default.CUSTOM,
            builderBegin,
            builderEnds,
            geometry,
            renderer,
            inflateCoordinatesArray
          ]);
          this.hitDetectionInstructions.push([
            Instruction_default.CUSTOM,
            builderBegin,
            builderEnds,
            geometry,
            hitDetectionRenderer || renderer,
            inflateCoordinatesArray
          ]);
          break;
        case GeometryType_default.LINE_STRING:
        case GeometryType_default.CIRCLE:
          flatCoordinates = geometry.getFlatCoordinates();
          builderEnd = this.appendFlatLineCoordinates(flatCoordinates, 0, flatCoordinates.length, stride, false, false);
          this.instructions.push([
            Instruction_default.CUSTOM,
            builderBegin,
            builderEnd,
            geometry,
            renderer,
            inflateCoordinates
          ]);
          this.hitDetectionInstructions.push([
            Instruction_default.CUSTOM,
            builderBegin,
            builderEnd,
            geometry,
            hitDetectionRenderer || renderer,
            inflateCoordinates
          ]);
          break;
        case GeometryType_default.MULTI_POINT:
          flatCoordinates = geometry.getFlatCoordinates();
          builderEnd = this.appendFlatPointCoordinates(flatCoordinates, stride);
          if (builderEnd > builderBegin) {
            this.instructions.push([
              Instruction_default.CUSTOM,
              builderBegin,
              builderEnd,
              geometry,
              renderer,
              inflateCoordinates
            ]);
            this.hitDetectionInstructions.push([
              Instruction_default.CUSTOM,
              builderBegin,
              builderEnd,
              geometry,
              hitDetectionRenderer || renderer,
              inflateCoordinates
            ]);
          }
          break;
        case GeometryType_default.POINT:
          flatCoordinates = geometry.getFlatCoordinates();
          this.coordinates.push(flatCoordinates[0], flatCoordinates[1]);
          builderEnd = this.coordinates.length;
          this.instructions.push([
            Instruction_default.CUSTOM,
            builderBegin,
            builderEnd,
            geometry,
            renderer
          ]);
          this.hitDetectionInstructions.push([
            Instruction_default.CUSTOM,
            builderBegin,
            builderEnd,
            geometry,
            hitDetectionRenderer || renderer
          ]);
          break;
        default:
      }
      this.endGeometry(feature);
    };
    CanvasBuilder2.prototype.beginGeometry = function(geometry, feature) {
      this.beginGeometryInstruction1_ = [
        Instruction_default.BEGIN_GEOMETRY,
        feature,
        0,
        geometry
      ];
      this.instructions.push(this.beginGeometryInstruction1_);
      this.beginGeometryInstruction2_ = [
        Instruction_default.BEGIN_GEOMETRY,
        feature,
        0,
        geometry
      ];
      this.hitDetectionInstructions.push(this.beginGeometryInstruction2_);
    };
    CanvasBuilder2.prototype.finish = function() {
      return {
        instructions: this.instructions,
        hitDetectionInstructions: this.hitDetectionInstructions,
        coordinates: this.coordinates
      };
    };
    CanvasBuilder2.prototype.reverseHitDetectionInstructions = function() {
      var hitDetectionInstructions = this.hitDetectionInstructions;
      hitDetectionInstructions.reverse();
      var i;
      var n = hitDetectionInstructions.length;
      var instruction;
      var type;
      var begin = -1;
      for (i = 0; i < n; ++i) {
        instruction = hitDetectionInstructions[i];
        type = instruction[0];
        if (type == Instruction_default.END_GEOMETRY) {
          begin = i;
        } else if (type == Instruction_default.BEGIN_GEOMETRY) {
          instruction[2] = i;
          reverseSubArray(this.hitDetectionInstructions, begin, i);
          begin = -1;
        }
      }
    };
    CanvasBuilder2.prototype.setFillStrokeStyle = function(fillStyle, strokeStyle) {
      var state = this.state;
      if (fillStyle) {
        var fillStyleColor = fillStyle.getColor();
        state.fillStyle = asColorLike(fillStyleColor ? fillStyleColor : defaultFillStyle);
      } else {
        state.fillStyle = void 0;
      }
      if (strokeStyle) {
        var strokeStyleColor = strokeStyle.getColor();
        state.strokeStyle = asColorLike(strokeStyleColor ? strokeStyleColor : defaultStrokeStyle);
        var strokeStyleLineCap = strokeStyle.getLineCap();
        state.lineCap = strokeStyleLineCap !== void 0 ? strokeStyleLineCap : defaultLineCap;
        var strokeStyleLineDash = strokeStyle.getLineDash();
        state.lineDash = strokeStyleLineDash ? strokeStyleLineDash.slice() : defaultLineDash;
        var strokeStyleLineDashOffset = strokeStyle.getLineDashOffset();
        state.lineDashOffset = strokeStyleLineDashOffset ? strokeStyleLineDashOffset : defaultLineDashOffset;
        var strokeStyleLineJoin = strokeStyle.getLineJoin();
        state.lineJoin = strokeStyleLineJoin !== void 0 ? strokeStyleLineJoin : defaultLineJoin;
        var strokeStyleWidth = strokeStyle.getWidth();
        state.lineWidth = strokeStyleWidth !== void 0 ? strokeStyleWidth : defaultLineWidth;
        var strokeStyleMiterLimit = strokeStyle.getMiterLimit();
        state.miterLimit = strokeStyleMiterLimit !== void 0 ? strokeStyleMiterLimit : defaultMiterLimit;
        if (state.lineWidth > this.maxLineWidth) {
          this.maxLineWidth = state.lineWidth;
          this.bufferedMaxExtent_ = null;
        }
      } else {
        state.strokeStyle = void 0;
        state.lineCap = void 0;
        state.lineDash = null;
        state.lineDashOffset = void 0;
        state.lineJoin = void 0;
        state.lineWidth = void 0;
        state.miterLimit = void 0;
      }
    };
    CanvasBuilder2.prototype.createFill = function(state) {
      var fillStyle = state.fillStyle;
      var fillInstruction2 = [Instruction_default.SET_FILL_STYLE, fillStyle];
      if (typeof fillStyle !== "string") {
        fillInstruction2.push(true);
      }
      return fillInstruction2;
    };
    CanvasBuilder2.prototype.applyStroke = function(state) {
      this.instructions.push(this.createStroke(state));
    };
    CanvasBuilder2.prototype.createStroke = function(state) {
      return [
        Instruction_default.SET_STROKE_STYLE,
        state.strokeStyle,
        state.lineWidth * this.pixelRatio,
        state.lineCap,
        state.lineJoin,
        state.miterLimit,
        this.applyPixelRatio(state.lineDash),
        state.lineDashOffset * this.pixelRatio
      ];
    };
    CanvasBuilder2.prototype.updateFillStyle = function(state, createFill) {
      var fillStyle = state.fillStyle;
      if (typeof fillStyle !== "string" || state.currentFillStyle != fillStyle) {
        if (fillStyle !== void 0) {
          this.instructions.push(createFill.call(this, state));
        }
        state.currentFillStyle = fillStyle;
      }
    };
    CanvasBuilder2.prototype.updateStrokeStyle = function(state, applyStroke) {
      var strokeStyle = state.strokeStyle;
      var lineCap = state.lineCap;
      var lineDash = state.lineDash;
      var lineDashOffset = state.lineDashOffset;
      var lineJoin = state.lineJoin;
      var lineWidth = state.lineWidth;
      var miterLimit = state.miterLimit;
      if (state.currentStrokeStyle != strokeStyle || state.currentLineCap != lineCap || lineDash != state.currentLineDash && !equals(state.currentLineDash, lineDash) || state.currentLineDashOffset != lineDashOffset || state.currentLineJoin != lineJoin || state.currentLineWidth != lineWidth || state.currentMiterLimit != miterLimit) {
        if (strokeStyle !== void 0) {
          applyStroke.call(this, state);
        }
        state.currentStrokeStyle = strokeStyle;
        state.currentLineCap = lineCap;
        state.currentLineDash = lineDash;
        state.currentLineDashOffset = lineDashOffset;
        state.currentLineJoin = lineJoin;
        state.currentLineWidth = lineWidth;
        state.currentMiterLimit = miterLimit;
      }
    };
    CanvasBuilder2.prototype.endGeometry = function(feature) {
      this.beginGeometryInstruction1_[2] = this.instructions.length;
      this.beginGeometryInstruction1_ = null;
      this.beginGeometryInstruction2_[2] = this.hitDetectionInstructions.length;
      this.beginGeometryInstruction2_ = null;
      var endGeometryInstruction = [Instruction_default.END_GEOMETRY, feature];
      this.instructions.push(endGeometryInstruction);
      this.hitDetectionInstructions.push(endGeometryInstruction);
    };
    CanvasBuilder2.prototype.getBufferedMaxExtent = function() {
      if (!this.bufferedMaxExtent_) {
        this.bufferedMaxExtent_ = clone(this.maxExtent);
        if (this.maxLineWidth > 0) {
          var width = this.resolution * (this.maxLineWidth + 1) / 2;
          buffer(this.bufferedMaxExtent_, width, this.bufferedMaxExtent_);
        }
      }
      return this.bufferedMaxExtent_;
    };
    return CanvasBuilder2;
  }(VectorContext_default);
  var Builder_default = CanvasBuilder;

  // node_modules/ol/render/canvas/ImageBuilder.js
  var __extends32 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var CanvasImageBuilder = function(_super) {
    __extends32(CanvasImageBuilder2, _super);
    function CanvasImageBuilder2(tolerance, maxExtent, resolution, pixelRatio) {
      var _this = _super.call(this, tolerance, maxExtent, resolution, pixelRatio) || this;
      _this.hitDetectionImage_ = null;
      _this.image_ = null;
      _this.imagePixelRatio_ = void 0;
      _this.anchorX_ = void 0;
      _this.anchorY_ = void 0;
      _this.height_ = void 0;
      _this.opacity_ = void 0;
      _this.originX_ = void 0;
      _this.originY_ = void 0;
      _this.rotateWithView_ = void 0;
      _this.rotation_ = void 0;
      _this.scale_ = void 0;
      _this.width_ = void 0;
      _this.declutterImageWithText_ = void 0;
      return _this;
    }
    CanvasImageBuilder2.prototype.drawPoint = function(pointGeometry, feature) {
      if (!this.image_) {
        return;
      }
      this.beginGeometry(pointGeometry, feature);
      var flatCoordinates = pointGeometry.getFlatCoordinates();
      var stride = pointGeometry.getStride();
      var myBegin = this.coordinates.length;
      var myEnd = this.appendFlatPointCoordinates(flatCoordinates, stride);
      this.instructions.push([
        Instruction_default.DRAW_IMAGE,
        myBegin,
        myEnd,
        this.image_,
        this.anchorX_ * this.imagePixelRatio_,
        this.anchorY_ * this.imagePixelRatio_,
        Math.ceil(this.height_ * this.imagePixelRatio_),
        this.opacity_,
        this.originX_,
        this.originY_,
        this.rotateWithView_,
        this.rotation_,
        [
          this.scale_[0] * this.pixelRatio / this.imagePixelRatio_,
          this.scale_[1] * this.pixelRatio / this.imagePixelRatio_
        ],
        Math.ceil(this.width_ * this.imagePixelRatio_),
        this.declutterImageWithText_
      ]);
      this.hitDetectionInstructions.push([
        Instruction_default.DRAW_IMAGE,
        myBegin,
        myEnd,
        this.hitDetectionImage_,
        this.anchorX_,
        this.anchorY_,
        this.height_,
        this.opacity_,
        this.originX_,
        this.originY_,
        this.rotateWithView_,
        this.rotation_,
        this.scale_,
        this.width_,
        this.declutterImageWithText_
      ]);
      this.endGeometry(feature);
    };
    CanvasImageBuilder2.prototype.drawMultiPoint = function(multiPointGeometry, feature) {
      if (!this.image_) {
        return;
      }
      this.beginGeometry(multiPointGeometry, feature);
      var flatCoordinates = multiPointGeometry.getFlatCoordinates();
      var stride = multiPointGeometry.getStride();
      var myBegin = this.coordinates.length;
      var myEnd = this.appendFlatPointCoordinates(flatCoordinates, stride);
      this.instructions.push([
        Instruction_default.DRAW_IMAGE,
        myBegin,
        myEnd,
        this.image_,
        this.anchorX_ * this.imagePixelRatio_,
        this.anchorY_ * this.imagePixelRatio_,
        Math.ceil(this.height_ * this.imagePixelRatio_),
        this.opacity_,
        this.originX_,
        this.originY_,
        this.rotateWithView_,
        this.rotation_,
        [
          this.scale_[0] * this.pixelRatio / this.imagePixelRatio_,
          this.scale_[1] * this.pixelRatio / this.imagePixelRatio_
        ],
        Math.ceil(this.width_ * this.imagePixelRatio_),
        this.declutterImageWithText_
      ]);
      this.hitDetectionInstructions.push([
        Instruction_default.DRAW_IMAGE,
        myBegin,
        myEnd,
        this.hitDetectionImage_,
        this.anchorX_,
        this.anchorY_,
        this.height_,
        this.opacity_,
        this.originX_,
        this.originY_,
        this.rotateWithView_,
        this.rotation_,
        this.scale_,
        this.width_,
        this.declutterImageWithText_
      ]);
      this.endGeometry(feature);
    };
    CanvasImageBuilder2.prototype.finish = function() {
      this.reverseHitDetectionInstructions();
      this.anchorX_ = void 0;
      this.anchorY_ = void 0;
      this.hitDetectionImage_ = null;
      this.image_ = null;
      this.imagePixelRatio_ = void 0;
      this.height_ = void 0;
      this.scale_ = void 0;
      this.opacity_ = void 0;
      this.originX_ = void 0;
      this.originY_ = void 0;
      this.rotateWithView_ = void 0;
      this.rotation_ = void 0;
      this.width_ = void 0;
      return _super.prototype.finish.call(this);
    };
    CanvasImageBuilder2.prototype.setImageStyle = function(imageStyle, opt_sharedData) {
      var anchor = imageStyle.getAnchor();
      var size = imageStyle.getSize();
      var hitDetectionImage = imageStyle.getHitDetectionImage();
      var image = imageStyle.getImage(this.pixelRatio);
      var origin = imageStyle.getOrigin();
      this.imagePixelRatio_ = imageStyle.getPixelRatio(this.pixelRatio);
      this.anchorX_ = anchor[0];
      this.anchorY_ = anchor[1];
      this.hitDetectionImage_ = hitDetectionImage;
      this.image_ = image;
      this.height_ = size[1];
      this.opacity_ = imageStyle.getOpacity();
      this.originX_ = origin[0] * this.imagePixelRatio_;
      this.originY_ = origin[1] * this.imagePixelRatio_;
      this.rotateWithView_ = imageStyle.getRotateWithView();
      this.rotation_ = imageStyle.getRotation();
      this.scale_ = imageStyle.getScaleArray();
      this.width_ = size[0];
      this.declutterImageWithText_ = opt_sharedData;
    };
    return CanvasImageBuilder2;
  }(Builder_default);
  var ImageBuilder_default = CanvasImageBuilder;

  // node_modules/ol/render/canvas/LineStringBuilder.js
  var __extends33 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var CanvasLineStringBuilder = function(_super) {
    __extends33(CanvasLineStringBuilder2, _super);
    function CanvasLineStringBuilder2(tolerance, maxExtent, resolution, pixelRatio) {
      return _super.call(this, tolerance, maxExtent, resolution, pixelRatio) || this;
    }
    CanvasLineStringBuilder2.prototype.drawFlatCoordinates_ = function(flatCoordinates, offset, end, stride) {
      var myBegin = this.coordinates.length;
      var myEnd = this.appendFlatLineCoordinates(flatCoordinates, offset, end, stride, false, false);
      var moveToLineToInstruction = [
        Instruction_default.MOVE_TO_LINE_TO,
        myBegin,
        myEnd
      ];
      this.instructions.push(moveToLineToInstruction);
      this.hitDetectionInstructions.push(moveToLineToInstruction);
      return end;
    };
    CanvasLineStringBuilder2.prototype.drawLineString = function(lineStringGeometry, feature) {
      var state = this.state;
      var strokeStyle = state.strokeStyle;
      var lineWidth = state.lineWidth;
      if (strokeStyle === void 0 || lineWidth === void 0) {
        return;
      }
      this.updateStrokeStyle(state, this.applyStroke);
      this.beginGeometry(lineStringGeometry, feature);
      this.hitDetectionInstructions.push([
        Instruction_default.SET_STROKE_STYLE,
        state.strokeStyle,
        state.lineWidth,
        state.lineCap,
        state.lineJoin,
        state.miterLimit,
        defaultLineDash,
        defaultLineDashOffset
      ], beginPathInstruction);
      var flatCoordinates = lineStringGeometry.getFlatCoordinates();
      var stride = lineStringGeometry.getStride();
      this.drawFlatCoordinates_(flatCoordinates, 0, flatCoordinates.length, stride);
      this.hitDetectionInstructions.push(strokeInstruction);
      this.endGeometry(feature);
    };
    CanvasLineStringBuilder2.prototype.drawMultiLineString = function(multiLineStringGeometry, feature) {
      var state = this.state;
      var strokeStyle = state.strokeStyle;
      var lineWidth = state.lineWidth;
      if (strokeStyle === void 0 || lineWidth === void 0) {
        return;
      }
      this.updateStrokeStyle(state, this.applyStroke);
      this.beginGeometry(multiLineStringGeometry, feature);
      this.hitDetectionInstructions.push([
        Instruction_default.SET_STROKE_STYLE,
        state.strokeStyle,
        state.lineWidth,
        state.lineCap,
        state.lineJoin,
        state.miterLimit,
        state.lineDash,
        state.lineDashOffset
      ], beginPathInstruction);
      var ends = multiLineStringGeometry.getEnds();
      var flatCoordinates = multiLineStringGeometry.getFlatCoordinates();
      var stride = multiLineStringGeometry.getStride();
      var offset = 0;
      for (var i = 0, ii = ends.length; i < ii; ++i) {
        offset = this.drawFlatCoordinates_(flatCoordinates, offset, ends[i], stride);
      }
      this.hitDetectionInstructions.push(strokeInstruction);
      this.endGeometry(feature);
    };
    CanvasLineStringBuilder2.prototype.finish = function() {
      var state = this.state;
      if (state.lastStroke != void 0 && state.lastStroke != this.coordinates.length) {
        this.instructions.push(strokeInstruction);
      }
      this.reverseHitDetectionInstructions();
      this.state = null;
      return _super.prototype.finish.call(this);
    };
    CanvasLineStringBuilder2.prototype.applyStroke = function(state) {
      if (state.lastStroke != void 0 && state.lastStroke != this.coordinates.length) {
        this.instructions.push(strokeInstruction);
        state.lastStroke = this.coordinates.length;
      }
      state.lastStroke = 0;
      _super.prototype.applyStroke.call(this, state);
      this.instructions.push(beginPathInstruction);
    };
    return CanvasLineStringBuilder2;
  }(Builder_default);
  var LineStringBuilder_default = CanvasLineStringBuilder;

  // node_modules/ol/render/canvas/PolygonBuilder.js
  var __extends34 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var CanvasPolygonBuilder = function(_super) {
    __extends34(CanvasPolygonBuilder2, _super);
    function CanvasPolygonBuilder2(tolerance, maxExtent, resolution, pixelRatio) {
      return _super.call(this, tolerance, maxExtent, resolution, pixelRatio) || this;
    }
    CanvasPolygonBuilder2.prototype.drawFlatCoordinatess_ = function(flatCoordinates, offset, ends, stride) {
      var state = this.state;
      var fill = state.fillStyle !== void 0;
      var stroke = state.strokeStyle !== void 0;
      var numEnds = ends.length;
      this.instructions.push(beginPathInstruction);
      this.hitDetectionInstructions.push(beginPathInstruction);
      for (var i = 0; i < numEnds; ++i) {
        var end = ends[i];
        var myBegin = this.coordinates.length;
        var myEnd = this.appendFlatLineCoordinates(flatCoordinates, offset, end, stride, true, !stroke);
        var moveToLineToInstruction = [
          Instruction_default.MOVE_TO_LINE_TO,
          myBegin,
          myEnd
        ];
        this.instructions.push(moveToLineToInstruction);
        this.hitDetectionInstructions.push(moveToLineToInstruction);
        if (stroke) {
          this.instructions.push(closePathInstruction);
          this.hitDetectionInstructions.push(closePathInstruction);
        }
        offset = end;
      }
      if (fill) {
        this.instructions.push(fillInstruction);
        this.hitDetectionInstructions.push(fillInstruction);
      }
      if (stroke) {
        this.instructions.push(strokeInstruction);
        this.hitDetectionInstructions.push(strokeInstruction);
      }
      return offset;
    };
    CanvasPolygonBuilder2.prototype.drawCircle = function(circleGeometry, feature) {
      var state = this.state;
      var fillStyle = state.fillStyle;
      var strokeStyle = state.strokeStyle;
      if (fillStyle === void 0 && strokeStyle === void 0) {
        return;
      }
      this.setFillStrokeStyles_();
      this.beginGeometry(circleGeometry, feature);
      if (state.fillStyle !== void 0) {
        this.hitDetectionInstructions.push([
          Instruction_default.SET_FILL_STYLE,
          defaultFillStyle
        ]);
      }
      if (state.strokeStyle !== void 0) {
        this.hitDetectionInstructions.push([
          Instruction_default.SET_STROKE_STYLE,
          state.strokeStyle,
          state.lineWidth,
          state.lineCap,
          state.lineJoin,
          state.miterLimit,
          state.lineDash,
          state.lineDashOffset
        ]);
      }
      var flatCoordinates = circleGeometry.getFlatCoordinates();
      var stride = circleGeometry.getStride();
      var myBegin = this.coordinates.length;
      this.appendFlatLineCoordinates(flatCoordinates, 0, flatCoordinates.length, stride, false, false);
      var circleInstruction = [Instruction_default.CIRCLE, myBegin];
      this.instructions.push(beginPathInstruction, circleInstruction);
      this.hitDetectionInstructions.push(beginPathInstruction, circleInstruction);
      if (state.fillStyle !== void 0) {
        this.instructions.push(fillInstruction);
        this.hitDetectionInstructions.push(fillInstruction);
      }
      if (state.strokeStyle !== void 0) {
        this.instructions.push(strokeInstruction);
        this.hitDetectionInstructions.push(strokeInstruction);
      }
      this.endGeometry(feature);
    };
    CanvasPolygonBuilder2.prototype.drawPolygon = function(polygonGeometry, feature) {
      var state = this.state;
      var fillStyle = state.fillStyle;
      var strokeStyle = state.strokeStyle;
      if (fillStyle === void 0 && strokeStyle === void 0) {
        return;
      }
      this.setFillStrokeStyles_();
      this.beginGeometry(polygonGeometry, feature);
      if (state.fillStyle !== void 0) {
        this.hitDetectionInstructions.push([
          Instruction_default.SET_FILL_STYLE,
          defaultFillStyle
        ]);
      }
      if (state.strokeStyle !== void 0) {
        this.hitDetectionInstructions.push([
          Instruction_default.SET_STROKE_STYLE,
          state.strokeStyle,
          state.lineWidth,
          state.lineCap,
          state.lineJoin,
          state.miterLimit,
          state.lineDash,
          state.lineDashOffset
        ]);
      }
      var ends = polygonGeometry.getEnds();
      var flatCoordinates = polygonGeometry.getOrientedFlatCoordinates();
      var stride = polygonGeometry.getStride();
      this.drawFlatCoordinatess_(flatCoordinates, 0, ends, stride);
      this.endGeometry(feature);
    };
    CanvasPolygonBuilder2.prototype.drawMultiPolygon = function(multiPolygonGeometry, feature) {
      var state = this.state;
      var fillStyle = state.fillStyle;
      var strokeStyle = state.strokeStyle;
      if (fillStyle === void 0 && strokeStyle === void 0) {
        return;
      }
      this.setFillStrokeStyles_();
      this.beginGeometry(multiPolygonGeometry, feature);
      if (state.fillStyle !== void 0) {
        this.hitDetectionInstructions.push([
          Instruction_default.SET_FILL_STYLE,
          defaultFillStyle
        ]);
      }
      if (state.strokeStyle !== void 0) {
        this.hitDetectionInstructions.push([
          Instruction_default.SET_STROKE_STYLE,
          state.strokeStyle,
          state.lineWidth,
          state.lineCap,
          state.lineJoin,
          state.miterLimit,
          state.lineDash,
          state.lineDashOffset
        ]);
      }
      var endss = multiPolygonGeometry.getEndss();
      var flatCoordinates = multiPolygonGeometry.getOrientedFlatCoordinates();
      var stride = multiPolygonGeometry.getStride();
      var offset = 0;
      for (var i = 0, ii = endss.length; i < ii; ++i) {
        offset = this.drawFlatCoordinatess_(flatCoordinates, offset, endss[i], stride);
      }
      this.endGeometry(feature);
    };
    CanvasPolygonBuilder2.prototype.finish = function() {
      this.reverseHitDetectionInstructions();
      this.state = null;
      var tolerance = this.tolerance;
      if (tolerance !== 0) {
        var coordinates2 = this.coordinates;
        for (var i = 0, ii = coordinates2.length; i < ii; ++i) {
          coordinates2[i] = snap(coordinates2[i], tolerance);
        }
      }
      return _super.prototype.finish.call(this);
    };
    CanvasPolygonBuilder2.prototype.setFillStrokeStyles_ = function() {
      var state = this.state;
      var fillStyle = state.fillStyle;
      if (fillStyle !== void 0) {
        this.updateFillStyle(state, this.createFill);
      }
      if (state.strokeStyle !== void 0) {
        this.updateStrokeStyle(state, this.applyStroke);
      }
    };
    return CanvasPolygonBuilder2;
  }(Builder_default);
  var PolygonBuilder_default = CanvasPolygonBuilder;

  // node_modules/ol/style/TextPlacement.js
  var TextPlacement_default = {
    POINT: "point",
    LINE: "line"
  };

  // node_modules/ol/geom/flat/straightchunk.js
  function matchingChunk(maxAngle, flatCoordinates, offset, end, stride) {
    var chunkStart = offset;
    var chunkEnd = offset;
    var chunkM = 0;
    var m = 0;
    var start = offset;
    var acos, i, m12, m23, x1, y1, x12, y12, x23, y23;
    for (i = offset; i < end; i += stride) {
      var x2 = flatCoordinates[i];
      var y2 = flatCoordinates[i + 1];
      if (x1 !== void 0) {
        x23 = x2 - x1;
        y23 = y2 - y1;
        m23 = Math.sqrt(x23 * x23 + y23 * y23);
        if (x12 !== void 0) {
          m += m12;
          acos = Math.acos((x12 * x23 + y12 * y23) / (m12 * m23));
          if (acos > maxAngle) {
            if (m > chunkM) {
              chunkM = m;
              chunkStart = start;
              chunkEnd = i;
            }
            m = 0;
            start = i - stride;
          }
        }
        m12 = m23;
        x12 = x23;
        y12 = y23;
      }
      x1 = x2;
      y1 = y2;
    }
    m += m23;
    return m > chunkM ? [start, i] : [chunkStart, chunkEnd];
  }

  // node_modules/ol/render/canvas/TextBuilder.js
  var __extends35 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var TEXT_ALIGN = {
    "left": 0,
    "end": 0,
    "center": 0.5,
    "right": 1,
    "start": 1,
    "top": 0,
    "middle": 0.5,
    "hanging": 0.2,
    "alphabetic": 0.8,
    "ideographic": 0.8,
    "bottom": 1
  };
  var CanvasTextBuilder = function(_super) {
    __extends35(CanvasTextBuilder2, _super);
    function CanvasTextBuilder2(tolerance, maxExtent, resolution, pixelRatio) {
      var _this = _super.call(this, tolerance, maxExtent, resolution, pixelRatio) || this;
      _this.labels_ = null;
      _this.text_ = "";
      _this.textOffsetX_ = 0;
      _this.textOffsetY_ = 0;
      _this.textRotateWithView_ = void 0;
      _this.textRotation_ = 0;
      _this.textFillState_ = null;
      _this.fillStates = {};
      _this.textStrokeState_ = null;
      _this.strokeStates = {};
      _this.textState_ = {};
      _this.textStates = {};
      _this.textKey_ = "";
      _this.fillKey_ = "";
      _this.strokeKey_ = "";
      _this.declutterImageWithText_ = void 0;
      return _this;
    }
    CanvasTextBuilder2.prototype.finish = function() {
      var instructions = _super.prototype.finish.call(this);
      instructions.textStates = this.textStates;
      instructions.fillStates = this.fillStates;
      instructions.strokeStates = this.strokeStates;
      return instructions;
    };
    CanvasTextBuilder2.prototype.drawText = function(geometry, feature) {
      var fillState = this.textFillState_;
      var strokeState = this.textStrokeState_;
      var textState = this.textState_;
      if (this.text_ === "" || !textState || !fillState && !strokeState) {
        return;
      }
      var coordinates2 = this.coordinates;
      var begin = coordinates2.length;
      var geometryType = geometry.getType();
      var flatCoordinates = null;
      var stride = geometry.getStride();
      if (textState.placement === TextPlacement_default.LINE && (geometryType == GeometryType_default.LINE_STRING || geometryType == GeometryType_default.MULTI_LINE_STRING || geometryType == GeometryType_default.POLYGON || geometryType == GeometryType_default.MULTI_POLYGON)) {
        if (!intersects(this.getBufferedMaxExtent(), geometry.getExtent())) {
          return;
        }
        var ends = void 0;
        flatCoordinates = geometry.getFlatCoordinates();
        if (geometryType == GeometryType_default.LINE_STRING) {
          ends = [flatCoordinates.length];
        } else if (geometryType == GeometryType_default.MULTI_LINE_STRING) {
          ends = geometry.getEnds();
        } else if (geometryType == GeometryType_default.POLYGON) {
          ends = geometry.getEnds().slice(0, 1);
        } else if (geometryType == GeometryType_default.MULTI_POLYGON) {
          var endss = geometry.getEndss();
          ends = [];
          for (var i = 0, ii = endss.length; i < ii; ++i) {
            ends.push(endss[i][0]);
          }
        }
        this.beginGeometry(geometry, feature);
        var textAlign = textState.textAlign;
        var flatOffset = 0;
        var flatEnd = void 0;
        for (var o = 0, oo = ends.length; o < oo; ++o) {
          if (textAlign == void 0) {
            var range = matchingChunk(textState.maxAngle, flatCoordinates, flatOffset, ends[o], stride);
            flatOffset = range[0];
            flatEnd = range[1];
          } else {
            flatEnd = ends[o];
          }
          for (var i = flatOffset; i < flatEnd; i += stride) {
            coordinates2.push(flatCoordinates[i], flatCoordinates[i + 1]);
          }
          var end = coordinates2.length;
          flatOffset = ends[o];
          this.drawChars_(begin, end);
          begin = end;
        }
        this.endGeometry(feature);
      } else {
        var geometryWidths = textState.overflow ? null : [];
        switch (geometryType) {
          case GeometryType_default.POINT:
          case GeometryType_default.MULTI_POINT:
            flatCoordinates = geometry.getFlatCoordinates();
            break;
          case GeometryType_default.LINE_STRING:
            flatCoordinates = geometry.getFlatMidpoint();
            break;
          case GeometryType_default.CIRCLE:
            flatCoordinates = geometry.getCenter();
            break;
          case GeometryType_default.MULTI_LINE_STRING:
            flatCoordinates = geometry.getFlatMidpoints();
            stride = 2;
            break;
          case GeometryType_default.POLYGON:
            flatCoordinates = geometry.getFlatInteriorPoint();
            if (!textState.overflow) {
              geometryWidths.push(flatCoordinates[2] / this.resolution);
            }
            stride = 3;
            break;
          case GeometryType_default.MULTI_POLYGON:
            var interiorPoints = geometry.getFlatInteriorPoints();
            flatCoordinates = [];
            for (var i = 0, ii = interiorPoints.length; i < ii; i += 3) {
              if (!textState.overflow) {
                geometryWidths.push(interiorPoints[i + 2] / this.resolution);
              }
              flatCoordinates.push(interiorPoints[i], interiorPoints[i + 1]);
            }
            if (flatCoordinates.length === 0) {
              return;
            }
            stride = 2;
            break;
          default:
        }
        var end = this.appendFlatPointCoordinates(flatCoordinates, stride);
        if (end === begin) {
          return;
        }
        if (geometryWidths && (end - begin) / 2 !== flatCoordinates.length / stride) {
          var beg_1 = begin / 2;
          geometryWidths = geometryWidths.filter(function(w, i2) {
            var keep = coordinates2[(beg_1 + i2) * 2] === flatCoordinates[i2 * stride] && coordinates2[(beg_1 + i2) * 2 + 1] === flatCoordinates[i2 * stride + 1];
            if (!keep) {
              --beg_1;
            }
            return keep;
          });
        }
        this.saveTextStates_();
        if (textState.backgroundFill || textState.backgroundStroke) {
          this.setFillStrokeStyle(textState.backgroundFill, textState.backgroundStroke);
          if (textState.backgroundFill) {
            this.updateFillStyle(this.state, this.createFill);
            this.hitDetectionInstructions.push(this.createFill(this.state));
          }
          if (textState.backgroundStroke) {
            this.updateStrokeStyle(this.state, this.applyStroke);
            this.hitDetectionInstructions.push(this.createStroke(this.state));
          }
        }
        this.beginGeometry(geometry, feature);
        var padding = textState.padding;
        if (padding != defaultPadding && (textState.scale[0] < 0 || textState.scale[1] < 0)) {
          var p0 = textState.padding[0];
          var p12 = textState.padding[1];
          var p22 = textState.padding[2];
          var p32 = textState.padding[3];
          if (textState.scale[0] < 0) {
            p12 = -p12;
            p32 = -p32;
          }
          if (textState.scale[1] < 0) {
            p0 = -p0;
            p22 = -p22;
          }
          padding = [p0, p12, p22, p32];
        }
        var pixelRatio_1 = this.pixelRatio;
        this.instructions.push([
          Instruction_default.DRAW_IMAGE,
          begin,
          end,
          null,
          NaN,
          NaN,
          NaN,
          1,
          0,
          0,
          this.textRotateWithView_,
          this.textRotation_,
          [1, 1],
          NaN,
          this.declutterImageWithText_,
          padding == defaultPadding ? defaultPadding : padding.map(function(p) {
            return p * pixelRatio_1;
          }),
          !!textState.backgroundFill,
          !!textState.backgroundStroke,
          this.text_,
          this.textKey_,
          this.strokeKey_,
          this.fillKey_,
          this.textOffsetX_,
          this.textOffsetY_,
          geometryWidths
        ]);
        var scale2 = 1 / pixelRatio_1;
        this.hitDetectionInstructions.push([
          Instruction_default.DRAW_IMAGE,
          begin,
          end,
          null,
          NaN,
          NaN,
          NaN,
          1,
          0,
          0,
          this.textRotateWithView_,
          this.textRotation_,
          [scale2, scale2],
          NaN,
          this.declutterImageWithText_,
          padding,
          !!textState.backgroundFill,
          !!textState.backgroundStroke,
          this.text_,
          this.textKey_,
          this.strokeKey_,
          this.fillKey_,
          this.textOffsetX_,
          this.textOffsetY_,
          geometryWidths
        ]);
        this.endGeometry(feature);
      }
    };
    CanvasTextBuilder2.prototype.saveTextStates_ = function() {
      var strokeState = this.textStrokeState_;
      var textState = this.textState_;
      var fillState = this.textFillState_;
      var strokeKey = this.strokeKey_;
      if (strokeState) {
        if (!(strokeKey in this.strokeStates)) {
          this.strokeStates[strokeKey] = {
            strokeStyle: strokeState.strokeStyle,
            lineCap: strokeState.lineCap,
            lineDashOffset: strokeState.lineDashOffset,
            lineWidth: strokeState.lineWidth,
            lineJoin: strokeState.lineJoin,
            miterLimit: strokeState.miterLimit,
            lineDash: strokeState.lineDash
          };
        }
      }
      var textKey = this.textKey_;
      if (!(textKey in this.textStates)) {
        this.textStates[textKey] = {
          font: textState.font,
          textAlign: textState.textAlign || defaultTextAlign,
          textBaseline: textState.textBaseline || defaultTextBaseline,
          scale: textState.scale
        };
      }
      var fillKey = this.fillKey_;
      if (fillState) {
        if (!(fillKey in this.fillStates)) {
          this.fillStates[fillKey] = {
            fillStyle: fillState.fillStyle
          };
        }
      }
    };
    CanvasTextBuilder2.prototype.drawChars_ = function(begin, end) {
      var strokeState = this.textStrokeState_;
      var textState = this.textState_;
      var strokeKey = this.strokeKey_;
      var textKey = this.textKey_;
      var fillKey = this.fillKey_;
      this.saveTextStates_();
      var pixelRatio = this.pixelRatio;
      var baseline = TEXT_ALIGN[textState.textBaseline];
      var offsetY = this.textOffsetY_ * pixelRatio;
      var text = this.text_;
      var strokeWidth = strokeState ? strokeState.lineWidth * Math.abs(textState.scale[0]) / 2 : 0;
      this.instructions.push([
        Instruction_default.DRAW_CHARS,
        begin,
        end,
        baseline,
        textState.overflow,
        fillKey,
        textState.maxAngle,
        pixelRatio,
        offsetY,
        strokeKey,
        strokeWidth * pixelRatio,
        text,
        textKey,
        1
      ]);
      this.hitDetectionInstructions.push([
        Instruction_default.DRAW_CHARS,
        begin,
        end,
        baseline,
        textState.overflow,
        fillKey,
        textState.maxAngle,
        1,
        offsetY,
        strokeKey,
        strokeWidth,
        text,
        textKey,
        1 / pixelRatio
      ]);
    };
    CanvasTextBuilder2.prototype.setTextStyle = function(textStyle, opt_sharedData) {
      var textState, fillState, strokeState;
      if (!textStyle) {
        this.text_ = "";
      } else {
        var textFillStyle = textStyle.getFill();
        if (!textFillStyle) {
          fillState = null;
          this.textFillState_ = fillState;
        } else {
          fillState = this.textFillState_;
          if (!fillState) {
            fillState = {};
            this.textFillState_ = fillState;
          }
          fillState.fillStyle = asColorLike(textFillStyle.getColor() || defaultFillStyle);
        }
        var textStrokeStyle = textStyle.getStroke();
        if (!textStrokeStyle) {
          strokeState = null;
          this.textStrokeState_ = strokeState;
        } else {
          strokeState = this.textStrokeState_;
          if (!strokeState) {
            strokeState = {};
            this.textStrokeState_ = strokeState;
          }
          var lineDash = textStrokeStyle.getLineDash();
          var lineDashOffset = textStrokeStyle.getLineDashOffset();
          var lineWidth = textStrokeStyle.getWidth();
          var miterLimit = textStrokeStyle.getMiterLimit();
          strokeState.lineCap = textStrokeStyle.getLineCap() || defaultLineCap;
          strokeState.lineDash = lineDash ? lineDash.slice() : defaultLineDash;
          strokeState.lineDashOffset = lineDashOffset === void 0 ? defaultLineDashOffset : lineDashOffset;
          strokeState.lineJoin = textStrokeStyle.getLineJoin() || defaultLineJoin;
          strokeState.lineWidth = lineWidth === void 0 ? defaultLineWidth : lineWidth;
          strokeState.miterLimit = miterLimit === void 0 ? defaultMiterLimit : miterLimit;
          strokeState.strokeStyle = asColorLike(textStrokeStyle.getColor() || defaultStrokeStyle);
        }
        textState = this.textState_;
        var font = textStyle.getFont() || defaultFont;
        registerFont(font);
        var textScale = textStyle.getScaleArray();
        textState.overflow = textStyle.getOverflow();
        textState.font = font;
        textState.maxAngle = textStyle.getMaxAngle();
        textState.placement = textStyle.getPlacement();
        textState.textAlign = textStyle.getTextAlign();
        textState.textBaseline = textStyle.getTextBaseline() || defaultTextBaseline;
        textState.backgroundFill = textStyle.getBackgroundFill();
        textState.backgroundStroke = textStyle.getBackgroundStroke();
        textState.padding = textStyle.getPadding() || defaultPadding;
        textState.scale = textScale === void 0 ? [1, 1] : textScale;
        var textOffsetX = textStyle.getOffsetX();
        var textOffsetY = textStyle.getOffsetY();
        var textRotateWithView = textStyle.getRotateWithView();
        var textRotation = textStyle.getRotation();
        this.text_ = textStyle.getText() || "";
        this.textOffsetX_ = textOffsetX === void 0 ? 0 : textOffsetX;
        this.textOffsetY_ = textOffsetY === void 0 ? 0 : textOffsetY;
        this.textRotateWithView_ = textRotateWithView === void 0 ? false : textRotateWithView;
        this.textRotation_ = textRotation === void 0 ? 0 : textRotation;
        this.strokeKey_ = strokeState ? (typeof strokeState.strokeStyle == "string" ? strokeState.strokeStyle : getUid(strokeState.strokeStyle)) + strokeState.lineCap + strokeState.lineDashOffset + "|" + strokeState.lineWidth + strokeState.lineJoin + strokeState.miterLimit + "[" + strokeState.lineDash.join() + "]" : "";
        this.textKey_ = textState.font + textState.scale + (textState.textAlign || "?") + (textState.textBaseline || "?");
        this.fillKey_ = fillState ? typeof fillState.fillStyle == "string" ? fillState.fillStyle : "|" + getUid(fillState.fillStyle) : "";
      }
      this.declutterImageWithText_ = opt_sharedData;
    };
    return CanvasTextBuilder2;
  }(Builder_default);
  var TextBuilder_default = CanvasTextBuilder;

  // node_modules/ol/render/canvas/BuilderGroup.js
  var BATCH_CONSTRUCTORS = {
    "Circle": PolygonBuilder_default,
    "Default": Builder_default,
    "Image": ImageBuilder_default,
    "LineString": LineStringBuilder_default,
    "Polygon": PolygonBuilder_default,
    "Text": TextBuilder_default
  };
  var BuilderGroup = function() {
    function BuilderGroup2(tolerance, maxExtent, resolution, pixelRatio) {
      this.tolerance_ = tolerance;
      this.maxExtent_ = maxExtent;
      this.pixelRatio_ = pixelRatio;
      this.resolution_ = resolution;
      this.buildersByZIndex_ = {};
    }
    BuilderGroup2.prototype.finish = function() {
      var builderInstructions = {};
      for (var zKey in this.buildersByZIndex_) {
        builderInstructions[zKey] = builderInstructions[zKey] || {};
        var builders = this.buildersByZIndex_[zKey];
        for (var builderKey in builders) {
          var builderInstruction = builders[builderKey].finish();
          builderInstructions[zKey][builderKey] = builderInstruction;
        }
      }
      return builderInstructions;
    };
    BuilderGroup2.prototype.getBuilder = function(zIndex, builderType) {
      var zIndexKey = zIndex !== void 0 ? zIndex.toString() : "0";
      var replays = this.buildersByZIndex_[zIndexKey];
      if (replays === void 0) {
        replays = {};
        this.buildersByZIndex_[zIndexKey] = replays;
      }
      var replay = replays[builderType];
      if (replay === void 0) {
        var Constructor = BATCH_CONSTRUCTORS[builderType];
        replay = new Constructor(this.tolerance_, this.maxExtent_, this.resolution_, this.pixelRatio_);
        replays[builderType] = replay;
      }
      return replay;
    };
    return BuilderGroup2;
  }();
  var BuilderGroup_default = BuilderGroup;

  // node_modules/ol/renderer/Layer.js
  var __extends36 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var LayerRenderer = function(_super) {
    __extends36(LayerRenderer2, _super);
    function LayerRenderer2(layer) {
      var _this = _super.call(this) || this;
      _this.ready = true;
      _this.boundHandleImageChange_ = _this.handleImageChange_.bind(_this);
      _this.layer_ = layer;
      _this.declutterExecutorGroup = null;
      return _this;
    }
    LayerRenderer2.prototype.getFeatures = function(pixel) {
      return abstract();
    };
    LayerRenderer2.prototype.getData = function(pixel) {
      return null;
    };
    LayerRenderer2.prototype.prepareFrame = function(frameState) {
      return abstract();
    };
    LayerRenderer2.prototype.renderFrame = function(frameState, target) {
      return abstract();
    };
    LayerRenderer2.prototype.loadedTileCallback = function(tiles, zoom, tile) {
      if (!tiles[zoom]) {
        tiles[zoom] = {};
      }
      tiles[zoom][tile.tileCoord.toString()] = tile;
      return void 0;
    };
    LayerRenderer2.prototype.createLoadedTileFinder = function(source, projection, tiles) {
      return function(zoom, tileRange) {
        var callback = this.loadedTileCallback.bind(this, tiles, zoom);
        return source.forEachLoadedTile(projection, zoom, tileRange, callback);
      }.bind(this);
    };
    LayerRenderer2.prototype.forEachFeatureAtCoordinate = function(coordinate, frameState, hitTolerance, callback, matches) {
      return void 0;
    };
    LayerRenderer2.prototype.getDataAtPixel = function(pixel, frameState, hitTolerance) {
      return null;
    };
    LayerRenderer2.prototype.getLayer = function() {
      return this.layer_;
    };
    LayerRenderer2.prototype.handleFontsChanged = function() {
    };
    LayerRenderer2.prototype.handleImageChange_ = function(event) {
      var image = event.target;
      if (image.getState() === ImageState_default.LOADED) {
        this.renderIfReadyAndVisible();
      }
    };
    LayerRenderer2.prototype.loadImage = function(image) {
      var imageState = image.getState();
      if (imageState != ImageState_default.LOADED && imageState != ImageState_default.ERROR) {
        image.addEventListener(EventType_default.CHANGE, this.boundHandleImageChange_);
      }
      if (imageState == ImageState_default.IDLE) {
        image.load();
        imageState = image.getState();
      }
      return imageState == ImageState_default.LOADED;
    };
    LayerRenderer2.prototype.renderIfReadyAndVisible = function() {
      var layer = this.getLayer();
      if (layer.getVisible() && layer.getSourceState() == State_default.READY) {
        layer.changed();
      }
    };
    LayerRenderer2.prototype.disposeInternal = function() {
      delete this.layer_;
      _super.prototype.disposeInternal.call(this);
    };
    return LayerRenderer2;
  }(Observable_default);
  var Layer_default2 = LayerRenderer;

  // node_modules/ol/render/Event.js
  var __extends37 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var RenderEvent = function(_super) {
    __extends37(RenderEvent2, _super);
    function RenderEvent2(type, opt_inversePixelTransform, opt_frameState, opt_context) {
      var _this = _super.call(this, type) || this;
      _this.inversePixelTransform = opt_inversePixelTransform;
      _this.frameState = opt_frameState;
      _this.context = opt_context;
      return _this;
    }
    return RenderEvent2;
  }(Event_default);
  var Event_default2 = RenderEvent;

  // node_modules/ol/renderer/canvas/Layer.js
  var __extends38 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var pixelContext = null;
  function createPixelContext() {
    var canvas = document.createElement("canvas");
    canvas.width = 1;
    canvas.height = 1;
    pixelContext = canvas.getContext("2d");
  }
  var CanvasLayerRenderer = function(_super) {
    __extends38(CanvasLayerRenderer2, _super);
    function CanvasLayerRenderer2(layer) {
      var _this = _super.call(this, layer) || this;
      _this.container = null;
      _this.renderedResolution;
      _this.tempTransform = create();
      _this.pixelTransform = create();
      _this.inversePixelTransform = create();
      _this.context = null;
      _this.containerReused = false;
      _this.pixelContext_ = null;
      _this.frameState = null;
      return _this;
    }
    CanvasLayerRenderer2.prototype.getImageData = function(image, col, row) {
      if (!pixelContext) {
        createPixelContext();
      }
      pixelContext.clearRect(0, 0, 1, 1);
      var data;
      try {
        pixelContext.drawImage(image, col, row, 1, 1, 0, 0, 1, 1);
        data = pixelContext.getImageData(0, 0, 1, 1).data;
      } catch (err) {
        return null;
      }
      return data;
    };
    CanvasLayerRenderer2.prototype.getBackground = function(frameState) {
      var layer = this.getLayer();
      var background = layer.getBackground();
      if (typeof background === "function") {
        background = background(frameState.viewState.resolution);
      }
      return background || void 0;
    };
    CanvasLayerRenderer2.prototype.useContainer = function(target, transform2, opacity, opt_backgroundColor) {
      var layerClassName = this.getLayer().getClassName();
      var container, context;
      if (target && target.className === layerClassName && target.style.opacity === "" && opacity === 1 && (!opt_backgroundColor || target.style.backgroundColor && equals(asArray(target.style.backgroundColor), asArray(opt_backgroundColor)))) {
        var canvas = target.firstElementChild;
        if (canvas instanceof HTMLCanvasElement) {
          context = canvas.getContext("2d");
        }
      }
      if (context && context.canvas.style.transform === transform2) {
        this.container = target;
        this.context = context;
        this.containerReused = true;
      } else if (this.containerReused) {
        this.container = null;
        this.context = null;
        this.containerReused = false;
      }
      if (!this.container) {
        container = document.createElement("div");
        container.className = layerClassName;
        var style = container.style;
        style.position = "absolute";
        style.width = "100%";
        style.height = "100%";
        if (opt_backgroundColor) {
          style.backgroundColor = opt_backgroundColor;
        }
        context = createCanvasContext2D();
        var canvas = context.canvas;
        container.appendChild(canvas);
        style = canvas.style;
        style.position = "absolute";
        style.left = "0";
        style.transformOrigin = "top left";
        this.container = container;
        this.context = context;
      }
    };
    CanvasLayerRenderer2.prototype.clipUnrotated = function(context, frameState, extent) {
      var topLeft = getTopLeft(extent);
      var topRight = getTopRight(extent);
      var bottomRight = getBottomRight(extent);
      var bottomLeft = getBottomLeft(extent);
      apply(frameState.coordinateToPixelTransform, topLeft);
      apply(frameState.coordinateToPixelTransform, topRight);
      apply(frameState.coordinateToPixelTransform, bottomRight);
      apply(frameState.coordinateToPixelTransform, bottomLeft);
      var inverted = this.inversePixelTransform;
      apply(inverted, topLeft);
      apply(inverted, topRight);
      apply(inverted, bottomRight);
      apply(inverted, bottomLeft);
      context.save();
      context.beginPath();
      context.moveTo(Math.round(topLeft[0]), Math.round(topLeft[1]));
      context.lineTo(Math.round(topRight[0]), Math.round(topRight[1]));
      context.lineTo(Math.round(bottomRight[0]), Math.round(bottomRight[1]));
      context.lineTo(Math.round(bottomLeft[0]), Math.round(bottomLeft[1]));
      context.clip();
    };
    CanvasLayerRenderer2.prototype.dispatchRenderEvent_ = function(type, context, frameState) {
      var layer = this.getLayer();
      if (layer.hasListener(type)) {
        var event_1 = new Event_default2(type, this.inversePixelTransform, frameState, context);
        layer.dispatchEvent(event_1);
      }
    };
    CanvasLayerRenderer2.prototype.preRender = function(context, frameState) {
      this.frameState = frameState;
      this.dispatchRenderEvent_(EventType_default2.PRERENDER, context, frameState);
    };
    CanvasLayerRenderer2.prototype.postRender = function(context, frameState) {
      this.dispatchRenderEvent_(EventType_default2.POSTRENDER, context, frameState);
    };
    CanvasLayerRenderer2.prototype.getRenderTransform = function(center, resolution, rotation, pixelRatio, width, height, offsetX) {
      var dx1 = width / 2;
      var dy1 = height / 2;
      var sx = pixelRatio / resolution;
      var sy = -sx;
      var dx2 = -center[0] + offsetX;
      var dy2 = -center[1];
      return compose(this.tempTransform, dx1, dy1, sx, sy, -rotation, dx2, dy2);
    };
    CanvasLayerRenderer2.prototype.getDataAtPixel = function(pixel, frameState, hitTolerance) {
      var renderPixel = apply(this.inversePixelTransform, pixel.slice());
      var context = this.context;
      var layer = this.getLayer();
      var layerExtent = layer.getExtent();
      if (layerExtent) {
        var renderCoordinate = apply(frameState.pixelToCoordinateTransform, pixel.slice());
        if (!containsCoordinate(layerExtent, renderCoordinate)) {
          return null;
        }
      }
      var x = Math.round(renderPixel[0]);
      var y = Math.round(renderPixel[1]);
      var pixelContext2 = this.pixelContext_;
      if (!pixelContext2) {
        var pixelCanvas = document.createElement("canvas");
        pixelCanvas.width = 1;
        pixelCanvas.height = 1;
        pixelContext2 = pixelCanvas.getContext("2d");
        this.pixelContext_ = pixelContext2;
      }
      pixelContext2.clearRect(0, 0, 1, 1);
      var data;
      try {
        pixelContext2.drawImage(context.canvas, x, y, 1, 1, 0, 0, 1, 1);
        data = pixelContext2.getImageData(0, 0, 1, 1).data;
      } catch (err) {
        if (err.name === "SecurityError") {
          this.pixelContext_ = null;
          return new Uint8Array();
        }
        return data;
      }
      if (data[3] === 0) {
        return null;
      }
      return data;
    };
    CanvasLayerRenderer2.prototype.disposeInternal = function() {
      delete this.frameState;
      _super.prototype.disposeInternal.call(this);
    };
    return CanvasLayerRenderer2;
  }(Layer_default2);
  var Layer_default3 = CanvasLayerRenderer;

  // node_modules/ol/render/canvas/BuilderType.js
  var BuilderType_default = {
    CIRCLE: "Circle",
    DEFAULT: "Default",
    IMAGE: "Image",
    LINE_STRING: "LineString",
    POLYGON: "Polygon",
    TEXT: "Text"
  };

  // node_modules/ol/geom/flat/textpath.js
  function drawTextOnPath(flatCoordinates, offset, end, stride, text, startM, maxAngle, scale2, measureAndCacheTextWidth2, font, cache2, rotation) {
    var x2 = flatCoordinates[offset];
    var y2 = flatCoordinates[offset + 1];
    var x1 = 0;
    var y1 = 0;
    var segmentLength = 0;
    var segmentM = 0;
    function advance() {
      x1 = x2;
      y1 = y2;
      offset += stride;
      x2 = flatCoordinates[offset];
      y2 = flatCoordinates[offset + 1];
      segmentM += segmentLength;
      segmentLength = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }
    do {
      advance();
    } while (offset < end - stride && segmentM + segmentLength < startM);
    var interpolate = segmentLength === 0 ? 0 : (startM - segmentM) / segmentLength;
    var beginX = lerp(x1, x2, interpolate);
    var beginY = lerp(y1, y2, interpolate);
    var startOffset = offset - stride;
    var startLength = segmentM;
    var endM = startM + scale2 * measureAndCacheTextWidth2(font, text, cache2);
    while (offset < end - stride && segmentM + segmentLength < endM) {
      advance();
    }
    interpolate = segmentLength === 0 ? 0 : (endM - segmentM) / segmentLength;
    var endX = lerp(x1, x2, interpolate);
    var endY = lerp(y1, y2, interpolate);
    var reverse;
    if (rotation) {
      var flat = [beginX, beginY, endX, endY];
      rotate(flat, 0, 4, 2, rotation, flat, flat);
      reverse = flat[0] > flat[2];
    } else {
      reverse = beginX > endX;
    }
    var PI = Math.PI;
    var result = [];
    var singleSegment = startOffset + stride === offset;
    offset = startOffset;
    segmentLength = 0;
    segmentM = startLength;
    x2 = flatCoordinates[offset];
    y2 = flatCoordinates[offset + 1];
    var previousAngle;
    if (singleSegment) {
      advance();
      previousAngle = Math.atan2(y2 - y1, x2 - x1);
      if (reverse) {
        previousAngle += previousAngle > 0 ? -PI : PI;
      }
      var x = (endX + beginX) / 2;
      var y = (endY + beginY) / 2;
      result[0] = [x, y, (endM - startM) / 2, previousAngle, text];
      return result;
    }
    for (var i = 0, ii = text.length; i < ii; ) {
      advance();
      var angle = Math.atan2(y2 - y1, x2 - x1);
      if (reverse) {
        angle += angle > 0 ? -PI : PI;
      }
      if (previousAngle !== void 0) {
        var delta = angle - previousAngle;
        delta += delta > PI ? -2 * PI : delta < -PI ? 2 * PI : 0;
        if (Math.abs(delta) > maxAngle) {
          return null;
        }
      }
      previousAngle = angle;
      var iStart = i;
      var charLength = 0;
      for (; i < ii; ++i) {
        var index = reverse ? ii - i - 1 : i;
        var len = scale2 * measureAndCacheTextWidth2(font, text[index], cache2);
        if (offset + stride < end && segmentM + segmentLength < startM + charLength + len / 2) {
          break;
        }
        charLength += len;
      }
      if (i === iStart) {
        continue;
      }
      var chars = reverse ? text.substring(ii - iStart, ii - i) : text.substring(iStart, i);
      interpolate = segmentLength === 0 ? 0 : (startM + charLength / 2 - segmentM) / segmentLength;
      var x = lerp(x1, x2, interpolate);
      var y = lerp(y1, y2, interpolate);
      result.push([x, y, charLength / 2, angle, chars]);
      startM += charLength;
    }
    return result;
  }

  // node_modules/ol/render/canvas/Executor.js
  var tmpExtent = createEmpty();
  var p1 = [];
  var p2 = [];
  var p3 = [];
  var p4 = [];
  function getDeclutterBox(replayImageOrLabelArgs) {
    return replayImageOrLabelArgs[3].declutterBox;
  }
  var rtlRegEx = new RegExp("[" + String.fromCharCode(1425) + "-" + String.fromCharCode(2303) + String.fromCharCode(64285) + "-" + String.fromCharCode(65023) + String.fromCharCode(65136) + "-" + String.fromCharCode(65276) + String.fromCharCode(67584) + "-" + String.fromCharCode(69631) + String.fromCharCode(124928) + "-" + String.fromCharCode(126975) + "]");
  function horizontalTextAlign(text, align) {
    if ((align === "start" || align === "end") && !rtlRegEx.test(text)) {
      align = align === "start" ? "left" : "right";
    }
    return TEXT_ALIGN[align];
  }
  function createTextChunks(acc, line, i) {
    if (i > 0) {
      acc.push("\n", "");
    }
    acc.push(line, "");
    return acc;
  }
  var Executor = function() {
    function Executor2(resolution, pixelRatio, overlaps, instructions) {
      this.overlaps = overlaps;
      this.pixelRatio = pixelRatio;
      this.resolution = resolution;
      this.alignFill_;
      this.instructions = instructions.instructions;
      this.coordinates = instructions.coordinates;
      this.coordinateCache_ = {};
      this.renderedTransform_ = create();
      this.hitDetectionInstructions = instructions.hitDetectionInstructions;
      this.pixelCoordinates_ = null;
      this.viewRotation_ = 0;
      this.fillStates = instructions.fillStates || {};
      this.strokeStates = instructions.strokeStates || {};
      this.textStates = instructions.textStates || {};
      this.widths_ = {};
      this.labels_ = {};
    }
    Executor2.prototype.createLabel = function(text, textKey, fillKey, strokeKey) {
      var key = text + textKey + fillKey + strokeKey;
      if (this.labels_[key]) {
        return this.labels_[key];
      }
      var strokeState = strokeKey ? this.strokeStates[strokeKey] : null;
      var fillState = fillKey ? this.fillStates[fillKey] : null;
      var textState = this.textStates[textKey];
      var pixelRatio = this.pixelRatio;
      var scale2 = [
        textState.scale[0] * pixelRatio,
        textState.scale[1] * pixelRatio
      ];
      var textIsArray = Array.isArray(text);
      var align = horizontalTextAlign(textIsArray ? text[0] : text, textState.textAlign || defaultTextAlign);
      var strokeWidth = strokeKey && strokeState.lineWidth ? strokeState.lineWidth : 0;
      var chunks = textIsArray ? text : text.split("\n").reduce(createTextChunks, []);
      var _a = getTextDimensions(textState, chunks), width = _a.width, height = _a.height, widths = _a.widths, heights = _a.heights, lineWidths = _a.lineWidths;
      var renderWidth = width + strokeWidth;
      var contextInstructions = [];
      var w = (renderWidth + 2) * scale2[0];
      var h = (height + strokeWidth) * scale2[1];
      var label = {
        width: w < 0 ? Math.floor(w) : Math.ceil(w),
        height: h < 0 ? Math.floor(h) : Math.ceil(h),
        contextInstructions
      };
      if (scale2[0] != 1 || scale2[1] != 1) {
        contextInstructions.push("scale", scale2);
      }
      if (strokeKey) {
        contextInstructions.push("strokeStyle", strokeState.strokeStyle);
        contextInstructions.push("lineWidth", strokeWidth);
        contextInstructions.push("lineCap", strokeState.lineCap);
        contextInstructions.push("lineJoin", strokeState.lineJoin);
        contextInstructions.push("miterLimit", strokeState.miterLimit);
        var Context = WORKER_OFFSCREEN_CANVAS ? OffscreenCanvasRenderingContext2D : CanvasRenderingContext2D;
        if (Context.prototype.setLineDash) {
          contextInstructions.push("setLineDash", [strokeState.lineDash]);
          contextInstructions.push("lineDashOffset", strokeState.lineDashOffset);
        }
      }
      if (fillKey) {
        contextInstructions.push("fillStyle", fillState.fillStyle);
      }
      contextInstructions.push("textBaseline", "middle");
      contextInstructions.push("textAlign", "center");
      var leftRight = 0.5 - align;
      var x = align * renderWidth + leftRight * strokeWidth;
      var strokeInstructions = [];
      var fillInstructions = [];
      var lineHeight = 0;
      var lineOffset = 0;
      var widthHeightIndex = 0;
      var lineWidthIndex = 0;
      var previousFont;
      for (var i = 0, ii = chunks.length; i < ii; i += 2) {
        var text_1 = chunks[i];
        if (text_1 === "\n") {
          lineOffset += lineHeight;
          lineHeight = 0;
          x = align * renderWidth + leftRight * strokeWidth;
          ++lineWidthIndex;
          continue;
        }
        var font = chunks[i + 1] || textState.font;
        if (font !== previousFont) {
          if (strokeKey) {
            strokeInstructions.push("font", font);
          }
          if (fillKey) {
            fillInstructions.push("font", font);
          }
          previousFont = font;
        }
        lineHeight = Math.max(lineHeight, heights[widthHeightIndex]);
        var fillStrokeArgs = [
          text_1,
          x + leftRight * widths[widthHeightIndex] + align * (widths[widthHeightIndex] - lineWidths[lineWidthIndex]),
          0.5 * (strokeWidth + lineHeight) + lineOffset
        ];
        x += widths[widthHeightIndex];
        if (strokeKey) {
          strokeInstructions.push("strokeText", fillStrokeArgs);
        }
        if (fillKey) {
          fillInstructions.push("fillText", fillStrokeArgs);
        }
        ++widthHeightIndex;
      }
      Array.prototype.push.apply(contextInstructions, strokeInstructions);
      Array.prototype.push.apply(contextInstructions, fillInstructions);
      this.labels_[key] = label;
      return label;
    };
    Executor2.prototype.replayTextBackground_ = function(context, p12, p22, p32, p42, fillInstruction2, strokeInstruction2) {
      context.beginPath();
      context.moveTo.apply(context, p12);
      context.lineTo.apply(context, p22);
      context.lineTo.apply(context, p32);
      context.lineTo.apply(context, p42);
      context.lineTo.apply(context, p12);
      if (fillInstruction2) {
        this.alignFill_ = fillInstruction2[2];
        this.fill_(context);
      }
      if (strokeInstruction2) {
        this.setStrokeStyle_(context, strokeInstruction2);
        context.stroke();
      }
    };
    Executor2.prototype.calculateImageOrLabelDimensions_ = function(sheetWidth, sheetHeight, centerX, centerY, width, height, anchorX, anchorY, originX, originY, rotation, scale2, snapToPixel, padding, fillStroke, feature) {
      anchorX *= scale2[0];
      anchorY *= scale2[1];
      var x = centerX - anchorX;
      var y = centerY - anchorY;
      var w = width + originX > sheetWidth ? sheetWidth - originX : width;
      var h = height + originY > sheetHeight ? sheetHeight - originY : height;
      var boxW = padding[3] + w * scale2[0] + padding[1];
      var boxH = padding[0] + h * scale2[1] + padding[2];
      var boxX = x - padding[3];
      var boxY = y - padding[0];
      if (fillStroke || rotation !== 0) {
        p1[0] = boxX;
        p4[0] = boxX;
        p1[1] = boxY;
        p2[1] = boxY;
        p2[0] = boxX + boxW;
        p3[0] = p2[0];
        p3[1] = boxY + boxH;
        p4[1] = p3[1];
      }
      var transform2;
      if (rotation !== 0) {
        transform2 = compose(create(), centerX, centerY, 1, 1, rotation, -centerX, -centerY);
        apply(transform2, p1);
        apply(transform2, p2);
        apply(transform2, p3);
        apply(transform2, p4);
        createOrUpdate(Math.min(p1[0], p2[0], p3[0], p4[0]), Math.min(p1[1], p2[1], p3[1], p4[1]), Math.max(p1[0], p2[0], p3[0], p4[0]), Math.max(p1[1], p2[1], p3[1], p4[1]), tmpExtent);
      } else {
        createOrUpdate(Math.min(boxX, boxX + boxW), Math.min(boxY, boxY + boxH), Math.max(boxX, boxX + boxW), Math.max(boxY, boxY + boxH), tmpExtent);
      }
      if (snapToPixel) {
        x = Math.round(x);
        y = Math.round(y);
      }
      return {
        drawImageX: x,
        drawImageY: y,
        drawImageW: w,
        drawImageH: h,
        originX,
        originY,
        declutterBox: {
          minX: tmpExtent[0],
          minY: tmpExtent[1],
          maxX: tmpExtent[2],
          maxY: tmpExtent[3],
          value: feature
        },
        canvasTransform: transform2,
        scale: scale2
      };
    };
    Executor2.prototype.replayImageOrLabel_ = function(context, contextScale, imageOrLabel, dimensions, opacity, fillInstruction2, strokeInstruction2) {
      var fillStroke = !!(fillInstruction2 || strokeInstruction2);
      var box = dimensions.declutterBox;
      var canvas = context.canvas;
      var strokePadding = strokeInstruction2 ? strokeInstruction2[2] * dimensions.scale[0] / 2 : 0;
      var intersects2 = box.minX - strokePadding <= canvas.width / contextScale && box.maxX + strokePadding >= 0 && box.minY - strokePadding <= canvas.height / contextScale && box.maxY + strokePadding >= 0;
      if (intersects2) {
        if (fillStroke) {
          this.replayTextBackground_(context, p1, p2, p3, p4, fillInstruction2, strokeInstruction2);
        }
        drawImageOrLabel(context, dimensions.canvasTransform, opacity, imageOrLabel, dimensions.originX, dimensions.originY, dimensions.drawImageW, dimensions.drawImageH, dimensions.drawImageX, dimensions.drawImageY, dimensions.scale);
      }
      return true;
    };
    Executor2.prototype.fill_ = function(context) {
      if (this.alignFill_) {
        var origin_1 = apply(this.renderedTransform_, [0, 0]);
        var repeatSize = 512 * this.pixelRatio;
        context.save();
        context.translate(origin_1[0] % repeatSize, origin_1[1] % repeatSize);
        context.rotate(this.viewRotation_);
      }
      context.fill();
      if (this.alignFill_) {
        context.restore();
      }
    };
    Executor2.prototype.setStrokeStyle_ = function(context, instruction) {
      context["strokeStyle"] = instruction[1];
      context.lineWidth = instruction[2];
      context.lineCap = instruction[3];
      context.lineJoin = instruction[4];
      context.miterLimit = instruction[5];
      if (context.setLineDash) {
        context.lineDashOffset = instruction[7];
        context.setLineDash(instruction[6]);
      }
    };
    Executor2.prototype.drawLabelWithPointPlacement_ = function(text, textKey, strokeKey, fillKey) {
      var textState = this.textStates[textKey];
      var label = this.createLabel(text, textKey, fillKey, strokeKey);
      var strokeState = this.strokeStates[strokeKey];
      var pixelRatio = this.pixelRatio;
      var align = horizontalTextAlign(Array.isArray(text) ? text[0] : text, textState.textAlign || defaultTextAlign);
      var baseline = TEXT_ALIGN[textState.textBaseline || defaultTextBaseline];
      var strokeWidth = strokeState && strokeState.lineWidth ? strokeState.lineWidth : 0;
      var width = label.width / pixelRatio - 2 * textState.scale[0];
      var anchorX = align * width + 2 * (0.5 - align) * strokeWidth;
      var anchorY = baseline * label.height / pixelRatio + 2 * (0.5 - baseline) * strokeWidth;
      return {
        label,
        anchorX,
        anchorY
      };
    };
    Executor2.prototype.execute_ = function(context, contextScale, transform2, instructions, snapToPixel, opt_featureCallback, opt_hitExtent, opt_declutterTree) {
      var pixelCoordinates;
      if (this.pixelCoordinates_ && equals(transform2, this.renderedTransform_)) {
        pixelCoordinates = this.pixelCoordinates_;
      } else {
        if (!this.pixelCoordinates_) {
          this.pixelCoordinates_ = [];
        }
        pixelCoordinates = transform2D(this.coordinates, 0, this.coordinates.length, 2, transform2, this.pixelCoordinates_);
        setFromArray(this.renderedTransform_, transform2);
      }
      var i = 0;
      var ii = instructions.length;
      var d = 0;
      var dd;
      var anchorX, anchorY, prevX, prevY, roundX, roundY, image, text, textKey, strokeKey, fillKey;
      var pendingFill = 0;
      var pendingStroke = 0;
      var lastFillInstruction = null;
      var lastStrokeInstruction = null;
      var coordinateCache = this.coordinateCache_;
      var viewRotation = this.viewRotation_;
      var viewRotationFromTransform = Math.round(Math.atan2(-transform2[1], transform2[0]) * 1e12) / 1e12;
      var state = {
        context,
        pixelRatio: this.pixelRatio,
        resolution: this.resolution,
        rotation: viewRotation
      };
      var batchSize = this.instructions != instructions || this.overlaps ? 0 : 200;
      var feature;
      var x, y, currentGeometry;
      while (i < ii) {
        var instruction = instructions[i];
        var type = instruction[0];
        switch (type) {
          case Instruction_default.BEGIN_GEOMETRY:
            feature = instruction[1];
            currentGeometry = instruction[3];
            if (!feature.getGeometry()) {
              i = instruction[2];
            } else if (opt_hitExtent !== void 0 && !intersects(opt_hitExtent, currentGeometry.getExtent())) {
              i = instruction[2] + 1;
            } else {
              ++i;
            }
            break;
          case Instruction_default.BEGIN_PATH:
            if (pendingFill > batchSize) {
              this.fill_(context);
              pendingFill = 0;
            }
            if (pendingStroke > batchSize) {
              context.stroke();
              pendingStroke = 0;
            }
            if (!pendingFill && !pendingStroke) {
              context.beginPath();
              prevX = NaN;
              prevY = NaN;
            }
            ++i;
            break;
          case Instruction_default.CIRCLE:
            d = instruction[1];
            var x1 = pixelCoordinates[d];
            var y1 = pixelCoordinates[d + 1];
            var x2 = pixelCoordinates[d + 2];
            var y2 = pixelCoordinates[d + 3];
            var dx = x2 - x1;
            var dy = y2 - y1;
            var r = Math.sqrt(dx * dx + dy * dy);
            context.moveTo(x1 + r, y1);
            context.arc(x1, y1, r, 0, 2 * Math.PI, true);
            ++i;
            break;
          case Instruction_default.CLOSE_PATH:
            context.closePath();
            ++i;
            break;
          case Instruction_default.CUSTOM:
            d = instruction[1];
            dd = instruction[2];
            var geometry = instruction[3];
            var renderer = instruction[4];
            var fn = instruction.length == 6 ? instruction[5] : void 0;
            state.geometry = geometry;
            state.feature = feature;
            if (!(i in coordinateCache)) {
              coordinateCache[i] = [];
            }
            var coords = coordinateCache[i];
            if (fn) {
              fn(pixelCoordinates, d, dd, 2, coords);
            } else {
              coords[0] = pixelCoordinates[d];
              coords[1] = pixelCoordinates[d + 1];
              coords.length = 2;
            }
            renderer(coords, state);
            ++i;
            break;
          case Instruction_default.DRAW_IMAGE:
            d = instruction[1];
            dd = instruction[2];
            image = instruction[3];
            anchorX = instruction[4];
            anchorY = instruction[5];
            var height = instruction[6];
            var opacity = instruction[7];
            var originX = instruction[8];
            var originY = instruction[9];
            var rotateWithView = instruction[10];
            var rotation = instruction[11];
            var scale2 = instruction[12];
            var width = instruction[13];
            var declutterImageWithText = instruction[14];
            if (!image && instruction.length >= 19) {
              text = instruction[18];
              textKey = instruction[19];
              strokeKey = instruction[20];
              fillKey = instruction[21];
              var labelWithAnchor = this.drawLabelWithPointPlacement_(text, textKey, strokeKey, fillKey);
              image = labelWithAnchor.label;
              instruction[3] = image;
              var textOffsetX = instruction[22];
              anchorX = (labelWithAnchor.anchorX - textOffsetX) * this.pixelRatio;
              instruction[4] = anchorX;
              var textOffsetY = instruction[23];
              anchorY = (labelWithAnchor.anchorY - textOffsetY) * this.pixelRatio;
              instruction[5] = anchorY;
              height = image.height;
              instruction[6] = height;
              width = image.width;
              instruction[13] = width;
            }
            var geometryWidths = void 0;
            if (instruction.length > 24) {
              geometryWidths = instruction[24];
            }
            var padding = void 0, backgroundFill = void 0, backgroundStroke = void 0;
            if (instruction.length > 16) {
              padding = instruction[15];
              backgroundFill = instruction[16];
              backgroundStroke = instruction[17];
            } else {
              padding = defaultPadding;
              backgroundFill = false;
              backgroundStroke = false;
            }
            if (rotateWithView && viewRotationFromTransform) {
              rotation += viewRotation;
            } else if (!rotateWithView && !viewRotationFromTransform) {
              rotation -= viewRotation;
            }
            var widthIndex = 0;
            for (; d < dd; d += 2) {
              if (geometryWidths && geometryWidths[widthIndex++] < width / this.pixelRatio) {
                continue;
              }
              var dimensions = this.calculateImageOrLabelDimensions_(image.width, image.height, pixelCoordinates[d], pixelCoordinates[d + 1], width, height, anchorX, anchorY, originX, originY, rotation, scale2, snapToPixel, padding, backgroundFill || backgroundStroke, feature);
              var args = [
                context,
                contextScale,
                image,
                dimensions,
                opacity,
                backgroundFill ? lastFillInstruction : null,
                backgroundStroke ? lastStrokeInstruction : null
              ];
              var imageArgs = void 0;
              var imageDeclutterBox = void 0;
              if (opt_declutterTree && declutterImageWithText) {
                var index = dd - d;
                if (!declutterImageWithText[index]) {
                  declutterImageWithText[index] = args;
                  continue;
                }
                imageArgs = declutterImageWithText[index];
                delete declutterImageWithText[index];
                imageDeclutterBox = getDeclutterBox(imageArgs);
                if (opt_declutterTree.collides(imageDeclutterBox)) {
                  continue;
                }
              }
              if (opt_declutterTree && opt_declutterTree.collides(dimensions.declutterBox)) {
                continue;
              }
              if (imageArgs) {
                if (opt_declutterTree) {
                  opt_declutterTree.insert(imageDeclutterBox);
                }
                this.replayImageOrLabel_.apply(this, imageArgs);
              }
              if (opt_declutterTree) {
                opt_declutterTree.insert(dimensions.declutterBox);
              }
              this.replayImageOrLabel_.apply(this, args);
            }
            ++i;
            break;
          case Instruction_default.DRAW_CHARS:
            var begin = instruction[1];
            var end = instruction[2];
            var baseline = instruction[3];
            var overflow = instruction[4];
            fillKey = instruction[5];
            var maxAngle = instruction[6];
            var measurePixelRatio = instruction[7];
            var offsetY = instruction[8];
            strokeKey = instruction[9];
            var strokeWidth = instruction[10];
            text = instruction[11];
            textKey = instruction[12];
            var pixelRatioScale = [
              instruction[13],
              instruction[13]
            ];
            var textState = this.textStates[textKey];
            var font = textState.font;
            var textScale = [
              textState.scale[0] * measurePixelRatio,
              textState.scale[1] * measurePixelRatio
            ];
            var cachedWidths = void 0;
            if (font in this.widths_) {
              cachedWidths = this.widths_[font];
            } else {
              cachedWidths = {};
              this.widths_[font] = cachedWidths;
            }
            var pathLength = lineStringLength(pixelCoordinates, begin, end, 2);
            var textLength = Math.abs(textScale[0]) * measureAndCacheTextWidth(font, text, cachedWidths);
            if (overflow || textLength <= pathLength) {
              var textAlign = this.textStates[textKey].textAlign;
              var startM = (pathLength - textLength) * TEXT_ALIGN[textAlign];
              var parts = drawTextOnPath(pixelCoordinates, begin, end, 2, text, startM, maxAngle, Math.abs(textScale[0]), measureAndCacheTextWidth, font, cachedWidths, viewRotationFromTransform ? 0 : this.viewRotation_);
              drawChars:
                if (parts) {
                  var replayImageOrLabelArgs = [];
                  var c = void 0, cc = void 0, chars = void 0, label = void 0, part = void 0;
                  if (strokeKey) {
                    for (c = 0, cc = parts.length; c < cc; ++c) {
                      part = parts[c];
                      chars = part[4];
                      label = this.createLabel(chars, textKey, "", strokeKey);
                      anchorX = part[2] + (textScale[0] < 0 ? -strokeWidth : strokeWidth);
                      anchorY = baseline * label.height + (0.5 - baseline) * 2 * strokeWidth * textScale[1] / textScale[0] - offsetY;
                      var dimensions = this.calculateImageOrLabelDimensions_(label.width, label.height, part[0], part[1], label.width, label.height, anchorX, anchorY, 0, 0, part[3], pixelRatioScale, false, defaultPadding, false, feature);
                      if (opt_declutterTree && opt_declutterTree.collides(dimensions.declutterBox)) {
                        break drawChars;
                      }
                      replayImageOrLabelArgs.push([
                        context,
                        contextScale,
                        label,
                        dimensions,
                        1,
                        null,
                        null
                      ]);
                    }
                  }
                  if (fillKey) {
                    for (c = 0, cc = parts.length; c < cc; ++c) {
                      part = parts[c];
                      chars = part[4];
                      label = this.createLabel(chars, textKey, fillKey, "");
                      anchorX = part[2];
                      anchorY = baseline * label.height - offsetY;
                      var dimensions = this.calculateImageOrLabelDimensions_(label.width, label.height, part[0], part[1], label.width, label.height, anchorX, anchorY, 0, 0, part[3], pixelRatioScale, false, defaultPadding, false, feature);
                      if (opt_declutterTree && opt_declutterTree.collides(dimensions.declutterBox)) {
                        break drawChars;
                      }
                      replayImageOrLabelArgs.push([
                        context,
                        contextScale,
                        label,
                        dimensions,
                        1,
                        null,
                        null
                      ]);
                    }
                  }
                  if (opt_declutterTree) {
                    opt_declutterTree.load(replayImageOrLabelArgs.map(getDeclutterBox));
                  }
                  for (var i_1 = 0, ii_1 = replayImageOrLabelArgs.length; i_1 < ii_1; ++i_1) {
                    this.replayImageOrLabel_.apply(this, replayImageOrLabelArgs[i_1]);
                  }
                }
            }
            ++i;
            break;
          case Instruction_default.END_GEOMETRY:
            if (opt_featureCallback !== void 0) {
              feature = instruction[1];
              var result = opt_featureCallback(feature, currentGeometry);
              if (result) {
                return result;
              }
            }
            ++i;
            break;
          case Instruction_default.FILL:
            if (batchSize) {
              pendingFill++;
            } else {
              this.fill_(context);
            }
            ++i;
            break;
          case Instruction_default.MOVE_TO_LINE_TO:
            d = instruction[1];
            dd = instruction[2];
            x = pixelCoordinates[d];
            y = pixelCoordinates[d + 1];
            roundX = x + 0.5 | 0;
            roundY = y + 0.5 | 0;
            if (roundX !== prevX || roundY !== prevY) {
              context.moveTo(x, y);
              prevX = roundX;
              prevY = roundY;
            }
            for (d += 2; d < dd; d += 2) {
              x = pixelCoordinates[d];
              y = pixelCoordinates[d + 1];
              roundX = x + 0.5 | 0;
              roundY = y + 0.5 | 0;
              if (d == dd - 2 || roundX !== prevX || roundY !== prevY) {
                context.lineTo(x, y);
                prevX = roundX;
                prevY = roundY;
              }
            }
            ++i;
            break;
          case Instruction_default.SET_FILL_STYLE:
            lastFillInstruction = instruction;
            this.alignFill_ = instruction[2];
            if (pendingFill) {
              this.fill_(context);
              pendingFill = 0;
              if (pendingStroke) {
                context.stroke();
                pendingStroke = 0;
              }
            }
            context.fillStyle = instruction[1];
            ++i;
            break;
          case Instruction_default.SET_STROKE_STYLE:
            lastStrokeInstruction = instruction;
            if (pendingStroke) {
              context.stroke();
              pendingStroke = 0;
            }
            this.setStrokeStyle_(context, instruction);
            ++i;
            break;
          case Instruction_default.STROKE:
            if (batchSize) {
              pendingStroke++;
            } else {
              context.stroke();
            }
            ++i;
            break;
          default:
            ++i;
            break;
        }
      }
      if (pendingFill) {
        this.fill_(context);
      }
      if (pendingStroke) {
        context.stroke();
      }
      return void 0;
    };
    Executor2.prototype.execute = function(context, contextScale, transform2, viewRotation, snapToPixel, opt_declutterTree) {
      this.viewRotation_ = viewRotation;
      this.execute_(context, contextScale, transform2, this.instructions, snapToPixel, void 0, void 0, opt_declutterTree);
    };
    Executor2.prototype.executeHitDetection = function(context, transform2, viewRotation, opt_featureCallback, opt_hitExtent) {
      this.viewRotation_ = viewRotation;
      return this.execute_(context, 1, transform2, this.hitDetectionInstructions, true, opt_featureCallback, opt_hitExtent);
    };
    return Executor2;
  }();
  var Executor_default = Executor;

  // node_modules/ol/render/canvas/ExecutorGroup.js
  var ORDER = [
    BuilderType_default.POLYGON,
    BuilderType_default.CIRCLE,
    BuilderType_default.LINE_STRING,
    BuilderType_default.IMAGE,
    BuilderType_default.TEXT,
    BuilderType_default.DEFAULT
  ];
  var ExecutorGroup = function() {
    function ExecutorGroup2(maxExtent, resolution, pixelRatio, overlaps, allInstructions, opt_renderBuffer) {
      this.maxExtent_ = maxExtent;
      this.overlaps_ = overlaps;
      this.pixelRatio_ = pixelRatio;
      this.resolution_ = resolution;
      this.renderBuffer_ = opt_renderBuffer;
      this.executorsByZIndex_ = {};
      this.hitDetectionContext_ = null;
      this.hitDetectionTransform_ = create();
      this.createExecutors_(allInstructions);
    }
    ExecutorGroup2.prototype.clip = function(context, transform2) {
      var flatClipCoords = this.getClipCoords(transform2);
      context.beginPath();
      context.moveTo(flatClipCoords[0], flatClipCoords[1]);
      context.lineTo(flatClipCoords[2], flatClipCoords[3]);
      context.lineTo(flatClipCoords[4], flatClipCoords[5]);
      context.lineTo(flatClipCoords[6], flatClipCoords[7]);
      context.clip();
    };
    ExecutorGroup2.prototype.createExecutors_ = function(allInstructions) {
      for (var zIndex in allInstructions) {
        var executors = this.executorsByZIndex_[zIndex];
        if (executors === void 0) {
          executors = {};
          this.executorsByZIndex_[zIndex] = executors;
        }
        var instructionByZindex = allInstructions[zIndex];
        for (var builderType in instructionByZindex) {
          var instructions = instructionByZindex[builderType];
          executors[builderType] = new Executor_default(this.resolution_, this.pixelRatio_, this.overlaps_, instructions);
        }
      }
    };
    ExecutorGroup2.prototype.hasExecutors = function(executors) {
      for (var zIndex in this.executorsByZIndex_) {
        var candidates = this.executorsByZIndex_[zIndex];
        for (var i = 0, ii = executors.length; i < ii; ++i) {
          if (executors[i] in candidates) {
            return true;
          }
        }
      }
      return false;
    };
    ExecutorGroup2.prototype.forEachFeatureAtCoordinate = function(coordinate, resolution, rotation, hitTolerance, callback, declutteredFeatures) {
      hitTolerance = Math.round(hitTolerance);
      var contextSize = hitTolerance * 2 + 1;
      var transform2 = compose(this.hitDetectionTransform_, hitTolerance + 0.5, hitTolerance + 0.5, 1 / resolution, -1 / resolution, -rotation, -coordinate[0], -coordinate[1]);
      var newContext = !this.hitDetectionContext_;
      if (newContext) {
        this.hitDetectionContext_ = createCanvasContext2D(contextSize, contextSize);
      }
      var context = this.hitDetectionContext_;
      if (context.canvas.width !== contextSize || context.canvas.height !== contextSize) {
        context.canvas.width = contextSize;
        context.canvas.height = contextSize;
      } else if (!newContext) {
        context.clearRect(0, 0, contextSize, contextSize);
      }
      var hitExtent;
      if (this.renderBuffer_ !== void 0) {
        hitExtent = createEmpty();
        extendCoordinate(hitExtent, coordinate);
        buffer(hitExtent, resolution * (this.renderBuffer_ + hitTolerance), hitExtent);
      }
      var indexes = getPixelIndexArray(hitTolerance);
      var builderType;
      function featureCallback(feature, geometry) {
        var imageData = context.getImageData(0, 0, contextSize, contextSize).data;
        for (var i_1 = 0, ii = indexes.length; i_1 < ii; i_1++) {
          if (imageData[indexes[i_1]] > 0) {
            if (!declutteredFeatures || builderType !== BuilderType_default.IMAGE && builderType !== BuilderType_default.TEXT || declutteredFeatures.indexOf(feature) !== -1) {
              var idx = (indexes[i_1] - 3) / 4;
              var x = hitTolerance - idx % contextSize;
              var y = hitTolerance - (idx / contextSize | 0);
              var result_1 = callback(feature, geometry, x * x + y * y);
              if (result_1) {
                return result_1;
              }
            }
            context.clearRect(0, 0, contextSize, contextSize);
            break;
          }
        }
        return void 0;
      }
      var zs = Object.keys(this.executorsByZIndex_).map(Number);
      zs.sort(numberSafeCompareFunction);
      var i, j, executors, executor, result;
      for (i = zs.length - 1; i >= 0; --i) {
        var zIndexKey = zs[i].toString();
        executors = this.executorsByZIndex_[zIndexKey];
        for (j = ORDER.length - 1; j >= 0; --j) {
          builderType = ORDER[j];
          executor = executors[builderType];
          if (executor !== void 0) {
            result = executor.executeHitDetection(context, transform2, rotation, featureCallback, hitExtent);
            if (result) {
              return result;
            }
          }
        }
      }
      return void 0;
    };
    ExecutorGroup2.prototype.getClipCoords = function(transform2) {
      var maxExtent = this.maxExtent_;
      if (!maxExtent) {
        return null;
      }
      var minX = maxExtent[0];
      var minY = maxExtent[1];
      var maxX = maxExtent[2];
      var maxY = maxExtent[3];
      var flatClipCoords = [minX, minY, minX, maxY, maxX, maxY, maxX, minY];
      transform2D(flatClipCoords, 0, 8, 2, transform2, flatClipCoords);
      return flatClipCoords;
    };
    ExecutorGroup2.prototype.isEmpty = function() {
      return isEmpty(this.executorsByZIndex_);
    };
    ExecutorGroup2.prototype.execute = function(context, contextScale, transform2, viewRotation, snapToPixel, opt_builderTypes, opt_declutterTree) {
      var zs = Object.keys(this.executorsByZIndex_).map(Number);
      zs.sort(numberSafeCompareFunction);
      if (this.maxExtent_) {
        context.save();
        this.clip(context, transform2);
      }
      var builderTypes = opt_builderTypes ? opt_builderTypes : ORDER;
      var i, ii, j, jj, replays, replay;
      if (opt_declutterTree) {
        zs.reverse();
      }
      for (i = 0, ii = zs.length; i < ii; ++i) {
        var zIndexKey = zs[i].toString();
        replays = this.executorsByZIndex_[zIndexKey];
        for (j = 0, jj = builderTypes.length; j < jj; ++j) {
          var builderType = builderTypes[j];
          replay = replays[builderType];
          if (replay !== void 0) {
            replay.execute(context, contextScale, transform2, viewRotation, snapToPixel, opt_declutterTree);
          }
        }
      }
      if (this.maxExtent_) {
        context.restore();
      }
    };
    return ExecutorGroup2;
  }();
  var circlePixelIndexArrayCache = {};
  function getPixelIndexArray(radius) {
    if (circlePixelIndexArrayCache[radius] !== void 0) {
      return circlePixelIndexArrayCache[radius];
    }
    var size = radius * 2 + 1;
    var maxDistanceSq = radius * radius;
    var distances = new Array(maxDistanceSq + 1);
    for (var i = 0; i <= radius; ++i) {
      for (var j = 0; j <= radius; ++j) {
        var distanceSq = i * i + j * j;
        if (distanceSq > maxDistanceSq) {
          break;
        }
        var distance2 = distances[distanceSq];
        if (!distance2) {
          distance2 = [];
          distances[distanceSq] = distance2;
        }
        distance2.push(((radius + i) * size + (radius + j)) * 4 + 3);
        if (i > 0) {
          distance2.push(((radius - i) * size + (radius + j)) * 4 + 3);
        }
        if (j > 0) {
          distance2.push(((radius + i) * size + (radius - j)) * 4 + 3);
          if (i > 0) {
            distance2.push(((radius - i) * size + (radius - j)) * 4 + 3);
          }
        }
      }
    }
    var pixelIndex = [];
    for (var i = 0, ii = distances.length; i < ii; ++i) {
      if (distances[i]) {
        pixelIndex.push.apply(pixelIndex, distances[i]);
      }
    }
    circlePixelIndexArrayCache[radius] = pixelIndex;
    return pixelIndex;
  }
  var ExecutorGroup_default = ExecutorGroup;

  // node_modules/ol/ViewHint.js
  var ViewHint_default = {
    ANIMATING: 0,
    INTERACTING: 1
  };

  // node_modules/ol/render/canvas/Immediate.js
  var __extends39 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var CanvasImmediateRenderer = function(_super) {
    __extends39(CanvasImmediateRenderer2, _super);
    function CanvasImmediateRenderer2(context, pixelRatio, extent, transform2, viewRotation, opt_squaredTolerance, opt_userTransform) {
      var _this = _super.call(this) || this;
      _this.context_ = context;
      _this.pixelRatio_ = pixelRatio;
      _this.extent_ = extent;
      _this.transform_ = transform2;
      _this.viewRotation_ = viewRotation;
      _this.squaredTolerance_ = opt_squaredTolerance;
      _this.userTransform_ = opt_userTransform;
      _this.contextFillState_ = null;
      _this.contextStrokeState_ = null;
      _this.contextTextState_ = null;
      _this.fillState_ = null;
      _this.strokeState_ = null;
      _this.image_ = null;
      _this.imageAnchorX_ = 0;
      _this.imageAnchorY_ = 0;
      _this.imageHeight_ = 0;
      _this.imageOpacity_ = 0;
      _this.imageOriginX_ = 0;
      _this.imageOriginY_ = 0;
      _this.imageRotateWithView_ = false;
      _this.imageRotation_ = 0;
      _this.imageScale_ = [0, 0];
      _this.imageWidth_ = 0;
      _this.text_ = "";
      _this.textOffsetX_ = 0;
      _this.textOffsetY_ = 0;
      _this.textRotateWithView_ = false;
      _this.textRotation_ = 0;
      _this.textScale_ = [0, 0];
      _this.textFillState_ = null;
      _this.textStrokeState_ = null;
      _this.textState_ = null;
      _this.pixelCoordinates_ = [];
      _this.tmpLocalTransform_ = create();
      return _this;
    }
    CanvasImmediateRenderer2.prototype.drawImages_ = function(flatCoordinates, offset, end, stride) {
      if (!this.image_) {
        return;
      }
      var pixelCoordinates = transform2D(flatCoordinates, offset, end, stride, this.transform_, this.pixelCoordinates_);
      var context = this.context_;
      var localTransform = this.tmpLocalTransform_;
      var alpha = context.globalAlpha;
      if (this.imageOpacity_ != 1) {
        context.globalAlpha = alpha * this.imageOpacity_;
      }
      var rotation = this.imageRotation_;
      if (this.imageRotateWithView_) {
        rotation += this.viewRotation_;
      }
      for (var i = 0, ii = pixelCoordinates.length; i < ii; i += 2) {
        var x = pixelCoordinates[i] - this.imageAnchorX_;
        var y = pixelCoordinates[i + 1] - this.imageAnchorY_;
        if (rotation !== 0 || this.imageScale_[0] != 1 || this.imageScale_[1] != 1) {
          var centerX = x + this.imageAnchorX_;
          var centerY = y + this.imageAnchorY_;
          compose(localTransform, centerX, centerY, 1, 1, rotation, -centerX, -centerY);
          context.setTransform.apply(context, localTransform);
          context.translate(centerX, centerY);
          context.scale(this.imageScale_[0], this.imageScale_[1]);
          context.drawImage(this.image_, this.imageOriginX_, this.imageOriginY_, this.imageWidth_, this.imageHeight_, -this.imageAnchorX_, -this.imageAnchorY_, this.imageWidth_, this.imageHeight_);
          context.setTransform(1, 0, 0, 1, 0, 0);
        } else {
          context.drawImage(this.image_, this.imageOriginX_, this.imageOriginY_, this.imageWidth_, this.imageHeight_, x, y, this.imageWidth_, this.imageHeight_);
        }
      }
      if (this.imageOpacity_ != 1) {
        context.globalAlpha = alpha;
      }
    };
    CanvasImmediateRenderer2.prototype.drawText_ = function(flatCoordinates, offset, end, stride) {
      if (!this.textState_ || this.text_ === "") {
        return;
      }
      if (this.textFillState_) {
        this.setContextFillState_(this.textFillState_);
      }
      if (this.textStrokeState_) {
        this.setContextStrokeState_(this.textStrokeState_);
      }
      this.setContextTextState_(this.textState_);
      var pixelCoordinates = transform2D(flatCoordinates, offset, end, stride, this.transform_, this.pixelCoordinates_);
      var context = this.context_;
      var rotation = this.textRotation_;
      if (this.textRotateWithView_) {
        rotation += this.viewRotation_;
      }
      for (; offset < end; offset += stride) {
        var x = pixelCoordinates[offset] + this.textOffsetX_;
        var y = pixelCoordinates[offset + 1] + this.textOffsetY_;
        if (rotation !== 0 || this.textScale_[0] != 1 || this.textScale_[1] != 1) {
          var localTransform = compose(this.tmpLocalTransform_, x, y, 1, 1, rotation, -x, -y);
          context.setTransform.apply(context, localTransform);
          context.translate(x, y);
          context.scale(this.textScale_[0], this.textScale_[1]);
          if (this.textStrokeState_) {
            context.strokeText(this.text_, 0, 0);
          }
          if (this.textFillState_) {
            context.fillText(this.text_, 0, 0);
          }
          context.setTransform(1, 0, 0, 1, 0, 0);
        } else {
          if (this.textStrokeState_) {
            context.strokeText(this.text_, x, y);
          }
          if (this.textFillState_) {
            context.fillText(this.text_, x, y);
          }
        }
      }
    };
    CanvasImmediateRenderer2.prototype.moveToLineTo_ = function(flatCoordinates, offset, end, stride, close) {
      var context = this.context_;
      var pixelCoordinates = transform2D(flatCoordinates, offset, end, stride, this.transform_, this.pixelCoordinates_);
      context.moveTo(pixelCoordinates[0], pixelCoordinates[1]);
      var length = pixelCoordinates.length;
      if (close) {
        length -= 2;
      }
      for (var i = 2; i < length; i += 2) {
        context.lineTo(pixelCoordinates[i], pixelCoordinates[i + 1]);
      }
      if (close) {
        context.closePath();
      }
      return end;
    };
    CanvasImmediateRenderer2.prototype.drawRings_ = function(flatCoordinates, offset, ends, stride) {
      for (var i = 0, ii = ends.length; i < ii; ++i) {
        offset = this.moveToLineTo_(flatCoordinates, offset, ends[i], stride, true);
      }
      return offset;
    };
    CanvasImmediateRenderer2.prototype.drawCircle = function(geometry) {
      if (!intersects(this.extent_, geometry.getExtent())) {
        return;
      }
      if (this.fillState_ || this.strokeState_) {
        if (this.fillState_) {
          this.setContextFillState_(this.fillState_);
        }
        if (this.strokeState_) {
          this.setContextStrokeState_(this.strokeState_);
        }
        var pixelCoordinates = transformGeom2D(geometry, this.transform_, this.pixelCoordinates_);
        var dx = pixelCoordinates[2] - pixelCoordinates[0];
        var dy = pixelCoordinates[3] - pixelCoordinates[1];
        var radius = Math.sqrt(dx * dx + dy * dy);
        var context = this.context_;
        context.beginPath();
        context.arc(pixelCoordinates[0], pixelCoordinates[1], radius, 0, 2 * Math.PI);
        if (this.fillState_) {
          context.fill();
        }
        if (this.strokeState_) {
          context.stroke();
        }
      }
      if (this.text_ !== "") {
        this.drawText_(geometry.getCenter(), 0, 2, 2);
      }
    };
    CanvasImmediateRenderer2.prototype.setStyle = function(style) {
      this.setFillStrokeStyle(style.getFill(), style.getStroke());
      this.setImageStyle(style.getImage());
      this.setTextStyle(style.getText());
    };
    CanvasImmediateRenderer2.prototype.setTransform = function(transform2) {
      this.transform_ = transform2;
    };
    CanvasImmediateRenderer2.prototype.drawGeometry = function(geometry) {
      var type = geometry.getType();
      switch (type) {
        case GeometryType_default.POINT:
          this.drawPoint(geometry);
          break;
        case GeometryType_default.LINE_STRING:
          this.drawLineString(geometry);
          break;
        case GeometryType_default.POLYGON:
          this.drawPolygon(geometry);
          break;
        case GeometryType_default.MULTI_POINT:
          this.drawMultiPoint(geometry);
          break;
        case GeometryType_default.MULTI_LINE_STRING:
          this.drawMultiLineString(geometry);
          break;
        case GeometryType_default.MULTI_POLYGON:
          this.drawMultiPolygon(geometry);
          break;
        case GeometryType_default.GEOMETRY_COLLECTION:
          this.drawGeometryCollection(geometry);
          break;
        case GeometryType_default.CIRCLE:
          this.drawCircle(geometry);
          break;
        default:
      }
    };
    CanvasImmediateRenderer2.prototype.drawFeature = function(feature, style) {
      var geometry = style.getGeometryFunction()(feature);
      if (!geometry || !intersects(this.extent_, geometry.getExtent())) {
        return;
      }
      this.setStyle(style);
      this.drawGeometry(geometry);
    };
    CanvasImmediateRenderer2.prototype.drawGeometryCollection = function(geometry) {
      var geometries = geometry.getGeometriesArray();
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        this.drawGeometry(geometries[i]);
      }
    };
    CanvasImmediateRenderer2.prototype.drawPoint = function(geometry) {
      if (this.squaredTolerance_) {
        geometry = geometry.simplifyTransformed(this.squaredTolerance_, this.userTransform_);
      }
      var flatCoordinates = geometry.getFlatCoordinates();
      var stride = geometry.getStride();
      if (this.image_) {
        this.drawImages_(flatCoordinates, 0, flatCoordinates.length, stride);
      }
      if (this.text_ !== "") {
        this.drawText_(flatCoordinates, 0, flatCoordinates.length, stride);
      }
    };
    CanvasImmediateRenderer2.prototype.drawMultiPoint = function(geometry) {
      if (this.squaredTolerance_) {
        geometry = geometry.simplifyTransformed(this.squaredTolerance_, this.userTransform_);
      }
      var flatCoordinates = geometry.getFlatCoordinates();
      var stride = geometry.getStride();
      if (this.image_) {
        this.drawImages_(flatCoordinates, 0, flatCoordinates.length, stride);
      }
      if (this.text_ !== "") {
        this.drawText_(flatCoordinates, 0, flatCoordinates.length, stride);
      }
    };
    CanvasImmediateRenderer2.prototype.drawLineString = function(geometry) {
      if (this.squaredTolerance_) {
        geometry = geometry.simplifyTransformed(this.squaredTolerance_, this.userTransform_);
      }
      if (!intersects(this.extent_, geometry.getExtent())) {
        return;
      }
      if (this.strokeState_) {
        this.setContextStrokeState_(this.strokeState_);
        var context = this.context_;
        var flatCoordinates = geometry.getFlatCoordinates();
        context.beginPath();
        this.moveToLineTo_(flatCoordinates, 0, flatCoordinates.length, geometry.getStride(), false);
        context.stroke();
      }
      if (this.text_ !== "") {
        var flatMidpoint = geometry.getFlatMidpoint();
        this.drawText_(flatMidpoint, 0, 2, 2);
      }
    };
    CanvasImmediateRenderer2.prototype.drawMultiLineString = function(geometry) {
      if (this.squaredTolerance_) {
        geometry = geometry.simplifyTransformed(this.squaredTolerance_, this.userTransform_);
      }
      var geometryExtent = geometry.getExtent();
      if (!intersects(this.extent_, geometryExtent)) {
        return;
      }
      if (this.strokeState_) {
        this.setContextStrokeState_(this.strokeState_);
        var context = this.context_;
        var flatCoordinates = geometry.getFlatCoordinates();
        var offset = 0;
        var ends = geometry.getEnds();
        var stride = geometry.getStride();
        context.beginPath();
        for (var i = 0, ii = ends.length; i < ii; ++i) {
          offset = this.moveToLineTo_(flatCoordinates, offset, ends[i], stride, false);
        }
        context.stroke();
      }
      if (this.text_ !== "") {
        var flatMidpoints = geometry.getFlatMidpoints();
        this.drawText_(flatMidpoints, 0, flatMidpoints.length, 2);
      }
    };
    CanvasImmediateRenderer2.prototype.drawPolygon = function(geometry) {
      if (this.squaredTolerance_) {
        geometry = geometry.simplifyTransformed(this.squaredTolerance_, this.userTransform_);
      }
      if (!intersects(this.extent_, geometry.getExtent())) {
        return;
      }
      if (this.strokeState_ || this.fillState_) {
        if (this.fillState_) {
          this.setContextFillState_(this.fillState_);
        }
        if (this.strokeState_) {
          this.setContextStrokeState_(this.strokeState_);
        }
        var context = this.context_;
        context.beginPath();
        this.drawRings_(geometry.getOrientedFlatCoordinates(), 0, geometry.getEnds(), geometry.getStride());
        if (this.fillState_) {
          context.fill();
        }
        if (this.strokeState_) {
          context.stroke();
        }
      }
      if (this.text_ !== "") {
        var flatInteriorPoint = geometry.getFlatInteriorPoint();
        this.drawText_(flatInteriorPoint, 0, 2, 2);
      }
    };
    CanvasImmediateRenderer2.prototype.drawMultiPolygon = function(geometry) {
      if (this.squaredTolerance_) {
        geometry = geometry.simplifyTransformed(this.squaredTolerance_, this.userTransform_);
      }
      if (!intersects(this.extent_, geometry.getExtent())) {
        return;
      }
      if (this.strokeState_ || this.fillState_) {
        if (this.fillState_) {
          this.setContextFillState_(this.fillState_);
        }
        if (this.strokeState_) {
          this.setContextStrokeState_(this.strokeState_);
        }
        var context = this.context_;
        var flatCoordinates = geometry.getOrientedFlatCoordinates();
        var offset = 0;
        var endss = geometry.getEndss();
        var stride = geometry.getStride();
        context.beginPath();
        for (var i = 0, ii = endss.length; i < ii; ++i) {
          var ends = endss[i];
          offset = this.drawRings_(flatCoordinates, offset, ends, stride);
        }
        if (this.fillState_) {
          context.fill();
        }
        if (this.strokeState_) {
          context.stroke();
        }
      }
      if (this.text_ !== "") {
        var flatInteriorPoints = geometry.getFlatInteriorPoints();
        this.drawText_(flatInteriorPoints, 0, flatInteriorPoints.length, 2);
      }
    };
    CanvasImmediateRenderer2.prototype.setContextFillState_ = function(fillState) {
      var context = this.context_;
      var contextFillState = this.contextFillState_;
      if (!contextFillState) {
        context.fillStyle = fillState.fillStyle;
        this.contextFillState_ = {
          fillStyle: fillState.fillStyle
        };
      } else {
        if (contextFillState.fillStyle != fillState.fillStyle) {
          contextFillState.fillStyle = fillState.fillStyle;
          context.fillStyle = fillState.fillStyle;
        }
      }
    };
    CanvasImmediateRenderer2.prototype.setContextStrokeState_ = function(strokeState) {
      var context = this.context_;
      var contextStrokeState = this.contextStrokeState_;
      if (!contextStrokeState) {
        context.lineCap = strokeState.lineCap;
        if (context.setLineDash) {
          context.setLineDash(strokeState.lineDash);
          context.lineDashOffset = strokeState.lineDashOffset;
        }
        context.lineJoin = strokeState.lineJoin;
        context.lineWidth = strokeState.lineWidth;
        context.miterLimit = strokeState.miterLimit;
        context.strokeStyle = strokeState.strokeStyle;
        this.contextStrokeState_ = {
          lineCap: strokeState.lineCap,
          lineDash: strokeState.lineDash,
          lineDashOffset: strokeState.lineDashOffset,
          lineJoin: strokeState.lineJoin,
          lineWidth: strokeState.lineWidth,
          miterLimit: strokeState.miterLimit,
          strokeStyle: strokeState.strokeStyle
        };
      } else {
        if (contextStrokeState.lineCap != strokeState.lineCap) {
          contextStrokeState.lineCap = strokeState.lineCap;
          context.lineCap = strokeState.lineCap;
        }
        if (context.setLineDash) {
          if (!equals(contextStrokeState.lineDash, strokeState.lineDash)) {
            context.setLineDash(contextStrokeState.lineDash = strokeState.lineDash);
          }
          if (contextStrokeState.lineDashOffset != strokeState.lineDashOffset) {
            contextStrokeState.lineDashOffset = strokeState.lineDashOffset;
            context.lineDashOffset = strokeState.lineDashOffset;
          }
        }
        if (contextStrokeState.lineJoin != strokeState.lineJoin) {
          contextStrokeState.lineJoin = strokeState.lineJoin;
          context.lineJoin = strokeState.lineJoin;
        }
        if (contextStrokeState.lineWidth != strokeState.lineWidth) {
          contextStrokeState.lineWidth = strokeState.lineWidth;
          context.lineWidth = strokeState.lineWidth;
        }
        if (contextStrokeState.miterLimit != strokeState.miterLimit) {
          contextStrokeState.miterLimit = strokeState.miterLimit;
          context.miterLimit = strokeState.miterLimit;
        }
        if (contextStrokeState.strokeStyle != strokeState.strokeStyle) {
          contextStrokeState.strokeStyle = strokeState.strokeStyle;
          context.strokeStyle = strokeState.strokeStyle;
        }
      }
    };
    CanvasImmediateRenderer2.prototype.setContextTextState_ = function(textState) {
      var context = this.context_;
      var contextTextState = this.contextTextState_;
      var textAlign = textState.textAlign ? textState.textAlign : defaultTextAlign;
      if (!contextTextState) {
        context.font = textState.font;
        context.textAlign = textAlign;
        context.textBaseline = textState.textBaseline;
        this.contextTextState_ = {
          font: textState.font,
          textAlign,
          textBaseline: textState.textBaseline
        };
      } else {
        if (contextTextState.font != textState.font) {
          contextTextState.font = textState.font;
          context.font = textState.font;
        }
        if (contextTextState.textAlign != textAlign) {
          contextTextState.textAlign = textAlign;
          context.textAlign = textAlign;
        }
        if (contextTextState.textBaseline != textState.textBaseline) {
          contextTextState.textBaseline = textState.textBaseline;
          context.textBaseline = textState.textBaseline;
        }
      }
    };
    CanvasImmediateRenderer2.prototype.setFillStrokeStyle = function(fillStyle, strokeStyle) {
      var _this = this;
      if (!fillStyle) {
        this.fillState_ = null;
      } else {
        var fillStyleColor = fillStyle.getColor();
        this.fillState_ = {
          fillStyle: asColorLike(fillStyleColor ? fillStyleColor : defaultFillStyle)
        };
      }
      if (!strokeStyle) {
        this.strokeState_ = null;
      } else {
        var strokeStyleColor = strokeStyle.getColor();
        var strokeStyleLineCap = strokeStyle.getLineCap();
        var strokeStyleLineDash = strokeStyle.getLineDash();
        var strokeStyleLineDashOffset = strokeStyle.getLineDashOffset();
        var strokeStyleLineJoin = strokeStyle.getLineJoin();
        var strokeStyleWidth = strokeStyle.getWidth();
        var strokeStyleMiterLimit = strokeStyle.getMiterLimit();
        var lineDash = strokeStyleLineDash ? strokeStyleLineDash : defaultLineDash;
        this.strokeState_ = {
          lineCap: strokeStyleLineCap !== void 0 ? strokeStyleLineCap : defaultLineCap,
          lineDash: this.pixelRatio_ === 1 ? lineDash : lineDash.map(function(n) {
            return n * _this.pixelRatio_;
          }),
          lineDashOffset: (strokeStyleLineDashOffset ? strokeStyleLineDashOffset : defaultLineDashOffset) * this.pixelRatio_,
          lineJoin: strokeStyleLineJoin !== void 0 ? strokeStyleLineJoin : defaultLineJoin,
          lineWidth: (strokeStyleWidth !== void 0 ? strokeStyleWidth : defaultLineWidth) * this.pixelRatio_,
          miterLimit: strokeStyleMiterLimit !== void 0 ? strokeStyleMiterLimit : defaultMiterLimit,
          strokeStyle: asColorLike(strokeStyleColor ? strokeStyleColor : defaultStrokeStyle)
        };
      }
    };
    CanvasImmediateRenderer2.prototype.setImageStyle = function(imageStyle) {
      var imageSize;
      if (!imageStyle || !(imageSize = imageStyle.getSize())) {
        this.image_ = null;
        return;
      }
      var imageAnchor = imageStyle.getAnchor();
      var imageOrigin = imageStyle.getOrigin();
      this.image_ = imageStyle.getImage(this.pixelRatio_);
      this.imageAnchorX_ = imageAnchor[0] * this.pixelRatio_;
      this.imageAnchorY_ = imageAnchor[1] * this.pixelRatio_;
      this.imageHeight_ = imageSize[1] * this.pixelRatio_;
      this.imageOpacity_ = imageStyle.getOpacity();
      this.imageOriginX_ = imageOrigin[0];
      this.imageOriginY_ = imageOrigin[1];
      this.imageRotateWithView_ = imageStyle.getRotateWithView();
      this.imageRotation_ = imageStyle.getRotation();
      this.imageScale_ = imageStyle.getScaleArray();
      this.imageWidth_ = imageSize[0] * this.pixelRatio_;
    };
    CanvasImmediateRenderer2.prototype.setTextStyle = function(textStyle) {
      if (!textStyle) {
        this.text_ = "";
      } else {
        var textFillStyle = textStyle.getFill();
        if (!textFillStyle) {
          this.textFillState_ = null;
        } else {
          var textFillStyleColor = textFillStyle.getColor();
          this.textFillState_ = {
            fillStyle: asColorLike(textFillStyleColor ? textFillStyleColor : defaultFillStyle)
          };
        }
        var textStrokeStyle = textStyle.getStroke();
        if (!textStrokeStyle) {
          this.textStrokeState_ = null;
        } else {
          var textStrokeStyleColor = textStrokeStyle.getColor();
          var textStrokeStyleLineCap = textStrokeStyle.getLineCap();
          var textStrokeStyleLineDash = textStrokeStyle.getLineDash();
          var textStrokeStyleLineDashOffset = textStrokeStyle.getLineDashOffset();
          var textStrokeStyleLineJoin = textStrokeStyle.getLineJoin();
          var textStrokeStyleWidth = textStrokeStyle.getWidth();
          var textStrokeStyleMiterLimit = textStrokeStyle.getMiterLimit();
          this.textStrokeState_ = {
            lineCap: textStrokeStyleLineCap !== void 0 ? textStrokeStyleLineCap : defaultLineCap,
            lineDash: textStrokeStyleLineDash ? textStrokeStyleLineDash : defaultLineDash,
            lineDashOffset: textStrokeStyleLineDashOffset ? textStrokeStyleLineDashOffset : defaultLineDashOffset,
            lineJoin: textStrokeStyleLineJoin !== void 0 ? textStrokeStyleLineJoin : defaultLineJoin,
            lineWidth: textStrokeStyleWidth !== void 0 ? textStrokeStyleWidth : defaultLineWidth,
            miterLimit: textStrokeStyleMiterLimit !== void 0 ? textStrokeStyleMiterLimit : defaultMiterLimit,
            strokeStyle: asColorLike(textStrokeStyleColor ? textStrokeStyleColor : defaultStrokeStyle)
          };
        }
        var textFont = textStyle.getFont();
        var textOffsetX = textStyle.getOffsetX();
        var textOffsetY = textStyle.getOffsetY();
        var textRotateWithView = textStyle.getRotateWithView();
        var textRotation = textStyle.getRotation();
        var textScale = textStyle.getScaleArray();
        var textText = textStyle.getText();
        var textTextAlign = textStyle.getTextAlign();
        var textTextBaseline = textStyle.getTextBaseline();
        this.textState_ = {
          font: textFont !== void 0 ? textFont : defaultFont,
          textAlign: textTextAlign !== void 0 ? textTextAlign : defaultTextAlign,
          textBaseline: textTextBaseline !== void 0 ? textTextBaseline : defaultTextBaseline
        };
        this.text_ = textText !== void 0 ? Array.isArray(textText) ? textText.reduce(function(acc, t, i) {
          return acc += i % 2 ? " " : t;
        }, "") : textText : "";
        this.textOffsetX_ = textOffsetX !== void 0 ? this.pixelRatio_ * textOffsetX : 0;
        this.textOffsetY_ = textOffsetY !== void 0 ? this.pixelRatio_ * textOffsetY : 0;
        this.textRotateWithView_ = textRotateWithView !== void 0 ? textRotateWithView : false;
        this.textRotation_ = textRotation !== void 0 ? textRotation : 0;
        this.textScale_ = [
          this.pixelRatio_ * textScale[0],
          this.pixelRatio_ * textScale[1]
        ];
      }
    };
    return CanvasImmediateRenderer2;
  }(VectorContext_default);
  var Immediate_default = CanvasImmediateRenderer;

  // node_modules/ol/style/IconAnchorUnits.js
  var IconAnchorUnits_default = {
    FRACTION: "fraction",
    PIXELS: "pixels"
  };

  // node_modules/ol/style/IconOrigin.js
  var IconOrigin_default = {
    BOTTOM_LEFT: "bottom-left",
    BOTTOM_RIGHT: "bottom-right",
    TOP_LEFT: "top-left",
    TOP_RIGHT: "top-right"
  };

  // node_modules/ol/style/IconImageCache.js
  var IconImageCache = function() {
    function IconImageCache2() {
      this.cache_ = {};
      this.cacheSize_ = 0;
      this.maxCacheSize_ = 32;
    }
    IconImageCache2.prototype.clear = function() {
      this.cache_ = {};
      this.cacheSize_ = 0;
    };
    IconImageCache2.prototype.canExpireCache = function() {
      return this.cacheSize_ > this.maxCacheSize_;
    };
    IconImageCache2.prototype.expire = function() {
      if (this.canExpireCache()) {
        var i = 0;
        for (var key in this.cache_) {
          var iconImage = this.cache_[key];
          if ((i++ & 3) === 0 && !iconImage.hasListener()) {
            delete this.cache_[key];
            --this.cacheSize_;
          }
        }
      }
    };
    IconImageCache2.prototype.get = function(src, crossOrigin, color) {
      var key = getKey(src, crossOrigin, color);
      return key in this.cache_ ? this.cache_[key] : null;
    };
    IconImageCache2.prototype.set = function(src, crossOrigin, color, iconImage) {
      var key = getKey(src, crossOrigin, color);
      this.cache_[key] = iconImage;
      ++this.cacheSize_;
    };
    IconImageCache2.prototype.setSize = function(maxCacheSize) {
      this.maxCacheSize_ = maxCacheSize;
      this.expire();
    };
    return IconImageCache2;
  }();
  function getKey(src, crossOrigin, color) {
    var colorString = color ? asString(color) : "null";
    return crossOrigin + ":" + src + ":" + colorString;
  }
  var shared = new IconImageCache();

  // node_modules/ol/ImageBase.js
  var __extends40 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var ImageBase = function(_super) {
    __extends40(ImageBase2, _super);
    function ImageBase2(extent, resolution, pixelRatio, state) {
      var _this = _super.call(this) || this;
      _this.extent = extent;
      _this.pixelRatio_ = pixelRatio;
      _this.resolution = resolution;
      _this.state = state;
      return _this;
    }
    ImageBase2.prototype.changed = function() {
      this.dispatchEvent(EventType_default.CHANGE);
    };
    ImageBase2.prototype.getExtent = function() {
      return this.extent;
    };
    ImageBase2.prototype.getImage = function() {
      return abstract();
    };
    ImageBase2.prototype.getPixelRatio = function() {
      return this.pixelRatio_;
    };
    ImageBase2.prototype.getResolution = function() {
      return this.resolution;
    };
    ImageBase2.prototype.getState = function() {
      return this.state;
    };
    ImageBase2.prototype.load = function() {
      abstract();
    };
    return ImageBase2;
  }(Target_default);
  var ImageBase_default = ImageBase;

  // node_modules/ol/Image.js
  var __extends41 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var ImageWrapper = function(_super) {
    __extends41(ImageWrapper2, _super);
    function ImageWrapper2(extent, resolution, pixelRatio, src, crossOrigin, imageLoadFunction) {
      var _this = _super.call(this, extent, resolution, pixelRatio, ImageState_default.IDLE) || this;
      _this.src_ = src;
      _this.image_ = new Image();
      if (crossOrigin !== null) {
        _this.image_.crossOrigin = crossOrigin;
      }
      _this.unlisten_ = null;
      _this.state = ImageState_default.IDLE;
      _this.imageLoadFunction_ = imageLoadFunction;
      return _this;
    }
    ImageWrapper2.prototype.getImage = function() {
      return this.image_;
    };
    ImageWrapper2.prototype.handleImageError_ = function() {
      this.state = ImageState_default.ERROR;
      this.unlistenImage_();
      this.changed();
    };
    ImageWrapper2.prototype.handleImageLoad_ = function() {
      if (this.resolution === void 0) {
        this.resolution = getHeight(this.extent) / this.image_.height;
      }
      this.state = ImageState_default.LOADED;
      this.unlistenImage_();
      this.changed();
    };
    ImageWrapper2.prototype.load = function() {
      if (this.state == ImageState_default.IDLE || this.state == ImageState_default.ERROR) {
        this.state = ImageState_default.LOADING;
        this.changed();
        this.imageLoadFunction_(this, this.src_);
        this.unlisten_ = listenImage(this.image_, this.handleImageLoad_.bind(this), this.handleImageError_.bind(this));
      }
    };
    ImageWrapper2.prototype.setImage = function(image) {
      this.image_ = image;
      this.resolution = getHeight(this.extent) / this.image_.height;
    };
    ImageWrapper2.prototype.unlistenImage_ = function() {
      if (this.unlisten_) {
        this.unlisten_();
        this.unlisten_ = null;
      }
    };
    return ImageWrapper2;
  }(ImageBase_default);
  function listenImage(image, loadHandler, errorHandler) {
    var img = image;
    var listening = true;
    var decoding = false;
    var loaded = false;
    var listenerKeys = [
      listenOnce(img, EventType_default.LOAD, function() {
        loaded = true;
        if (!decoding) {
          loadHandler();
        }
      })
    ];
    if (img.src && IMAGE_DECODE) {
      decoding = true;
      img.decode().then(function() {
        if (listening) {
          loadHandler();
        }
      }).catch(function(error) {
        if (listening) {
          if (loaded) {
            loadHandler();
          } else {
            errorHandler();
          }
        }
      });
    } else {
      listenerKeys.push(listenOnce(img, EventType_default.ERROR, errorHandler));
    }
    return function unlisten() {
      listening = false;
      listenerKeys.forEach(unlistenByKey);
    };
  }

  // node_modules/ol/style/IconImage.js
  var __extends42 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var taintedTestContext = null;
  var IconImage = function(_super) {
    __extends42(IconImage2, _super);
    function IconImage2(image, src, size, crossOrigin, imageState, color) {
      var _this = _super.call(this) || this;
      _this.hitDetectionImage_ = null;
      _this.image_ = !image ? new Image() : image;
      if (crossOrigin !== null) {
        _this.image_.crossOrigin = crossOrigin;
      }
      _this.canvas_ = {};
      _this.color_ = color;
      _this.unlisten_ = null;
      _this.imageState_ = imageState;
      _this.size_ = size;
      _this.src_ = src;
      _this.tainted_;
      return _this;
    }
    IconImage2.prototype.isTainted_ = function() {
      if (this.tainted_ === void 0 && this.imageState_ === ImageState_default.LOADED) {
        if (!taintedTestContext) {
          taintedTestContext = createCanvasContext2D(1, 1);
        }
        taintedTestContext.drawImage(this.image_, 0, 0);
        try {
          taintedTestContext.getImageData(0, 0, 1, 1);
          this.tainted_ = false;
        } catch (e) {
          taintedTestContext = null;
          this.tainted_ = true;
        }
      }
      return this.tainted_ === true;
    };
    IconImage2.prototype.dispatchChangeEvent_ = function() {
      this.dispatchEvent(EventType_default.CHANGE);
    };
    IconImage2.prototype.handleImageError_ = function() {
      this.imageState_ = ImageState_default.ERROR;
      this.unlistenImage_();
      this.dispatchChangeEvent_();
    };
    IconImage2.prototype.handleImageLoad_ = function() {
      this.imageState_ = ImageState_default.LOADED;
      if (this.size_) {
        this.image_.width = this.size_[0];
        this.image_.height = this.size_[1];
      } else {
        this.size_ = [this.image_.width, this.image_.height];
      }
      this.unlistenImage_();
      this.dispatchChangeEvent_();
    };
    IconImage2.prototype.getImage = function(pixelRatio) {
      this.replaceColor_(pixelRatio);
      return this.canvas_[pixelRatio] ? this.canvas_[pixelRatio] : this.image_;
    };
    IconImage2.prototype.getPixelRatio = function(pixelRatio) {
      this.replaceColor_(pixelRatio);
      return this.canvas_[pixelRatio] ? pixelRatio : 1;
    };
    IconImage2.prototype.getImageState = function() {
      return this.imageState_;
    };
    IconImage2.prototype.getHitDetectionImage = function() {
      if (!this.hitDetectionImage_) {
        if (this.isTainted_()) {
          var width = this.size_[0];
          var height = this.size_[1];
          var context = createCanvasContext2D(width, height);
          context.fillRect(0, 0, width, height);
          this.hitDetectionImage_ = context.canvas;
        } else {
          this.hitDetectionImage_ = this.image_;
        }
      }
      return this.hitDetectionImage_;
    };
    IconImage2.prototype.getSize = function() {
      return this.size_;
    };
    IconImage2.prototype.getSrc = function() {
      return this.src_;
    };
    IconImage2.prototype.load = function() {
      if (this.imageState_ == ImageState_default.IDLE) {
        this.imageState_ = ImageState_default.LOADING;
        try {
          this.image_.src = this.src_;
        } catch (e) {
          this.handleImageError_();
        }
        this.unlisten_ = listenImage(this.image_, this.handleImageLoad_.bind(this), this.handleImageError_.bind(this));
      }
    };
    IconImage2.prototype.replaceColor_ = function(pixelRatio) {
      if (!this.color_ || this.canvas_[pixelRatio] || this.imageState_ !== ImageState_default.LOADED) {
        return;
      }
      var canvas = document.createElement("canvas");
      this.canvas_[pixelRatio] = canvas;
      canvas.width = Math.ceil(this.image_.width * pixelRatio);
      canvas.height = Math.ceil(this.image_.height * pixelRatio);
      var ctx = canvas.getContext("2d");
      ctx.scale(pixelRatio, pixelRatio);
      ctx.drawImage(this.image_, 0, 0);
      ctx.globalCompositeOperation = "multiply";
      if (ctx.globalCompositeOperation === "multiply" || this.isTainted_()) {
        ctx.fillStyle = asString(this.color_);
        ctx.fillRect(0, 0, canvas.width / pixelRatio, canvas.height / pixelRatio);
        ctx.globalCompositeOperation = "destination-in";
        ctx.drawImage(this.image_, 0, 0);
      } else {
        var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imgData.data;
        var r = this.color_[0] / 255;
        var g = this.color_[1] / 255;
        var b = this.color_[2] / 255;
        var a = this.color_[3];
        for (var i = 0, ii = data.length; i < ii; i += 4) {
          data[i] *= r;
          data[i + 1] *= g;
          data[i + 2] *= b;
          data[i + 3] *= a;
        }
        ctx.putImageData(imgData, 0, 0);
      }
    };
    IconImage2.prototype.unlistenImage_ = function() {
      if (this.unlisten_) {
        this.unlisten_();
        this.unlisten_ = null;
      }
    };
    return IconImage2;
  }(Target_default);
  function get4(image, src, size, crossOrigin, imageState, color) {
    var iconImage = shared.get(src, crossOrigin, color);
    if (!iconImage) {
      iconImage = new IconImage(image, src, size, crossOrigin, imageState, color);
      shared.set(src, crossOrigin, color, iconImage);
    }
    return iconImage;
  }

  // node_modules/ol/style/Icon.js
  var __extends43 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Icon = function(_super) {
    __extends43(Icon2, _super);
    function Icon2(opt_options) {
      var _this = this;
      var options = opt_options || {};
      var opacity = options.opacity !== void 0 ? options.opacity : 1;
      var rotation = options.rotation !== void 0 ? options.rotation : 0;
      var scale2 = options.scale !== void 0 ? options.scale : 1;
      var rotateWithView = options.rotateWithView !== void 0 ? options.rotateWithView : false;
      _this = _super.call(this, {
        opacity,
        rotation,
        scale: scale2,
        displacement: options.displacement !== void 0 ? options.displacement : [0, 0],
        rotateWithView
      }) || this;
      _this.anchor_ = options.anchor !== void 0 ? options.anchor : [0.5, 0.5];
      _this.normalizedAnchor_ = null;
      _this.anchorOrigin_ = options.anchorOrigin !== void 0 ? options.anchorOrigin : IconOrigin_default.TOP_LEFT;
      _this.anchorXUnits_ = options.anchorXUnits !== void 0 ? options.anchorXUnits : IconAnchorUnits_default.FRACTION;
      _this.anchorYUnits_ = options.anchorYUnits !== void 0 ? options.anchorYUnits : IconAnchorUnits_default.FRACTION;
      _this.crossOrigin_ = options.crossOrigin !== void 0 ? options.crossOrigin : null;
      var image = options.img !== void 0 ? options.img : null;
      _this.imgSize_ = options.imgSize;
      var src = options.src;
      assert(!(src !== void 0 && image), 4);
      assert(!image || image && _this.imgSize_, 5);
      if ((src === void 0 || src.length === 0) && image) {
        src = image.src || getUid(image);
      }
      assert(src !== void 0 && src.length > 0, 6);
      var imageState = options.src !== void 0 ? ImageState_default.IDLE : ImageState_default.LOADED;
      _this.color_ = options.color !== void 0 ? asArray(options.color) : null;
      _this.iconImage_ = get4(image, src, _this.imgSize_ !== void 0 ? _this.imgSize_ : null, _this.crossOrigin_, imageState, _this.color_);
      _this.offset_ = options.offset !== void 0 ? options.offset : [0, 0];
      _this.offsetOrigin_ = options.offsetOrigin !== void 0 ? options.offsetOrigin : IconOrigin_default.TOP_LEFT;
      _this.origin_ = null;
      _this.size_ = options.size !== void 0 ? options.size : null;
      return _this;
    }
    Icon2.prototype.clone = function() {
      var scale2 = this.getScale();
      return new Icon2({
        anchor: this.anchor_.slice(),
        anchorOrigin: this.anchorOrigin_,
        anchorXUnits: this.anchorXUnits_,
        anchorYUnits: this.anchorYUnits_,
        color: this.color_ && this.color_.slice ? this.color_.slice() : this.color_ || void 0,
        crossOrigin: this.crossOrigin_,
        imgSize: this.imgSize_,
        offset: this.offset_.slice(),
        offsetOrigin: this.offsetOrigin_,
        opacity: this.getOpacity(),
        rotateWithView: this.getRotateWithView(),
        rotation: this.getRotation(),
        scale: Array.isArray(scale2) ? scale2.slice() : scale2,
        size: this.size_ !== null ? this.size_.slice() : void 0,
        src: this.getSrc()
      });
    };
    Icon2.prototype.getAnchor = function() {
      var anchor = this.normalizedAnchor_;
      if (!anchor) {
        anchor = this.anchor_;
        var size = this.getSize();
        if (this.anchorXUnits_ == IconAnchorUnits_default.FRACTION || this.anchorYUnits_ == IconAnchorUnits_default.FRACTION) {
          if (!size) {
            return null;
          }
          anchor = this.anchor_.slice();
          if (this.anchorXUnits_ == IconAnchorUnits_default.FRACTION) {
            anchor[0] *= size[0];
          }
          if (this.anchorYUnits_ == IconAnchorUnits_default.FRACTION) {
            anchor[1] *= size[1];
          }
        }
        if (this.anchorOrigin_ != IconOrigin_default.TOP_LEFT) {
          if (!size) {
            return null;
          }
          if (anchor === this.anchor_) {
            anchor = this.anchor_.slice();
          }
          if (this.anchorOrigin_ == IconOrigin_default.TOP_RIGHT || this.anchorOrigin_ == IconOrigin_default.BOTTOM_RIGHT) {
            anchor[0] = -anchor[0] + size[0];
          }
          if (this.anchorOrigin_ == IconOrigin_default.BOTTOM_LEFT || this.anchorOrigin_ == IconOrigin_default.BOTTOM_RIGHT) {
            anchor[1] = -anchor[1] + size[1];
          }
        }
        this.normalizedAnchor_ = anchor;
      }
      var displacement = this.getDisplacement();
      return [anchor[0] - displacement[0], anchor[1] + displacement[1]];
    };
    Icon2.prototype.setAnchor = function(anchor) {
      this.anchor_ = anchor;
      this.normalizedAnchor_ = null;
    };
    Icon2.prototype.getColor = function() {
      return this.color_;
    };
    Icon2.prototype.getImage = function(pixelRatio) {
      return this.iconImage_.getImage(pixelRatio);
    };
    Icon2.prototype.getPixelRatio = function(pixelRatio) {
      return this.iconImage_.getPixelRatio(pixelRatio);
    };
    Icon2.prototype.getImageSize = function() {
      return this.iconImage_.getSize();
    };
    Icon2.prototype.getImageState = function() {
      return this.iconImage_.getImageState();
    };
    Icon2.prototype.getHitDetectionImage = function() {
      return this.iconImage_.getHitDetectionImage();
    };
    Icon2.prototype.getOrigin = function() {
      if (this.origin_) {
        return this.origin_;
      }
      var offset = this.offset_;
      if (this.offsetOrigin_ != IconOrigin_default.TOP_LEFT) {
        var size = this.getSize();
        var iconImageSize = this.iconImage_.getSize();
        if (!size || !iconImageSize) {
          return null;
        }
        offset = offset.slice();
        if (this.offsetOrigin_ == IconOrigin_default.TOP_RIGHT || this.offsetOrigin_ == IconOrigin_default.BOTTOM_RIGHT) {
          offset[0] = iconImageSize[0] - size[0] - offset[0];
        }
        if (this.offsetOrigin_ == IconOrigin_default.BOTTOM_LEFT || this.offsetOrigin_ == IconOrigin_default.BOTTOM_RIGHT) {
          offset[1] = iconImageSize[1] - size[1] - offset[1];
        }
      }
      this.origin_ = offset;
      return this.origin_;
    };
    Icon2.prototype.getSrc = function() {
      return this.iconImage_.getSrc();
    };
    Icon2.prototype.getSize = function() {
      return !this.size_ ? this.iconImage_.getSize() : this.size_;
    };
    Icon2.prototype.listenImageChange = function(listener) {
      this.iconImage_.addEventListener(EventType_default.CHANGE, listener);
    };
    Icon2.prototype.load = function() {
      this.iconImage_.load();
    };
    Icon2.prototype.unlistenImageChange = function(listener) {
      this.iconImage_.removeEventListener(EventType_default.CHANGE, listener);
    };
    return Icon2;
  }(Image_default);
  var Icon_default = Icon;

  // node_modules/ol/render/canvas/hitdetect.js
  var HIT_DETECT_RESOLUTION = 0.5;
  function createHitDetectionImageData(size, transforms2, features, styleFunction, extent, resolution, rotation) {
    var width = size[0] * HIT_DETECT_RESOLUTION;
    var height = size[1] * HIT_DETECT_RESOLUTION;
    var context = createCanvasContext2D(width, height);
    context.imageSmoothingEnabled = false;
    var canvas = context.canvas;
    var renderer = new Immediate_default(context, HIT_DETECT_RESOLUTION, extent, null, rotation);
    var featureCount = features.length;
    var indexFactor = Math.floor((256 * 256 * 256 - 1) / featureCount);
    var featuresByZIndex = {};
    for (var i = 1; i <= featureCount; ++i) {
      var feature = features[i - 1];
      var featureStyleFunction = feature.getStyleFunction() || styleFunction;
      if (!styleFunction) {
        continue;
      }
      var styles = featureStyleFunction(feature, resolution);
      if (!styles) {
        continue;
      }
      if (!Array.isArray(styles)) {
        styles = [styles];
      }
      var index = i * indexFactor;
      var color = "#" + ("000000" + index.toString(16)).slice(-6);
      for (var j = 0, jj = styles.length; j < jj; ++j) {
        var originalStyle = styles[j];
        var geometry = originalStyle.getGeometryFunction()(feature);
        if (!geometry || !intersects(extent, geometry.getExtent())) {
          continue;
        }
        var style = originalStyle.clone();
        var fill = style.getFill();
        if (fill) {
          fill.setColor(color);
        }
        var stroke = style.getStroke();
        if (stroke) {
          stroke.setColor(color);
          stroke.setLineDash(null);
        }
        style.setText(void 0);
        var image = originalStyle.getImage();
        if (image && image.getOpacity() !== 0) {
          var imgSize = image.getImageSize();
          if (!imgSize) {
            continue;
          }
          var imgContext = createCanvasContext2D(imgSize[0], imgSize[1], void 0, { alpha: false });
          var img = imgContext.canvas;
          imgContext.fillStyle = color;
          imgContext.fillRect(0, 0, img.width, img.height);
          style.setImage(new Icon_default({
            img,
            imgSize,
            anchor: image.getAnchor(),
            anchorXUnits: IconAnchorUnits_default.PIXELS,
            anchorYUnits: IconAnchorUnits_default.PIXELS,
            offset: image.getOrigin(),
            opacity: 1,
            size: image.getSize(),
            scale: image.getScale(),
            rotation: image.getRotation(),
            rotateWithView: image.getRotateWithView()
          }));
        }
        var zIndex = style.getZIndex() || 0;
        var byGeometryType = featuresByZIndex[zIndex];
        if (!byGeometryType) {
          byGeometryType = {};
          featuresByZIndex[zIndex] = byGeometryType;
          byGeometryType[GeometryType_default.POLYGON] = [];
          byGeometryType[GeometryType_default.CIRCLE] = [];
          byGeometryType[GeometryType_default.LINE_STRING] = [];
          byGeometryType[GeometryType_default.POINT] = [];
        }
        byGeometryType[geometry.getType().replace("Multi", "")].push(geometry, style);
      }
    }
    var zIndexKeys = Object.keys(featuresByZIndex).map(Number).sort(numberSafeCompareFunction);
    for (var i = 0, ii = zIndexKeys.length; i < ii; ++i) {
      var byGeometryType = featuresByZIndex[zIndexKeys[i]];
      for (var type in byGeometryType) {
        var geomAndStyle = byGeometryType[type];
        for (var j = 0, jj = geomAndStyle.length; j < jj; j += 2) {
          renderer.setStyle(geomAndStyle[j + 1]);
          for (var k = 0, kk = transforms2.length; k < kk; ++k) {
            renderer.setTransform(transforms2[k]);
            renderer.drawGeometry(geomAndStyle[j]);
          }
        }
      }
    }
    return context.getImageData(0, 0, canvas.width, canvas.height);
  }
  function hitDetect(pixel, features, imageData) {
    var resultFeatures = [];
    if (imageData) {
      var x = Math.floor(Math.round(pixel[0]) * HIT_DETECT_RESOLUTION);
      var y = Math.floor(Math.round(pixel[1]) * HIT_DETECT_RESOLUTION);
      var index = (clamp(x, 0, imageData.width - 1) + clamp(y, 0, imageData.height - 1) * imageData.width) * 4;
      var r = imageData.data[index];
      var g = imageData.data[index + 1];
      var b = imageData.data[index + 2];
      var i = b + 256 * (g + 256 * r);
      var indexFactor = Math.floor((256 * 256 * 256 - 1) / features.length);
      if (i && i % indexFactor === 0) {
        resultFeatures.push(features[i / indexFactor - 1]);
      }
    }
    return resultFeatures;
  }

  // node_modules/ol/renderer/vector.js
  var SIMPLIFY_TOLERANCE = 0.5;
  var GEOMETRY_RENDERERS = {
    "Point": renderPointGeometry,
    "LineString": renderLineStringGeometry,
    "Polygon": renderPolygonGeometry,
    "MultiPoint": renderMultiPointGeometry,
    "MultiLineString": renderMultiLineStringGeometry,
    "MultiPolygon": renderMultiPolygonGeometry,
    "GeometryCollection": renderGeometryCollectionGeometry,
    "Circle": renderCircleGeometry
  };
  function defaultOrder(feature1, feature2) {
    return parseInt(getUid(feature1), 10) - parseInt(getUid(feature2), 10);
  }
  function getSquaredTolerance(resolution, pixelRatio) {
    var tolerance = getTolerance(resolution, pixelRatio);
    return tolerance * tolerance;
  }
  function getTolerance(resolution, pixelRatio) {
    return SIMPLIFY_TOLERANCE * resolution / pixelRatio;
  }
  function renderCircleGeometry(builderGroup, geometry, style, feature, opt_declutterBuilderGroup) {
    var fillStyle = style.getFill();
    var strokeStyle = style.getStroke();
    if (fillStyle || strokeStyle) {
      var circleReplay = builderGroup.getBuilder(style.getZIndex(), BuilderType_default.CIRCLE);
      circleReplay.setFillStrokeStyle(fillStyle, strokeStyle);
      circleReplay.drawCircle(geometry, feature);
    }
    var textStyle = style.getText();
    if (textStyle && textStyle.getText()) {
      var textReplay = (opt_declutterBuilderGroup || builderGroup).getBuilder(style.getZIndex(), BuilderType_default.TEXT);
      textReplay.setTextStyle(textStyle);
      textReplay.drawText(geometry, feature);
    }
  }
  function renderFeature(replayGroup, feature, style, squaredTolerance, listener, opt_transform, opt_declutterBuilderGroup) {
    var loading = false;
    var imageStyle = style.getImage();
    if (imageStyle) {
      var imageState = imageStyle.getImageState();
      if (imageState == ImageState_default.LOADED || imageState == ImageState_default.ERROR) {
        imageStyle.unlistenImageChange(listener);
      } else {
        if (imageState == ImageState_default.IDLE) {
          imageStyle.load();
        }
        imageState = imageStyle.getImageState();
        imageStyle.listenImageChange(listener);
        loading = true;
      }
    }
    renderFeatureInternal(replayGroup, feature, style, squaredTolerance, opt_transform, opt_declutterBuilderGroup);
    return loading;
  }
  function renderFeatureInternal(replayGroup, feature, style, squaredTolerance, opt_transform, opt_declutterBuilderGroup) {
    var geometry = style.getGeometryFunction()(feature);
    if (!geometry) {
      return;
    }
    var simplifiedGeometry = geometry.simplifyTransformed(squaredTolerance, opt_transform);
    var renderer = style.getRenderer();
    if (renderer) {
      renderGeometry(replayGroup, simplifiedGeometry, style, feature);
    } else {
      var geometryRenderer = GEOMETRY_RENDERERS[simplifiedGeometry.getType()];
      geometryRenderer(replayGroup, simplifiedGeometry, style, feature, opt_declutterBuilderGroup);
    }
  }
  function renderGeometry(replayGroup, geometry, style, feature) {
    if (geometry.getType() == GeometryType_default.GEOMETRY_COLLECTION) {
      var geometries = geometry.getGeometries();
      for (var i = 0, ii = geometries.length; i < ii; ++i) {
        renderGeometry(replayGroup, geometries[i], style, feature);
      }
      return;
    }
    var replay = replayGroup.getBuilder(style.getZIndex(), BuilderType_default.DEFAULT);
    replay.drawCustom(geometry, feature, style.getRenderer(), style.getHitDetectionRenderer());
  }
  function renderGeometryCollectionGeometry(replayGroup, geometry, style, feature, opt_declutterBuilderGroup) {
    var geometries = geometry.getGeometriesArray();
    var i, ii;
    for (i = 0, ii = geometries.length; i < ii; ++i) {
      var geometryRenderer = GEOMETRY_RENDERERS[geometries[i].getType()];
      geometryRenderer(replayGroup, geometries[i], style, feature, opt_declutterBuilderGroup);
    }
  }
  function renderLineStringGeometry(builderGroup, geometry, style, feature, opt_declutterBuilderGroup) {
    var strokeStyle = style.getStroke();
    if (strokeStyle) {
      var lineStringReplay = builderGroup.getBuilder(style.getZIndex(), BuilderType_default.LINE_STRING);
      lineStringReplay.setFillStrokeStyle(null, strokeStyle);
      lineStringReplay.drawLineString(geometry, feature);
    }
    var textStyle = style.getText();
    if (textStyle && textStyle.getText()) {
      var textReplay = (opt_declutterBuilderGroup || builderGroup).getBuilder(style.getZIndex(), BuilderType_default.TEXT);
      textReplay.setTextStyle(textStyle);
      textReplay.drawText(geometry, feature);
    }
  }
  function renderMultiLineStringGeometry(builderGroup, geometry, style, feature, opt_declutterBuilderGroup) {
    var strokeStyle = style.getStroke();
    if (strokeStyle) {
      var lineStringReplay = builderGroup.getBuilder(style.getZIndex(), BuilderType_default.LINE_STRING);
      lineStringReplay.setFillStrokeStyle(null, strokeStyle);
      lineStringReplay.drawMultiLineString(geometry, feature);
    }
    var textStyle = style.getText();
    if (textStyle && textStyle.getText()) {
      var textReplay = (opt_declutterBuilderGroup || builderGroup).getBuilder(style.getZIndex(), BuilderType_default.TEXT);
      textReplay.setTextStyle(textStyle);
      textReplay.drawText(geometry, feature);
    }
  }
  function renderMultiPolygonGeometry(builderGroup, geometry, style, feature, opt_declutterBuilderGroup) {
    var fillStyle = style.getFill();
    var strokeStyle = style.getStroke();
    if (strokeStyle || fillStyle) {
      var polygonReplay = builderGroup.getBuilder(style.getZIndex(), BuilderType_default.POLYGON);
      polygonReplay.setFillStrokeStyle(fillStyle, strokeStyle);
      polygonReplay.drawMultiPolygon(geometry, feature);
    }
    var textStyle = style.getText();
    if (textStyle && textStyle.getText()) {
      var textReplay = (opt_declutterBuilderGroup || builderGroup).getBuilder(style.getZIndex(), BuilderType_default.TEXT);
      textReplay.setTextStyle(textStyle);
      textReplay.drawText(geometry, feature);
    }
  }
  function renderPointGeometry(builderGroup, geometry, style, feature, opt_declutterBuilderGroup) {
    var imageStyle = style.getImage();
    var textStyle = style.getText();
    var declutterImageWithText;
    if (opt_declutterBuilderGroup) {
      builderGroup = opt_declutterBuilderGroup;
      declutterImageWithText = imageStyle && textStyle && textStyle.getText() ? {} : void 0;
    }
    if (imageStyle) {
      if (imageStyle.getImageState() != ImageState_default.LOADED) {
        return;
      }
      var imageReplay = builderGroup.getBuilder(style.getZIndex(), BuilderType_default.IMAGE);
      imageReplay.setImageStyle(imageStyle, declutterImageWithText);
      imageReplay.drawPoint(geometry, feature);
    }
    if (textStyle && textStyle.getText()) {
      var textReplay = builderGroup.getBuilder(style.getZIndex(), BuilderType_default.TEXT);
      textReplay.setTextStyle(textStyle, declutterImageWithText);
      textReplay.drawText(geometry, feature);
    }
  }
  function renderMultiPointGeometry(builderGroup, geometry, style, feature, opt_declutterBuilderGroup) {
    var imageStyle = style.getImage();
    var textStyle = style.getText();
    var declutterImageWithText;
    if (opt_declutterBuilderGroup) {
      builderGroup = opt_declutterBuilderGroup;
      declutterImageWithText = imageStyle && textStyle && textStyle.getText() ? {} : void 0;
    }
    if (imageStyle) {
      if (imageStyle.getImageState() != ImageState_default.LOADED) {
        return;
      }
      var imageReplay = builderGroup.getBuilder(style.getZIndex(), BuilderType_default.IMAGE);
      imageReplay.setImageStyle(imageStyle, declutterImageWithText);
      imageReplay.drawMultiPoint(geometry, feature);
    }
    if (textStyle && textStyle.getText()) {
      var textReplay = (opt_declutterBuilderGroup || builderGroup).getBuilder(style.getZIndex(), BuilderType_default.TEXT);
      textReplay.setTextStyle(textStyle, declutterImageWithText);
      textReplay.drawText(geometry, feature);
    }
  }
  function renderPolygonGeometry(builderGroup, geometry, style, feature, opt_declutterBuilderGroup) {
    var fillStyle = style.getFill();
    var strokeStyle = style.getStroke();
    if (fillStyle || strokeStyle) {
      var polygonReplay = builderGroup.getBuilder(style.getZIndex(), BuilderType_default.POLYGON);
      polygonReplay.setFillStrokeStyle(fillStyle, strokeStyle);
      polygonReplay.drawPolygon(geometry, feature);
    }
    var textStyle = style.getText();
    if (textStyle && textStyle.getText()) {
      var textReplay = (opt_declutterBuilderGroup || builderGroup).getBuilder(style.getZIndex(), BuilderType_default.TEXT);
      textReplay.setTextStyle(textStyle);
      textReplay.drawText(geometry, feature);
    }
  }

  // node_modules/ol/renderer/canvas/VectorLayer.js
  var __extends44 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var CanvasVectorLayerRenderer = function(_super) {
    __extends44(CanvasVectorLayerRenderer2, _super);
    function CanvasVectorLayerRenderer2(vectorLayer) {
      var _this = _super.call(this, vectorLayer) || this;
      _this.boundHandleStyleImageChange_ = _this.handleStyleImageChange_.bind(_this);
      _this.animatingOrInteracting_;
      _this.dirty_ = false;
      _this.hitDetectionImageData_ = null;
      _this.renderedFeatures_ = null;
      _this.renderedRevision_ = -1;
      _this.renderedResolution_ = NaN;
      _this.renderedExtent_ = createEmpty();
      _this.wrappedRenderedExtent_ = createEmpty();
      _this.renderedRotation_;
      _this.renderedCenter_ = null;
      _this.renderedProjection_ = null;
      _this.renderedRenderOrder_ = null;
      _this.replayGroup_ = null;
      _this.replayGroupChanged = true;
      _this.declutterExecutorGroup = null;
      _this.clipping = true;
      return _this;
    }
    CanvasVectorLayerRenderer2.prototype.renderWorlds = function(executorGroup, frameState, opt_declutterTree) {
      var extent = frameState.extent;
      var viewState = frameState.viewState;
      var center = viewState.center;
      var resolution = viewState.resolution;
      var projection = viewState.projection;
      var rotation = viewState.rotation;
      var projectionExtent = projection.getExtent();
      var vectorSource = this.getLayer().getSource();
      var pixelRatio = frameState.pixelRatio;
      var viewHints = frameState.viewHints;
      var snapToPixel = !(viewHints[ViewHint_default.ANIMATING] || viewHints[ViewHint_default.INTERACTING]);
      var context = this.context;
      var width = Math.round(frameState.size[0] * pixelRatio);
      var height = Math.round(frameState.size[1] * pixelRatio);
      var multiWorld = vectorSource.getWrapX() && projection.canWrapX();
      var worldWidth = multiWorld ? getWidth(projectionExtent) : null;
      var endWorld = multiWorld ? Math.ceil((extent[2] - projectionExtent[2]) / worldWidth) + 1 : 1;
      var world = multiWorld ? Math.floor((extent[0] - projectionExtent[0]) / worldWidth) : 0;
      do {
        var transform2 = this.getRenderTransform(center, resolution, rotation, pixelRatio, width, height, world * worldWidth);
        executorGroup.execute(context, 1, transform2, rotation, snapToPixel, void 0, opt_declutterTree);
      } while (++world < endWorld);
    };
    CanvasVectorLayerRenderer2.prototype.renderDeclutter = function(frameState) {
      if (this.declutterExecutorGroup) {
        this.renderWorlds(this.declutterExecutorGroup, frameState, frameState.declutterTree);
      }
    };
    CanvasVectorLayerRenderer2.prototype.renderFrame = function(frameState, target) {
      var pixelRatio = frameState.pixelRatio;
      var layerState = frameState.layerStatesArray[frameState.layerIndex];
      makeScale(this.pixelTransform, 1 / pixelRatio, 1 / pixelRatio);
      makeInverse(this.inversePixelTransform, this.pixelTransform);
      var canvasTransform = toString(this.pixelTransform);
      this.useContainer(target, canvasTransform, layerState.opacity, this.getBackground(frameState));
      var context = this.context;
      var canvas = context.canvas;
      var replayGroup = this.replayGroup_;
      var declutterExecutorGroup = this.declutterExecutorGroup;
      if ((!replayGroup || replayGroup.isEmpty()) && (!declutterExecutorGroup || declutterExecutorGroup.isEmpty())) {
        return null;
      }
      var width = Math.round(frameState.size[0] * pixelRatio);
      var height = Math.round(frameState.size[1] * pixelRatio);
      if (canvas.width != width || canvas.height != height) {
        canvas.width = width;
        canvas.height = height;
        if (canvas.style.transform !== canvasTransform) {
          canvas.style.transform = canvasTransform;
        }
      } else if (!this.containerReused) {
        context.clearRect(0, 0, width, height);
      }
      this.preRender(context, frameState);
      var viewState = frameState.viewState;
      var projection = viewState.projection;
      var clipped = false;
      var render = true;
      if (layerState.extent && this.clipping) {
        var layerExtent = fromUserExtent(layerState.extent, projection);
        render = intersects(layerExtent, frameState.extent);
        clipped = render && !containsExtent(layerExtent, frameState.extent);
        if (clipped) {
          this.clipUnrotated(context, frameState, layerExtent);
        }
      }
      if (render) {
        this.renderWorlds(replayGroup, frameState);
      }
      if (clipped) {
        context.restore();
      }
      this.postRender(context, frameState);
      var opacity = cssOpacity(layerState.opacity);
      var container = this.container;
      if (opacity !== container.style.opacity) {
        container.style.opacity = opacity;
      }
      if (this.renderedRotation_ !== viewState.rotation) {
        this.renderedRotation_ = viewState.rotation;
        this.hitDetectionImageData_ = null;
      }
      return this.container;
    };
    CanvasVectorLayerRenderer2.prototype.getFeatures = function(pixel) {
      return new Promise(function(resolve) {
        if (!this.hitDetectionImageData_ && !this.animatingOrInteracting_) {
          var size = [this.context.canvas.width, this.context.canvas.height];
          apply(this.pixelTransform, size);
          var center = this.renderedCenter_;
          var resolution = this.renderedResolution_;
          var rotation = this.renderedRotation_;
          var projection = this.renderedProjection_;
          var extent = this.wrappedRenderedExtent_;
          var layer = this.getLayer();
          var transforms2 = [];
          var width = size[0] * HIT_DETECT_RESOLUTION;
          var height = size[1] * HIT_DETECT_RESOLUTION;
          transforms2.push(this.getRenderTransform(center, resolution, rotation, HIT_DETECT_RESOLUTION, width, height, 0).slice());
          var source = layer.getSource();
          var projectionExtent = projection.getExtent();
          if (source.getWrapX() && projection.canWrapX() && !containsExtent(projectionExtent, extent)) {
            var startX = extent[0];
            var worldWidth = getWidth(projectionExtent);
            var world = 0;
            var offsetX = void 0;
            while (startX < projectionExtent[0]) {
              --world;
              offsetX = worldWidth * world;
              transforms2.push(this.getRenderTransform(center, resolution, rotation, HIT_DETECT_RESOLUTION, width, height, offsetX).slice());
              startX += worldWidth;
            }
            world = 0;
            startX = extent[2];
            while (startX > projectionExtent[2]) {
              ++world;
              offsetX = worldWidth * world;
              transforms2.push(this.getRenderTransform(center, resolution, rotation, HIT_DETECT_RESOLUTION, width, height, offsetX).slice());
              startX -= worldWidth;
            }
          }
          this.hitDetectionImageData_ = createHitDetectionImageData(size, transforms2, this.renderedFeatures_, layer.getStyleFunction(), extent, resolution, rotation);
        }
        resolve(hitDetect(pixel, this.renderedFeatures_, this.hitDetectionImageData_));
      }.bind(this));
    };
    CanvasVectorLayerRenderer2.prototype.forEachFeatureAtCoordinate = function(coordinate, frameState, hitTolerance, callback, matches) {
      var _this = this;
      if (!this.replayGroup_) {
        return void 0;
      }
      var resolution = frameState.viewState.resolution;
      var rotation = frameState.viewState.rotation;
      var layer = this.getLayer();
      var features = {};
      var featureCallback = function(feature, geometry, distanceSq) {
        var key = getUid(feature);
        var match = features[key];
        if (!match) {
          if (distanceSq === 0) {
            features[key] = true;
            return callback(feature, layer, geometry);
          }
          matches.push(features[key] = {
            feature,
            layer,
            geometry,
            distanceSq,
            callback
          });
        } else if (match !== true && distanceSq < match.distanceSq) {
          if (distanceSq === 0) {
            features[key] = true;
            matches.splice(matches.lastIndexOf(match), 1);
            return callback(feature, layer, geometry);
          }
          match.geometry = geometry;
          match.distanceSq = distanceSq;
        }
        return void 0;
      };
      var result;
      var executorGroups = [this.replayGroup_];
      if (this.declutterExecutorGroup) {
        executorGroups.push(this.declutterExecutorGroup);
      }
      executorGroups.some(function(executorGroup) {
        return result = executorGroup.forEachFeatureAtCoordinate(coordinate, resolution, rotation, hitTolerance, featureCallback, executorGroup === _this.declutterExecutorGroup && frameState.declutterTree ? frameState.declutterTree.all().map(function(item) {
          return item.value;
        }) : null);
      });
      return result;
    };
    CanvasVectorLayerRenderer2.prototype.handleFontsChanged = function() {
      var layer = this.getLayer();
      if (layer.getVisible() && this.replayGroup_) {
        layer.changed();
      }
    };
    CanvasVectorLayerRenderer2.prototype.handleStyleImageChange_ = function(event) {
      this.renderIfReadyAndVisible();
    };
    CanvasVectorLayerRenderer2.prototype.prepareFrame = function(frameState) {
      var vectorLayer = this.getLayer();
      var vectorSource = vectorLayer.getSource();
      if (!vectorSource) {
        return false;
      }
      var animating = frameState.viewHints[ViewHint_default.ANIMATING];
      var interacting = frameState.viewHints[ViewHint_default.INTERACTING];
      var updateWhileAnimating = vectorLayer.getUpdateWhileAnimating();
      var updateWhileInteracting = vectorLayer.getUpdateWhileInteracting();
      if (!this.dirty_ && !updateWhileAnimating && animating || !updateWhileInteracting && interacting) {
        this.animatingOrInteracting_ = true;
        return true;
      }
      this.animatingOrInteracting_ = false;
      var frameStateExtent = frameState.extent;
      var viewState = frameState.viewState;
      var projection = viewState.projection;
      var resolution = viewState.resolution;
      var pixelRatio = frameState.pixelRatio;
      var vectorLayerRevision = vectorLayer.getRevision();
      var vectorLayerRenderBuffer = vectorLayer.getRenderBuffer();
      var vectorLayerRenderOrder = vectorLayer.getRenderOrder();
      if (vectorLayerRenderOrder === void 0) {
        vectorLayerRenderOrder = defaultOrder;
      }
      var center = viewState.center.slice();
      var extent = buffer(frameStateExtent, vectorLayerRenderBuffer * resolution);
      var renderedExtent = extent.slice();
      var loadExtents = [extent.slice()];
      var projectionExtent = projection.getExtent();
      if (vectorSource.getWrapX() && projection.canWrapX() && !containsExtent(projectionExtent, frameState.extent)) {
        var worldWidth = getWidth(projectionExtent);
        var gutter = Math.max(getWidth(extent) / 2, worldWidth);
        extent[0] = projectionExtent[0] - gutter;
        extent[2] = projectionExtent[2] + gutter;
        wrapX2(center, projection);
        var loadExtent = wrapX(loadExtents[0], projection);
        if (loadExtent[0] < projectionExtent[0] && loadExtent[2] < projectionExtent[2]) {
          loadExtents.push([
            loadExtent[0] + worldWidth,
            loadExtent[1],
            loadExtent[2] + worldWidth,
            loadExtent[3]
          ]);
        } else if (loadExtent[0] > projectionExtent[0] && loadExtent[2] > projectionExtent[2]) {
          loadExtents.push([
            loadExtent[0] - worldWidth,
            loadExtent[1],
            loadExtent[2] - worldWidth,
            loadExtent[3]
          ]);
        }
      }
      if (!this.dirty_ && this.renderedResolution_ == resolution && this.renderedRevision_ == vectorLayerRevision && this.renderedRenderOrder_ == vectorLayerRenderOrder && containsExtent(this.wrappedRenderedExtent_, extent)) {
        if (!equals(this.renderedExtent_, renderedExtent)) {
          this.hitDetectionImageData_ = null;
          this.renderedExtent_ = renderedExtent;
        }
        this.renderedCenter_ = center;
        this.replayGroupChanged = false;
        return true;
      }
      this.replayGroup_ = null;
      this.dirty_ = false;
      var replayGroup = new BuilderGroup_default(getTolerance(resolution, pixelRatio), extent, resolution, pixelRatio);
      var declutterBuilderGroup;
      if (this.getLayer().getDeclutter()) {
        declutterBuilderGroup = new BuilderGroup_default(getTolerance(resolution, pixelRatio), extent, resolution, pixelRatio);
      }
      var userProjection2 = getUserProjection();
      var userTransform;
      if (userProjection2) {
        for (var i = 0, ii = loadExtents.length; i < ii; ++i) {
          var extent_1 = loadExtents[i];
          var userExtent_1 = toUserExtent(extent_1, projection);
          vectorSource.loadFeatures(userExtent_1, toUserResolution(resolution, projection), userProjection2);
        }
        userTransform = getTransformFromProjections(userProjection2, projection);
      } else {
        for (var i = 0, ii = loadExtents.length; i < ii; ++i) {
          vectorSource.loadFeatures(loadExtents[i], resolution, projection);
        }
      }
      var squaredTolerance = getSquaredTolerance(resolution, pixelRatio);
      var render = function(feature) {
        var styles;
        var styleFunction = feature.getStyleFunction() || vectorLayer.getStyleFunction();
        if (styleFunction) {
          styles = styleFunction(feature, resolution);
        }
        if (styles) {
          var dirty = this.renderFeature(feature, squaredTolerance, styles, replayGroup, userTransform, declutterBuilderGroup);
          this.dirty_ = this.dirty_ || dirty;
        }
      }.bind(this);
      var userExtent = toUserExtent(extent, projection);
      var features = vectorSource.getFeaturesInExtent(userExtent);
      if (vectorLayerRenderOrder) {
        features.sort(vectorLayerRenderOrder);
      }
      for (var i = 0, ii = features.length; i < ii; ++i) {
        render(features[i]);
      }
      this.renderedFeatures_ = features;
      var replayGroupInstructions = replayGroup.finish();
      var executorGroup = new ExecutorGroup_default(extent, resolution, pixelRatio, vectorSource.getOverlaps(), replayGroupInstructions, vectorLayer.getRenderBuffer());
      if (declutterBuilderGroup) {
        this.declutterExecutorGroup = new ExecutorGroup_default(extent, resolution, pixelRatio, vectorSource.getOverlaps(), declutterBuilderGroup.finish(), vectorLayer.getRenderBuffer());
      }
      this.renderedResolution_ = resolution;
      this.renderedRevision_ = vectorLayerRevision;
      this.renderedRenderOrder_ = vectorLayerRenderOrder;
      this.renderedExtent_ = renderedExtent;
      this.wrappedRenderedExtent_ = extent;
      this.renderedCenter_ = center;
      this.renderedProjection_ = projection;
      this.replayGroup_ = executorGroup;
      this.hitDetectionImageData_ = null;
      this.replayGroupChanged = true;
      return true;
    };
    CanvasVectorLayerRenderer2.prototype.renderFeature = function(feature, squaredTolerance, styles, builderGroup, opt_transform, opt_declutterBuilderGroup) {
      if (!styles) {
        return false;
      }
      var loading = false;
      if (Array.isArray(styles)) {
        for (var i = 0, ii = styles.length; i < ii; ++i) {
          loading = renderFeature(builderGroup, feature, styles[i], squaredTolerance, this.boundHandleStyleImageChange_, opt_transform, opt_declutterBuilderGroup) || loading;
        }
      } else {
        loading = renderFeature(builderGroup, feature, styles, squaredTolerance, this.boundHandleStyleImageChange_, opt_transform, opt_declutterBuilderGroup);
      }
      return loading;
    };
    return CanvasVectorLayerRenderer2;
  }(Layer_default3);
  var VectorLayer_default = CanvasVectorLayerRenderer;

  // node_modules/ol/layer/Vector.js
  var __extends45 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var VectorLayer = function(_super) {
    __extends45(VectorLayer2, _super);
    function VectorLayer2(opt_options) {
      return _super.call(this, opt_options) || this;
    }
    VectorLayer2.prototype.createRenderer = function() {
      return new VectorLayer_default(this);
    };
    return VectorLayer2;
  }(BaseVector_default);
  var Vector_default = VectorLayer;

  // node_modules/ol/structs/RBush.js
  var import_rbush2 = __toESM(require_rbush_min(), 1);
  var RBush2 = function() {
    function RBush3(opt_maxEntries) {
      this.rbush_ = new import_rbush2.default(opt_maxEntries);
      this.items_ = {};
    }
    RBush3.prototype.insert = function(extent, value) {
      var item = {
        minX: extent[0],
        minY: extent[1],
        maxX: extent[2],
        maxY: extent[3],
        value
      };
      this.rbush_.insert(item);
      this.items_[getUid(value)] = item;
    };
    RBush3.prototype.load = function(extents, values) {
      var items = new Array(values.length);
      for (var i = 0, l = values.length; i < l; i++) {
        var extent = extents[i];
        var value = values[i];
        var item = {
          minX: extent[0],
          minY: extent[1],
          maxX: extent[2],
          maxY: extent[3],
          value
        };
        items[i] = item;
        this.items_[getUid(value)] = item;
      }
      this.rbush_.load(items);
    };
    RBush3.prototype.remove = function(value) {
      var uid = getUid(value);
      var item = this.items_[uid];
      delete this.items_[uid];
      return this.rbush_.remove(item) !== null;
    };
    RBush3.prototype.update = function(extent, value) {
      var item = this.items_[getUid(value)];
      var bbox = [item.minX, item.minY, item.maxX, item.maxY];
      if (!equals2(bbox, extent)) {
        this.remove(value);
        this.insert(extent, value);
      }
    };
    RBush3.prototype.getAll = function() {
      var items = this.rbush_.all();
      return items.map(function(item) {
        return item.value;
      });
    };
    RBush3.prototype.getInExtent = function(extent) {
      var bbox = {
        minX: extent[0],
        minY: extent[1],
        maxX: extent[2],
        maxY: extent[3]
      };
      var items = this.rbush_.search(bbox);
      return items.map(function(item) {
        return item.value;
      });
    };
    RBush3.prototype.forEach = function(callback) {
      return this.forEach_(this.getAll(), callback);
    };
    RBush3.prototype.forEachInExtent = function(extent, callback) {
      return this.forEach_(this.getInExtent(extent), callback);
    };
    RBush3.prototype.forEach_ = function(values, callback) {
      var result;
      for (var i = 0, l = values.length; i < l; i++) {
        result = callback(values[i]);
        if (result) {
          return result;
        }
      }
      return result;
    };
    RBush3.prototype.isEmpty = function() {
      return isEmpty(this.items_);
    };
    RBush3.prototype.clear = function() {
      this.rbush_.clear();
      this.items_ = {};
    };
    RBush3.prototype.getExtent = function(opt_extent) {
      var data = this.rbush_.toJSON();
      return createOrUpdate(data.minX, data.minY, data.maxX, data.maxY, opt_extent);
    };
    RBush3.prototype.concat = function(rbush) {
      this.rbush_.load(rbush.rbush_.all());
      for (var i in rbush.items_) {
        this.items_[i] = rbush.items_[i];
      }
    };
    return RBush3;
  }();
  var RBush_default = RBush2;

  // node_modules/ol/source/Source.js
  var __extends46 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Source = function(_super) {
    __extends46(Source2, _super);
    function Source2(options) {
      var _this = _super.call(this) || this;
      _this.projection = get3(options.projection);
      _this.attributions_ = adaptAttributions(options.attributions);
      _this.attributionsCollapsible_ = options.attributionsCollapsible !== void 0 ? options.attributionsCollapsible : true;
      _this.loading = false;
      _this.state_ = options.state !== void 0 ? options.state : State_default.READY;
      _this.wrapX_ = options.wrapX !== void 0 ? options.wrapX : false;
      _this.interpolate_ = !!options.interpolate;
      _this.viewResolver = null;
      _this.viewRejector = null;
      var self2 = _this;
      _this.viewPromise_ = new Promise(function(resolve, reject) {
        self2.viewResolver = resolve;
        self2.viewRejector = reject;
      });
      return _this;
    }
    Source2.prototype.getAttributions = function() {
      return this.attributions_;
    };
    Source2.prototype.getAttributionsCollapsible = function() {
      return this.attributionsCollapsible_;
    };
    Source2.prototype.getProjection = function() {
      return this.projection;
    };
    Source2.prototype.getResolutions = function() {
      return abstract();
    };
    Source2.prototype.getView = function() {
      return this.viewPromise_;
    };
    Source2.prototype.getState = function() {
      return this.state_;
    };
    Source2.prototype.getWrapX = function() {
      return this.wrapX_;
    };
    Source2.prototype.getInterpolate = function() {
      return this.interpolate_;
    };
    Source2.prototype.refresh = function() {
      this.changed();
    };
    Source2.prototype.setAttributions = function(attributions) {
      this.attributions_ = adaptAttributions(attributions);
      this.changed();
    };
    Source2.prototype.setState = function(state) {
      this.state_ = state;
      this.changed();
    };
    return Source2;
  }(Object_default);
  function adaptAttributions(attributionLike) {
    if (!attributionLike) {
      return null;
    }
    if (Array.isArray(attributionLike)) {
      return function(frameState) {
        return attributionLike;
      };
    }
    if (typeof attributionLike === "function") {
      return attributionLike;
    }
    return function(frameState) {
      return [attributionLike];
    };
  }
  var Source_default = Source;

  // node_modules/ol/source/VectorEventType.js
  var VectorEventType_default = {
    ADDFEATURE: "addfeature",
    CHANGEFEATURE: "changefeature",
    CLEAR: "clear",
    REMOVEFEATURE: "removefeature",
    FEATURESLOADSTART: "featuresloadstart",
    FEATURESLOADEND: "featuresloadend",
    FEATURESLOADERROR: "featuresloaderror"
  };

  // node_modules/ol/loadingstrategy.js
  function all(extent, resolution) {
    return [[-Infinity, -Infinity, Infinity, Infinity]];
  }

  // node_modules/ol/featureloader.js
  var withCredentials = false;
  function loadFeaturesXhr(url, format, extent, resolution, projection, success, failure) {
    var xhr2 = new XMLHttpRequest();
    xhr2.open("GET", typeof url === "function" ? url(extent, resolution, projection) : url, true);
    if (format.getType() == FormatType_default.ARRAY_BUFFER) {
      xhr2.responseType = "arraybuffer";
    }
    xhr2.withCredentials = withCredentials;
    xhr2.onload = function(event) {
      if (!xhr2.status || xhr2.status >= 200 && xhr2.status < 300) {
        var type = format.getType();
        var source = void 0;
        if (type == FormatType_default.JSON || type == FormatType_default.TEXT) {
          source = xhr2.responseText;
        } else if (type == FormatType_default.XML) {
          source = xhr2.responseXML;
          if (!source) {
            source = new DOMParser().parseFromString(xhr2.responseText, "application/xml");
          }
        } else if (type == FormatType_default.ARRAY_BUFFER) {
          source = xhr2.response;
        }
        if (source) {
          success(format.readFeatures(source, {
            extent,
            featureProjection: projection
          }), format.readProjection(source));
        } else {
          failure();
        }
      } else {
        failure();
      }
    };
    xhr2.onerror = failure;
    xhr2.send();
  }
  function xhr(url, format) {
    return function(extent, resolution, projection, success, failure) {
      var source = this;
      loadFeaturesXhr(url, format, extent, resolution, projection, function(features, dataProjection) {
        source.addFeatures(features);
        if (success !== void 0) {
          success(features);
        }
      }, failure ? failure : VOID);
    };
  }

  // node_modules/ol/source/Vector.js
  var __extends47 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var VectorSourceEvent = function(_super) {
    __extends47(VectorSourceEvent2, _super);
    function VectorSourceEvent2(type, opt_feature, opt_features) {
      var _this = _super.call(this, type) || this;
      _this.feature = opt_feature;
      _this.features = opt_features;
      return _this;
    }
    return VectorSourceEvent2;
  }(Event_default);
  var VectorSource = function(_super) {
    __extends47(VectorSource2, _super);
    function VectorSource2(opt_options) {
      var _this = this;
      var options = opt_options || {};
      _this = _super.call(this, {
        attributions: options.attributions,
        interpolate: true,
        projection: void 0,
        state: State_default.READY,
        wrapX: options.wrapX !== void 0 ? options.wrapX : true
      }) || this;
      _this.on;
      _this.once;
      _this.un;
      _this.loader_ = VOID;
      _this.format_ = options.format;
      _this.overlaps_ = options.overlaps === void 0 ? true : options.overlaps;
      _this.url_ = options.url;
      if (options.loader !== void 0) {
        _this.loader_ = options.loader;
      } else if (_this.url_ !== void 0) {
        assert(_this.format_, 7);
        _this.loader_ = xhr(_this.url_, _this.format_);
      }
      _this.strategy_ = options.strategy !== void 0 ? options.strategy : all;
      var useSpatialIndex = options.useSpatialIndex !== void 0 ? options.useSpatialIndex : true;
      _this.featuresRtree_ = useSpatialIndex ? new RBush_default() : null;
      _this.loadedExtentsRtree_ = new RBush_default();
      _this.loadingExtentsCount_ = 0;
      _this.nullGeometryFeatures_ = {};
      _this.idIndex_ = {};
      _this.uidIndex_ = {};
      _this.featureChangeKeys_ = {};
      _this.featuresCollection_ = null;
      var collection, features;
      if (Array.isArray(options.features)) {
        features = options.features;
      } else if (options.features) {
        collection = options.features;
        features = collection.getArray();
      }
      if (!useSpatialIndex && collection === void 0) {
        collection = new Collection_default(features);
      }
      if (features !== void 0) {
        _this.addFeaturesInternal(features);
      }
      if (collection !== void 0) {
        _this.bindFeaturesCollection_(collection);
      }
      return _this;
    }
    VectorSource2.prototype.addFeature = function(feature) {
      this.addFeatureInternal(feature);
      this.changed();
    };
    VectorSource2.prototype.addFeatureInternal = function(feature) {
      var featureKey = getUid(feature);
      if (!this.addToIndex_(featureKey, feature)) {
        if (this.featuresCollection_) {
          this.featuresCollection_.remove(feature);
        }
        return;
      }
      this.setupChangeEvents_(featureKey, feature);
      var geometry = feature.getGeometry();
      if (geometry) {
        var extent = geometry.getExtent();
        if (this.featuresRtree_) {
          this.featuresRtree_.insert(extent, feature);
        }
      } else {
        this.nullGeometryFeatures_[featureKey] = feature;
      }
      this.dispatchEvent(new VectorSourceEvent(VectorEventType_default.ADDFEATURE, feature));
    };
    VectorSource2.prototype.setupChangeEvents_ = function(featureKey, feature) {
      this.featureChangeKeys_[featureKey] = [
        listen(feature, EventType_default.CHANGE, this.handleFeatureChange_, this),
        listen(feature, ObjectEventType_default.PROPERTYCHANGE, this.handleFeatureChange_, this)
      ];
    };
    VectorSource2.prototype.addToIndex_ = function(featureKey, feature) {
      var valid = true;
      var id = feature.getId();
      if (id !== void 0) {
        if (!(id.toString() in this.idIndex_)) {
          this.idIndex_[id.toString()] = feature;
        } else {
          valid = false;
        }
      }
      if (valid) {
        assert(!(featureKey in this.uidIndex_), 30);
        this.uidIndex_[featureKey] = feature;
      }
      return valid;
    };
    VectorSource2.prototype.addFeatures = function(features) {
      this.addFeaturesInternal(features);
      this.changed();
    };
    VectorSource2.prototype.addFeaturesInternal = function(features) {
      var extents = [];
      var newFeatures = [];
      var geometryFeatures = [];
      for (var i = 0, length_1 = features.length; i < length_1; i++) {
        var feature = features[i];
        var featureKey = getUid(feature);
        if (this.addToIndex_(featureKey, feature)) {
          newFeatures.push(feature);
        }
      }
      for (var i = 0, length_2 = newFeatures.length; i < length_2; i++) {
        var feature = newFeatures[i];
        var featureKey = getUid(feature);
        this.setupChangeEvents_(featureKey, feature);
        var geometry = feature.getGeometry();
        if (geometry) {
          var extent = geometry.getExtent();
          extents.push(extent);
          geometryFeatures.push(feature);
        } else {
          this.nullGeometryFeatures_[featureKey] = feature;
        }
      }
      if (this.featuresRtree_) {
        this.featuresRtree_.load(extents, geometryFeatures);
      }
      if (this.hasListener(VectorEventType_default.ADDFEATURE)) {
        for (var i = 0, length_3 = newFeatures.length; i < length_3; i++) {
          this.dispatchEvent(new VectorSourceEvent(VectorEventType_default.ADDFEATURE, newFeatures[i]));
        }
      }
    };
    VectorSource2.prototype.bindFeaturesCollection_ = function(collection) {
      var modifyingCollection = false;
      this.addEventListener(VectorEventType_default.ADDFEATURE, function(evt) {
        if (!modifyingCollection) {
          modifyingCollection = true;
          collection.push(evt.feature);
          modifyingCollection = false;
        }
      });
      this.addEventListener(VectorEventType_default.REMOVEFEATURE, function(evt) {
        if (!modifyingCollection) {
          modifyingCollection = true;
          collection.remove(evt.feature);
          modifyingCollection = false;
        }
      });
      collection.addEventListener(CollectionEventType_default.ADD, function(evt) {
        if (!modifyingCollection) {
          modifyingCollection = true;
          this.addFeature(evt.element);
          modifyingCollection = false;
        }
      }.bind(this));
      collection.addEventListener(CollectionEventType_default.REMOVE, function(evt) {
        if (!modifyingCollection) {
          modifyingCollection = true;
          this.removeFeature(evt.element);
          modifyingCollection = false;
        }
      }.bind(this));
      this.featuresCollection_ = collection;
    };
    VectorSource2.prototype.clear = function(opt_fast) {
      if (opt_fast) {
        for (var featureId in this.featureChangeKeys_) {
          var keys = this.featureChangeKeys_[featureId];
          keys.forEach(unlistenByKey);
        }
        if (!this.featuresCollection_) {
          this.featureChangeKeys_ = {};
          this.idIndex_ = {};
          this.uidIndex_ = {};
        }
      } else {
        if (this.featuresRtree_) {
          var removeAndIgnoreReturn = function(feature) {
            this.removeFeatureInternal(feature);
          }.bind(this);
          this.featuresRtree_.forEach(removeAndIgnoreReturn);
          for (var id in this.nullGeometryFeatures_) {
            this.removeFeatureInternal(this.nullGeometryFeatures_[id]);
          }
        }
      }
      if (this.featuresCollection_) {
        this.featuresCollection_.clear();
      }
      if (this.featuresRtree_) {
        this.featuresRtree_.clear();
      }
      this.nullGeometryFeatures_ = {};
      var clearEvent = new VectorSourceEvent(VectorEventType_default.CLEAR);
      this.dispatchEvent(clearEvent);
      this.changed();
    };
    VectorSource2.prototype.forEachFeature = function(callback) {
      if (this.featuresRtree_) {
        return this.featuresRtree_.forEach(callback);
      } else if (this.featuresCollection_) {
        this.featuresCollection_.forEach(callback);
      }
    };
    VectorSource2.prototype.forEachFeatureAtCoordinateDirect = function(coordinate, callback) {
      var extent = [coordinate[0], coordinate[1], coordinate[0], coordinate[1]];
      return this.forEachFeatureInExtent(extent, function(feature) {
        var geometry = feature.getGeometry();
        if (geometry.intersectsCoordinate(coordinate)) {
          return callback(feature);
        } else {
          return void 0;
        }
      });
    };
    VectorSource2.prototype.forEachFeatureInExtent = function(extent, callback) {
      if (this.featuresRtree_) {
        return this.featuresRtree_.forEachInExtent(extent, callback);
      } else if (this.featuresCollection_) {
        this.featuresCollection_.forEach(callback);
      }
    };
    VectorSource2.prototype.forEachFeatureIntersectingExtent = function(extent, callback) {
      return this.forEachFeatureInExtent(extent, function(feature) {
        var geometry = feature.getGeometry();
        if (geometry.intersectsExtent(extent)) {
          var result = callback(feature);
          if (result) {
            return result;
          }
        }
      });
    };
    VectorSource2.prototype.getFeaturesCollection = function() {
      return this.featuresCollection_;
    };
    VectorSource2.prototype.getFeatures = function() {
      var features;
      if (this.featuresCollection_) {
        features = this.featuresCollection_.getArray().slice(0);
      } else if (this.featuresRtree_) {
        features = this.featuresRtree_.getAll();
        if (!isEmpty(this.nullGeometryFeatures_)) {
          extend(features, getValues(this.nullGeometryFeatures_));
        }
      }
      return features;
    };
    VectorSource2.prototype.getFeaturesAtCoordinate = function(coordinate) {
      var features = [];
      this.forEachFeatureAtCoordinateDirect(coordinate, function(feature) {
        features.push(feature);
      });
      return features;
    };
    VectorSource2.prototype.getFeaturesInExtent = function(extent) {
      if (this.featuresRtree_) {
        return this.featuresRtree_.getInExtent(extent);
      } else if (this.featuresCollection_) {
        return this.featuresCollection_.getArray().slice(0);
      } else {
        return [];
      }
    };
    VectorSource2.prototype.getClosestFeatureToCoordinate = function(coordinate, opt_filter) {
      var x = coordinate[0];
      var y = coordinate[1];
      var closestFeature = null;
      var closestPoint = [NaN, NaN];
      var minSquaredDistance = Infinity;
      var extent = [-Infinity, -Infinity, Infinity, Infinity];
      var filter = opt_filter ? opt_filter : TRUE;
      this.featuresRtree_.forEachInExtent(extent, function(feature) {
        if (filter(feature)) {
          var geometry = feature.getGeometry();
          var previousMinSquaredDistance = minSquaredDistance;
          minSquaredDistance = geometry.closestPointXY(x, y, closestPoint, minSquaredDistance);
          if (minSquaredDistance < previousMinSquaredDistance) {
            closestFeature = feature;
            var minDistance = Math.sqrt(minSquaredDistance);
            extent[0] = x - minDistance;
            extent[1] = y - minDistance;
            extent[2] = x + minDistance;
            extent[3] = y + minDistance;
          }
        }
      });
      return closestFeature;
    };
    VectorSource2.prototype.getExtent = function(opt_extent) {
      return this.featuresRtree_.getExtent(opt_extent);
    };
    VectorSource2.prototype.getFeatureById = function(id) {
      var feature = this.idIndex_[id.toString()];
      return feature !== void 0 ? feature : null;
    };
    VectorSource2.prototype.getFeatureByUid = function(uid) {
      var feature = this.uidIndex_[uid];
      return feature !== void 0 ? feature : null;
    };
    VectorSource2.prototype.getFormat = function() {
      return this.format_;
    };
    VectorSource2.prototype.getOverlaps = function() {
      return this.overlaps_;
    };
    VectorSource2.prototype.getUrl = function() {
      return this.url_;
    };
    VectorSource2.prototype.handleFeatureChange_ = function(event) {
      var feature = event.target;
      var featureKey = getUid(feature);
      var geometry = feature.getGeometry();
      if (!geometry) {
        if (!(featureKey in this.nullGeometryFeatures_)) {
          if (this.featuresRtree_) {
            this.featuresRtree_.remove(feature);
          }
          this.nullGeometryFeatures_[featureKey] = feature;
        }
      } else {
        var extent = geometry.getExtent();
        if (featureKey in this.nullGeometryFeatures_) {
          delete this.nullGeometryFeatures_[featureKey];
          if (this.featuresRtree_) {
            this.featuresRtree_.insert(extent, feature);
          }
        } else {
          if (this.featuresRtree_) {
            this.featuresRtree_.update(extent, feature);
          }
        }
      }
      var id = feature.getId();
      if (id !== void 0) {
        var sid = id.toString();
        if (this.idIndex_[sid] !== feature) {
          this.removeFromIdIndex_(feature);
          this.idIndex_[sid] = feature;
        }
      } else {
        this.removeFromIdIndex_(feature);
        this.uidIndex_[featureKey] = feature;
      }
      this.changed();
      this.dispatchEvent(new VectorSourceEvent(VectorEventType_default.CHANGEFEATURE, feature));
    };
    VectorSource2.prototype.hasFeature = function(feature) {
      var id = feature.getId();
      if (id !== void 0) {
        return id in this.idIndex_;
      } else {
        return getUid(feature) in this.uidIndex_;
      }
    };
    VectorSource2.prototype.isEmpty = function() {
      if (this.featuresRtree_) {
        return this.featuresRtree_.isEmpty() && isEmpty(this.nullGeometryFeatures_);
      }
      if (this.featuresCollection_) {
        return this.featuresCollection_.getLength() === 0;
      }
      return true;
    };
    VectorSource2.prototype.loadFeatures = function(extent, resolution, projection) {
      var loadedExtentsRtree = this.loadedExtentsRtree_;
      var extentsToLoad = this.strategy_(extent, resolution, projection);
      var _loop_1 = function(i2, ii2) {
        var extentToLoad = extentsToLoad[i2];
        var alreadyLoaded = loadedExtentsRtree.forEachInExtent(extentToLoad, function(object) {
          return containsExtent(object.extent, extentToLoad);
        });
        if (!alreadyLoaded) {
          ++this_1.loadingExtentsCount_;
          this_1.dispatchEvent(new VectorSourceEvent(VectorEventType_default.FEATURESLOADSTART));
          this_1.loader_.call(this_1, extentToLoad, resolution, projection, function(features) {
            --this.loadingExtentsCount_;
            this.dispatchEvent(new VectorSourceEvent(VectorEventType_default.FEATURESLOADEND, void 0, features));
          }.bind(this_1), function() {
            --this.loadingExtentsCount_;
            this.dispatchEvent(new VectorSourceEvent(VectorEventType_default.FEATURESLOADERROR));
          }.bind(this_1));
          loadedExtentsRtree.insert(extentToLoad, { extent: extentToLoad.slice() });
        }
      };
      var this_1 = this;
      for (var i = 0, ii = extentsToLoad.length; i < ii; ++i) {
        _loop_1(i, ii);
      }
      this.loading = this.loader_.length < 4 ? false : this.loadingExtentsCount_ > 0;
    };
    VectorSource2.prototype.refresh = function() {
      this.clear(true);
      this.loadedExtentsRtree_.clear();
      _super.prototype.refresh.call(this);
    };
    VectorSource2.prototype.removeLoadedExtent = function(extent) {
      var loadedExtentsRtree = this.loadedExtentsRtree_;
      var obj;
      loadedExtentsRtree.forEachInExtent(extent, function(object) {
        if (equals2(object.extent, extent)) {
          obj = object;
          return true;
        }
      });
      if (obj) {
        loadedExtentsRtree.remove(obj);
      }
    };
    VectorSource2.prototype.removeFeature = function(feature) {
      if (!feature) {
        return;
      }
      var featureKey = getUid(feature);
      if (featureKey in this.nullGeometryFeatures_) {
        delete this.nullGeometryFeatures_[featureKey];
      } else {
        if (this.featuresRtree_) {
          this.featuresRtree_.remove(feature);
        }
      }
      var result = this.removeFeatureInternal(feature);
      if (result) {
        this.changed();
      }
    };
    VectorSource2.prototype.removeFeatureInternal = function(feature) {
      var featureKey = getUid(feature);
      var featureChangeKeys = this.featureChangeKeys_[featureKey];
      if (!featureChangeKeys) {
        return;
      }
      featureChangeKeys.forEach(unlistenByKey);
      delete this.featureChangeKeys_[featureKey];
      var id = feature.getId();
      if (id !== void 0) {
        delete this.idIndex_[id.toString()];
      }
      delete this.uidIndex_[featureKey];
      this.dispatchEvent(new VectorSourceEvent(VectorEventType_default.REMOVEFEATURE, feature));
      return feature;
    };
    VectorSource2.prototype.removeFromIdIndex_ = function(feature) {
      var removed = false;
      for (var id in this.idIndex_) {
        if (this.idIndex_[id] === feature) {
          delete this.idIndex_[id];
          removed = true;
          break;
        }
      }
      return removed;
    };
    VectorSource2.prototype.setLoader = function(loader) {
      this.loader_ = loader;
    };
    VectorSource2.prototype.setUrl = function(url) {
      assert(this.format_, 7);
      this.url_ = url;
      this.setLoader(xhr(url, this.format_));
    };
    return VectorSource2;
  }(Source_default);
  var Vector_default2 = VectorSource;

  // node_modules/ol/interaction/Draw.js
  var __extends48 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var Mode = {
    POINT: "Point",
    LINE_STRING: "LineString",
    POLYGON: "Polygon",
    CIRCLE: "Circle"
  };
  var DrawEventType = {
    DRAWSTART: "drawstart",
    DRAWEND: "drawend",
    DRAWABORT: "drawabort"
  };
  var DrawEvent = function(_super) {
    __extends48(DrawEvent2, _super);
    function DrawEvent2(type, feature) {
      var _this = _super.call(this, type) || this;
      _this.feature = feature;
      return _this;
    }
    return DrawEvent2;
  }(Event_default);
  var Draw = function(_super) {
    __extends48(Draw2, _super);
    function Draw2(options) {
      var _this = this;
      var pointerOptions = options;
      if (!pointerOptions.stopDown) {
        pointerOptions.stopDown = FALSE;
      }
      _this = _super.call(this, pointerOptions) || this;
      _this.on;
      _this.once;
      _this.un;
      _this.shouldHandle_ = false;
      _this.downPx_ = null;
      _this.downTimeout_;
      _this.lastDragTime_;
      _this.pointerType_;
      _this.freehand_ = false;
      _this.source_ = options.source ? options.source : null;
      _this.features_ = options.features ? options.features : null;
      _this.snapTolerance_ = options.snapTolerance ? options.snapTolerance : 12;
      _this.type_ = options.type;
      _this.mode_ = getMode(_this.type_);
      _this.stopClick_ = !!options.stopClick;
      _this.minPoints_ = options.minPoints ? options.minPoints : _this.mode_ === Mode.POLYGON ? 3 : 2;
      _this.maxPoints_ = _this.mode_ === Mode.CIRCLE ? 2 : options.maxPoints ? options.maxPoints : Infinity;
      _this.finishCondition_ = options.finishCondition ? options.finishCondition : TRUE;
      var geometryFunction = options.geometryFunction;
      if (!geometryFunction) {
        var mode_1 = _this.mode_;
        if (mode_1 === Mode.CIRCLE) {
          geometryFunction = function(coordinates2, geometry, projection) {
            var circle = geometry ? geometry : new Circle_default([NaN, NaN]);
            var center = fromUserCoordinate(coordinates2[0], projection);
            var squaredLength = squaredDistance2(center, fromUserCoordinate(coordinates2[coordinates2.length - 1], projection));
            circle.setCenterAndRadius(center, Math.sqrt(squaredLength));
            var userProjection2 = getUserProjection();
            if (userProjection2) {
              circle.transform(projection, userProjection2);
            }
            return circle;
          };
        } else {
          var Constructor_1;
          if (mode_1 === Mode.POINT) {
            Constructor_1 = Point_default;
          } else if (mode_1 === Mode.LINE_STRING) {
            Constructor_1 = LineString_default;
          } else if (mode_1 === Mode.POLYGON) {
            Constructor_1 = Polygon_default;
          }
          geometryFunction = function(coordinates2, geometry, projection) {
            if (geometry) {
              if (mode_1 === Mode.POLYGON) {
                if (coordinates2[0].length) {
                  geometry.setCoordinates([
                    coordinates2[0].concat([coordinates2[0][0]])
                  ]);
                } else {
                  geometry.setCoordinates([]);
                }
              } else {
                geometry.setCoordinates(coordinates2);
              }
            } else {
              geometry = new Constructor_1(coordinates2);
            }
            return geometry;
          };
        }
      }
      _this.geometryFunction_ = geometryFunction;
      _this.dragVertexDelay_ = options.dragVertexDelay !== void 0 ? options.dragVertexDelay : 500;
      _this.finishCoordinate_ = null;
      _this.sketchFeature_ = null;
      _this.sketchPoint_ = null;
      _this.sketchCoords_ = null;
      _this.sketchLine_ = null;
      _this.sketchLineCoords_ = null;
      _this.squaredClickTolerance_ = options.clickTolerance ? options.clickTolerance * options.clickTolerance : 36;
      _this.overlay_ = new Vector_default({
        source: new Vector_default2({
          useSpatialIndex: false,
          wrapX: options.wrapX ? options.wrapX : false
        }),
        style: options.style ? options.style : getDefaultStyleFunction(),
        updateWhileInteracting: true
      });
      _this.geometryName_ = options.geometryName;
      _this.condition_ = options.condition ? options.condition : noModifierKeys;
      _this.freehandCondition_;
      if (options.freehand) {
        _this.freehandCondition_ = always;
      } else {
        _this.freehandCondition_ = options.freehandCondition ? options.freehandCondition : shiftKeyOnly;
      }
      _this.addChangeListener(Property_default.ACTIVE, _this.updateState_);
      return _this;
    }
    Draw2.prototype.setMap = function(map) {
      _super.prototype.setMap.call(this, map);
      this.updateState_();
    };
    Draw2.prototype.getOverlay = function() {
      return this.overlay_;
    };
    Draw2.prototype.handleEvent = function(event) {
      if (event.originalEvent.type === EventType_default.CONTEXTMENU) {
        event.originalEvent.preventDefault();
      }
      this.freehand_ = this.mode_ !== Mode.POINT && this.freehandCondition_(event);
      var move = event.type === MapBrowserEventType_default.POINTERMOVE;
      var pass = true;
      if (!this.freehand_ && this.lastDragTime_ && event.type === MapBrowserEventType_default.POINTERDRAG) {
        var now = Date.now();
        if (now - this.lastDragTime_ >= this.dragVertexDelay_) {
          this.downPx_ = event.pixel;
          this.shouldHandle_ = !this.freehand_;
          move = true;
        } else {
          this.lastDragTime_ = void 0;
        }
        if (this.shouldHandle_ && this.downTimeout_ !== void 0) {
          clearTimeout(this.downTimeout_);
          this.downTimeout_ = void 0;
        }
      }
      if (this.freehand_ && event.type === MapBrowserEventType_default.POINTERDRAG && this.sketchFeature_ !== null) {
        this.addToDrawing_(event.coordinate);
        pass = false;
      } else if (this.freehand_ && event.type === MapBrowserEventType_default.POINTERDOWN) {
        pass = false;
      } else if (move && this.getPointerCount() < 2) {
        pass = event.type === MapBrowserEventType_default.POINTERMOVE;
        if (pass && this.freehand_) {
          this.handlePointerMove_(event);
          if (this.shouldHandle_) {
            event.originalEvent.preventDefault();
          }
        } else if (event.originalEvent.pointerType === "mouse" || event.type === MapBrowserEventType_default.POINTERDRAG && this.downTimeout_ === void 0) {
          this.handlePointerMove_(event);
        }
      } else if (event.type === MapBrowserEventType_default.DBLCLICK) {
        pass = false;
      }
      return _super.prototype.handleEvent.call(this, event) && pass;
    };
    Draw2.prototype.handleDownEvent = function(event) {
      this.shouldHandle_ = !this.freehand_;
      if (this.freehand_) {
        this.downPx_ = event.pixel;
        if (!this.finishCoordinate_) {
          this.startDrawing_(event.coordinate);
        }
        return true;
      } else if (this.condition_(event)) {
        this.lastDragTime_ = Date.now();
        this.downTimeout_ = setTimeout(function() {
          this.handlePointerMove_(new MapBrowserEvent_default(MapBrowserEventType_default.POINTERMOVE, event.map, event.originalEvent, false, event.frameState));
        }.bind(this), this.dragVertexDelay_);
        this.downPx_ = event.pixel;
        return true;
      } else {
        this.lastDragTime_ = void 0;
        return false;
      }
    };
    Draw2.prototype.handleUpEvent = function(event) {
      var pass = true;
      if (this.getPointerCount() === 0) {
        if (this.downTimeout_) {
          clearTimeout(this.downTimeout_);
          this.downTimeout_ = void 0;
        }
        this.handlePointerMove_(event);
        if (this.shouldHandle_) {
          var startingToDraw = !this.finishCoordinate_;
          if (startingToDraw) {
            this.startDrawing_(event.coordinate);
          }
          if (!startingToDraw && this.freehand_) {
            this.finishDrawing();
          } else if (!this.freehand_ && (!startingToDraw || this.mode_ === Mode.POINT)) {
            if (this.atFinish_(event.pixel)) {
              if (this.finishCondition_(event)) {
                this.finishDrawing();
              }
            } else {
              this.addToDrawing_(event.coordinate);
            }
          }
          pass = false;
        } else if (this.freehand_) {
          this.abortDrawing();
        }
      }
      if (!pass && this.stopClick_) {
        event.preventDefault();
      }
      return pass;
    };
    Draw2.prototype.handlePointerMove_ = function(event) {
      this.pointerType_ = event.originalEvent.pointerType;
      if (this.downPx_ && (!this.freehand_ && this.shouldHandle_ || this.freehand_ && !this.shouldHandle_)) {
        var downPx = this.downPx_;
        var clickPx = event.pixel;
        var dx = downPx[0] - clickPx[0];
        var dy = downPx[1] - clickPx[1];
        var squaredDistance3 = dx * dx + dy * dy;
        this.shouldHandle_ = this.freehand_ ? squaredDistance3 > this.squaredClickTolerance_ : squaredDistance3 <= this.squaredClickTolerance_;
        if (!this.shouldHandle_) {
          return;
        }
      }
      if (this.finishCoordinate_) {
        this.modifyDrawing_(event.coordinate);
      } else {
        this.createOrUpdateSketchPoint_(event.coordinate.slice());
      }
    };
    Draw2.prototype.atFinish_ = function(pixel) {
      var at = false;
      if (this.sketchFeature_) {
        var potentiallyDone = false;
        var potentiallyFinishCoordinates = [this.finishCoordinate_];
        var mode = this.mode_;
        if (mode === Mode.POINT) {
          at = true;
        } else if (mode === Mode.CIRCLE) {
          at = this.sketchCoords_.length === 2;
        } else if (mode === Mode.LINE_STRING) {
          potentiallyDone = this.sketchCoords_.length > this.minPoints_;
        } else if (mode === Mode.POLYGON) {
          var sketchCoords = this.sketchCoords_;
          potentiallyDone = sketchCoords[0].length > this.minPoints_;
          potentiallyFinishCoordinates = [
            sketchCoords[0][0],
            sketchCoords[0][sketchCoords[0].length - 2]
          ];
        }
        if (potentiallyDone) {
          var map = this.getMap();
          for (var i = 0, ii = potentiallyFinishCoordinates.length; i < ii; i++) {
            var finishCoordinate = potentiallyFinishCoordinates[i];
            var finishPixel = map.getPixelFromCoordinate(finishCoordinate);
            var dx = pixel[0] - finishPixel[0];
            var dy = pixel[1] - finishPixel[1];
            var snapTolerance = this.freehand_ ? 1 : this.snapTolerance_;
            at = Math.sqrt(dx * dx + dy * dy) <= snapTolerance;
            if (at) {
              this.finishCoordinate_ = finishCoordinate;
              break;
            }
          }
        }
      }
      return at;
    };
    Draw2.prototype.createOrUpdateSketchPoint_ = function(coordinates2) {
      if (!this.sketchPoint_) {
        this.sketchPoint_ = new Feature_default(new Point_default(coordinates2));
        this.updateSketchFeatures_();
      } else {
        var sketchPointGeom = this.sketchPoint_.getGeometry();
        sketchPointGeom.setCoordinates(coordinates2);
      }
    };
    Draw2.prototype.createOrUpdateCustomSketchLine_ = function(geometry) {
      if (!this.sketchLine_) {
        this.sketchLine_ = new Feature_default();
      }
      var ring = geometry.getLinearRing(0);
      var sketchLineGeom = this.sketchLine_.getGeometry();
      if (!sketchLineGeom) {
        sketchLineGeom = new LineString_default(ring.getFlatCoordinates(), ring.getLayout());
        this.sketchLine_.setGeometry(sketchLineGeom);
      } else {
        sketchLineGeom.setFlatCoordinates(ring.getLayout(), ring.getFlatCoordinates());
        sketchLineGeom.changed();
      }
    };
    Draw2.prototype.startDrawing_ = function(start) {
      var projection = this.getMap().getView().getProjection();
      this.finishCoordinate_ = start;
      if (this.mode_ === Mode.POINT) {
        this.sketchCoords_ = start.slice();
      } else if (this.mode_ === Mode.POLYGON) {
        this.sketchCoords_ = [[start.slice(), start.slice()]];
        this.sketchLineCoords_ = this.sketchCoords_[0];
      } else {
        this.sketchCoords_ = [start.slice(), start.slice()];
      }
      if (this.sketchLineCoords_) {
        this.sketchLine_ = new Feature_default(new LineString_default(this.sketchLineCoords_));
      }
      var geometry = this.geometryFunction_(this.sketchCoords_, void 0, projection);
      this.sketchFeature_ = new Feature_default();
      if (this.geometryName_) {
        this.sketchFeature_.setGeometryName(this.geometryName_);
      }
      this.sketchFeature_.setGeometry(geometry);
      this.updateSketchFeatures_();
      this.dispatchEvent(new DrawEvent(DrawEventType.DRAWSTART, this.sketchFeature_));
    };
    Draw2.prototype.modifyDrawing_ = function(coordinate) {
      var map = this.getMap();
      var geometry = this.sketchFeature_.getGeometry();
      var projection = map.getView().getProjection();
      var coordinates2, last;
      if (this.mode_ === Mode.POINT) {
        last = this.sketchCoords_;
      } else if (this.mode_ === Mode.POLYGON) {
        coordinates2 = this.sketchCoords_[0];
        last = coordinates2[coordinates2.length - 1];
        if (this.atFinish_(map.getPixelFromCoordinate(coordinate))) {
          coordinate = this.finishCoordinate_.slice();
        }
      } else {
        coordinates2 = this.sketchCoords_;
        last = coordinates2[coordinates2.length - 1];
      }
      last[0] = coordinate[0];
      last[1] = coordinate[1];
      this.geometryFunction_(this.sketchCoords_, geometry, projection);
      if (this.sketchPoint_) {
        var sketchPointGeom = this.sketchPoint_.getGeometry();
        sketchPointGeom.setCoordinates(coordinate);
      }
      if (geometry.getType() === GeometryType_default.POLYGON && this.mode_ !== Mode.POLYGON) {
        this.createOrUpdateCustomSketchLine_(geometry);
      } else if (this.sketchLineCoords_) {
        var sketchLineGeom = this.sketchLine_.getGeometry();
        sketchLineGeom.setCoordinates(this.sketchLineCoords_);
      }
      this.updateSketchFeatures_();
    };
    Draw2.prototype.addToDrawing_ = function(coordinate) {
      var geometry = this.sketchFeature_.getGeometry();
      var projection = this.getMap().getView().getProjection();
      var done;
      var coordinates2;
      var mode = this.mode_;
      if (mode === Mode.LINE_STRING || mode === Mode.CIRCLE) {
        this.finishCoordinate_ = coordinate.slice();
        coordinates2 = this.sketchCoords_;
        if (coordinates2.length >= this.maxPoints_) {
          if (this.freehand_) {
            coordinates2.pop();
          } else {
            done = true;
          }
        }
        coordinates2.push(coordinate.slice());
        this.geometryFunction_(coordinates2, geometry, projection);
      } else if (mode === Mode.POLYGON) {
        coordinates2 = this.sketchCoords_[0];
        if (coordinates2.length >= this.maxPoints_) {
          if (this.freehand_) {
            coordinates2.pop();
          } else {
            done = true;
          }
        }
        coordinates2.push(coordinate.slice());
        if (done) {
          this.finishCoordinate_ = coordinates2[0];
        }
        this.geometryFunction_(this.sketchCoords_, geometry, projection);
      }
      this.createOrUpdateSketchPoint_(coordinate.slice());
      this.updateSketchFeatures_();
      if (done) {
        this.finishDrawing();
      }
    };
    Draw2.prototype.removeLastPoint = function() {
      if (!this.sketchFeature_) {
        return;
      }
      var geometry = this.sketchFeature_.getGeometry();
      var projection = this.getMap().getView().getProjection();
      var coordinates2;
      var mode = this.mode_;
      if (mode === Mode.LINE_STRING || mode === Mode.CIRCLE) {
        coordinates2 = this.sketchCoords_;
        coordinates2.splice(-2, 1);
        if (coordinates2.length >= 2) {
          this.finishCoordinate_ = coordinates2[coordinates2.length - 2].slice();
          var finishCoordinate = this.finishCoordinate_.slice();
          coordinates2[coordinates2.length - 1] = finishCoordinate;
          this.createOrUpdateSketchPoint_(finishCoordinate);
        }
        this.geometryFunction_(coordinates2, geometry, projection);
        if (geometry.getType() === GeometryType_default.POLYGON && this.sketchLine_) {
          this.createOrUpdateCustomSketchLine_(geometry);
        }
      } else if (mode === Mode.POLYGON) {
        coordinates2 = this.sketchCoords_[0];
        coordinates2.splice(-2, 1);
        var sketchLineGeom = this.sketchLine_.getGeometry();
        if (coordinates2.length >= 2) {
          var finishCoordinate = coordinates2[coordinates2.length - 2].slice();
          coordinates2[coordinates2.length - 1] = finishCoordinate;
          this.createOrUpdateSketchPoint_(finishCoordinate);
        }
        sketchLineGeom.setCoordinates(coordinates2);
        this.geometryFunction_(this.sketchCoords_, geometry, projection);
      }
      if (coordinates2.length === 1) {
        this.abortDrawing();
      }
      this.updateSketchFeatures_();
    };
    Draw2.prototype.finishDrawing = function() {
      var sketchFeature = this.abortDrawing_();
      if (!sketchFeature) {
        return;
      }
      var coordinates2 = this.sketchCoords_;
      var geometry = sketchFeature.getGeometry();
      var projection = this.getMap().getView().getProjection();
      if (this.mode_ === Mode.LINE_STRING) {
        coordinates2.pop();
        this.geometryFunction_(coordinates2, geometry, projection);
      } else if (this.mode_ === Mode.POLYGON) {
        coordinates2[0].pop();
        this.geometryFunction_(coordinates2, geometry, projection);
        coordinates2 = geometry.getCoordinates();
      }
      if (this.type_ === GeometryType_default.MULTI_POINT) {
        sketchFeature.setGeometry(new MultiPoint_default([coordinates2]));
      } else if (this.type_ === GeometryType_default.MULTI_LINE_STRING) {
        sketchFeature.setGeometry(new MultiLineString_default([coordinates2]));
      } else if (this.type_ === GeometryType_default.MULTI_POLYGON) {
        sketchFeature.setGeometry(new MultiPolygon_default([coordinates2]));
      }
      this.dispatchEvent(new DrawEvent(DrawEventType.DRAWEND, sketchFeature));
      if (this.features_) {
        this.features_.push(sketchFeature);
      }
      if (this.source_) {
        this.source_.addFeature(sketchFeature);
      }
    };
    Draw2.prototype.abortDrawing_ = function() {
      this.finishCoordinate_ = null;
      var sketchFeature = this.sketchFeature_;
      this.sketchFeature_ = null;
      this.sketchPoint_ = null;
      this.sketchLine_ = null;
      this.overlay_.getSource().clear(true);
      return sketchFeature;
    };
    Draw2.prototype.abortDrawing = function() {
      var sketchFeature = this.abortDrawing_();
      if (sketchFeature) {
        this.dispatchEvent(new DrawEvent(DrawEventType.DRAWABORT, sketchFeature));
      }
    };
    Draw2.prototype.appendCoordinates = function(coordinates2) {
      var mode = this.mode_;
      var newDrawing = !this.sketchFeature_;
      if (newDrawing) {
        this.startDrawing_(coordinates2[0]);
      }
      var sketchCoords;
      if (mode === Mode.LINE_STRING || mode === Mode.CIRCLE) {
        sketchCoords = this.sketchCoords_;
      } else if (mode === Mode.POLYGON) {
        sketchCoords = this.sketchCoords_ && this.sketchCoords_.length ? this.sketchCoords_[0] : [];
      } else {
        return;
      }
      if (newDrawing) {
        sketchCoords.shift();
      }
      sketchCoords.pop();
      for (var i = 0; i < coordinates2.length; i++) {
        this.addToDrawing_(coordinates2[i]);
      }
      var ending = coordinates2[coordinates2.length - 1];
      this.addToDrawing_(ending);
      this.modifyDrawing_(ending);
    };
    Draw2.prototype.extend = function(feature) {
      var geometry = feature.getGeometry();
      var lineString = geometry;
      this.sketchFeature_ = feature;
      this.sketchCoords_ = lineString.getCoordinates();
      var last = this.sketchCoords_[this.sketchCoords_.length - 1];
      this.finishCoordinate_ = last.slice();
      this.sketchCoords_.push(last.slice());
      this.sketchPoint_ = new Feature_default(new Point_default(last));
      this.updateSketchFeatures_();
      this.dispatchEvent(new DrawEvent(DrawEventType.DRAWSTART, this.sketchFeature_));
    };
    Draw2.prototype.updateSketchFeatures_ = function() {
      var sketchFeatures = [];
      if (this.sketchFeature_) {
        sketchFeatures.push(this.sketchFeature_);
      }
      if (this.sketchLine_) {
        sketchFeatures.push(this.sketchLine_);
      }
      if (this.sketchPoint_) {
        sketchFeatures.push(this.sketchPoint_);
      }
      var overlaySource = this.overlay_.getSource();
      overlaySource.clear(true);
      overlaySource.addFeatures(sketchFeatures);
    };
    Draw2.prototype.updateState_ = function() {
      var map = this.getMap();
      var active = this.getActive();
      if (!map || !active) {
        this.abortDrawing();
      }
      this.overlay_.setMap(active ? map : null);
    };
    return Draw2;
  }(Pointer_default);
  function getDefaultStyleFunction() {
    var styles = createEditingStyle();
    return function(feature, resolution) {
      return styles[feature.getGeometry().getType()];
    };
  }
  function getMode(type) {
    switch (type) {
      case GeometryType_default.POINT:
      case GeometryType_default.MULTI_POINT:
        return Mode.POINT;
      case GeometryType_default.LINE_STRING:
      case GeometryType_default.MULTI_LINE_STRING:
        return Mode.LINE_STRING;
      case GeometryType_default.POLYGON:
      case GeometryType_default.MULTI_POLYGON:
        return Mode.POLYGON;
      case GeometryType_default.CIRCLE:
        return Mode.CIRCLE;
      default:
        throw new Error("Invalid type: " + type);
    }
  }
  var Draw_default = Draw;

  // node_modules/ol/interaction/Modify.js
  var __extends49 = function() {
    var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
        d2.__proto__ = b2;
      } || function(d2, b2) {
        for (var p in b2)
          if (Object.prototype.hasOwnProperty.call(b2, p))
            d2[p] = b2[p];
      };
      return extendStatics(d, b);
    };
    return function(d, b) {
      if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
  }();
  var CIRCLE_CENTER_INDEX = 0;
  var CIRCLE_CIRCUMFERENCE_INDEX = 1;
  var tempExtent = [0, 0, 0, 0];
  var tempSegment = [];
  var ModifyEventType = {
    MODIFYSTART: "modifystart",
    MODIFYEND: "modifyend"
  };
  var ModifyEvent = function(_super) {
    __extends49(ModifyEvent2, _super);
    function ModifyEvent2(type, features, mapBrowserEvent) {
      var _this = _super.call(this, type) || this;
      _this.features = features;
      _this.mapBrowserEvent = mapBrowserEvent;
      return _this;
    }
    return ModifyEvent2;
  }(Event_default);
  var Modify = function(_super) {
    __extends49(Modify2, _super);
    function Modify2(options) {
      var _this = _super.call(this, options) || this;
      _this.on;
      _this.once;
      _this.un;
      _this.boundHandleFeatureChange_ = _this.handleFeatureChange_.bind(_this);
      _this.condition_ = options.condition ? options.condition : primaryAction;
      _this.defaultDeleteCondition_ = function(mapBrowserEvent) {
        return altKeyOnly(mapBrowserEvent) && singleClick(mapBrowserEvent);
      };
      _this.deleteCondition_ = options.deleteCondition ? options.deleteCondition : _this.defaultDeleteCondition_;
      _this.insertVertexCondition_ = options.insertVertexCondition ? options.insertVertexCondition : always;
      _this.vertexFeature_ = null;
      _this.vertexSegments_ = null;
      _this.lastPixel_ = [0, 0];
      _this.ignoreNextSingleClick_ = false;
      _this.featuresBeingModified_ = null;
      _this.rBush_ = new RBush_default();
      _this.pixelTolerance_ = options.pixelTolerance !== void 0 ? options.pixelTolerance : 10;
      _this.snappedToVertex_ = false;
      _this.changingFeature_ = false;
      _this.dragSegments_ = [];
      _this.overlay_ = new Vector_default({
        source: new Vector_default2({
          useSpatialIndex: false,
          wrapX: !!options.wrapX
        }),
        style: options.style ? options.style : getDefaultStyleFunction2(),
        updateWhileAnimating: true,
        updateWhileInteracting: true
      });
      _this.SEGMENT_WRITERS_ = {
        "Point": _this.writePointGeometry_.bind(_this),
        "LineString": _this.writeLineStringGeometry_.bind(_this),
        "LinearRing": _this.writeLineStringGeometry_.bind(_this),
        "Polygon": _this.writePolygonGeometry_.bind(_this),
        "MultiPoint": _this.writeMultiPointGeometry_.bind(_this),
        "MultiLineString": _this.writeMultiLineStringGeometry_.bind(_this),
        "MultiPolygon": _this.writeMultiPolygonGeometry_.bind(_this),
        "Circle": _this.writeCircleGeometry_.bind(_this),
        "GeometryCollection": _this.writeGeometryCollectionGeometry_.bind(_this)
      };
      _this.source_ = null;
      _this.hitDetection_ = null;
      var features;
      if (options.features) {
        features = options.features;
      } else if (options.source) {
        _this.source_ = options.source;
        features = new Collection_default(_this.source_.getFeatures());
        _this.source_.addEventListener(VectorEventType_default.ADDFEATURE, _this.handleSourceAdd_.bind(_this));
        _this.source_.addEventListener(VectorEventType_default.REMOVEFEATURE, _this.handleSourceRemove_.bind(_this));
      }
      if (!features) {
        throw new Error("The modify interaction requires features, a source or a layer");
      }
      if (options.hitDetection) {
        _this.hitDetection_ = options.hitDetection;
      }
      _this.features_ = features;
      _this.features_.forEach(_this.addFeature_.bind(_this));
      _this.features_.addEventListener(CollectionEventType_default.ADD, _this.handleFeatureAdd_.bind(_this));
      _this.features_.addEventListener(CollectionEventType_default.REMOVE, _this.handleFeatureRemove_.bind(_this));
      _this.lastPointerEvent_ = null;
      _this.delta_ = [0, 0];
      _this.snapToPointer_ = options.snapToPointer === void 0 ? !_this.hitDetection_ : options.snapToPointer;
      return _this;
    }
    Modify2.prototype.addFeature_ = function(feature) {
      var geometry = feature.getGeometry();
      if (geometry) {
        var writer = this.SEGMENT_WRITERS_[geometry.getType()];
        if (writer) {
          writer(feature, geometry);
        }
      }
      var map = this.getMap();
      if (map && map.isRendered() && this.getActive()) {
        this.handlePointerAtPixel_(this.lastPixel_, map);
      }
      feature.addEventListener(EventType_default.CHANGE, this.boundHandleFeatureChange_);
    };
    Modify2.prototype.willModifyFeatures_ = function(evt, segments) {
      if (!this.featuresBeingModified_) {
        this.featuresBeingModified_ = new Collection_default();
        var features = this.featuresBeingModified_.getArray();
        for (var i = 0, ii = segments.length; i < ii; ++i) {
          var segment = segments[i];
          for (var s = 0, ss = segment.length; s < ss; ++s) {
            var feature = segment[s].feature;
            if (feature && features.indexOf(feature) === -1) {
              this.featuresBeingModified_.push(feature);
            }
          }
        }
        if (this.featuresBeingModified_.getLength() === 0) {
          this.featuresBeingModified_ = null;
        } else {
          this.dispatchEvent(new ModifyEvent(ModifyEventType.MODIFYSTART, this.featuresBeingModified_, evt));
        }
      }
    };
    Modify2.prototype.removeFeature_ = function(feature) {
      this.removeFeatureSegmentData_(feature);
      if (this.vertexFeature_ && this.features_.getLength() === 0) {
        this.overlay_.getSource().removeFeature(this.vertexFeature_);
        this.vertexFeature_ = null;
      }
      feature.removeEventListener(EventType_default.CHANGE, this.boundHandleFeatureChange_);
    };
    Modify2.prototype.removeFeatureSegmentData_ = function(feature) {
      var rBush = this.rBush_;
      var nodesToRemove = [];
      rBush.forEach(function(node) {
        if (feature === node.feature) {
          nodesToRemove.push(node);
        }
      });
      for (var i = nodesToRemove.length - 1; i >= 0; --i) {
        var nodeToRemove = nodesToRemove[i];
        for (var j = this.dragSegments_.length - 1; j >= 0; --j) {
          if (this.dragSegments_[j][0] === nodeToRemove) {
            this.dragSegments_.splice(j, 1);
          }
        }
        rBush.remove(nodeToRemove);
      }
    };
    Modify2.prototype.setActive = function(active) {
      if (this.vertexFeature_ && !active) {
        this.overlay_.getSource().removeFeature(this.vertexFeature_);
        this.vertexFeature_ = null;
      }
      _super.prototype.setActive.call(this, active);
    };
    Modify2.prototype.setMap = function(map) {
      this.overlay_.setMap(map);
      _super.prototype.setMap.call(this, map);
    };
    Modify2.prototype.getOverlay = function() {
      return this.overlay_;
    };
    Modify2.prototype.handleSourceAdd_ = function(event) {
      if (event.feature) {
        this.features_.push(event.feature);
      }
    };
    Modify2.prototype.handleSourceRemove_ = function(event) {
      if (event.feature) {
        this.features_.remove(event.feature);
      }
    };
    Modify2.prototype.handleFeatureAdd_ = function(evt) {
      this.addFeature_(evt.element);
    };
    Modify2.prototype.handleFeatureChange_ = function(evt) {
      if (!this.changingFeature_) {
        var feature = evt.target;
        this.removeFeature_(feature);
        this.addFeature_(feature);
      }
    };
    Modify2.prototype.handleFeatureRemove_ = function(evt) {
      var feature = evt.element;
      this.removeFeature_(feature);
    };
    Modify2.prototype.writePointGeometry_ = function(feature, geometry) {
      var coordinates2 = geometry.getCoordinates();
      var segmentData = {
        feature,
        geometry,
        segment: [coordinates2, coordinates2]
      };
      this.rBush_.insert(geometry.getExtent(), segmentData);
    };
    Modify2.prototype.writeMultiPointGeometry_ = function(feature, geometry) {
      var points = geometry.getCoordinates();
      for (var i = 0, ii = points.length; i < ii; ++i) {
        var coordinates2 = points[i];
        var segmentData = {
          feature,
          geometry,
          depth: [i],
          index: i,
          segment: [coordinates2, coordinates2]
        };
        this.rBush_.insert(geometry.getExtent(), segmentData);
      }
    };
    Modify2.prototype.writeLineStringGeometry_ = function(feature, geometry) {
      var coordinates2 = geometry.getCoordinates();
      for (var i = 0, ii = coordinates2.length - 1; i < ii; ++i) {
        var segment = coordinates2.slice(i, i + 2);
        var segmentData = {
          feature,
          geometry,
          index: i,
          segment
        };
        this.rBush_.insert(boundingExtent(segment), segmentData);
      }
    };
    Modify2.prototype.writeMultiLineStringGeometry_ = function(feature, geometry) {
      var lines = geometry.getCoordinates();
      for (var j = 0, jj = lines.length; j < jj; ++j) {
        var coordinates2 = lines[j];
        for (var i = 0, ii = coordinates2.length - 1; i < ii; ++i) {
          var segment = coordinates2.slice(i, i + 2);
          var segmentData = {
            feature,
            geometry,
            depth: [j],
            index: i,
            segment
          };
          this.rBush_.insert(boundingExtent(segment), segmentData);
        }
      }
    };
    Modify2.prototype.writePolygonGeometry_ = function(feature, geometry) {
      var rings = geometry.getCoordinates();
      for (var j = 0, jj = rings.length; j < jj; ++j) {
        var coordinates2 = rings[j];
        for (var i = 0, ii = coordinates2.length - 1; i < ii; ++i) {
          var segment = coordinates2.slice(i, i + 2);
          var segmentData = {
            feature,
            geometry,
            depth: [j],
            index: i,
            segment
          };
          this.rBush_.insert(boundingExtent(segment), segmentData);
        }
      }
    };
    Modify2.prototype.writeMultiPolygonGeometry_ = function(feature, geometry) {
      var polygons = geometry.getCoordinates();
      for (var k = 0, kk = polygons.length; k < kk; ++k) {
        var rings = polygons[k];
        for (var j = 0, jj = rings.length; j < jj; ++j) {
          var coordinates2 = rings[j];
          for (var i = 0, ii = coordinates2.length - 1; i < ii; ++i) {
            var segment = coordinates2.slice(i, i + 2);
            var segmentData = {
              feature,
              geometry,
              depth: [j, k],
              index: i,
              segment
            };
            this.rBush_.insert(boundingExtent(segment), segmentData);
          }
        }
      }
    };
    Modify2.prototype.writeCircleGeometry_ = function(feature, geometry) {
      var coordinates2 = geometry.getCenter();
      var centerSegmentData = {
        feature,
        geometry,
        index: CIRCLE_CENTER_INDEX,
        segment: [coordinates2, coordinates2]
      };
      var circumferenceSegmentData = {
        feature,
        geometry,
        index: CIRCLE_CIRCUMFERENCE_INDEX,
        segment: [coordinates2, coordinates2]
      };
      var featureSegments = [centerSegmentData, circumferenceSegmentData];
      centerSegmentData.featureSegments = featureSegments;
      circumferenceSegmentData.featureSegments = featureSegments;
      this.rBush_.insert(createOrUpdateFromCoordinate(coordinates2), centerSegmentData);
      var circleGeometry = geometry;
      var userProjection2 = getUserProjection();
      if (userProjection2 && this.getMap()) {
        var projection = this.getMap().getView().getProjection();
        circleGeometry = circleGeometry.clone().transform(userProjection2, projection);
        circleGeometry = fromCircle(circleGeometry).transform(projection, userProjection2);
      }
      this.rBush_.insert(circleGeometry.getExtent(), circumferenceSegmentData);
    };
    Modify2.prototype.writeGeometryCollectionGeometry_ = function(feature, geometry) {
      var geometries = geometry.getGeometriesArray();
      for (var i = 0; i < geometries.length; ++i) {
        var geometry_1 = geometries[i];
        var writer = this.SEGMENT_WRITERS_[geometry_1.getType()];
        writer(feature, geometry_1);
      }
    };
    Modify2.prototype.createOrUpdateVertexFeature_ = function(coordinates2, features, geometries) {
      var vertexFeature = this.vertexFeature_;
      if (!vertexFeature) {
        vertexFeature = new Feature_default(new Point_default(coordinates2));
        this.vertexFeature_ = vertexFeature;
        this.overlay_.getSource().addFeature(vertexFeature);
      } else {
        var geometry = vertexFeature.getGeometry();
        geometry.setCoordinates(coordinates2);
      }
      vertexFeature.set("features", features);
      vertexFeature.set("geometries", geometries);
      return vertexFeature;
    };
    Modify2.prototype.handleEvent = function(mapBrowserEvent) {
      if (!mapBrowserEvent.originalEvent) {
        return true;
      }
      this.lastPointerEvent_ = mapBrowserEvent;
      var handled;
      if (!mapBrowserEvent.map.getView().getInteracting() && mapBrowserEvent.type == MapBrowserEventType_default.POINTERMOVE && !this.handlingDownUpSequence) {
        this.handlePointerMove_(mapBrowserEvent);
      }
      if (this.vertexFeature_ && this.deleteCondition_(mapBrowserEvent)) {
        if (mapBrowserEvent.type != MapBrowserEventType_default.SINGLECLICK || !this.ignoreNextSingleClick_) {
          handled = this.removePoint();
        } else {
          handled = true;
        }
      }
      if (mapBrowserEvent.type == MapBrowserEventType_default.SINGLECLICK) {
        this.ignoreNextSingleClick_ = false;
      }
      return _super.prototype.handleEvent.call(this, mapBrowserEvent) && !handled;
    };
    Modify2.prototype.handleDragEvent = function(evt) {
      this.ignoreNextSingleClick_ = false;
      this.willModifyFeatures_(evt, this.dragSegments_);
      var vertex = [
        evt.coordinate[0] + this.delta_[0],
        evt.coordinate[1] + this.delta_[1]
      ];
      var features = [];
      var geometries = [];
      for (var i = 0, ii = this.dragSegments_.length; i < ii; ++i) {
        var dragSegment = this.dragSegments_[i];
        var segmentData = dragSegment[0];
        var feature = segmentData.feature;
        if (features.indexOf(feature) === -1) {
          features.push(feature);
        }
        var geometry = segmentData.geometry;
        if (geometries.indexOf(geometry) === -1) {
          geometries.push(geometry);
        }
        var depth = segmentData.depth;
        var coordinates2 = void 0;
        var segment = segmentData.segment;
        var index = dragSegment[1];
        while (vertex.length < geometry.getStride()) {
          vertex.push(segment[index][vertex.length]);
        }
        switch (geometry.getType()) {
          case GeometryType_default.POINT:
            coordinates2 = vertex;
            segment[0] = vertex;
            segment[1] = vertex;
            break;
          case GeometryType_default.MULTI_POINT:
            coordinates2 = geometry.getCoordinates();
            coordinates2[segmentData.index] = vertex;
            segment[0] = vertex;
            segment[1] = vertex;
            break;
          case GeometryType_default.LINE_STRING:
            coordinates2 = geometry.getCoordinates();
            coordinates2[segmentData.index + index] = vertex;
            segment[index] = vertex;
            break;
          case GeometryType_default.MULTI_LINE_STRING:
            coordinates2 = geometry.getCoordinates();
            coordinates2[depth[0]][segmentData.index + index] = vertex;
            segment[index] = vertex;
            break;
          case GeometryType_default.POLYGON:
            coordinates2 = geometry.getCoordinates();
            coordinates2[depth[0]][segmentData.index + index] = vertex;
            segment[index] = vertex;
            break;
          case GeometryType_default.MULTI_POLYGON:
            coordinates2 = geometry.getCoordinates();
            coordinates2[depth[1]][depth[0]][segmentData.index + index] = vertex;
            segment[index] = vertex;
            break;
          case GeometryType_default.CIRCLE:
            segment[0] = vertex;
            segment[1] = vertex;
            if (segmentData.index === CIRCLE_CENTER_INDEX) {
              this.changingFeature_ = true;
              geometry.setCenter(vertex);
              this.changingFeature_ = false;
            } else {
              this.changingFeature_ = true;
              var projection = evt.map.getView().getProjection();
              var radius = distance(fromUserCoordinate(geometry.getCenter(), projection), fromUserCoordinate(vertex, projection));
              var userProjection2 = getUserProjection();
              if (userProjection2) {
                var circleGeometry = geometry.clone().transform(userProjection2, projection);
                circleGeometry.setRadius(radius);
                radius = circleGeometry.transform(projection, userProjection2).getRadius();
              }
              geometry.setRadius(radius);
              this.changingFeature_ = false;
            }
            break;
          default:
        }
        if (coordinates2) {
          this.setGeometryCoordinates_(geometry, coordinates2);
        }
      }
      this.createOrUpdateVertexFeature_(vertex, features, geometries);
    };
    Modify2.prototype.handleDownEvent = function(evt) {
      if (!this.condition_(evt)) {
        return false;
      }
      var pixelCoordinate = evt.coordinate;
      this.handlePointerAtPixel_(evt.pixel, evt.map, pixelCoordinate);
      this.dragSegments_.length = 0;
      this.featuresBeingModified_ = null;
      var vertexFeature = this.vertexFeature_;
      if (vertexFeature) {
        var projection = evt.map.getView().getProjection();
        var insertVertices = [];
        var vertex = vertexFeature.getGeometry().getCoordinates();
        var vertexExtent = boundingExtent([vertex]);
        var segmentDataMatches = this.rBush_.getInExtent(vertexExtent);
        var componentSegments = {};
        segmentDataMatches.sort(compareIndexes);
        for (var i = 0, ii = segmentDataMatches.length; i < ii; ++i) {
          var segmentDataMatch = segmentDataMatches[i];
          var segment = segmentDataMatch.segment;
          var uid = getUid(segmentDataMatch.geometry);
          var depth = segmentDataMatch.depth;
          if (depth) {
            uid += "-" + depth.join("-");
          }
          if (!componentSegments[uid]) {
            componentSegments[uid] = new Array(2);
          }
          if (segmentDataMatch.geometry.getType() === GeometryType_default.CIRCLE && segmentDataMatch.index === CIRCLE_CIRCUMFERENCE_INDEX) {
            var closestVertex = closestOnSegmentData(pixelCoordinate, segmentDataMatch, projection);
            if (equals3(closestVertex, vertex) && !componentSegments[uid][0]) {
              this.dragSegments_.push([segmentDataMatch, 0]);
              componentSegments[uid][0] = segmentDataMatch;
            }
            continue;
          }
          if (equals3(segment[0], vertex) && !componentSegments[uid][0]) {
            this.dragSegments_.push([segmentDataMatch, 0]);
            componentSegments[uid][0] = segmentDataMatch;
            continue;
          }
          if (equals3(segment[1], vertex) && !componentSegments[uid][1]) {
            if ((segmentDataMatch.geometry.getType() === GeometryType_default.LINE_STRING || segmentDataMatch.geometry.getType() === GeometryType_default.MULTI_LINE_STRING) && componentSegments[uid][0] && componentSegments[uid][0].index === 0) {
              continue;
            }
            this.dragSegments_.push([segmentDataMatch, 1]);
            componentSegments[uid][1] = segmentDataMatch;
            continue;
          }
          if (getUid(segment) in this.vertexSegments_ && !componentSegments[uid][0] && !componentSegments[uid][1] && this.insertVertexCondition_(evt)) {
            insertVertices.push(segmentDataMatch);
          }
        }
        if (insertVertices.length) {
          this.willModifyFeatures_(evt, [insertVertices]);
        }
        for (var j = insertVertices.length - 1; j >= 0; --j) {
          this.insertVertex_(insertVertices[j], vertex);
        }
      }
      return !!this.vertexFeature_;
    };
    Modify2.prototype.handleUpEvent = function(evt) {
      for (var i = this.dragSegments_.length - 1; i >= 0; --i) {
        var segmentData = this.dragSegments_[i][0];
        var geometry = segmentData.geometry;
        if (geometry.getType() === GeometryType_default.CIRCLE) {
          var coordinates2 = geometry.getCenter();
          var centerSegmentData = segmentData.featureSegments[0];
          var circumferenceSegmentData = segmentData.featureSegments[1];
          centerSegmentData.segment[0] = coordinates2;
          centerSegmentData.segment[1] = coordinates2;
          circumferenceSegmentData.segment[0] = coordinates2;
          circumferenceSegmentData.segment[1] = coordinates2;
          this.rBush_.update(createOrUpdateFromCoordinate(coordinates2), centerSegmentData);
          var circleGeometry = geometry;
          var userProjection2 = getUserProjection();
          if (userProjection2) {
            var projection = evt.map.getView().getProjection();
            circleGeometry = circleGeometry.clone().transform(userProjection2, projection);
            circleGeometry = fromCircle(circleGeometry).transform(projection, userProjection2);
          }
          this.rBush_.update(circleGeometry.getExtent(), circumferenceSegmentData);
        } else {
          this.rBush_.update(boundingExtent(segmentData.segment), segmentData);
        }
      }
      if (this.featuresBeingModified_) {
        this.dispatchEvent(new ModifyEvent(ModifyEventType.MODIFYEND, this.featuresBeingModified_, evt));
        this.featuresBeingModified_ = null;
      }
      return false;
    };
    Modify2.prototype.handlePointerMove_ = function(evt) {
      this.lastPixel_ = evt.pixel;
      this.handlePointerAtPixel_(evt.pixel, evt.map, evt.coordinate);
    };
    Modify2.prototype.handlePointerAtPixel_ = function(pixel, map, opt_coordinate) {
      var _this = this;
      var pixelCoordinate = opt_coordinate || map.getCoordinateFromPixel(pixel);
      var projection = map.getView().getProjection();
      var sortByDistance = function(a, b) {
        return projectedDistanceToSegmentDataSquared(pixelCoordinate, a, projection) - projectedDistanceToSegmentDataSquared(pixelCoordinate, b, projection);
      };
      var nodes;
      var hitPointGeometry;
      if (this.hitDetection_) {
        var layerFilter = typeof this.hitDetection_ === "object" ? function(layer) {
          return layer === _this.hitDetection_;
        } : void 0;
        map.forEachFeatureAtPixel(pixel, function(feature, layer, geometry) {
          geometry = geometry || feature.getGeometry();
          if (geometry.getType() === GeometryType_default.POINT && includes(_this.features_.getArray(), feature)) {
            hitPointGeometry = geometry;
            var coordinate = geometry.getFlatCoordinates().slice(0, 2);
            nodes = [
              {
                feature,
                geometry,
                segment: [coordinate, coordinate]
              }
            ];
          }
          return true;
        }, { layerFilter });
      }
      if (!nodes) {
        var viewExtent = fromUserExtent(createOrUpdateFromCoordinate(pixelCoordinate, tempExtent), projection);
        var buffer2 = map.getView().getResolution() * this.pixelTolerance_;
        var box = toUserExtent(buffer(viewExtent, buffer2, tempExtent), projection);
        nodes = this.rBush_.getInExtent(box);
      }
      if (nodes && nodes.length > 0) {
        var node = nodes.sort(sortByDistance)[0];
        var closestSegment = node.segment;
        var vertex = closestOnSegmentData(pixelCoordinate, node, projection);
        var vertexPixel = map.getPixelFromCoordinate(vertex);
        var dist = distance(pixel, vertexPixel);
        if (hitPointGeometry || dist <= this.pixelTolerance_) {
          var vertexSegments = {};
          vertexSegments[getUid(closestSegment)] = true;
          if (!this.snapToPointer_) {
            this.delta_[0] = vertex[0] - pixelCoordinate[0];
            this.delta_[1] = vertex[1] - pixelCoordinate[1];
          }
          if (node.geometry.getType() === GeometryType_default.CIRCLE && node.index === CIRCLE_CIRCUMFERENCE_INDEX) {
            this.snappedToVertex_ = true;
            this.createOrUpdateVertexFeature_(vertex, [node.feature], [node.geometry]);
          } else {
            var pixel1 = map.getPixelFromCoordinate(closestSegment[0]);
            var pixel2 = map.getPixelFromCoordinate(closestSegment[1]);
            var squaredDist1 = squaredDistance2(vertexPixel, pixel1);
            var squaredDist2 = squaredDistance2(vertexPixel, pixel2);
            dist = Math.sqrt(Math.min(squaredDist1, squaredDist2));
            this.snappedToVertex_ = dist <= this.pixelTolerance_;
            if (this.snappedToVertex_) {
              vertex = squaredDist1 > squaredDist2 ? closestSegment[1] : closestSegment[0];
            }
            this.createOrUpdateVertexFeature_(vertex, [node.feature], [node.geometry]);
            var geometries = {};
            geometries[getUid(node.geometry)] = true;
            for (var i = 1, ii = nodes.length; i < ii; ++i) {
              var segment = nodes[i].segment;
              if (equals3(closestSegment[0], segment[0]) && equals3(closestSegment[1], segment[1]) || equals3(closestSegment[0], segment[1]) && equals3(closestSegment[1], segment[0])) {
                var geometryUid = getUid(nodes[i].geometry);
                if (!(geometryUid in geometries)) {
                  geometries[geometryUid] = true;
                  vertexSegments[getUid(segment)] = true;
                }
              } else {
                break;
              }
            }
          }
          this.vertexSegments_ = vertexSegments;
          return;
        }
      }
      if (this.vertexFeature_) {
        this.overlay_.getSource().removeFeature(this.vertexFeature_);
        this.vertexFeature_ = null;
      }
    };
    Modify2.prototype.insertVertex_ = function(segmentData, vertex) {
      var segment = segmentData.segment;
      var feature = segmentData.feature;
      var geometry = segmentData.geometry;
      var depth = segmentData.depth;
      var index = segmentData.index;
      var coordinates2;
      while (vertex.length < geometry.getStride()) {
        vertex.push(0);
      }
      switch (geometry.getType()) {
        case GeometryType_default.MULTI_LINE_STRING:
          coordinates2 = geometry.getCoordinates();
          coordinates2[depth[0]].splice(index + 1, 0, vertex);
          break;
        case GeometryType_default.POLYGON:
          coordinates2 = geometry.getCoordinates();
          coordinates2[depth[0]].splice(index + 1, 0, vertex);
          break;
        case GeometryType_default.MULTI_POLYGON:
          coordinates2 = geometry.getCoordinates();
          coordinates2[depth[1]][depth[0]].splice(index + 1, 0, vertex);
          break;
        case GeometryType_default.LINE_STRING:
          coordinates2 = geometry.getCoordinates();
          coordinates2.splice(index + 1, 0, vertex);
          break;
        default:
          return;
      }
      this.setGeometryCoordinates_(geometry, coordinates2);
      var rTree = this.rBush_;
      rTree.remove(segmentData);
      this.updateSegmentIndices_(geometry, index, depth, 1);
      var newSegmentData = {
        segment: [segment[0], vertex],
        feature,
        geometry,
        depth,
        index
      };
      rTree.insert(boundingExtent(newSegmentData.segment), newSegmentData);
      this.dragSegments_.push([newSegmentData, 1]);
      var newSegmentData2 = {
        segment: [vertex, segment[1]],
        feature,
        geometry,
        depth,
        index: index + 1
      };
      rTree.insert(boundingExtent(newSegmentData2.segment), newSegmentData2);
      this.dragSegments_.push([newSegmentData2, 0]);
      this.ignoreNextSingleClick_ = true;
    };
    Modify2.prototype.removePoint = function() {
      if (this.lastPointerEvent_ && this.lastPointerEvent_.type != MapBrowserEventType_default.POINTERDRAG) {
        var evt = this.lastPointerEvent_;
        this.willModifyFeatures_(evt, this.dragSegments_);
        var removed = this.removeVertex_();
        if (this.featuresBeingModified_) {
          this.dispatchEvent(new ModifyEvent(ModifyEventType.MODIFYEND, this.featuresBeingModified_, evt));
        }
        this.featuresBeingModified_ = null;
        return removed;
      }
      return false;
    };
    Modify2.prototype.removeVertex_ = function() {
      var dragSegments = this.dragSegments_;
      var segmentsByFeature = {};
      var deleted = false;
      var component, coordinates2, dragSegment, geometry, i, index, left;
      var newIndex, right, segmentData, uid;
      for (i = dragSegments.length - 1; i >= 0; --i) {
        dragSegment = dragSegments[i];
        segmentData = dragSegment[0];
        uid = getUid(segmentData.feature);
        if (segmentData.depth) {
          uid += "-" + segmentData.depth.join("-");
        }
        if (!(uid in segmentsByFeature)) {
          segmentsByFeature[uid] = {};
        }
        if (dragSegment[1] === 0) {
          segmentsByFeature[uid].right = segmentData;
          segmentsByFeature[uid].index = segmentData.index;
        } else if (dragSegment[1] == 1) {
          segmentsByFeature[uid].left = segmentData;
          segmentsByFeature[uid].index = segmentData.index + 1;
        }
      }
      for (uid in segmentsByFeature) {
        right = segmentsByFeature[uid].right;
        left = segmentsByFeature[uid].left;
        index = segmentsByFeature[uid].index;
        newIndex = index - 1;
        if (left !== void 0) {
          segmentData = left;
        } else {
          segmentData = right;
        }
        if (newIndex < 0) {
          newIndex = 0;
        }
        geometry = segmentData.geometry;
        coordinates2 = geometry.getCoordinates();
        component = coordinates2;
        deleted = false;
        switch (geometry.getType()) {
          case GeometryType_default.MULTI_LINE_STRING:
            if (coordinates2[segmentData.depth[0]].length > 2) {
              coordinates2[segmentData.depth[0]].splice(index, 1);
              deleted = true;
            }
            break;
          case GeometryType_default.LINE_STRING:
            if (coordinates2.length > 2) {
              coordinates2.splice(index, 1);
              deleted = true;
            }
            break;
          case GeometryType_default.MULTI_POLYGON:
            component = component[segmentData.depth[1]];
          case GeometryType_default.POLYGON:
            component = component[segmentData.depth[0]];
            if (component.length > 4) {
              if (index == component.length - 1) {
                index = 0;
              }
              component.splice(index, 1);
              deleted = true;
              if (index === 0) {
                component.pop();
                component.push(component[0]);
                newIndex = component.length - 1;
              }
            }
            break;
          default:
        }
        if (deleted) {
          this.setGeometryCoordinates_(geometry, coordinates2);
          var segments = [];
          if (left !== void 0) {
            this.rBush_.remove(left);
            segments.push(left.segment[0]);
          }
          if (right !== void 0) {
            this.rBush_.remove(right);
            segments.push(right.segment[1]);
          }
          if (left !== void 0 && right !== void 0) {
            var newSegmentData = {
              depth: segmentData.depth,
              feature: segmentData.feature,
              geometry: segmentData.geometry,
              index: newIndex,
              segment: segments
            };
            this.rBush_.insert(boundingExtent(newSegmentData.segment), newSegmentData);
          }
          this.updateSegmentIndices_(geometry, index, segmentData.depth, -1);
          if (this.vertexFeature_) {
            this.overlay_.getSource().removeFeature(this.vertexFeature_);
            this.vertexFeature_ = null;
          }
          dragSegments.length = 0;
        }
      }
      return deleted;
    };
    Modify2.prototype.setGeometryCoordinates_ = function(geometry, coordinates2) {
      this.changingFeature_ = true;
      geometry.setCoordinates(coordinates2);
      this.changingFeature_ = false;
    };
    Modify2.prototype.updateSegmentIndices_ = function(geometry, index, depth, delta) {
      this.rBush_.forEachInExtent(geometry.getExtent(), function(segmentDataMatch) {
        if (segmentDataMatch.geometry === geometry && (depth === void 0 || segmentDataMatch.depth === void 0 || equals(segmentDataMatch.depth, depth)) && segmentDataMatch.index > index) {
          segmentDataMatch.index += delta;
        }
      });
    };
    return Modify2;
  }(Pointer_default);
  function compareIndexes(a, b) {
    return a.index - b.index;
  }
  function projectedDistanceToSegmentDataSquared(pointCoordinates, segmentData, projection) {
    var geometry = segmentData.geometry;
    if (geometry.getType() === GeometryType_default.CIRCLE) {
      var circleGeometry = geometry;
      if (segmentData.index === CIRCLE_CIRCUMFERENCE_INDEX) {
        var userProjection2 = getUserProjection();
        if (userProjection2) {
          circleGeometry = circleGeometry.clone().transform(userProjection2, projection);
        }
        var distanceToCenterSquared = squaredDistance2(circleGeometry.getCenter(), fromUserCoordinate(pointCoordinates, projection));
        var distanceToCircumference = Math.sqrt(distanceToCenterSquared) - circleGeometry.getRadius();
        return distanceToCircumference * distanceToCircumference;
      }
    }
    var coordinate = fromUserCoordinate(pointCoordinates, projection);
    tempSegment[0] = fromUserCoordinate(segmentData.segment[0], projection);
    tempSegment[1] = fromUserCoordinate(segmentData.segment[1], projection);
    return squaredDistanceToSegment(coordinate, tempSegment);
  }
  function closestOnSegmentData(pointCoordinates, segmentData, projection) {
    var geometry = segmentData.geometry;
    if (geometry.getType() === GeometryType_default.CIRCLE && segmentData.index === CIRCLE_CIRCUMFERENCE_INDEX) {
      var circleGeometry = geometry;
      var userProjection2 = getUserProjection();
      if (userProjection2) {
        circleGeometry = circleGeometry.clone().transform(userProjection2, projection);
      }
      return toUserCoordinate(circleGeometry.getClosestPoint(fromUserCoordinate(pointCoordinates, projection)), projection);
    }
    var coordinate = fromUserCoordinate(pointCoordinates, projection);
    tempSegment[0] = fromUserCoordinate(segmentData.segment[0], projection);
    tempSegment[1] = fromUserCoordinate(segmentData.segment[1], projection);
    return toUserCoordinate(closestOnSegment(coordinate, tempSegment), projection);
  }
  function getDefaultStyleFunction2() {
    var style = createEditingStyle();
    return function(feature, resolution) {
      return style[GeometryType_default.POINT];
    };
  }
  var Modify_default = Modify;

  // src/widget.ts
  var Widget = class {
    mapWrapperElement;
    sourceTextAreaElement;
    vectorSource;
    vectorLayer;
    map;
    mapId;
    vectorLayerId;
    drawOrModifyInteraction = null;
    op = null;
    cardinality;
    constructor(openlayers, mapWrapperElement) {
      this.mapWrapperElement = mapWrapperElement;
      this.sourceTextAreaElement = this.mapWrapperElement.querySelector(".source");
      const map_id = openlayers.getMapId();
      this.map = openlayers.getMap();
      this.cardinality = this.mapWrapperElement?.dataset.cardinality;
      this.mapId = map_id;
      this.vectorLayerId = Object.keys(openlayers.getData())[0];
      this.vectorLayer = openlayers.getLayers()[1];
      this.vectorSource = openlayers.getSources()[Object.keys(openlayers.getSources())[0]];
      if (this.vectorSource.getFeatures().length == 0) {
        this.mapWrapperElement.querySelector('.op-select[value="modify"]')?.setAttribute("disabled", "disabled");
        this.mapWrapperElement.querySelector('.op-select[value="remove"]')?.setAttribute("disabled", "disabled");
      }
      if (this.cardinality > 0 && this.vectorSource.getFeatures().length >= this.cardinality) {
        this.mapWrapperElement?.querySelector('.op-select[value="add"]')?.setAttribute("disabled", "disabled");
      }
      let features = new GeoJSON_default().readFeatures(this.sourceTextAreaElement.value, {
        featureProjection: "EPSG:3857"
      });
      if (features.length) {
        this.vectorSource.addFeatures(features);
      }
      console.log("foo");
      this.addInteractions();
    }
    addInteractions = () => {
      if (this.map) {
        this.map.on("click", (e) => {
          if (this.op == "remove") {
            if (e.dragging)
              return;
            this.map.forEachFeatureAtPixel(e.pixel, (f) => {
              if (this.vectorSource.hasFeature(f)) {
                this.vectorSource.removeFeature(f);
                return true;
              }
            });
            const count = this.vectorSource.getFeatures().length;
            if (this.cardinality > 0 && count < this.cardinality) {
              this.mapWrapperElement?.querySelector('.op-select[value="add"]')?.removeAttribute("disabled");
            }
            if (count == 0) {
              this.mapWrapperElement?.querySelector('.op-select[value="modify"]')?.setAttribute("disabled", "disabled");
              this.mapWrapperElement?.querySelector('.op-select[value="remove"]')?.setAttribute("disabled", "disabled");
            }
          }
        });
        this.map.on("pointermove", (e) => {
          this.map.getViewport().style.cursor = "";
          this.map.forEachFeatureAtPixel(e.pixel, (f) => {
            if (f instanceof Feature_default && this.vectorSource.hasFeature(f)) {
              if (this.op === "remove") {
                this.map.getViewport().style.cursor = "crosshair";
              }
            }
          });
        });
        this.mapWrapperElement?.querySelectorAll(".op-select").forEach((item) => {
          item.addEventListener("click", () => {
            this.modifyInteraction();
          });
        });
        this.mapWrapperElement?.querySelector(".interaction-select")?.addEventListener("change", (e) => {
          this.modifyInteraction();
        });
        this.vectorSource.addEventListener("change", () => {
          const features = this.vectorSource.getFeatures();
          const geojson = new GeoJSON_default().writeFeatures(features, {
            featureProjection: "EPSG:3857"
          });
          this.sourceTextAreaElement.value = JSON.stringify(JSON.parse(geojson), null, 4);
        });
        this.sourceTextAreaElement?.addEventListener("input", (e) => {
          const source_error = this.mapWrapperElement?.querySelector(".source-error");
          if (source_error) {
            source_error.innerHTML = "";
          }
          const cursorStart = e.target?.selectionStart;
          const cursorEnd = e.target?.selectionEnd;
          try {
            let features = [];
            if (e.target.value) {
              features = new GeoJSON_default().readFeatures(e.target.value, {
                featureProjection: "EPSG:3857"
              });
              features.forEach((feature) => {
                feature.set("_map_id", this.mapId, true);
                feature.set("_layer_id", this.vectorLayerId, true);
              });
            }
            if (this.cardinality > 0 && features.length > this.cardinality) {
              throw {
                type: "cardinality",
                message: Drupal.formatPlural(this.cardinality, "Cannot add more than one feature", "Cannot add more than @count features")
              };
            }
            this.vectorSource.clear();
            if (features) {
              this.vectorSource.addFeatures(features);
              const extent = this.vectorSource.getExtent();
              if (!isEmpty2(extent)) {
                this.map.getView().fit(extent, {
                  padding: [100, 100, 100, 100],
                  maxZoom: 15,
                  duration: 1e3
                });
              }
            }
          } catch (error) {
            const message_div = document.createElement("div");
            message_div.className = "form-item--error-message";
            let message = "";
            if (error?.type == "cardinality") {
              message = error?.message;
              message_div.innerHTML = Drupal ? Drupal.t(message) : message;
              this.mapWrapperElement?.querySelector(".source-error")?.appendChild(message_div);
            } else {
              switch (error.constructor) {
                case SyntaxError:
                  message = Drupal.t("Incorrect JSON format.");
                  message_div.innerHTML = Drupal ? Drupal.t(message) : message;
                  this.mapWrapperElement?.querySelector(".source-error")?.appendChild(message_div);
                  break;
                default:
                  message = Drupal.t("Incorrect GeoJSON format.");
                  message_div.innerHTML = Drupal ? Drupal.t(message) : message;
                  this.mapWrapperElement?.querySelector(".source-error")?.appendChild(message_div);
                  break;
              }
            }
          }
          e.target.setSelectionRange(cursorStart, cursorEnd);
        });
      }
    };
    modifyInteraction() {
      const interactionElement = this.mapWrapperElement?.querySelector(".interaction-select");
      const interaction = interactionElement.options[interactionElement.selectedIndex].value;
      this.op = this.mapWrapperElement?.querySelector(".op-select:checked")?.value;
      if (this.drawOrModifyInteraction) {
        this.map.removeInteraction(this.drawOrModifyInteraction);
      }
      if (!this.op) {
        return;
      }
      switch (this.op) {
        case "add":
          this.drawOrModifyInteraction = new Draw_default({
            source: this.vectorSource,
            type: interaction
          });
          this.drawOrModifyInteraction.on("drawend", (event) => {
            event.feature.set("_map_id", this.mapId, true);
            event.feature.set("_layer_id", this.vectorLayerId, true);
            const count = this.vectorSource.getFeatures().length + 1;
            if (this.cardinality > 0 && count >= this.cardinality) {
              this.mapWrapperElement?.querySelector('.op-select[value="add"]')?.setAttribute("disabled", "disabled");
            }
            this.mapWrapperElement?.querySelector('.op-select[value="modify"]')?.removeAttribute("disabled");
            this.mapWrapperElement?.querySelector('.op-select[value="remove"]')?.removeAttribute("disabled");
          });
          this.map.addInteraction(this.drawOrModifyInteraction);
          break;
        case "modify":
          this.drawOrModifyInteraction = new Modify_default({
            source: this.vectorSource
          });
          this.map.addInteraction(this.drawOrModifyInteraction);
          break;
        case "remove":
          break;
      }
    }
  };
  (function($, Drupal2, drupalSettings2) {
    Drupal2.behaviors.openlayersWidget = {
      attach: function attach(context, settings) {
        if (Drupal2.openlayers) {
          const elements = once("openlayers-widgets", context.querySelectorAll(".openlayers-map-tools"));
          if (elements) {
            elements.forEach((element) => {
              const map_id = element?.querySelector(".openlayers-map").id;
              new Widget(Drupal2.openlayers.maps[map_id], element);
            });
          }
        }
      }
    };
  })(jQuery, Drupal, drupalSettings);
})();
