<?php

namespace Drupal\openlayers\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the Openlayers Layer annotation object.
 *
 * @Annotation
 */
class Layer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  protected $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

}
