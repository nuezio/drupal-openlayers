import {Openlayers} from './Openlayers';
import {isEmpty} from 'ol/extent';
import {Draw, Extent} from 'ol/interaction';
import VectorSource from 'ol/source/Vector';
import {Feature} from 'ol';
import GeoJSON from 'ol/format/GeoJSON';
import {Geojson} from './Plugins/Layers/Geojson';
import {DrupalJS, LazyLoaderElement, OpenlayersSettings} from "./types";

type Once = (name: string, element: Node) => void

/**
 * This wraps all Drupal specific functionality.
 */
(function ($, Drupal: DrupalJS, drupalSettings) {
  Drupal.openlayers = {
    maps: {},
  };

  /**
   * Update Map command to update the openlayers map on ajax.
   */
  Drupal.AjaxCommands.prototype.updateMap = function (ajax, response, status) {
    const data = response.data;

    const map_id = response.map_id;

    const openlayers: Openlayers = Drupal.openlayers.maps[map_id];

    for (const layer_id in data) {
      const geojsonPlugin: Geojson = openlayers.layerPlugins[layer_id];
      geojsonPlugin.updateLayer(data[layer_id]);
    }

    let extent;

    if (openlayers.getSpatialFilterSource() && openlayers.getSpatialFilterSource()?.getFeatures().length) {
      extent = openlayers.getSpatialFilterSource()?.getExtent();
    } else {
      extent = openlayers.getCombinedExtent();
    }

    if (extent && !isEmpty(extent)) {
      openlayers
        .getMap()
        .getView()
        .fit(extent, {
          padding: [100, 100, 100, 100],
          maxZoom: 15,
          duration: 1000,
        });
    }
  };

  /**
   * General openlayers behavior.
   */
  Drupal.behaviors.openlayers = {
    attach: (context: Element, settings: OpenlayersSettings) => {
      let maps = context.querySelectorAll('.openlayers-map');
      if (maps.length == 0) {
        return;
      }

      // Update map size and zoom to extents when the map becomes visible in the browser.
      // This solution should work for every type of interaction such as modals,
      // field groups, tabs and states.
      const lazyLoad = (target: Element) => {
        const io = new IntersectionObserver((entries,observer)=>{
          entries.forEach(entry=>{
            if(entry.isIntersecting){
              Drupal.openlayers.maps[target.id].getMap().updateSize();
              let extent = Drupal.openlayers.maps[target.id].getCombinedExtent();
              if (!isEmpty(extent)) {
                Drupal.openlayers.maps[target.id]
                  .getMap()
                  .getView()
                  .fit(extent, {
                    padding: [100, 100, 100, 100],
                    maxZoom: 15,
                  });
              }
              observer.disconnect();
            }
          })
        },{threshold:[0.7]});

        io.observe(target);
      }
      maps.forEach(lazyLoad);


      maps = once('init-openlayers', maps);
      maps.forEach(function (element) {
        const map_id = element.getAttribute('id');
        if (!map_id) {
          throw new Error('The map element should have an ID attribute');
        }
        const mapConfig = settings?.openlayers?.maps[map_id]?.mapConfig;
        const tooltipElement = context.querySelector('#' + map_id?.replaceAll('_', '-') + '__tooltip');
        const lazyLoadingProgressElement = context.querySelector('.openlayers-lazy-loading-progress');
        const featureSelectContainerElement = context.querySelector('.feature-select-container');
        const data = settings.openlayers.maps[map_id]?.data;
        const lazyData = settings.openlayers.maps[map_id]?.lazy_data;

        // Add map data to the openlayers global object.
        let openlayers = new Openlayers(
          map_id,
          mapConfig,
          data,
          lazyData,
          tooltipElement,
          lazyLoadingProgressElement,
          featureSelectContainerElement,
        );
        Drupal.openlayers.maps[map_id] = openlayers;
      });

    },
  };

  /**
   * Adds theming for the openlayers lazy progress bar.
   */
  Drupal.theme.openlayersLazyProgress = (lazy_loader_element: LazyLoaderElement, loaded_items: number) => {
    const total = lazy_loader_element.dataset.total;
    const progress = +lazy_loader_element.dataset.progress + +loaded_items;
    lazy_loader_element.dataset.progress = progress;
    const percentage = (progress / total) * 100;
    const progress_percentage = percentage < 100 ? percentage : 100;
    lazy_loader_element.querySelector('.progress').style.width = progress_percentage + '%';
    if (progress_percentage == 100) {
      lazy_loader_element.classList.add('complete');
    }
  };

  /**
   * Adds behavior for the spatial selection filters.
   */
  Drupal.behaviors.openlayersSpatialFilter = {
    attach: (context: Document, settings: {}) => {
      // Add spatial filteres. Either the spatial filters or the maps might not have been
      // visualised by Drupal yet, specially with caches and big pipe switched on.
      // We first detect if the necessary elements are present before we continue.
      const spatial_exposed_filters = context.querySelectorAll('.openlayers-exposed-spatial-filter');

      if (spatial_exposed_filters.length == 0) {
        return;
      }
      if (Drupal.openlayers == undefined) {
        return;
      }

      spatial_exposed_filters.forEach((filterElement: Element) => {
        const map_id = filterElement.getAttribute('data-openlayers-views-id');
        const spatial_layer_id = filterElement.getAttribute('data-spatial-select-layer');
        if (!spatial_layer_id) {
          return;
        }
        const openlayers: Openlayers =
          Drupal.openlayers.maps[map_id] != undefined ? Drupal.openlayers.maps[map_id] : null;

        // Only set the filters once, check upon very ajax request whether everything is ready.
        if (openlayers && !openlayers.hasSpatialFilter()) {
          openlayers.enableSpatialFilter();
          const map = openlayers.getMap();
          const filterSource: VectorSource = openlayers.getSourceByLayerId(spatial_layer_id);
          openlayers.setSpatialFilterSource(filterSource);
          const draw = new Draw({
            source: filterSource,
            type: 'Polygon',
          });

          const bufferControlElement = filterElement.querySelector('.buffer-control');
          const bufferControlRangeElement = filterElement.querySelector('.buffer-control-range');
          const bufferControlInfoElement = filterElement.querySelector('.buffer-control-info');
          bufferControlInfoElement.innerText = bufferControlRangeElement?.value;
          const applyButton = filterElement.querySelector('.apply');
          const cancelButton = filterElement.querySelector('.cancel');
          const drawButton = filterElement.querySelector('.draw');
          const removeButton = filterElement.querySelector('.remove');
          const geojsonElement = filterElement.querySelector('textarea');

          // The geojson in the exposed filter is the buffer geometry.
          if (filterSource.getFeatures().length == 1) {
            const bufferFeature = filterSource.getFeatureById('buffer');
            if (bufferFeature) {
              const filterAreaFeature: Feature = openlayers.tools.bufferApplier.deduceFilterArea(
                filterSource,
              );

              filterSource.addFeature(filterAreaFeature);
            }

            drawButton?.classList.add('hidden');
            removeButton?.classList.remove('hidden');
          }

          draw.on('drawstart', function () {
            this.getMap().set('drawing', true);
            filterSource.clear();
          });

          // When drawing is finished.
          draw.on('drawend', function (event) {
            map.removeInteraction(draw);

            event.feature.set('_map_id', map_id, true);
            event.feature.set('_layer_id', spatial_layer_id, true);
            event.feature.setId('filter_area', true);
            const bufferedFeature = openlayers.tools.bufferApplier.createBuffer(
              event.feature,
              bufferControlRangeElement?.value,
            );
            filterSource.addFeature(bufferedFeature);

            const geojson = new GeoJSON().writeFeatures([bufferedFeature], {
              featureProjection: openlayers.mapConfig.getLayerConfig(spatial_layer_id).projection,
            });
            bufferControlElement?.classList.remove('hidden');
            geojsonElement.value = geojson;
            applyButton?.classList.remove('hidden');
            cancelButton?.classList.add('hidden');
            cancelButton?.classList.remove('hidden');

            // Wrap in timeout so no features are clicked upon finishing a drawing.
            setTimeout(function () {
              map.set('drawing', false);
            }, 500);
          });

          // When clicking on 'draw'.
          drawButton?.addEventListener('click', (event) => {
            map.set('drawing', true);
            drawButton?.classList.add('hidden');
            geojsonElement.value = '';
            cancelButton?.classList.remove('hidden');
            map.addInteraction(draw);
          });

          // When changing the 'buffer range'
          bufferControlElement?.addEventListener('input', (event) => {

            bufferControlInfoElement.innerText = bufferControlRangeElement.value;
            const bufferedFeature = openlayers.tools.bufferApplier.adjustBuffer(
              filterSource,
              bufferControlRangeElement.value,
            );
            const geojson = new GeoJSON().writeFeatures([bufferedFeature], {
              featureProjection: openlayers.mapConfig.getLayerConfig(spatial_layer_id).projection,
            });

            geojsonElement.value = geojson;
            applyButton?.classList.remove('hidden');
          });

          // When click on remove.
          removeButton?.addEventListener('click', (event) => {
            filterSource.clear();
            geojsonElement.value = '';
            drawButton?.classList.remove('hidden');
            removeButton.classList.add('hidden');
            //  applyButton?.classList.remove('hidden');
            const formElement = applyButton.closest('form');
            formElement?.querySelector('.form-submit').click();
            bufferControlElement?.classList.add('hidden');
          });

          // When cancelling a draw interaction.
          cancelButton?.addEventListener('click', (event) => {
            map.removeInteraction(draw);
            filterSource.clear();
            cancelButton.classList.add('hidden');
            applyButton?.classList.add('hidden');
            drawButton?.classList.remove('hidden');
            bufferControlElement?.classList.add('hidden');
          });

          // When clicking apply
          applyButton?.addEventListener('click', (event) => {
            const formElement = applyButton.closest('form');
            applyButton.classList.add('hidden');
            removeButton?.classList.remove('hidden');
            cancelButton?.classList.add('hidden');
            formElement?.querySelector('.form-submit').click();
          });
        }
      });
    },
  };

})(jQuery, Drupal, drupalSettings);
