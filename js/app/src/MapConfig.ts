import { GeojsonLayerConfig } from './Plugins/Layers/Geojson';

/**
 * The interface for a Map Layer Configuration.
 */
export interface MapLayerConfig {
    uuid: string;
    id: string;
    weight: number;
    data: never;
}

/**
 *
 */
export class MapConfig {
    /**
     * The typed data.
     */
    protected data: {
        view: never;
        id: string;
        layers: never;
    };

    /**
     *  The constructor.
     */
    constructor(data: { view: never; id: string; layers: never }) {
        this.data = data;
    }

    /**
     * Get the plugin ID used.
     */
    getPluginId(): string {
        return this.data['id'];
    }

    /**
     * Get the config for a specific layer.
     */
    getLayerConfig(layer_id: string): GeojsonLayerConfig | never {
        return this.data.layers[layer_id].data;
    }

    /**
     * Get the style config for a specific layer.
     */
    getStyleConfigForLayer(layer_id: string): never {
        const layerConfig = this.getLayerConfig(layer_id);
        return layerConfig.style_config;
    }

    /**
     * Get the view settings.
     */
    getView(): {
        zoom: number;
        center: {
            latitude: number;
            longitude: number;
        };
    } {
        return this.data['view'];
    }

    /**
     * Get the layers in the Map configuration entity.
     */
    getLayers(): MapLayerConfig[] {
        return this.data['layers'];
    }
}
