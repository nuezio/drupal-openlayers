<?php

namespace Drupal\openlayers\Plugin\openlayers\Layer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\openlayers\ConfigurableLayerPluginBase;

/**
 * Implements the Open Street Map as an openlayers layer.
 *
 * @Layer(
 *   id = "stamen",
 *   label = @Translation("Stamen"),
 *   description = @Translation("Stamen layers"),
 * )
 */
class Stamen extends ConfigurableLayerPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() +
      [
        'type' => NULL,
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [
      'type' => [
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => [
          'Toner' => [
            'toner' => $this->t('Standard'),
            'toner-hybrid' => $this->t('Hybrid'),
            'toner-labels' => $this->t('Labels'),
            'toner-lines' => $this->t('lines'),
            'toner-background' => $this->t('Background'),
            'toner-lite' => $this->t('Lite'),
          ],
          'Terrain' => [
            'terrain-standard' => $this->t('Standard'),
            'terrain-labels' => $this->t('Labels'),
            'terrain-lines' => $this->t('Lines'),
            'terrain-background' => $this->t('Background'),
          ],
          'watercolor' => 'Watercolor',
          'trees-cabs-crime' => 'Trees, Cabs & Crime (Only San Francisco)',
        ],
        '#default_value' => $this->configuration['type'],
      ],
    ];
  }

}
