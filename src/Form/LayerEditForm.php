<?php

namespace Drupal\openlayers\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\openlayers\MapConfigInterface;

/**
 * Provides an edit form for layers.
 *
 * @internal
 */
class LayerEditForm extends LayerFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MapConfigInterface $openlayers_map_config = NULL, $layer = NULL) {
    $form = parent::buildForm($form, $form_state, $openlayers_map_config, $layer);

    $form['#title'] = $this->t('Edit %label layer', ['%label' => $this->layer->label()]);
    $form['actions']['submit']['#value'] = $this->t('Update layer');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareLayer($layer) {
    return $this->mapConfig->getLayer($layer);
  }

}
