<?php

namespace Drupal\openlayers\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler for rendering an ajax callback.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("openlayers_ajax_callback")
 */
class OpenlayersAjaxCallback extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // $this->ensureMyTable();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $row->_entity;
    $relation = $this->options['relationship'];
    if (isset($row->_relationship_entities[$relation])) {
      /** @var \Drupal\lupus\Entity\LupusEntityInterface $relationship_entity */
      $relationship_entity = $row->_relationship_entities[$relation];
      if ($relationship_entity->access('view')) {
        $entity = $relationship_entity;
      }
    }
    if ($entity) {
      return Url::fromRoute('openlayers.ajax_commands', [
        'entity' => $entity->getLoadedRevisionId(),
        'entity_type' => $entity->getEntityTypeId(),
        'event' => $this->options['event'],
      ])->setAbsolute(TRUE)->toString();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['event'] = [
      '#type' => 'select',
      '#title' => $this->t('Event type'),
      '#default_value' => $this->options['event'],
      '#options' => [
        'click' => $this->t('Click'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    unset($options['text']);
    $options['event'] = ['default' => 'click'];
    $options['relationship'] = ['default' => '_none'];
    return $options;
  }

}
