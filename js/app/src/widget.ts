import GeoJSON from 'ol/format/GeoJSON';
import {Vector as VectorSource} from 'ol/source';
import {Draw, Modify} from 'ol/interaction';
import Map from 'ol/Map';
import {DrawEvent} from 'ol/interaction/Draw';
import {Feature} from 'ol';
import {isEmpty} from 'ol/extent';
import {FeatureLike} from 'ol/Feature';
import {Openlayers} from './Openlayers';
import {LayerBase} from './Plugins/Layers/LayerBase';
import {Geometry} from "ol/geom";

/**
 * The widget class.
 */
class Widget {

  mapWrapperElement: HTMLElement | null;

  sourceTextAreaElement: HTMLElement | null;

  vectorSource: VectorSource;

  vectorLayer: LayerBase;

  map: Map;

  mapId: string;

  vectorLayerId: string;

  drawOrModifyInteraction: Draw | Modify | null = null;

  op: string | null = null;

  cardinality: number;

  /**
   * The class constructor.
   *
   *   The css selector of the widget.
   *   The marker to be used for pointer markers.
   *   The marker to be used for hover pointers.
   * @param openlayers
   *   The openlayers.
   * @param mapWrapperElement
   */
  constructor(openlayers: Openlayers, mapWrapperElement: HTMLElement) {
    this.mapWrapperElement = mapWrapperElement;
    this.sourceTextAreaElement = this.mapWrapperElement.querySelector('.source');
    const map_id = openlayers.getMapId();
    this.map = openlayers.getMap();
    this.cardinality = this.mapWrapperElement?.dataset.cardinality as unknown as number;
    this.mapId = map_id;
    this.vectorLayerId = Object.keys(openlayers.getData())[0];
    this.vectorLayer = openlayers.getLayers()[1];
    this.vectorSource = openlayers.getSources()[Object.keys(openlayers.getSources())[0]];
    if (this.vectorSource.getFeatures().length == 0) {
      this.mapWrapperElement.querySelector('.op-select[value="modify"]')?.setAttribute('disabled', 'disabled');
      this.mapWrapperElement.querySelector('.op-select[value="remove"]')?.setAttribute('disabled', 'disabled');
    }
    if (this.cardinality > 0 && this.vectorSource.getFeatures().length >= this.cardinality) {
      this.mapWrapperElement?.querySelector('.op-select[value="add"]')?.setAttribute('disabled', 'disabled');
    }
    // Set the default features
    let features = new GeoJSON().readFeatures(this.sourceTextAreaElement.value, {
      featureProjection: 'EPSG:3857',
    });
    if(features.length) {
      this.vectorSource.addFeatures(features)
    }
    console.log('foo');
    this.addInteractions();
  }

  /**
   * Add all the interactions for the widget.
   */
  addInteractions = () => {
    if (this.map) {
      // Remove the feature when op = remove.
      this.map.on('click', (e) => {
        if (this.op == 'remove') {
          if (e.dragging) return;
          this.map.forEachFeatureAtPixel(e.pixel, (f: FeatureLike) => {
            if (this.vectorSource.hasFeature(f)) {
              this.vectorSource.removeFeature(f);
              return true;
            }
          });

          const count = this.vectorSource.getFeatures().length;
          if (this.cardinality > 0 && count < this.cardinality) {
            this.mapWrapperElement?.querySelector('.op-select[value="add"]')?.removeAttribute('disabled');
          }
          if (count == 0) {
            this.mapWrapperElement?.querySelector('.op-select[value="modify"]')?.setAttribute('disabled', 'disabled');
            this.mapWrapperElement?.querySelector('.op-select[value="remove"]')?.setAttribute('disabled', 'disabled');
          }
        }
      });

      // Set cursor to crosshair when deleting.
      this.map.on('pointermove', (e) => {
        this.map.getViewport().style.cursor = '';
        this.map.forEachFeatureAtPixel(e.pixel, (f) => {
          if (f instanceof Feature && this.vectorSource.hasFeature(f)) {
            if (this.op === 'remove') {
              this.map.getViewport().style.cursor = 'crosshair';
            }
          }
        });
      });

      // Modify interactions when operators or interactions are selected.
      this.mapWrapperElement?.querySelectorAll('.op-select').forEach((item) => {
        item.addEventListener('click', () => {
          this.modifyInteraction();
        });
      });
      this.mapWrapperElement?.querySelector('.interaction-select')?.addEventListener('change', (e) => {
        this.modifyInteraction();
      });

      // Update the textarea with the vectorsource from the map.
      this.vectorSource.addEventListener('change', () => {
        const features = this.vectorSource.getFeatures();
        const geojson = new GeoJSON().writeFeatures(features, {
          featureProjection: 'EPSG:3857',
        });
        this.sourceTextAreaElement.value = JSON.stringify(JSON.parse(geojson), null, 4);
      });

      // Update the map with values edited directly in the geojson field.
      this.sourceTextAreaElement?.addEventListener('input', (e) => {
        const source_error = this.mapWrapperElement?.querySelector('.source-error');
        if (source_error) {
          source_error.innerHTML = '';
        }

        // The textarea will be updated directly after editing it.
        // We save the cursor position to put it back in the same place.
        const cursorStart = e.target?.selectionStart;
        const cursorEnd = e.target?.selectionEnd;
        try {
          let features: Feature<Geometry>[] = [];
          if (e.target.value) {
            features = new GeoJSON().readFeatures(e.target.value, {
              featureProjection: 'EPSG:3857',
            });
            features.forEach((feature: Feature) => {
              feature.set('_map_id', this.mapId, true);
              feature.set('_layer_id', this.vectorLayerId, true);
            });
          }

          if (this.cardinality > 0 && features.length > this.cardinality) {
            throw {
              type: 'cardinality',
              message: Drupal.formatPlural(this.cardinality, 'Cannot add more than one feature', 'Cannot add more than @count features')
            };
          }
          this.vectorSource.clear();
          if (features) {
            this.vectorSource.addFeatures(features);
            const extent = this.vectorSource.getExtent();
            if (!isEmpty(extent)) {
              this.map.getView().fit(extent, {
                padding: [100, 100, 100, 100],
                maxZoom: 15,
                duration: 1000,
              });
            }
          }
        } catch (error) {
          const message_div = document.createElement('div');
          message_div.className = 'form-item--error-message';
          let message = '';
          if (error?.type == 'cardinality') {
            message = error?.message;
            message_div.innerHTML = Drupal ? Drupal.t(message) : message;
            this.mapWrapperElement?.querySelector('.source-error')?.appendChild(message_div);
          } else {
            switch (error.constructor) {
              case SyntaxError:
                message = Drupal.t('Incorrect JSON format.');
                message_div.innerHTML = Drupal ? Drupal.t(message) : message;
                this.mapWrapperElement?.querySelector('.source-error')?.appendChild(message_div);
                break;
              default:
                message = Drupal.t('Incorrect GeoJSON format.');
                message_div.innerHTML = Drupal ? Drupal.t(message) : message;
                this.mapWrapperElement?.querySelector('.source-error')?.appendChild(message_div);
                break;
            }
          }
        }

        // Put the cursor back into the same place.
        e.target.setSelectionRange(cursorStart, cursorEnd);
      });
    }
  };

  /**
   * Modifies the interaction and operator based on the user selection.
   */
  modifyInteraction() {
    const interactionElement = this.mapWrapperElement?.querySelector('.interaction-select');
    const interaction = interactionElement.options[interactionElement.selectedIndex].value;
    this.op = this.mapWrapperElement?.querySelector('.op-select:checked')?.value;
    if (this.drawOrModifyInteraction) {
      this.map.removeInteraction(this.drawOrModifyInteraction);
    }
    if (!this.op) {
      return;
    }
    switch (this.op) {
      case 'add':
        this.drawOrModifyInteraction = new Draw({
          source: this.vectorSource,
          type: interaction,
        });
        this.drawOrModifyInteraction.on('drawend', (event: DrawEvent) => {
          event.feature.set('_map_id', this.mapId, true);
          event.feature.set('_layer_id', this.vectorLayerId, true);

          const count = this.vectorSource.getFeatures().length + 1;
          if (this.cardinality > 0 && count >= this.cardinality) {
            this.mapWrapperElement?.querySelector('.op-select[value="add"]')?.setAttribute('disabled', 'disabled');
          }
          this.mapWrapperElement?.querySelector('.op-select[value="modify"]')?.removeAttribute('disabled');
          this.mapWrapperElement?.querySelector('.op-select[value="remove"]')?.removeAttribute('disabled');
        });
        this.map.addInteraction(this.drawOrModifyInteraction);

        break;
      case 'modify':
        this.drawOrModifyInteraction = new Modify({
          source: this.vectorSource,
        });
        this.map.addInteraction(this.drawOrModifyInteraction);
        break;
      case 'remove':
        break;
    }
  }
}

/**
 * Hook into Drupal to enable the widget on all available widgets.
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.openlayersWidget = {
    attach: function attach(context, settings) {
      if (Drupal.openlayers) {
        const elements = once('openlayers-widgets', context.querySelectorAll('.openlayers-map-tools'));
        if (elements) {
          elements.forEach((element: HTMLElement) => {
            const map_id = element?.querySelector('.openlayers-map').id;
            new Widget(Drupal.openlayers.maps[map_id], element);
          });
        }
      }
    },
  };
})(jQuery, Drupal, drupalSettings);
