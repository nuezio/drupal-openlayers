<?php

namespace Drupal\openlayers\Plugin\openlayers\Layer;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\Token;
use Drupal\openlayers\ConfigurableVectorLayerPluginBase;
use Drupal\openlayers\Openlayers;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Geojson based map layer.
 *
 * @Layer(
 *   id = "geojson",
 *   label = @Translation("GeoJSON"),
 *   description = @Translation("GeoJSON.")
 * )
 */
class GeoJSON extends ConfigurableVectorLayerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, Token $token) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger, $entity_type_manager);
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('openlayers'),
      $container->get('entity_type.manager'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $style_configs = $this->entityTypeManager
      ->getStorage('openlayers_style_config')
      ->loadMultiple();
    foreach ($style_configs as $key => $value) {
      $style_configs[$key] = $value->label();
    }
    $token_link = [];
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $token_link = [
        '#theme' => 'token_tree_link',
        '#token_types' => [],
      ];
    }
    return [
      'static_data' => [
        '#type' => 'textarea',
        '#title' => $this->t('Static data'),
        '#description' => $this->t('Data might be provided externally. However you can also enter geojson data here. Tokens are supported'),
        '#default_value' => $this->configuration['static_data'],
      ],
      'token_link' => $token_link,
      'projection' => [
        '#type' => 'select',
        '#title' => $this->t('Projection'),
        '#options' => Openlayers::getProjections(),
        '#default_value' => $this->configuration['projection'],
      ],
      'cluster' => [
        '#type' => 'details',
        '#title' => $this->t('Clustering'),
        'cluster_enable' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable clustering'),
          '#default_value' => $this->configuration['cluster_enable'],
        ],
        'cluster_distance' => [
          '#type' => 'number',
          '#title' => $this->t('Distance in pixels'),
          '#default_value' => $this->configuration['cluster_distance'],
          '#states' => [
            'visible' => [
              ':input[name="data[cluster][cluster_enable]"]' => ['checked' => TRUE],
            ],
          ],
        ],
      ],

      'fit_extents' => [
        '#type' => 'checkbox',
        '#title' => new TranslatableMarkup('Fit extents'),
        '#description' => new TranslatableMarkup('Fit view to the extents of this vector layers. The extent of all features in all the layers with this option checked apply.'),
        '#default_value' => $this->configuration['fit_extents'],
      ],
      'style_config' => [
        '#type' => 'select',
        '#title' => $this->t('Style configuration'),
        '#options' => $style_configs,
        '#default_value' => $this->configuration['style_config'],
      ],
      'hover' => [
        '#type' => 'details',
        '#title' => $this->t('Hover styling'),
        '#description' => $this->t('Add a property to each feature or matching features that is being hovered upon. This property can be used for specific styling.'),
        'property' => [
          '#type' => 'textfield',
          '#title' => $this->t('Hover property'),
          '#description' => $this->t('The property that should be added to each feature when hovering over them.'),
          '#default_value' => $this->configuration['hover']['property'],
        ],
        'property_match' => [
          '#type' => 'textfield',
          '#title' => $this->t('Hover property match'),
          '#description' => $this->t('The hover property should also be added to all other features that share a certain property.
            This allows highlighting upon hover all features of the same entity ID.'),
          '#default_value' => $this->configuration['hover']['property_match'],
        ],
        'tooltip' => [
          '#type' => 'textfield',
          '#title' => $this->t('Tooltip property'),
          '#description' => $this->t('The property of each feature that should be used for populating the tooltip.'),
          '#default_value' => $this->configuration['hover']['tooltip'],
        ],
      ],
    ] + parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() +
      [
        'static_data' => NULL,
        'projection' => Openlayers::PROJECTION_WEB_MERCATOR,
        'cluster_enable' => FALSE,
        'cluster_distance' => 10,
        'style_config' => NULL,
        'fit_extents' => FALSE,
        'hover' => [
          'property' => NULL,
          'property_match' => NULL,
          'tooltip' => NULL,
        ],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['static_data'] = $form_state->getValue('static_data');
    $this->configuration['projection'] = $form_state->getValue('projection');
    $this->configuration['fit_extents'] = $form_state->getValue('fit_extents');
    $this->configuration['cluster_enable'] = $form_state->getValue([
      'cluster',
      'cluster_enable',
    ]);
    $this->configuration['cluster_distance'] = $form_state->getValue([
      'cluster',
      'cluster_distance',
    ]);
    $this->configuration['hover'] = $form_state->getValue('hover');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      'source' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => '@todo summary',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isVectorLayer(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getStaticData(): array {
    try {
      $data = Json::decode($this->token->replace($this->configuration['static_data']));
      return is_array($data) ? $data : [];
    }
    catch (\Exception $e) {
      return [];
    }
  }

}
