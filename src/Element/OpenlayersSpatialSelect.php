<?php

namespace Drupal\openlayers\Element;

use Drupal\Core\Render\Element\Textarea;

/**
 * Provides a render element for openlayers map.
 *
 * @code
 * $form['foo'] = array(
 *   '#type' => 'openlayers_spatial_select',
 *   '#title' => $this->t('Spatial select tools'),
 *   '#view_id' => $view_id,
 *   '#spatial_filter_layer' => $layer_id,
 * );
 * @endcode
 *
 * @RenderElement("openlayers_spatial_select")
 */
class OpenlayersSpatialSelect extends Textarea {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#view'] = NULL;
    $info['#pre_render'][] = [get_called_class(), 'preRenderSpatialSelect'];
    return $info;
  }

  /**
   * Prerender the widget to add spatial selection tools.
   */
  public static function preRenderSpatialSelect($element) {

    // Unset pre_render so the pre render only runs once and doesn't cause a
    // loop.
    unset($element['#pre_render']);
    $element['class'][] = 'openlayers-spatial-filter-textarea';
    $element = [
      '#theme_wrappers' => ['form_element'],
      '#title' => $element['#title'] ?? '',
      '#theme' => 'openlayers_spatial_select',
      '#attached' => [
        'library' => [
          'openlayers/exposed_filter',
        ],
      ],
      '#attributes' => [
        'class' => ['openlayers-exposed-spatial-filter'],
        'data-openlayers-views-id' => $element['#view_id'],
        'data-spatial-select-layer' => $element['#spatial_filter_layer'] ?? '',
      ],
      'value' => $element,
      'buffer_control' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['buffer-control', 'hidden'],
        ],
        'buffer_control_range' => [
          '#type' => 'range',
          '#attributes' => [
            'class' => ['buffer-control-range'],
          ],
          '#min' => 1,
          '#max' => 50000,
          '#value' => 10000,
        ],
        'buffer_control_info' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => '1000m',
          '#attributes' => [
            'class' => ['buffer-control-info'],
          ],
        ],
      ],
      'draw' => [
        '#type' => 'inline_template',
        '#template' => '<button type="button" class="draw">' . t('Draw selection') . '</button>',
      ],
      'cancel' => [
        '#type' => 'inline_template',
        '#template' => '<button type="button" class="cancel hidden">' . t('Cancel') . '</button>',
      ],
      'apply' => [
        '#type' => 'inline_template',
        '#template' => '<button type="button" class="apply hidden">' . t('Apply') . '</button>',
      ],
      'remove' => [
        '#type' => 'inline_template',
        '#template' => '<button type="button" class="remove hidden">' . t('Remove selection') . '</button>',
      ],
    ];
    $element['value']['#wrapper_attributes']['class'][] = 'hidden';
    return $element;
  }

}
