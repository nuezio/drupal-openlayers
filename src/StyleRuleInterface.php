<?php

namespace Drupal\openlayers;

/**
 * The interface for the style rule class.
 */
interface StyleRuleInterface {

  /**
   * Get the uuid.
   *
   * @return string
   *   The uuid.
   */
  public function getUuid();

  /**
   * Get the label.
   *
   * @return string
   *   The label.
   */
  public function label();

  /**
   * Get the array of conditions for this style rule.
   *
   * @return array
   *   The conditions.
   */
  public function getConditions(): array;

  /**
   * Get the definition of open layers style objects.
   *
   * @return array
   *   The style definition.
   */
  public function getStyleDefinition():array;

  /**
   * {@inheritdoc}
   */
  public function getWeight();

}
