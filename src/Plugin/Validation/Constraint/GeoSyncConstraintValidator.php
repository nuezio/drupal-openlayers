<?php

namespace Drupal\openlayers\Plugin\Validation\Constraint;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Exception;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the GeoSync constraint.
 */
class GeoSyncConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * The class constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    $entity = $items->getEntity();

    $this->connection->delete('openlayers_geofield_sync')
      ->condition('entity_id', $entity->id())
      ->condition('entity_type_id', $entity->getEntityTypeId())
      ->condition('field_name', $items->getName())
      ->execute();
    foreach ($items as $delta => $item) {
      try {
        $query = 'INSERT INTO openlayers_geofield_sync (entity_id, entity_type_id, field_name, delta, geometry) VALUES (:entity_id, :entity_type_id, :field_name, :delta, ST_GeomFromText(:geometry))';
        $this->connection->query($query, [
          ':entity_id' => $entity->id(),
          ':entity_type_id' => $entity->getEntityTypeId(),
          ':field_name' => $items->getName(),
          ':delta' => $delta,
          ':geometry' => $item->value,
        ]);
      }
      catch (Exception $e) {
        $this->context->buildViolation($constraint->errorMessage)->atPath(0)->addViolation();
      }
    }

  }

}
