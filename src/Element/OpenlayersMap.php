<?php

namespace Drupal\openlayers\Element;

use Drupal;
use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;
use Drupal\openlayers\VectorLayerInterface;
use Exception;

/**
 * Provides a render element for openlayers map.
 *
 * @RenderElement("openlayers_map")
 */
class OpenlayersMap extends RenderElement {

  const DEFAULT_WIDTH = '100%';

  const DEFAULT_HEIGHT = '400px';

  const DEFAULT_PAGE_QUERY_PARAM = 'page';

  /**
   * Provides an openlayers map render element.
   *
   * Data can be fed to each vector layer that is configured in the map
   * by adding the UUID as a key to the data attribute with the geojson
   * as a value.
   *
   * @code
   * $build['openlayers'] = [
   *   '#type' => 'openlayers_map',
   *   '#map_id' => '',
   *   '#map_config' => $mapConfig',
   *   '#data' => [
   *     '7c463f64-0f8e-4935-be60-a7d51ede9a9c' => [
   *        ...geojson...
   *     ],
   *   ],
   *   '#lazy_data' => [
   *     '7c463f64-0f8e-4935-be60-a7d51ede9a9c' => [
   *       'route' => 'foo.bar',
   *       'route_parameters' => [
   *          'foo' => 'foo',
   *          'bar' => 'bar',
   *       ],
   *       'page_query_param' => 'page',
   *       'total' => 100,
   *       'items_per_page' => 50
   *     ],
   *   ],
   *   '#provider' => '',
   * ];
   * @endcode
   *
   * @RenderElement("openlayers")
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRender'],
      ],
    ];
  }

  /**
   * Prerender the element.
   */
  public static function preRender($element) {
    if (!$element['#map_config']) {
      throw new Exception('Map config is not present.');
    }
    $map_id = $element['#map_id'];
    $width = isset($element['#width']) && !empty($element['#width']) ? $element['#width'] : static::DEFAULT_WIDTH;
    $height = isset($element['#height']) && !empty($element['#height']) ? $element['#height'] : static::DEFAULT_HEIGHT;
    $element['map'] = [
      '#theme' => 'openlayers_map',
      '#provider' => $element['#provider'] ?? '',
      '#attributes' => [
        'tabindex' => 1,
        'id' => $map_id,
        'class' => [
          'openlayers-map',
        ],
        'style' => "width:$width;height:$height",
      ],
    ];
    $element['tooltip'] = [
      '#theme' => 'openlayers_tooltip',
      '#attributes' => [
        'id' => Html::cleanCssIdentifier($map_id . '__tooltip'),
        'class' => ['openlayers-tooltip'],
      ],
    ];

    /** @var \Drupal\openlayers\MapConfigInterface $mapConfig */
    $mapConfig = $element['#map_config'];

    // Add lazy data to settings so they can be accessed from the JS app.
    $lazy_data = $element['#lazy_data'] ?? [];
    $element['#attached']['drupalSettings']['openlayers']['maps'][$map_id]['lazy_data'] = [];
    if ($lazy_data) {
      $total = 0;
      foreach ($lazy_data as $layer_id => $lazy_data_item) {
        $element['#attached']['drupalSettings']['openlayers']['maps'][$map_id]['lazy_data'][$layer_id] = [
          'url' => Url::fromRoute($lazy_data_item['route'], $lazy_data_item['route_parameters'], ['query' => Drupal::requestStack()->getCurrentRequest()->query->all()])
            ->setAbsolute(TRUE)
            ->toString(),
          'page_query_param' => $lazy_data_item['page_query_param'] ?? static::DEFAULT_PAGE_QUERY_PARAM,
          'items_per_page' => $lazy_data_item['items_per_page'],
          'total' => $lazy_data_item['total'],
        ];
        $total += $lazy_data_item['total'];
      }
      $element['lazy_loading_progress'] = [
        '#theme' => 'openlayers_lazy_loading_progress',
        '#total' => $total,
      ];
    }

    // Add data to settings so they can be accessed from the JS app.
    // Also include static data provided by the map config.
    $data = $element['#data'];

    foreach ($mapConfig->getLayers() as $layer) {
      if ($layer instanceof VectorLayerInterface) {
        if (!isset($data[$layer->getUuid()])) {
          $data[$layer->getUuid()] = $layer->getStaticData();
        }
      }
    }
    $element['#attached']['drupalSettings']['openlayers']['maps'][$map_id]['data'] = $data;

    $element['#cache']['tags'] = $mapConfig->getCacheTags();

    $element['#attached']['library'][] = 'openlayers/map';
    $element['#attached']['drupalSettings']['openlayers']['maps'][$map_id]['mapConfig'] = $mapConfig->toArray();
    // Add style config and static data to the drupal settings.
    foreach ($mapConfig->getLayers() as $layer) {
      if ($layer instanceof VectorLayerInterface) {
        $style_config = $layer->getStyleConfig()->toArray();
        $element['#attached']['drupalSettings']['openlayers']['maps'][$map_id]['mapConfig']['layers'][$layer->getUuid()]['data']['style_config'] = $style_config;
      }
    }
    $element['#attached']['drupalSettings']['openlayers']['maps'][$map_id]['provider'] = $element['#provider'];
    return $element;
  }

}
