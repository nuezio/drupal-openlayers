import { LayerPluginInterface } from '../LayerInterface';
import BaseLayer from 'ol/layer/Base';
import { Openlayers } from '../../Openlayers';

export abstract class LayerBase implements LayerPluginInterface {
    protected openlayers: Openlayers;

    protected layerId: string;

    constructor(openlayers_map: Openlayers, layer_id: string) {
        this.openlayers = openlayers_map;
        this.layerId = layer_id;
    }

    buildLayer(): BaseLayer | null {
        return null;
    }
}
