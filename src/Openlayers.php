<?php

namespace Drupal\openlayers;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Contains the events dispatched by the open layers module.
 */
final class Openlayers implements OpenlayersInterface {

  const PROJECTION_WEB_MERCATOR = 'EPSG:3857';

  const PROJECTION_ITRF = 'EPSG:7789';

  const PROJECTION_WGS_84 = 'EPSG:4326';

  /**
   * {@inheritdoc}
   */
  public static function getProjections() {
    return [
      static::PROJECTION_WEB_MERCATOR => new TranslatableMarkup('Web Mercator'),
      static::PROJECTION_ITRF => new TranslatableMarkup('International Terrestrial Reference Frame'),
      static::PROJECTION_WGS_84 => new TranslatableMarkup('WGS 84'),
    ];
  }

}
