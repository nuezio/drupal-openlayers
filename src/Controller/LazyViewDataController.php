<?php

namespace Drupal\openlayers\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\views\Entity\View;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for openlayers routes.
 */
class LazyViewDataController extends ControllerBase {

  const DEFAULT_ITEMS_PER_PAGE = 50;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The controller constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   */
  public function __construct(RequestStack $request_stack) {
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * @param \Drupal\views\Entity\View $view
   *   The view entity.
   * @param string $display_id
   *   The display id.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user account.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(View $view, string $display_id, AccountProxyInterface $account) {
    $view = Views::getView($view->id());
    $view->setDisplay($display_id);
    $view->initStyle();
    return AccessResult::allowedIf($view->getStyle()->getPluginId() == 'openlayers' && $view->access([$display_id], $account));
  }

  /**
   * Builds the response.
   *
   * @param \Drupal\views\Entity\View $view
   *   The view.
   * @param string $display_id
   *   The display id.
   * @param string $field
   *   The field.
   */
  public function data(View $view, string $display_id, string $field) {

    $view = Views::getView($view->id());

    $view->setDisplay($display_id);
    $page = $this->currentRequest->query->get('page');
    $pager = $view->display_handler->getOption('pager');
    $pager['type'] = 'some';
    $pager['options']['items_per_page'] = static::DEFAULT_ITEMS_PER_PAGE;
    $pager['options']['offset'] = $page * static::DEFAULT_ITEMS_PER_PAGE;
    $view->display_handler->setOption('pager', $pager);

    $view->preExecute();

    $view->preview = TRUE;
    $view->execute();
    /** @var \Drupal\openlayers\Plugin\views\style\Openlayers $style_handler */
    $style_handler = $view->style_plugin;
    $features = $style_handler->getFeaturesPerField($field, $view->style_plugin->options['layers'][$field]);
    $response = JsonResponse::create($features);
    return $response;
  }

}
